package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.Presenter.DomainsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.PaymentMthodsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.PostMethodsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.RegisterActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SplashScreenActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.ThemesActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.tkeunebr.gravatar.Gravatar;


public class SettingFragments extends Fragment {
    View view;
    @BindView(R.id.btn_exit)
    Button btn_exit;
    @BindView(R.id.lbl_motto)
    TextView lbl_motto;
    @BindView(R.id.img_profile_avatart)
    CircleImageView img_profile_avatart;
    @BindView(R.id.fml_profile)
    FrameLayout fml_profile;
    @BindView(R.id.lbl_profile_name)
    TextView lbl_profile_name;
    @BindView(R.id.rlt_post_methods)
    RelativeLayout rlt_post_methods;
    @BindView(R.id.lbl_post_methods)
    TextView lbl_post_methods;
    @BindView(R.id.action_post_methods_more)
    ImageView action_post_methods_more;
    @BindView(R.id.rlt_payment_methods)
    RelativeLayout rlt_payment_methods;
    @BindView(R.id.lbl_text_payment_method)
    TextView lbl_text_payment_method;
    @BindView(R.id.action_payment_method_more)
    ImageView action_payment_method_more;
    @BindView(R.id.rlt_domains)
    RelativeLayout rlt_domains;
    @BindView(R.id.lbl_text_domains)
    TextView lbl_text_domains;
    @BindView(R.id.action_domains_more)
    ImageView action_domains_more;
    @BindView(R.id.rlt_change_password)
    RelativeLayout rlt_change_password;
    @BindView(R.id.lbl_text_change_password)
    TextView lbl_text_change_password;
    @BindView(R.id.action_change_password_more)
    ImageView action_change_password_more;
    @BindView(R.id.rlt_themes)
    RelativeLayout rlt_themes;
    @BindView(R.id.lbl_text_themes)
    TextView lbl_text_themes;
    @BindView(R.id.action_themes_more)
    ImageView action_themes_more;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_setting, container, false);
        ButterKnife.bind(this, view);
        initialView();
        BindViewControl();
        return view;
    }

    private void initialView() {
        btn_exit.setTypeface(Fonts.VazirBoldFD);
        lbl_motto.setTypeface(Fonts.VazirBoldFD);
        lbl_profile_name.setTypeface(Fonts.VazirBoldFD);
        lbl_post_methods.setTypeface(Fonts.VazirBoldFD);
        lbl_text_payment_method.setTypeface(Fonts.VazirBoldFD);
        lbl_text_domains.setTypeface(Fonts.VazirBoldFD);
        lbl_text_change_password.setTypeface(Fonts.VazirBoldFD);
        lbl_text_themes.setTypeface(Fonts.VazirBoldFD);
        String UserFullName = PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity.getApplicationContext()).getString("UserFullName", "");
        lbl_profile_name.setText(UserFullName);
        Gravatar myGravatar = new Gravatar.Builder().ssl().build();
        String gravatarUrl = myGravatar.with("").defaultImage("<div>Icons made by <a href=\"https://www.flaticon.com/authors/smashicons\" title=\"Smashicons\">Smashicons</a> from <a href=\"https://www.flaticon.com/\" title=\"Flaticon\">www.flaticon.com</a> is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\" target=\"_blank\">CC 3.0 BY</a></div>").size(150).build();
        try {
            Picasso.with(getContext())
                    .load(gravatarUrl)
                    .placeholder(R.drawable.ic_avatar)
                    .error(R.drawable.ic_avatar)
                    .into(img_profile_avatart);
        } catch (Exception e) {

        }
    }

    private void BindViewControl() {
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity).edit().remove("Token").commit();
                Intent i = new Intent(G.CurrentActivity, SplashScreenActivity.class);
                startActivity(i);
                G.CurrentActivity.finish();
            }
        });
        rlt_payment_methods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), PaymentMthodsActivity.class);
                startActivity(i);
            }
        });
        rlt_post_methods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), PostMethodsActivity.class);
                startActivity(i);
            }
        });
        lbl_motto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(getActivity()).show("http://sepehrcc.com/");
            }
        });
        rlt_domains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), DomainsActivity.class);
                startActivity(i);
            }
        });
        rlt_themes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), ThemesActivity.class);
                startActivity(i);
            }
        });
    }
}
