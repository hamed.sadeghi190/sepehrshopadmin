package com.sepehrcc.apps.sepehradmin.SharedClass;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import com.activeandroid.ActiveAndroid;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.LoadingSplashActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.LoginWithPhoneActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SplashScreenActivity;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.AppData;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class G extends Application {
    public static Context context;
    public static LayoutInflater Inflator;
    public static Activity CurrentActivity;
    public static String desc_save = "";
    public static String desc_category_save = "";
    public static SharedPreferences pref;
    public static String ApiToken = null;
    public static int uploaded = 1;
    public static String BaseUrl = "http://192.168.1.106:3000/";
    public static String PhotoUrl;
    public static String Upload_url = "http://192.168.1.102:3000/";
    public static ArrayList<KindGroup> changekindslist = new ArrayList<KindGroup>();
    public static boolean show_log = true;
    public static boolean Release = false;
    public static AppData appData;
    public static Bitmap b;
    public static boolean is_prodict_change = false;
    public static String DataFolder;
    public static String GroupImages;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
        Seturls();
        if (Release)
            BaseUrl = "www.sepehrcc.com/adminapi/";
    }

    private void init() {
        context = getApplicationContext();
        Inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        Fonts.LoadFonts();
        appData = new AppData();
        ContextWrapper wrapper = new ContextWrapper(this);
        DataFolder = wrapper.getFilesDir().getPath();
        GroupImages  = DataFolder +"/images/groups";
        File fileHandler = new File(GroupImages);
        if(!fileHandler.exists()) {
            fileHandler.mkdirs();
        }
        ActiveAndroid.initialize(this);
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) G.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void log(String tag, String msg) {
        if (show_log) {
            Log.i(tag, msg);
        }
    }

    public static String persianNumbers(String num) {
        try {
            char[] persianNumbers = {'٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'};
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < num.length(); i++) {
                if (Character.isDigit(num.charAt(i))) {
                    builder.append(persianNumbers[(int) (num.charAt(i)) - 48]);
                } else {
                    builder.append(num.charAt(i));
                }
            }
            return builder.toString();
        } catch (Exception e) {
            return num;
        }
    }

    public static String formatPrice(String num) {
        if (num == null || num.isEmpty())
            return "";
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(Float.valueOf(num));
    }

    public static void Seturls() {
        G.BaseUrl = "http://88.99.60.61:82/";
        G.Upload_url = "http://88.99.60.61:82/";
//         G.BaseUrl="http://192.168.1.102:3000/";
//         G.PhotoUrl = "http://192.168.1.102:3000/";
//         G.Upload_url = "http://192.168.1.102:3000/";
    }

    public static void CheckInfo() {
        if(ApiToken.isEmpty())
        {
            Intent intent = new Intent(context, LoadingSplashActivity.class);
            context.startActivity(intent);
        }
    }
}
