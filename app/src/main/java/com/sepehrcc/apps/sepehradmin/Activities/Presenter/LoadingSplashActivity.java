package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.sepehrcc.apps.sepehradmin.DbModels.KalaDB;
import com.sepehrcc.apps.sepehradmin.DbModels.ProductGroup;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ShopInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.GroupRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoadingSplashActivity extends AppCompatActivity {
    @BindView(R.id.lbl_loading)
    TextView lbl_loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_loading_splash);
        ButterKnife.bind(this);
        G.CurrentActivity = this;
        lbl_loading.setTypeface(Fonts.VazirBoldFD);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                WebServicelistener servicelistener = new WebServicelistener() {
                    @SuppressLint("ApplySharedPref")
                    @Override
                    public void OnComplete(Object object) {
                        G.appData.shopInfo = (ShopInfo) object;
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putLong("Login_time", System.currentTimeMillis()).commit();
                        G.PhotoUrl = "http://www." + PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity).getString("SubDomain", "") + ".sepehrcc.com";
                        //G.BaseUrl = "http://www." + PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity).getString("SubDomain", "") + ".sepehrcc.com/adminapi/";
                        G.Seturls();
                        GetallGroups();
                    }

                    @Override
                    public void OnFail(Object object) {
                        PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity).edit().remove("Token").apply();
                        Intent i = new Intent(G.CurrentActivity, SplashScreenActivity.class);
                        startActivity(i);
                        finish();
                    }
                };
                new SecurityRepository().shopInfo(servicelistener);
            }
        }, 2500);
    }

    private void GetallGroups() {
        WebServicelistener servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                try {
                    ArrayList<Group> tdata = (ArrayList<Group>) object;
                    new Delete().from(ProductGroup.class).execute();
                    ActiveAndroid.beginTransaction();
                    try {
                        for (Group group : tdata) {
                            ProductGroup Ngroup = new ProductGroup(group);
                            Ngroup.save();
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        G.appData.shopInfo.SetGroupCount(tdata.size());
                    } finally {
                        ActiveAndroid.endTransaction();
                    }
                } catch (Throwable ignored) {
                }
                GetallProducts();
            }

            @Override
            public void OnFail(Object object) {
                Intent intent = new Intent(G.CurrentActivity, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };
        new GroupRepository().GetAllGroups(servicelistener);
    }

    private void GetallProducts() {
        WebServicelistener servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                try {
                    ArrayList<Kala> tdata = (ArrayList<Kala>) object;
                    int a = 0;
                    new Delete().from(KalaDB.class).execute();
                    ActiveAndroid.beginTransaction();
                    try {
                        for (Kala kalaitem : tdata) {
                            KalaDB newkala = new KalaDB(kalaitem);
                            newkala.save();
                        }
                        ActiveAndroid.setTransactionSuccessful();
                        G.appData.shopInfo.SetKalaCount(tdata.size());
                    }
                    catch (Exception ex)
                    {
                       Log.i("test",ex.getMessage());
                    }
                    finally {
                        ActiveAndroid.endTransaction();
                    }
                } catch (Throwable ex) {
                    Log.i("test",ex.getMessage());
                }
                Intent intent = new Intent(G.CurrentActivity, MainActivity.class);
                startActivity(intent);
                finish();
            }
            @Override
            public void OnFail(Object object) {
                Intent intent = new Intent(G.CurrentActivity, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };
        new KalaRepository().GetAllForCache(servicelistener);

    }
}
