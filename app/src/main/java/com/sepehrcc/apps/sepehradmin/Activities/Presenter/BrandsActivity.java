package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.support.v7.widget.SearchView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.BrandsAdapter;

import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Brands;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DomainModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.DomainRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.GroupRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.MarkRepository;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.thefinestartist.utils.service.ServiceUtil.getLayoutInflater;

public class BrandsActivity extends AppCompatActivity implements BrandsAdapter.EditBrand, IPickResult {
    @BindView(R.id.lbl_brand_title)
    TextView lbl_brand_title;
    @BindView(R.id.brand_search_view)
    SearchView brand_search_view;
    @BindView(R.id.list_brand)
    RecyclerView list_brand;
    @BindView(R.id.lin_progress)
    LinearLayout lin_progress;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    BrandsAdapter brandsAdapter;
    ArrayList<Mark> brand_methods = new ArrayList<Mark>();
    WebServicelistener servicelistener;
    int offset = 0;
    int limit = 10;
    PickSetup setup = new PickSetup();
    String ImageUrl = "";
    Mark selected_mark = new Mark();
    private boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;
    int totalcount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brands);
        ButterKnife.bind(this);
        G.CurrentActivity = this;
        mLayoutManager = new LinearLayoutManager(this);
        init();
        downloadMarkMethods();
        BindViewControl();
    }

    private void init() {
        lbl_brand_title.setTypeface(Fonts.VazirBoldFD);
        brand_search_view.setIconifiedByDefault(false);
        brand_search_view.setFocusable(false);
        brand_search_view.setIconified(true);
        lin_progress.setVisibility(View.VISIBLE);
        setup.setTitle("انتخاب")
                .setTitleColor(Color.BLACK)
                .setBackgroundColor(Color.WHITE)
                .setProgressText("صبر کنید...")
                .setProgressTextColor(Color.BLACK)
                .setCancelText("بستن")
                .setCancelTextColor(Color.BLACK)
                .setButtonTextColor(Color.BLACK)
                .setMaxSize(500)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText("دوربین")
                .setGalleryButtonText("گالری")
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setSystemDialog(false)
                .setCameraToPictures(false)
                .setGalleryIcon(R.drawable.ic_gallery_picker)
                .setCameraIcon(R.drawable.ic_photo_camera);

    }

    private void fill_list() {
        if(brand_search_view.getQuery().toString().equals("")) {
            brandsAdapter = new BrandsAdapter(brand_methods, BrandsActivity.this, false);
        }else if(!brand_search_view.getQuery().toString().equals(""))
        {
            brandsAdapter = new BrandsAdapter(brand_methods, BrandsActivity.this, true);
        }
        list_brand.setAdapter(brandsAdapter);
        list_brand.setLayoutManager(mLayoutManager);
        list_brand.scrollToPosition(pastVisiblesItems + 1);

    }

    private void downloadMarkMethods() {
        final MarkRepository markRepository = new MarkRepository();
        lin_progress.setVisibility(View.VISIBLE);

        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                try {
                    ArrayList<Mark> tdata = (ArrayList<Mark>) object;
                    if (totalcount == 0 && tdata.size() == 0) {
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    } else {
                        brand_methods.addAll(tdata);
                        totalcount += tdata.size();
                        offset = totalcount - 1;
                        if (tdata.size() < limit) {
                            limit = 0;
                            offset = 0;
                        }
                        fill_list();
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    }
                } catch (Throwable t) {
                }

            }

            @Override
            public void OnFail(Object object) {
            }
        };
        if (limit > 0) {
            markRepository.GetMarks(servicelistener, offset, limit);
        }
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }

    private void search_marks(String Key) {
        MarkRepository markRepository = new MarkRepository();
        lin_progress.setVisibility(View.VISIBLE);

        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                try {
                    ArrayList<Mark> tdata = (ArrayList<Mark>) object;
                    if (totalcount == 0 && tdata.size() == 0) {
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    }else {
                    brand_methods.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list();
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                }} catch (Throwable t) {

                }
            }

            @Override
            public void OnFail(Object object) {
            }
        };
        if (limit > 0) {
            markRepository.SearchMarks(servicelistener, Key, offset, limit);
        }
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }

    private void BindViewControl() {
        list_brand.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            if (brand_search_view.getQuery().toString().equals("")) {
                                downloadMarkMethods();

                            } else {
                                search_marks(brand_search_view.getQuery().toString());
                            }
                        }
                    }
                }
            }
        });
        brand_search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    brand_methods.clear();
                    fill_list();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    search_marks(newText);
                } else {
                    brand_methods.clear();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    downloadMarkMethods();
                }
                return false;
            }
        });
    }

    private void EditMarkName(final Mark mark) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BrandsActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_edit_mark_name, null);
        dialogBuilder.setView(dialogView);

        final EditText edt_brand = (EditText) dialogView.findViewById(R.id.edt_brand);
        TextView lbl_brand_title = (TextView) dialogView.findViewById(R.id.lbl_brand_title);

        edt_brand.setText("");
        edt_brand.setTypeface(Fonts.VazirBoldFD);
        lbl_brand_title.setTypeface(Fonts.VazirBoldFD);
        edt_brand.setText(mark.GetMarkName());
        dialogBuilder.setPositiveButton("ویرایش", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final SweetAlertDialog pDialog=new SweetAlertDialog(BrandsActivity.this,SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText("ارسال ...");
                pDialog.setCancelable(true);
                mark.setMarkName(edt_brand.getText().toString());
                MarkRepository markRepository = new MarkRepository();
                servicelistener = new WebServicelistener() {
                    @Override
                    public void OnComplete(Object object) {
                        pDialog.hide();
                        feedback feedback = (feedback) object;
                        offset = 0;
                        limit = 10;
                        brand_methods.clear();
                        if (brand_search_view.getQuery().toString().equals("")) {
                            downloadMarkMethods();

                        } else {
                            search_marks(brand_search_view.getQuery().toString());
                        }
                    }

                    @Override
                    public void OnFail(Object object) {
                        pDialog.hide();
                        SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(BrandsActivity.this, SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog .setTitleText("خطا")
                                .setContentText("تغییرات انجام نشد لطفا بعدا تلاش کنید.")
                                .setConfirmText("بستن")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.hide();
                                    }
                                })
                                .show();
                    }
                };
                markRepository.EditMark(servicelistener, mark);
                pDialog.show();
            }
        });
        dialogBuilder.setNegativeButton("لغو", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        b.getButton(DialogInterface.BUTTON_POSITIVE).setTypeface(Fonts.VazirBoldFD);
        b.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirBoldFD);
    }

    @Override
    public void EditBrand(Mark mark) {
        selected_mark = mark;
        EditMarkName(mark);
    }

    @Override
    public void EditPhoto(Mark mark) {
        selected_mark = mark;
        PickImageDialog.build(setup).setOnPickResult(new IPickResult() {
            @Override
            public void onPickResult(PickResult pickResult) {
                if (pickResult.getError() == null) {
                    save_image(pickResult.getBitmap());
                } else {
                }
            }
        }).show(BrandsActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 10) {
            save_image_croped(G.b);
        }
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            save_image(pickResult.getBitmap());

        }
    }

    public void save_image(Bitmap result) {
        G.b = result;
        Intent i = new Intent(BrandsActivity.this, CropActivity.class);
        i.putExtra("PhotoName", "sepehrbrand.jpg");
        startActivityForResult(i, 10);
    }

    public void save_image_croped(Bitmap result) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        ImageUrl = path + "/sepehrbrand.jpg";
        Integer counter = 0;
        File file = new File(path, "sepehrbrand.jpg");
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        result.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
        try {
            fOut.flush(); // Not really required
            fOut.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
        final SweetAlertDialog pDialog=new SweetAlertDialog(BrandsActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(true);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                pDialog.hide();
                offset = 0;
                limit = 10;
                brand_methods.clear();
                if (brand_search_view.getQuery().toString().equals("")) {
                    downloadMarkMethods();

                } else {
                    search_marks(brand_search_view.getQuery().toString());
                }
            }

            @Override
            public void OnFail(Object object) {
                pDialog.hide();
                SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(BrandsActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("خطا")
                        .setContentText("عکس آپلود نشد.لطفا بعدا تلاش کنید")

                        .setConfirmText("بستن")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.hide();
                            }
                        })
                        .show();
            }
        };
        new MarkRepository().UploadPhoto(servicelistener, ImageUrl, selected_mark.GetMarkID());
        pDialog.show();
    }
}
