package com.sepehrcc.apps.sepehradmin.SharedClass.WebService;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IBanks;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IDomain;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IGroups;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IMark;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IOrders;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IPosts;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.IProducts;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.ISecurity;
import com.sepehrcc.apps.sepehradmin.WebService.Interfaces.ITheme;

import java.util.concurrent.TimeUnit;

import retrofit.GsonConverterFactory;

public class WebServiceHandler {

    private static com.squareup.okhttp.OkHttpClient okHttpClient = new com.squareup.okhttp.OkHttpClient();

    private static com.squareup.okhttp.OkHttpClient getOkHttpClient() {
        okHttpClient.setReadTimeout(1, TimeUnit.MINUTES);
        okHttpClient.setConnectTimeout(1, TimeUnit.MINUTES);
        return okHttpClient;
    }

    private static retrofit.Retrofit retrofit = new retrofit.Retrofit.Builder()
            .baseUrl(G.BaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClient())
            .build();
    public static IProducts KalaClient = retrofit.create(IProducts.class);
    public static IGroups GroupsClient = retrofit.create(IGroups.class);
    public static ISecurity SecurityClient = retrofit.create(ISecurity.class);
    public static IBanks BankClient = retrofit.create(IBanks.class);
    public static IPosts PostClient = retrofit.create(IPosts.class);
    public static IDomain DomainClinet = retrofit.create(IDomain.class);
    public static IOrders OrderClient = retrofit.create(IOrders.class);
    public static ITheme ThemeClient = retrofit.create(ITheme.class);
    public static IMark MarkClient = retrofit.create(IMark.class);
}
