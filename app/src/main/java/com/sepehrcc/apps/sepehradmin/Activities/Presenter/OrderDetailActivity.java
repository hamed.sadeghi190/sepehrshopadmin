package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLayout;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Categoray_list_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.OrderItemsAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Order_list_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.SpinnerAdapter;
import com.sepehrcc.apps.sepehradmin.Models.OrderItemModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.ShamsiDate;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ChangeOrder;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderDetials;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderItems;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.DomainRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.OrderRepository;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderDetailActivity extends AppCompatActivity {

    @BindView(R.id.lbl_order_details)
    TextView lbl_order_details;
    @BindView(R.id.btn_cancle)
    Button btn_cancle;
    @BindView(R.id.lbl_customer_name)
    TextView lbl_customer_name;
    @BindView(R.id.lbl_customer_phone_number)
    TextView lbl_customer_phone_number;
    @BindView(R.id.list_orders)
    RecyclerView list_orders;
    @BindView(R.id.lbl_status)
    TextView lbl_status;
    @BindView(R.id.fml_status)
    FrameLayout fml_status;
    @BindView(R.id.spin_status)
    Spinner spin_status;
    @BindView(R.id.lbl_post_delivery)
    TextView lbl_post_delivery;
    @BindView(R.id.edt_post_delivery)
    EditText edt_post_delivery;
    @BindView(R.id.lbl_note)
    TextView lbl_note;
    @BindView(R.id.fml_note)
    FrameLayout fml_note;
    @BindView(R.id.edt_note)
    EditText edt_note;
    @BindView(R.id.lbl_total_price)
    TextView lbl_total_price;
    @BindView(R.id.expandableLayout)
    ExpandableLayout expandableLayout;
    @BindView(R.id.lbl_total_basket_price)
    TextView lbl_total_basket_price;
    @BindView(R.id.lbl_discount)
    TextView lbl_discount;
    @BindView(R.id.lbl_shipping_cost)
    TextView lbl_shipping_cost;
    @BindView(R.id.lbl_package_cost)
    TextView lbl_package_cost;
    @BindView(R.id.lbl_pardakht_karmozd)
    TextView lbl_pardakht_karmozd;
    @BindView(R.id.lbl_orders_list)
    TextView lbl_orders_list;
    @BindView(R.id.lin_profile)
    RelativeLayout lin_profile;
    @BindView(R.id.lbl_status_sentence)
    TextView lbl_status_sentence;
    @BindView(R.id.edt_status_sentence)
    EditText edt_status_sentence;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.rlt_vrify_paid)
    RelativeLayout rlt_vrify_paid;

    @BindView(R.id.lbl_order_id)
    TextView lbl_order_id;
    @BindView(R.id.lbl_order_date)
    TextView lbl_order_date;
    @BindView(R.id.lbl_status_header)
    TextView lbl_status_header;
    @BindView(R.id.lbl_customer_information_title)
    TextView lbl_customer_information_title;
    @BindView(R.id.img_profile_avatar)
    CircleImageView img_profile_avatar;
    @BindView(R.id.img_verify)
    ImageView img_verify;
    @BindView(R.id.lbl_is_paid)
    TextView lbl_is_paid;
    @BindView(R.id.lbl_total_basket_price_title)
    TextView lbl_total_basket_price_title;
    @BindView(R.id.lbl_discount_title)
    TextView lbl_discount_title;
    @BindView(R.id.lbl_shipping_cost_title)
    TextView lbl_shipping_cost_title;
    @BindView(R.id.lbl_package_cost_title)
    TextView lbl_package_cost_title;
    @BindView(R.id.lbl_pardakht_karmozd_title)
    TextView lbl_pardakht_karmozd_title;
    @BindView(R.id.lbl_total_price_title)
    TextView lbl_total_price_title;
    @BindView(R.id.btn_more_detail)
    Button btn_more_detail;
    @BindView(R.id.lbl_payment_method_title)
    TextView lbl_payment_method_title;
    @BindView(R.id.rlt_payment_method)
    RelativeLayout rlt_payment_method;
    @BindView(R.id.img_payment_method)
    ImageView img_payment_method;
    @BindView(R.id.lbl_no_payment_method)
    TextView lbl_no_payment_method;


    OrderItemsAdapter order_item_adapter;

    LinearLayoutManager mLayoutManager;
    WebServicelistener servicelistener;
    Order order = new Order();
    OrderDetials orderDetials = new OrderDetials();
    ArrayList<OrderItems> OrderItems = new ArrayList<>();
    OrderInfo orderInfo = new OrderInfo();
    int is_paid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        G.CurrentActivity = this;
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Gson gson = new Gson();
            order = gson.fromJson(extras.getString("order_object"), Order.class);
            is_paid = extras.getInt("is_paid");
            download_order_detail(order.GetOrdrID());
        }
        initialView();
        BindViewControl();
    }

    private void initialView() {
        lbl_order_details.setTypeface(Fonts.VazirBold);
        btn_cancle.setTypeface(Fonts.VazirBold);
        lbl_customer_name.setTypeface(Fonts.VazirBold);
        lbl_customer_phone_number.setTypeface(Fonts.VazirBold);
        lbl_status.setTypeface(Fonts.VazirLight);
        lbl_post_delivery.setTypeface(Fonts.VazirLight);
        edt_post_delivery.setTypeface(Fonts.VazirLight);
        lbl_note.setTypeface(Fonts.VazirLight);
        edt_note.setTypeface(Fonts.VazirLight);
        lbl_total_price.setTypeface(Fonts.VazirBold);
        lbl_total_basket_price.setTypeface(Fonts.VazirBold);
        lbl_discount.setTypeface(Fonts.VazirBold);
        lbl_shipping_cost.setTypeface(Fonts.VazirBold);
        lbl_package_cost.setTypeface(Fonts.VazirBold);
        lbl_pardakht_karmozd.setTypeface(Fonts.VazirBold);
        lbl_orders_list.setTypeface(Fonts.VazirBold);
        lbl_status_sentence.setTypeface(Fonts.VazirLight);
        edt_status_sentence.setTypeface(Fonts.VazirBold);
        lbl_order_id.setTypeface(Fonts.VazirBold);
        lbl_order_date.setTypeface(Fonts.VazirFD);
        lbl_customer_information_title.setTypeface(Fonts.VazirBold);
        lbl_is_paid.setTypeface(Fonts.VazirBold);
        lbl_total_basket_price_title.setTypeface(Fonts.VazirBold);
        lbl_discount_title.setTypeface(Fonts.VazirBold);
        lbl_shipping_cost_title.setTypeface(Fonts.VazirBold);
        lbl_package_cost_title.setTypeface(Fonts.VazirBold);
        lbl_pardakht_karmozd_title.setTypeface(Fonts.VazirBold);
        lbl_total_price_title.setTypeface(Fonts.VazirBold);
        lbl_status_header.setTypeface(Fonts.VazirBold);
        btn_more_detail.setTypeface(Fonts.VazirBold);
        lbl_payment_method_title.setTypeface(Fonts.VazirBold);
        lbl_no_payment_method.setTypeface(Fonts.VazirBold);
        SpinnerAdapter spinAdapter = new SpinnerAdapter(OrderDetailActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.order_status)));
        spin_status.setAdapter(spinAdapter);
        if (is_paid > 0) {
            img_verify.setVisibility(View.VISIBLE);
            lbl_is_paid.setText("پرداخت شده");
        } else {
            img_verify.setVisibility(View.INVISIBLE);
            lbl_is_paid.setText("پرداخت نشده");
        }
    }

    private void BindViewControl() {
        btn_more_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderDetailActivity.this, ChangeOrderDetailActivity.class);
                Gson gson = new Gson();
                String order_info = gson.toJson(orderInfo);
                intent.putExtra("order_info", order_info);
                startActivity(intent);
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        spin_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (adapterView.getSelectedItem().toString()) {
                    case "جدید":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما به فروشگاه ارسال شده است");
                        }
                        break;
                    case "تایید شده":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما تایید شد");
                        }
                        break;
                    case "آماده ارسال":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {

                            edt_status_sentence.setText("سفارش شما آماده ارسال است");
                        }
                        break;
                    case "ارسال شده":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما ارسال شده است");
                        }
                        break;
                    case "کامل شده":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("تمامي مراحل خريد شما به پايان رسيد");
                        }
                        break;
                    case "معلق":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما در دست بررسي است");
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        edt_note.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_note) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        rlt_vrify_paid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandableLayout.isExpanded()) {
                    expandableLayout.collapse();
                } else if (!expandableLayout.isExpanded()) {
                    expandableLayout.expand();
                }
            }
        });
        lin_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                String user_info = gson.toJson(orderInfo);
                Intent intent = new Intent(OrderDetailActivity.this, CustomerProfileActivity.class);
                intent.putExtra("user_info", user_info);
                startActivity(intent);
            }
        });
//        btn_Save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ChangeOrder changeOrder = new ChangeOrder();
//                changeOrder.setID(order.GetOrdrID());
//                changeOrder.setRahgiry(edt_post_delivery.getText().toString());
//                changeOrder.setYadasht(edt_note.getText().toString());
//                changeOrder.setStatus(spin_status.getSelectedItemPosition() + 1);
//                changeOrder.setStatusText(edt_status_sentence.getText().toString());
//                change_order(changeOrder);
//            }
//        });
    }

    private void fill_list() {
//        OrderItems orderItem = new OrderItems();
//        orderItem.setoCount(10);
//        orderItem.setkName("کالا");
//        orderItem.setkPrice(10000);
//        OrderItems.add(orderItem);
//        orderItem = new OrderItems();
//        orderItem.setoCount(10);
//        orderItem.setkName("کالا");
//        orderItem.setkPrice(10000);
//        OrderItems.add(orderItem);
        order_item_adapter = new OrderItemsAdapter(OrderItems, OrderDetailActivity.this);
        list_orders.setAdapter(order_item_adapter);
        list_orders.setLayoutManager(new LinearLayoutManager(OrderDetailActivity.this));
    }

    private void download_order_detail(int id) {
        progress.setVisibility(View.VISIBLE);
        OrderRepository orderRepository = new OrderRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                orderDetials = (OrderDetials) object;
                OrderItems = orderDetials.getItems();
                orderInfo = orderDetials.getInfo();
                fill_information();
                fill_list();
            }

            @Override
            public void OnFail(Object object) {
            }
        };
        orderRepository.GetOrderDetail(servicelistener, id);
    }

    private void fill_information() {
        progress.setVisibility(View.GONE);
        lbl_order_id.setText(" #" + orderInfo.getID() + "/" + orderInfo.getCode());
        String[] whole_date = orderInfo.getSDate().split(" ");
        String[] date = whole_date[0].split("/");
        String[] time = whole_date[1].split(":");
        String shown_date = "";
        ShamsiDate shamsiDate = new ShamsiDate();
        int y = Integer.parseInt(date[0]);
        int m = Integer.parseInt(date[1]);
        int d = Integer.parseInt(date[2]);
        shown_date = shamsiDate.shamsiDate(y, m, d);
        shown_date = time[0] + ":" + time[1] + ":" + time[2] + " " + shown_date;
        if (orderInfo.getSDate().startsWith("1")) {
            lbl_order_date.setText(whole_date[1] + " " + whole_date[0]);
        } else if (orderInfo.getSDate().startsWith("20")) {
            lbl_order_date.setText(shown_date);
        }
        lbl_customer_name.setText(orderInfo.getUname());
        //  lbl_customer_phone_number.setText(orderInfo.getUtel2());
        lbl_total_price.setText(G.formatPrice(orderInfo.getCost() + "") + "  تومان");
        lbl_total_basket_price.setText(G.formatPrice(orderInfo.getsBasket() + "") + "  تومان");
        lbl_discount.setText(G.formatPrice(orderInfo.getsDcost() + "") + "  تومان");
        lbl_shipping_cost.setText(G.formatPrice(orderInfo.getsDcost() + "") + "  تومان");
        lbl_package_cost.setText(G.formatPrice(orderInfo.getsPcost() + "") + "  تومان");
        lbl_pardakht_karmozd.setText(G.formatPrice("0") + "  تومان");
        //move to another activity
        /*
        spin_status.setSelection(orderInfo.getStatus() - 1);
        edt_post_delivery.setText(orderInfo.getRahgiry() + "");
        edt_note.setText(orderInfo.getYadasht());
        edt_status_sentence.setText(orderInfo.getsStxt());*/
        switch (orderInfo.getStatus()) {
            case 1:
                lbl_status_header.setText("جدید");
                lbl_status_header.setBackground(getResources().getDrawable(R.drawable.sty_order_status_new_background));
                break;
            case 2:
                lbl_status_header.setText("تایید شده");
                lbl_status_header.setBackground(getResources().getDrawable(R.drawable.sty_order_status_verify_background));
                break;
            case 3:
                lbl_status_header.setText("آماده ارسال");
                lbl_status_header.setBackground(getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                break;
            case 4:
                lbl_status_header.setText("ارسال شده");
                lbl_status_header.setBackground(getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                break;
            case 5:
                lbl_status_header.setText("کامل شده");
                lbl_status_header.setBackground(getResources().getDrawable(R.drawable.sty_order_status_complete_background));
                break;
            case 6:
                lbl_status_header.setText("معلق");
                lbl_status_header.setBackground(getResources().getDrawable(R.drawable.sty_order_status_archive_background));
                break;
        }
        if (orderInfo.getBankName() != null && !orderInfo.getBankName().equals("")) {
            img_payment_method.setVisibility(View.VISIBLE);
            lbl_no_payment_method.setVisibility(View.GONE);
            switch (orderInfo.getBankName()) {
                case "melli":
                    img_payment_method.setImageResource(R.drawable.ic_melli);
                    break;
                case "mellat":
                    img_payment_method.setImageResource(R.drawable.ic_mellat);
                    break;
                case "MasterCard":
                    img_payment_method.setImageResource(R.drawable.ic_mastercard);
                    break;
                case "Credit":
                    img_payment_method.setImageResource(R.drawable.ic_credit);
                    break;
                case "irankish":
                    img_payment_method.setImageResource(R.drawable.ic_kish);
                    break;
                case "paypal":
                    img_payment_method.setImageResource(R.drawable.ic_paypal);
                    break;
                case "zarinpal":
                    img_payment_method.setImageResource(R.drawable.ic_zarinpal);
                    break;
                case "parsian":
                    img_payment_method.setImageResource(R.drawable.ic_parsian);
                    break;
                case "payinplace":
                    img_payment_method.setImageResource(R.drawable.ic_payonlocation);
                    break;
                case "check":
                    img_payment_method.setImageResource(R.drawable.ic_cheque);
                    break;
                case "havale":
                    img_payment_method.setImageResource(R.drawable.ic_havale);
                    break;
                case "saman":
                    img_payment_method.setImageResource(R.drawable.ic_saman);
                    break;
                case "cod":
                    img_payment_method.setImageResource(R.drawable.ic_post);
                    break;
            }
        } else {
            lbl_no_payment_method.setVisibility(View.VISIBLE);
            img_payment_method.setVisibility(View.GONE);

        }
    }

    private void change_order(ChangeOrder changeOrder) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(OrderDetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(true);
        Gson gson = new Gson();
        String json = gson.toJson(changeOrder);
        OrderRepository orderRepository = new OrderRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                feedback feedback = (feedback) object;
                if (feedback.Status == 1) {
                    pDialog.hide();
                    download_order_detail(order.GetOrdrID());
                    Intent intent = new Intent(OrderDetailActivity.this, SuccessfullyActivity.class);
                    intent.putExtra("successfully_message", "تغییرات مورد نظر با موفقیت اعمال شد.");
                    startActivity(intent);
                } else {
                    pDialog.hide();
                    new SweetAlertDialog(OrderDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText("تغییرات اعمال نشد لطفا بعدا تلاش کنید.")
                            .setConfirmText("بستن")
                            .show();
                }
            }

            @Override
            public void OnFail(Object object) {
                pDialog.hide();
                new SweetAlertDialog(OrderDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("خطا")
                        .setContentText("تغییرات اعمال نشد لطفا بعدا تلاش کنید.")
                        .setConfirmText("بستن")
                        .show();
            }
        };
        pDialog.show();
        orderRepository.ChangeOrderStatus(servicelistener, changeOrder);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // download_order_detail(order.GetOrdrID());
    }

}
