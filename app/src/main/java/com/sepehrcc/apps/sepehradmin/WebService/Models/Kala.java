package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;
import com.sepehrcc.apps.sepehradmin.DbModels.KalaDB;

import java.util.ArrayList;

public class Kala {

    @SerializedName("kId")
    private int _kId;
    public Kala()
    {

    }
    public Kala(KalaDB kalaitem) {
        this._kId = kalaitem.GetID();
        this._kName = kalaitem.GetName();
        this._kDesc = kalaitem.GetDescription();
        this._kGroupName = kalaitem.GetGroupName();
        this._kGroup = kalaitem.GetGroup();
        this._kImage = kalaitem.GetImage();
        this._kMark = kalaitem.GetMark();
        this._kMarkName = kalaitem.GetkMarkName();
        this._kPrice = kalaitem.GetPrice();
    }

    public int GetID() { return _kId; }
    public Kala SetID(int value) {_kId = value;return this;}

    @SerializedName("kName")
    private String _kName;
    public String GetName() { return _kName; }
    public Kala SetName(String value) {_kName = value;return this;}

    @SerializedName("kImage")
    private String _kImage;
    public String GetImage() { return _kImage; }
    public Kala SetImage(String value) {_kImage = value;return this;}

    @SerializedName("kPrice")
    private int _kPrice;
    public int GetPrice () { return _kPrice; }
    public Kala SetPrice(int  value) {_kPrice = value;return this;}

    @SerializedName("kDesc")
    private String _kDesc;
    public String GetDescription() { return _kDesc; }
    public Kala SetDescription(String value) {_kDesc = value;return this;}

    @SerializedName("kGroup")
    private int _kGroup;
    public int GetGroup () { return _kGroup; }
    public Kala SetGroup(int  value) {_kGroup = value;return this;}

    @SerializedName("kGroupName")
    private String _kGroupName;
    public String GetGroupName () { return _kGroupName; }
    public Kala SetGroupName(String  value) {_kGroupName = value;return this;}

    @SerializedName("kMark")
    private String _kMark;
    public String GetMark() { return _kMark; }
    public Kala SetMark(String value) {_kMark = value;return this;}

    @SerializedName("Types")
    private ArrayList<KalaTypes> _Types  = new ArrayList<>();
    public ArrayList<KalaTypes>  GetKinds() { return _Types; }
    public Kala SetKinds(ArrayList<KalaTypes>  value) {_Types.addAll(value);return this;}

    @SerializedName("Photos")
    private ArrayList<Photos> _Photos  = new ArrayList<>();
    public ArrayList<Photos>  GetPhotos() { return _Photos; }
    public Kala SetPhotos(ArrayList<Photos>  value) {_Photos.addAll(value);return this;}

    @SerializedName("kMarkName")
    private String _kMarkName;
    public String GetkMarkName() { return _kMarkName; }
    public Kala SetkMarkName(String value) {_kMarkName = value;return this;}

}
