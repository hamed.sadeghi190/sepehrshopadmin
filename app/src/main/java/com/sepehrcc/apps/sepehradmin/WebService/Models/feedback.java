package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed Sadeghi on 28/04/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class feedback {
    @SerializedName("status")
    public int Status;
    @SerializedName("message")
    public String Message;
    @SerializedName("exception")
    public String exception;
    @SerializedName("value")
    public String value;
    public feedback(int _Status, String _Message) {
        Status = _Status;
        Message = _Message;
    }
    public feedback(int _Status, String _Message, String _exception, String _value) {
        Status = _Status;
        Message = _Message;
        exception = _exception;
        value = _value;
    }

}
