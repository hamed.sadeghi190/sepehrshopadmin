package com.sepehrcc.apps.sepehradmin.WebService.Models;

/**
 * Created by Administrator on 29/05/2018.
 */

public class UserInfo {
    public int OrderID;
    public String Utel;
    public String Utel2;
    public String SAdd;
    public String SAdd2;
    public String PostalCode;
    public String sReceiver;

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public String getUtel() {
        return Utel;
    }

    public void setUtel(String utel) {
        Utel = utel;
    }

    public String getUtel2() {
        return Utel2;
    }

    public void setUtel2(String utel2) {
        Utel2 = utel2;
    }

    public String getSAdd() {
        return SAdd;
    }

    public void setSAdd(String SAdd) {
        this.SAdd = SAdd;
    }

    public String getSAdd2() {
        return SAdd2;
    }

    public void setSAdd2(String SAdd2) {
        this.SAdd2 = SAdd2;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getsReceiver() {
        return sReceiver;
    }

    public void setsReceiver(String sReceiver) {
        this.sReceiver = sReceiver;
    }
}
