package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Models.ProvinceModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderInfo;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.tkeunebr.gravatar.Gravatar;

public class CustomerProfileActivity extends AppCompatActivity {
    @BindView(R.id.lbl_customer_info)
    TextView lbl_customer_info;
    @BindView(R.id.lin_history)
    LinearLayout lin_history;
    @BindView(R.id.lbl_history)
    TextView lbl_history;
    @BindView(R.id.lbl_history_amount)
    TextView lbl_history_amount;
    @BindView(R.id.fml_profile)
    FrameLayout fml_profile;
    @BindView(R.id.img_profile_avatar)
    CircleImageView img_profile_avatar;
    @BindView(R.id.lbl_customer_name)
    TextView lbl_customer_name;
    @BindView(R.id.btn_modify_profile)
    Button btn_modify_profile;
    @BindView(R.id.lin_phone)
    LinearLayout lin_phone;
    @BindView(R.id.lbl_phone)
    TextView lbl_phone;
    @BindView(R.id.lin_mobile)
    LinearLayout lin_mobile;
    @BindView(R.id.lbl_mobile)
    TextView lbl_mobile;
    @BindView(R.id.lbl_email)
    TextView lbl_email;
    @BindView(R.id.lbl_address_title)
    TextView lbl_address_title;
    @BindView(R.id.lbl_address_name)
    TextView lbl_address_name;
    @BindView(R.id.lbl_postalcode_title)
    TextView lbl_postalcode_title;
    @BindView(R.id.lbl_postal_code_name)
    TextView lbl_postal_code_name;
    @BindView(R.id.lbl_desc_title)
    TextView lbl_desc_title;
    @BindView(R.id.lbl_desc_name)
    TextView lbl_desc_name;
    OrderInfo orderInfo = new OrderInfo();
    String cities_json = "";
    ArrayList<ProvinceModel> provices = new ArrayList<ProvinceModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);
        ButterKnife.bind(this);


        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("text/cities.txt"), "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                cities_json = cities_json.concat(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<ProvinceModel>>() {
        }.getType();
        try {
            provices = gson.fromJson(cities_json, collectionType);
        } catch (JsonSyntaxException | IllegalStateException e) {
            e.printStackTrace();
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Gson gson1 = new Gson();
            orderInfo = gson1.fromJson(extras.getString("user_info"), OrderInfo.class);
            set_information();
        }
        initialView();
        BindViewControl();
    }

    private void initialView() {
        lbl_customer_info.setTypeface(Fonts.VazirBoldFD);
        lbl_history.setTypeface(Fonts.VazirBoldFD);
        lbl_history_amount.setTypeface(Fonts.VazirBoldFD);
        lbl_customer_name.setTypeface(Fonts.VazirBoldFD);
        btn_modify_profile.setTypeface(Fonts.VazirBoldFD);
        lbl_phone.setTypeface(Fonts.VazirBoldFD);
        lbl_mobile.setTypeface(Fonts.VazirBoldFD);
        lbl_email.setTypeface(Fonts.VazirBoldFD);
        lbl_address_title.setTypeface(Fonts.VazirBoldFD);
        lbl_address_name.setTypeface(Fonts.VazirBoldFD);
        lbl_postalcode_title.setTypeface(Fonts.VazirBoldFD);
        lbl_postal_code_name.setTypeface(Fonts.VazirBoldFD);
        lbl_desc_title.setTypeface(Fonts.VazirBoldFD);
        lbl_desc_name.setTypeface(Fonts.VazirBoldFD);
        lbl_history_amount.setText(G.formatPrice("52000000") + " تومان");
        Gravatar myGravatar = new Gravatar.Builder().ssl().build();
        String gravatarUrl = myGravatar.with(orderInfo.getEmail()).defaultImage("<div>Icons made by <a href=\"https://www.flaticon.com/authors/smashicons\" title=\"Smashicons\">Smashicons</a> from <a href=\"https://www.flaticon.com/\" title=\"Flaticon\">www.flaticon.com</a> is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\" target=\"_blank\">CC 3.0 BY</a></div>").size(100).build();
        try {
            Picasso.with(this)
                    .load(gravatarUrl)
                    .placeholder(R.drawable.ic_avatar)
                    .error(R.drawable.ic_avatar)
                    .into(img_profile_avatar);
        } catch (Exception e) {
        }
    }

    private void BindViewControl() {
        lin_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + lbl_phone.getText()));
                startActivity(intent);
            }
        });
        lin_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + lbl_mobile.getText()));
                startActivity(intent);
            }
        });
        btn_modify_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerProfileActivity.this, EditProfileActivity.class);
                Bundle extras = getIntent().getExtras();
                intent.putExtra("user_info", extras.getString("user_info"));
                startActivity(intent);
                finish();
            }
        });
    }

    private void set_information() {
        lbl_customer_name.setText(orderInfo.getUname());
        lbl_phone.setText(orderInfo.getUtel());
        lbl_mobile.setText(orderInfo.getUtel2());
        lbl_email.setText(orderInfo.getEmail());
        lbl_postal_code_name.setText(orderInfo.getPostalCode());
        lbl_desc_name.setText(orderInfo.getTozihat());
        for (int i = 0; i < provices.size(); i++) {
            if (orderInfo.getOstan() == provices.get(i).getID()) {
                lbl_address_name.setText(provices.get(i).getName());
                for (int j = 0; j < provices.get(i).getCitites().size(); j++) {
                    if (provices.get(i).getCitites().get(j).getID() == orderInfo.getCity()) {
                        lbl_address_name.setText(lbl_address_name.getText() + "," + provices.get(i).getCitites().get(j).getName());

                    }
                }
            }

        }
        if (!lbl_address_name.getText().toString().equals("")) {
            lbl_address_name.setText(lbl_address_name.getText() + "," + orderInfo.getSAdd());
        } else {
            lbl_address_name.setText(orderInfo.getSAdd());
        }

    }
}
