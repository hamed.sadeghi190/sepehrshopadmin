package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.github.clans.fab.FloatingActionButton;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Recycler_View_Adapter;
import com.sepehrcc.apps.sepehradmin.DbModels.KalaDB;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProductListActivity extends AppCompatActivity {
    @BindView(R.id.lbl_title)
    TextView lbl_title;
    @BindView(R.id.editSearch_Orders)
    SearchView product_Edit_Search;
    @BindView(R.id.fab_adding_product)
    FloatingActionButton fab_adding_product;
    @BindView(R.id.lst_products)
    RecyclerView recyclerView;
    @BindView(R.id.lin_progress)
    LinearLayout lin_progress;
    Handler handler;
    Recycler_View_Adapter kalaadapter;
    List<Kala> data = new ArrayList<>();
    List<KalaDB> LocalData = new ArrayList<>();
    WebServicelistener servicelistener;
    LinearLayoutManager mLayoutManager;
    int offset = 0;
    int limit = 20;
    int totalcount;
    int groupID = 0;
    boolean IsForResualt = false;

    private boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_product_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);
        product_Edit_Search.setIconifiedByDefault(false);
        product_Edit_Search.setFocusable(false);
        product_Edit_Search.setIconified(true);
        mLayoutManager = new LinearLayoutManager(this);
        initialView();
        handler = new Handler();
        BindEvents();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            groupID = bundle.getInt("groupID", 0);
            GetDataFromServer();
        } else {
            GetDataFromServer();
        }
    }

    private void GetDataFromServer() {
        if (G.isNetworkAvailable()) {
            lin_progress.setVisibility(View.VISIBLE);
            if (limit > 0) {
                if (groupID > 0)
                    LocalData = new Select().from(KalaDB.class).where("kGroup=" + groupID).offset(offset).limit(limit).execute();
                else
                    LocalData = new Select().from(KalaDB.class).orderBy("kId desc ").offset(offset).limit(limit).execute();
                ArrayList<Kala> tdata = new ArrayList<>();
                for (KalaDB kitem : LocalData) {
                    tdata.add(new Kala(kitem));
                }
                if (totalcount == 0 && tdata.size() == 0) {
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                    finish();
                } else {
                    data.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list(data);
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                }
            }
            if (limit == 0) {
                lin_progress.setVisibility(View.GONE);
            }


        } else if (!G.isNetworkAvailable()) {
            final SweetAlertDialog pDialog1 = new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE);
            pDialog1
                    .setTitleText("خطا!")
                    .setContentText("شما به اینترنت متصل نیستید!")
                    .setConfirmText("امتحان مجدد")
                    .setCancelText("بستن")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GetDataFromServer();
                            pDialog1.dismiss();
                        }
                    })
                    .show();
        }
    }

    private void GetDataFromServer2() {
        final KalaRepository repository = new KalaRepository();
        lin_progress.setVisibility(View.VISIBLE);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                servicelistener = new WebServicelistener() {
                    @Override
                    public void OnComplete(Object value) {
                        try {
                            ArrayList<Kala> tdata = (ArrayList<Kala>) value;
                            if (totalcount == 0 && tdata.size() == 0) {
                                // Toast.makeText(G.CurrentActivity, "Empty list", Toast.LENGTH_LONG).show();
                                loading = false;
                                lin_progress.setVisibility(View.GONE);
                                finish();
                            } else {
                                data.addAll(tdata);
                                totalcount += tdata.size();
                                offset = totalcount - 1;
                                if (tdata.size() < limit) {
                                    limit = 0;
                                    offset = 0;

                                }
                                fill_list(data);
                                loading = false;
                                lin_progress.setVisibility(View.GONE);
                            }
                        } catch (Throwable t) {
                            int a = 0;
                        }
                    }

                    @Override
                    public void OnFail(Object object) {
                        lin_progress.setVisibility(View.GONE);
                    }
                };
                if (limit > 0) {
                    repository.GetAllKala(servicelistener, offset, limit);
                }
            }
        }, 1000);
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }

    private void BindEvents() {
        fab_adding_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddProductActivity.class);
                startActivity(intent);
            }
        });
        product_Edit_Search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    data.clear();
                    fill_list(data);
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    searchGroup(newText);
                } else {
                    data.clear();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    GetDataFromServer();
                }
                return false;
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            if (product_Edit_Search.getQuery().toString().equals("")) {
                                GetDataFromServer();

                            } else {
                                searchGroup(product_Edit_Search.getQuery().toString());
                            }
                        }
                    }
                }
            }
        });

    }

    private void initialView() {
        lbl_title.setTypeface(Fonts.VazirBold);

//        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
//        itemAnimator.setAddDuration(100);
//        itemAnimator.setRemoveDuration(1000);
        // data.addAll(fill_with_data());

        // fill_list(data);
        lin_progress.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(50);
    }

    private void fill_list(List<Kala> data) {
        Recycler_View_Adapter adapter = new Recycler_View_Adapter(data, this);
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(pastVisiblesItems);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.scrollToPosition(pastVisiblesItems);

    }

    //when back top layout the list update
    @Override
    protected void onRestart() {
        super.onRestart();
        data.clear();
        loading = true;
        offset = 0;
        limit = 10;
        totalcount = 0;
//        pastVisiblesItems = 0;
//        visibleItemCount = 0;
//        totalItemCount = 0;
        GetDataFromServer();
    }

    public void getKala(int ID) {
        KalaRepository repository = new KalaRepository();
        lin_progress.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Kala> tdata = (ArrayList<Kala>) value;
                    data.clear();
                    data.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list_kala(data);
                    loading = false;
                    lin_progress.setVisibility(View.GONE);

                } catch (Throwable t) {
                }
                // dialog2.dismiss();
            }

            @Override
            public void OnFail(Object object) {
                lin_progress.setVisibility(View.GONE);
            }
        };
        // dialog2.show();
        if (limit > 0) {
            repository.GetKalaByGroup(servicelistener, ID, offset, limit);
        }
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }

    private void fill_list_kala(List<Kala> data) {
        kalaadapter = new Recycler_View_Adapter(data, this);
        recyclerView.setAdapter(kalaadapter);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    public void searchGroup(String key) {
        KalaRepository repository = new KalaRepository();
        lin_progress.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Kala> tdata = (ArrayList<Kala>) value;
                    data.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list(data);
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                } catch (Throwable t) {

                }
            }

            @Override
            public void OnFail(Object object) {
                lin_progress.setVisibility(View.GONE);
            }
        };
        if (limit > 0) {
            repository.search(servicelistener, offset, limit, key);
        }
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }
}
