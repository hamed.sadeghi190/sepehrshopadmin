package com.sepehrcc.apps.sepehradmin.GoogleModel;

import java.util.ArrayList;

/**
 * Created by Administrator on 30/05/2018.
 */

public class queries {
    ArrayList<request> request=new ArrayList<request>();
    ArrayList<nextPage> nextPage=new ArrayList<nextPage>();

    public ArrayList<com.sepehrcc.apps.sepehradmin.GoogleModel.request> getRequest() {
        return request;
    }

    public void setRequest(ArrayList<com.sepehrcc.apps.sepehradmin.GoogleModel.request> request) {
        this.request = request;
    }

    public ArrayList<com.sepehrcc.apps.sepehradmin.GoogleModel.nextPage> getNextPage() {
        return nextPage;
    }

    public void setNextPage(ArrayList<com.sepehrcc.apps.sepehradmin.GoogleModel.nextPage> nextPage) {
        this.nextPage = nextPage;
    }
}
