package com.sepehrcc.apps.sepehradmin.Models;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by Administrator on 03/06/2018.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

     @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("sepehradmin");
    }

}

