package com.sepehrcc.apps.sepehradmin.DbModels;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;

import java.util.ArrayList;

/**
 * Created by Hamed Sadeghi on 13/06/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
@Table(name = "Products")
public class KalaDB extends Model {
    public KalaDB(){}
    public KalaDB(Kala kalaitem) {
        this._kId = kalaitem.GetID();
        this._kName = kalaitem.GetName();
        this._kDesc = kalaitem.GetDescription();
        this._kGroupName = kalaitem.GetGroupName();
        this._kGroup = kalaitem.GetGroup();
        this._kImage = kalaitem.GetImage();
        this._kMark = kalaitem.GetMark();
        this._kMarkName = kalaitem.GetkMarkName();
        this._kPrice = kalaitem.GetPrice();
       // this._Photos.addAll(kalaitem.GetPhotos());
      //  this._Types.addAll(kalaitem._Types);
    }

    @Column(name ="kId")
    private int _kId;
    public int GetID() { return _kId; }
    public KalaDB SetID(int value) {_kId = value;return this;}

    @Column(name ="kName")
    private String _kName;
    public String GetName() { return _kName; }
    public KalaDB SetName(String value) {_kName = value;return this;}

    @Column(name ="kImage")
    private String _kImage;
    public String GetImage() { return _kImage; }
    public KalaDB SetImage(String value) {_kImage = value;return this;}

    @Column(name ="kPrice")
    private int _kPrice;
    public int GetPrice () { return _kPrice; }
    public KalaDB SetPrice(int  value) {_kPrice = value;return this;}

    @Column(name ="kDesc")
    private String _kDesc;
    public String GetDescription() { return _kDesc; }
    public KalaDB SetDescription(String value) {_kDesc = value;return this;}

    @Column(name ="kGroup")
    private int _kGroup;
    public int GetGroup () { return _kGroup; }
    public KalaDB SetGroup(int  value) {_kGroup = value;return this;}

    @Column(name ="kGroupName")
    private String _kGroupName;
    public String GetGroupName () { return _kGroupName; }
    public KalaDB SetGroupName(String  value) {_kGroupName = value;return this;}

    @Column(name ="kMark")
    private String _kMark;
    public String GetMark() { return _kMark; }
    public KalaDB SetMark(String value) {_kMark = value;return this;}
//
//    @Column(name ="Types")
//    private ArrayList<KalaTypes> _Types  = new ArrayList<>();
//    public ArrayList<KalaTypes>  GetKinds() { return _Types; }
//    public KalaDB SetKinds(ArrayList<KalaTypes>  value) {_Types.addAll(value);return this;}

//    @Column(name ="Photos")
//    private ArrayList<Photos> _Photos  = new ArrayList<>();
//    public ArrayList<Photos>  GetPhotos() { return _Photos; }
//    public KalaDB SetPhotos(ArrayList<Photos>  value) {_Photos.addAll(value);return this;}

    @Column(name ="kMarkName")
    private String _kMarkName;
    public String GetkMarkName() { return _kMarkName; }
    public KalaDB SetkMarkName(String value) {_kMarkName = value;return this;}


}

