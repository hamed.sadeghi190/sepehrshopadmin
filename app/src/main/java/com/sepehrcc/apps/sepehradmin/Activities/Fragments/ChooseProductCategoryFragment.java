package com.sepehrcc.apps.sepehradmin.Activities.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddCategorayActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddProductActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.CategoryListActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.FirstGroupActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.FirstProductActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.LoadingSplashActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.MainActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.PaymentMthodsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.ProductListActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SplashScreenActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ShopInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;
import com.thefinestartist.finestwebview.FinestWebView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChooseProductCategoryFragment extends Fragment {
    View view;
    @BindView(R.id.lbl_title_choose_products_ctegory)
    TextView lbl_title_choose_products_ctegory;
    @BindView(R.id.lbl_all_rpoducts)
    TextView lbl_all_rpoducts;
    @BindView(R.id.lbl_all_categories)
    TextView lbl_all_categories;

//    @BindView(R.id.crd_all_product)
//    CardView crd_all_product;
    @BindView(R.id.crd_all_category)
    CardView crd_all_category;
    @BindView(R.id.fab_adding_group)
    FloatingActionButton fab_adding_group;
    @BindView(R.id.fab_adding_product)
    FloatingActionButton fab_adding_product;
    @BindView(R.id.ripple_all_product)
    RippleView ripple_all_product;
    @BindView(R.id.ripple_all_category)
    RippleView ripple_all_category;
    @BindView(R.id.count_all_category)
    TextView count_all_category;
    @BindView(R.id.count_all_products)
    TextView count_all_products;
    @BindView(R.id.lbl_question)
    TextView lbl_question;
    @BindView(R.id.lbl_guide_one)
    TextView lbl_guide_one;
    @BindView(R.id.lbl_guide_two)
    TextView lbl_guide_two;
    @BindView(R.id.lbl_guide_three)
    TextView lbl_guide_three;
    @BindView(R.id.lbl_guide_four)
    TextView lbl_guide_four;
    @BindView(R.id.add_menu)
    FloatingActionMenu add_menu;

    public ChooseProductCategoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_choose_product_category, container, false);
        ButterKnife.bind(this, view);
        initialView();
        bindControlEvents();
        return view;
    }

    private void initialView() {
        lbl_title_choose_products_ctegory.setTypeface(Fonts.VazirBold);
        lbl_all_rpoducts.setTypeface(Fonts.VazirBold);
        lbl_all_categories.setTypeface(Fonts.VazirBold);
        count_all_category.setTypeface(Fonts.VazirBoldFD);
        count_all_products.setTypeface(Fonts.VazirBoldFD);
        lbl_question.setTypeface(Fonts.VazirBold);
        lbl_guide_one.setTypeface(Fonts.VazirBold);
        lbl_guide_two.setTypeface(Fonts.VazirBold);
        lbl_guide_three.setTypeface(Fonts.VazirBold);
        lbl_guide_four.setTypeface(Fonts.VazirBold);
        changeCount();
    }

    private void bindControlEvents() {
        ripple_all_category.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (G.appData.shopInfo.GetGroupCount() == 0) {
                    add_menu.close(false);
                    Intent intent = new Intent(getContext(), FirstGroupActivity.class);
                    startActivity(intent);
                } else {
                    add_menu.close(false);
                    Intent intent = new Intent(getContext(), CategoryListActivity.class);
                    startActivity(intent);
                }
            }
        });
        ripple_all_product.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (G.appData.shopInfo.GetKalaCount() == 0) {
                    add_menu.close(false);
                    Intent intent = new Intent(getContext(), FirstProductActivity.class);
                    startActivity(intent);
                } else {
                    add_menu.close(false);
                    Intent intent = new Intent(getContext(), ProductListActivity.class);
                    startActivity(intent);
                }
            }
        });

        fab_adding_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_menu.close(false);
                Intent intent = new Intent(getContext(), AddCategorayActivity.class);
                startActivity(intent);
            }
        });
        fab_adding_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_menu.close(false);
                Intent intent = new Intent(getContext(), AddProductActivity.class);
                startActivity(intent);

            }
        });
        lbl_guide_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/products"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(getActivity()).show("http://help.sepehrcc.com/article/start");

            }
        });
        lbl_guide_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/groups"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(getActivity()).show("http://help.sepehrcc.com/article/group");

            }
        });
        lbl_guide_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/orders"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(getActivity()).show("http://help.sepehrcc.com/orders");

            }
        });
        lbl_guide_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/pages"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(getActivity()).show("http://help.sepehrcc.com/pages");

            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        changeCount();
    }
    @Override
    public void onStart() {
        super.onStart();
        changeCount();
    }
    private void changeCount() {
        count_all_products.setText(G.appData.shopInfo.GetKalaCount() + "");
        count_all_category.setText(G.appData.shopInfo.GetGroupCount() + "");
    }
}
