package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.KindListAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Photo_Adapter;
import com.sepehrcc.apps.sepehradmin.DbModels.ProductGroup;
import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.Models.Gallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.GroupRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thefinestartist.finestwebview.FinestWebView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChangeGroupActivity extends AppCompatActivity implements IPickResult {
    @BindView(R.id.edit_group_name)
    TextInputLayout edit_group_name;
    @BindView(R.id.edit_new_group_desc)
    TextInputLayout edit_new_group_desc;
    @BindView(R.id.txt_new_group)
    TextView txt_new_group;
    @BindView(R.id.txt_how_to_grouping)
    TextView txt_how_to_grouping;
    @BindView(R.id.txt_example)
    TextView txt_example;
    @BindView(R.id.btn_Save)
    Button btn_send;
    @BindView(R.id.btn_cancle)
    Button btn_cancle;
    @BindView(R.id.txt_category_desc)
    EditText txt_category_desc;
    @BindView(R.id.edt_Prodcut_Name)
    EditText edt_Prodcut_Name;
    @BindView(R.id.img_take_pic)
    ImageView img_take_pic;
    @BindView(R.id.lbl_group_pic)
    TextView lbl_group_pic;
    @BindView(R.id.kind_list)
    RecyclerView kind_list;
    @BindView(R.id.lbl_types)
    TextView lbl_types;
    @BindView(R.id.add_new_type)
    TextView add_new_type;
    @BindView(R.id.btn_group_del)
    Button btn_group_del;
    @BindView(R.id.lin_change_group_pic)
    LinearLayout lin_change_group_pic;

    KindListAdapter typeAdapter;
    List<Gallery> galleries;
    Photo_Adapter adpter;
    boolean isOpened = false;
    WebServicelistener servicelistener;
    SweetAlertDialog pDialog;
    List<KindGroup> chech_data = new ArrayList<KindGroup>();
    Group currentGroup = null;
    List<KindGroup> changelist = new ArrayList<KindGroup>();
    boolean has_image = false;
    String ImageUrl = "";
    PickSetup setup = new PickSetup();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_change_group);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);
        G.CurrentActivity = this;
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String Citem = bundle.getString("group", "");
            if (!(Citem.equals(""))) {
                Gson gson = new Gson();
                try {
                    currentGroup = gson.fromJson(Citem, Group.class);

                } catch (Exception ex) {
                }
            }
        }
        initialView();
        init();
        BindViewControls();
        setup
                .setTitle("انتخاب")
                .setTitleColor(Color.BLACK)
                .setBackgroundColor(Color.WHITE)
                .setProgressText("صبر کنید...")
                .setProgressTextColor(Color.BLACK)
                .setCancelText("بستن")
                .setCancelTextColor(Color.BLACK)
                .setButtonTextColor(Color.BLACK)
                .setMaxSize(500)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText("دوربین")
                .setGalleryButtonText("گالری")
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setSystemDialog(false)
                .setCameraToPictures(false)
                .setGalleryIcon(R.drawable.ic_gallery_picker)
                .setCameraIcon(R.drawable.ic_photo_camera);
    }

    private void initialView() {
        edit_group_name.setTypeface(Fonts.VazirBold);
        edit_new_group_desc.setTypeface(Fonts.VazirBold);
        // edit_types.setTypeface(Fonts.VazirBold);
        txt_new_group.setTypeface(Fonts.VazirBold);

        txt_how_to_grouping.setTypeface(Fonts.VazirBold);
        txt_example.setTypeface(Fonts.VazirBold);
        btn_send.setTypeface(Fonts.VazirBold);
        btn_cancle.setTypeface(Fonts.VazirBold);
        edt_Prodcut_Name.setTypeface(Fonts.VazirBold);

        txt_category_desc.setTypeface(Fonts.VazirBold);
        lbl_group_pic.setTypeface(Fonts.VazirBold);
        add_new_type.setTypeface(Fonts.VazirBold);
        lbl_types.setTypeface(Fonts.VazirBold);
        btn_group_del.setTypeface(Fonts.VazirBoldFD);
        edt_Prodcut_Name.setText(currentGroup.GetName());
        G.desc_save = currentGroup.GetDescription();

        // Document message = Jsoup.parse(currentGroup.GetDescription());
        txt_category_desc.setText(currentGroup.GetDescription());
        for (int i = 0; i < currentGroup.GetKinds().size(); i++) {
            KindGroup c = new KindGroup();
            c.SetTitle(currentGroup.GetKinds().get(i).GetTitle());
            c.SetID(currentGroup.GetKinds().get(i).GetID());
            chech_data.add(c);
        }
        fill_list_type(chech_data);
        //for kind list
        changelist = currentGroup.GetKinds();
        for (int i = 0; i < changelist.size(); i++) {
            changelist.get(i).SetStatus(0);
        }
        //for G
        G.changekindslist = currentGroup.GetKinds();
        for (int i = 0; i < G.changekindslist.size(); i++) {
            G.changekindslist.get(i).SetStatus(0);
        }
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                img_take_pic.setImageBitmap(bitmap);
                String path = Environment.getExternalStorageDirectory().toString();
                OutputStream fOut = null;
                Integer counter = 0;
                ImageUrl = path + "/changegroup.jpg";
                File file = new File(path, "changegroup.jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
                try {
                    fOut = new FileOutputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                try {
                    fOut.flush(); // Not really required
                    fOut.close();
                    MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
                    has_image = true;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                img_take_pic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                img_take_pic.setImageDrawable(placeHolderDrawable);
            }
        };
        try {
            String url = G.PhotoUrl + currentGroup.GetImage();
            url = url.replaceAll(" ", "%20");
            Picasso.with(getApplicationContext())
                    .load(url)
                    .placeholder(R.drawable.ic_no_photos)
                    .error(R.drawable.ic_no_photos)
                    .into(target);
        } catch (Exception e) {
            String url = G.PhotoUrl + currentGroup.GetImage();
            url = url.replaceAll(" ", "%20");
            Picasso.with(getApplicationContext())
                    .load(url)
                    .placeholder(R.drawable.ic_no_photos)
                    .error(R.drawable.ic_no_photos)
                    .into(target);
        }
    }

    private void init() {
//        galleries = fill_with_data();
//        fill_list(galleries);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void BindViewControls() {
        txt_category_desc.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.txt_category_desc) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!attemp_change_group()) {
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("ارسال ...");
                    pDialog.setCancelable(true);

                    final Group groupObj = new Group();
                    groupObj.SetName(edt_Prodcut_Name.getText().toString());
                    groupObj.SetDescription(txt_category_desc.getText().toString());
                    groupObj.SetID(currentGroup.GetID());
                    groupObj.SetParentID(currentGroup.GetParentID());
                    // groupObj.SetImage(Environment.getExternalStorageDirectory().toString() + "sepehrgroup.jpg");
                    ArrayList<KindGroup> _gkinds = new ArrayList<KindGroup>();

                    for (int i = 0; i < chech_data.size(); i++) {
                        KindGroup kind = new KindGroup();
                        // kind.SetGroupID(25);
                        kind.SetTitle(chech_data.get(i).GetTitle());
                        kind.SetGroupID(currentGroup.GetID());
                        _gkinds.add(kind);
                    }
                    // groupObj.SetKinds(_gkinds);
                    groupObj.SetKinds((ArrayList<KindGroup>) changelist);
                    // groupObj.SetKinds(G.changekindslist);
                    servicelistener = new WebServicelistener() {
                        @Override
                        public void OnComplete(Object object) {
                            AddResponse response = (AddResponse) object;
                            ProductGroup item = new Select()
                                    .from(ProductGroup.class)
                                    .where("GID = ?", currentGroup.GetID())
                                    .executeSingle();
                            item.UpdateInfo(groupObj);
                            item.save();
                            if (has_image == true) {
                                if (response != null) {
                                    WebServicelistener lis = new WebServicelistener() {
                                        @Override
                                        public void OnComplete(Object object) {
                                            G.changekindslist = new ArrayList<KindGroup>();
                                            G.desc_save = "";
                                            pDialog.hide();

                                            Intent intent = new Intent(ChangeGroupActivity.this, SuccessfullyActivity.class);
                                            intent.putExtra("successfully_message", "تغییرات مورد نظر با موفقیت اضافه شد");
                                            startActivity(intent);
                                            finish();

                                        }

                                        @Override
                                        public void OnFail(Object object) {

                                        }
                                    };
                                    new GroupRepository().UploadPhoto(lis, ImageUrl, response.id);

                                }
                            } else {
                                G.changekindslist = new ArrayList<KindGroup>();
                                G.desc_save = "";
                                pDialog.hide();

                                Intent intent = new Intent(ChangeGroupActivity.this, SuccessfullyActivity.class);
                                intent.putExtra("successfully_message", "تغییرات مورد نظر با موفقیت اضافه شد");
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void OnFail(Object object) {
                            pDialog.hide();
                            new SweetAlertDialog(ChangeGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("خطا!")
                                    .setContentText("تغییرات اعمال نشد")
                                    .setConfirmText("بستن")
                                    .show();
                        }
                    };
                    pDialog.show();
                    new GroupRepository().EditGroup(servicelistener, groupObj);

                    //G.desc_save = "";
                    //  finish();
                } else {
                    Toast.makeText(ChangeGroupActivity.this, "لطفا تمامی فیلد ها را به درستی تکمیل کنید", Toast.LENGTH_LONG).show();

                }
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.desc_save = "";
                finish();

            }
        });
        lin_change_group_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(setup).setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult pickResult) {
                        if (pickResult.getError() == null) {
                            save_image(pickResult.getBitmap());

                        } else {

                        }
                    }
                }).show(ChangeGroupActivity.this);
            }
        });
        add_new_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                String jsonToString = gson.toJson(changelist);
                // Intent i = new Intent(G.CurrentActivity, TypesActivity.class);
                Intent i = new Intent(ChangeGroupActivity.this, KindsActivity.class);
                ArrayList<String> kindList = new ArrayList<String>();
                for (int j = 0; j < chech_data.size(); j++) {
                    kindList.add(chech_data.get(j).GetTitle());
                }
                i.putStringArrayListExtra("hhh", kindList);
                i.putExtra("withoutchangelist", jsonToString);
                startActivityForResult(i, 4);
            }
        });
        txt_how_to_grouping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/groups"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(ChangeGroupActivity.this).show("http://help.sepehrcc.com/groups");

            }
        });
        btn_group_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentGroup.GetKalaCount() == 0 && currentGroup.GetGroupSubCount() == 0) {
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    final SweetAlertDialog sdialog = new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.WARNING_TYPE);
                    sdialog.setTitleText("حذف کالا");
                    sdialog.setContentText("آیا مطمئن هستید ؟");
                    sdialog.setConfirmText("تایید");
                    sdialog.setCancelText("انصراف");
                    sdialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {

                            final SweetAlertDialog pDialog = new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.PROGRESS_TYPE);
                            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                            pDialog.setTitleText("ارسال اطلاعات ...");
                            pDialog.setCancelable(false);
                            pDialog.show();
                            servicelistener = new WebServicelistener() {
                                @Override
                                public void OnComplete(Object object) {
                                    ResponseBase resualt = (ResponseBase) object;
                                    if (resualt.Status == 200) {
                                        pDialog.hide();
                                        new Delete().from(ProductGroup.class).where("GID = ? ", currentGroup.GetID()).execute();
                                        Intent intent = new Intent(ChangeGroupActivity.this, SuccessfullyActivity.class);
                                        intent.putExtra("delete_message", "گروه مورد نظر با موفقیت حذف شد");
                                        startActivity(intent);
                                        G.appData.shopInfo.SetGroupCount(G.appData.shopInfo.GetGroupCount()-1);
                                        finish();
                                    } else if (resualt.Status == 700) {
                                        pDialog.hide();
                                        new SweetAlertDialog(ChangeGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("خطا")
                                                .setContentText("گروه مورد نظر زیر گروه یا کالا دارد")
                                                .setConfirmText("بستن")
                                                .show();
                                    }
                                }

                                @Override
                                public void OnFail(Object object) {
                                    pDialog.hide();
                                    new SweetAlertDialog(ChangeGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("خطا")
                                            .setContentText("گروه مورد نظر حذف نشد!")
                                            .setConfirmText("بستن")
                                            .show();
                                }
                            };
                            pDialog.show();
                            new GroupRepository().DeleteGroup(servicelistener, currentGroup.GetID());
                        }
                    });
                    sdialog.show();
                } else {
                    new SweetAlertDialog(ChangeGroupActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText("  حذف گروه مورد نظر به علت داشتن زیر گروه یا کالا امکانپذیر نیست !")
                            .setConfirmText("بستن")
                            .show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == 1) {
            String rich_edit_text_result = data.getStringExtra("rich_edit_text_html");
            txt_category_desc.setText(rich_edit_text_result);
        } else if (resultCode == 4) {
            chech_data.clear();
            for (int i = 0; i < data.getStringArrayListExtra("typelist").size(); i++) {
                KindGroup kg = new KindGroup();
                kg.SetTitle(data.getStringArrayListExtra("typelist").get(i));
                chech_data.add(kg);
            }
            fill_list_type(chech_data);
            // Bundle bundle = getIntent().getExtras();
            String Citem = data.getStringExtra("withoutchangelist");
            if (!(Citem.equals(""))) {

                Gson gson = new Gson();
                try {
                    Type type = new TypeToken<ArrayList<KindGroup>>() {
                    }.getType();
                    changelist = gson.fromJson(Citem, type);
                } catch (Exception ignored) {

                }
            }
        } else if (resultCode == 10) {
            save_image_croped(G.b);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpened = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        G.desc_save = "";
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            save_image(pickResult.getBitmap());
        }
    }

    private void fill_list_type(List<KindGroup> data) {
        typeAdapter = new KindListAdapter(data, this);
        kind_list.setAdapter(typeAdapter);
        kind_list.setLayoutManager(new LinearLayoutManager(this));
    }

    public void save_image(Bitmap result) {
        G.b = result;
        Intent i = new Intent(ChangeGroupActivity.this, CropActivity.class);
        i.putExtra("PhotoName", "changegroup.jpg");
        startActivityForResult(i, 10);
    }

    public void save_image_croped(Bitmap result) {
        img_take_pic.setImageBitmap(result);
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        ImageUrl = path + "/changegroup.jpg";
        Integer counter = 0;
        File file = new File(path, "changegroup.jpg"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        result.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
        try {
            fOut.flush(); // Not really required
            fOut.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            has_image = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Boolean attemp_change_group() {
        edt_Prodcut_Name.setError(null);
        String group_name = edt_Prodcut_Name.getText().toString();
        Boolean cancle = false;
        View focusView = null;
        if (group_name.length() == 0 || group_name.equals("") || group_name == null) {
            edt_Prodcut_Name.setError("نام گروه نمی تواند خالی باشد");
            cancle = true;
            focusView = edt_Prodcut_Name;
        }
        if (cancle) {
            focusView.requestFocus();
        }
        return cancle;
    }
}
