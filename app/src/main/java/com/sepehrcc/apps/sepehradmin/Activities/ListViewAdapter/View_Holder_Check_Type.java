package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 14/11/2017.
 */

public class View_Holder_Check_Type extends RecyclerView.ViewHolder {

    @BindView(R.id.check_box_type)
    CheckBox check_box_type;

    public View_Holder_Check_Type(View itemview) {
        super(itemview);
        ButterKnife.bind(this, itemView);
        check_box_type.setTypeface(Fonts.VazirBoldFD);
    }
}