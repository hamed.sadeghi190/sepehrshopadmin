package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;

import java.util.Collections;
import java.util.List;

public class ChechTypeAdapter extends RecyclerView.Adapter<View_Holder_Check_Type> {

    List<KindGroup> list = Collections.emptyList();
    Context context;

    public ChechTypeAdapter(List<KindGroup> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder_Check_Type onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_chackbox_types_row_themplate, parent, false);
        return new View_Holder_Check_Type(v);
    }

    @Override
    public void onBindViewHolder(final View_Holder_Check_Type holder, @SuppressLint("RecyclerView") final int position) {
        KindGroup  citem = list.get(position);
        holder.check_box_type.setText(citem.GetTitle());
        holder.check_box_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.get(position).SetChecked(holder.check_box_type.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, KindGroup gallery) {
        list.add(position, gallery);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(KindGroup order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
    }

}