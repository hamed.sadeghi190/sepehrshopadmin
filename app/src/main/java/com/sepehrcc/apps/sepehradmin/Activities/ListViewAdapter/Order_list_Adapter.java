package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.OrderDetailActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.RandomColor;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;

import java.util.Collections;
import java.util.List;

public class Order_list_Adapter extends RecyclerView.Adapter<View_Holder_Order> {

    List<Order> list = Collections.emptyList();
    Context context;

    public Order_list_Adapter(List<Order> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder_Order onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_order_row_themplate, parent, false);
        View_Holder_Order holder = new View_Holder_Order(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_Order holder, final int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.name.setText(list.get(position).GetOrdrUname());
        holder.ID.setText("# " + list.get(position).GetOrdrID() + "/" + list.get(position).GetOrdrCode());
        holder.price.setText(list.get(position).GetOrdrCost() + "");
        if (list.get(position).GetOrdrPayed() > 0) {
            // viewHolder.img_paid.setVisibility(View.VISIBLE);
            holder.lbl_payed.setVisibility(View.VISIBLE);
        } else {
            // viewHolder.img_paid.setVisibility(View.INVISIBLE);
            holder.lbl_payed.setVisibility(View.INVISIBLE);
        }
        switch (list.get(position).GetOrdrState()) {
            case 1:
                holder.lbl_order_status.setText("جدید");
                holder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_new_background));
                break;
            case 2:
                holder.lbl_order_status.setText("تایید شده");
                holder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_verify_background));
                break;
            case 3:
                holder.lbl_order_status.setText("آماده ارسال");
                holder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                break;
            case 4:
                holder.lbl_order_status.setText("ارسال شده");
                holder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                break;
            case 5:
                holder.lbl_order_status.setText("کامل شده");
                holder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_complete_background));
                break;
            case 6:
                holder.lbl_order_status.setText("معلق");
                holder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_archive_background));


                break;
        }
        holder.rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Gson gson = new Gson();
                String order_json = gson.toJson(list.get(position));
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("order_object", order_json);
                intent.putExtra("is_paid",list.get(position).GetOrdrPayed());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Order order) {
        list.add(position, order);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Order order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
    }

}

