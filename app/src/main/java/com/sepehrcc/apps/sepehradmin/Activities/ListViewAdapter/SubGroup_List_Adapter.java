package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.Models.Category;
import com.sepehrcc.apps.sepehradmin.Models.SubGroup;
import com.sepehrcc.apps.sepehradmin.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 12/11/2017.
 */


public class SubGroup_List_Adapter extends RecyclerView.Adapter<View_Holder_Sub_Group> {

    List<SubGroup> list = Collections.emptyList();
    Context context;

    public SubGroup_List_Adapter(List<SubGroup> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder_Sub_Group onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_sub_group_row_themplate, parent, false);
        View_Holder_Sub_Group holder = new View_Holder_Sub_Group(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_Sub_Group holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.sub_group_list_item_title.setText(list.get(position).title);
        holder.sub_group_list_item_count.setText(list.get(position).count + " کالا");
        holder.sub_group_list_item_image.setImageResource(list.get(position).image);
        holder.sub_group_list_ripple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                //do somthing after ripple compelete
            }
        });

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, SubGroup data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(SubGroup data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

}