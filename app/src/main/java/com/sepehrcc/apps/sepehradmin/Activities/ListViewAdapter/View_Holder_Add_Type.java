package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 15/11/2017.
 */

public class View_Holder_Add_Type extends RecyclerView.ViewHolder {
    @BindView(R.id.lbl_type_item_title)
    TextView lbl_type_item_title;
    @BindView(R.id.lbl_type_delete)
    TextView lbl_type_delete;
    @BindView(R.id.type_list_ripple)
    RippleView type_list_ripple;


    public View_Holder_Add_Type(View itemview) {
        super(itemview);
        ButterKnife.bind(this, itemView);
        lbl_type_delete.setTypeface(Fonts.VazirBoldFD);
        lbl_type_item_title.setTypeface(Fonts.VazirBoldFD);
    }
}
