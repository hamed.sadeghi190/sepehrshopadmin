package com.sepehrcc.apps.sepehradmin.WebService.Models;

/**
 * Created by Administrator on 19/05/2018.
 */

public class ThemeModel {
    String name;
    String link;
    String code;
    int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
