package com.sepehrcc.apps.sepehradmin.Models;

/**
 * Created by Hamed Sadeghi on 15/10/2017.
 */

public class Category {
    public String title;
    public int count;
    public int image;

    public Category(String title, int count, int image) {
        this.title = title;
        this.count = count;
        this.image = image;

    }
}
