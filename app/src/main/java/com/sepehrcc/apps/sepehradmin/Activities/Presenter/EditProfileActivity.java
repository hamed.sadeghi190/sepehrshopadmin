package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.BrandsAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.UserInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.OrderRepository;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class EditProfileActivity extends AppCompatActivity {
    @BindView(R.id.btn_send)
    TextView btn_send;
    @BindView(R.id.lbl_edit_profile)
    TextView lbl_edit_profile;
    @BindView(R.id.btn_Back)
    TextView btn_Back;
    @BindView(R.id.crd_phone)
    CardView crd_phone;
    @BindView(R.id.lbl_phone)
    TextView lbl_phone;
    @BindView(R.id.edt_phone)
    EditText edt_phone;
    @BindView(R.id.lbl_mobile)
    TextView lbl_mobile;
    @BindView(R.id.edt_mobile)
    EditText edt_mobile;
    @BindView(R.id.crd_address)
    CardView crd_address;
    @BindView(R.id.lbl_first_address)
    TextView lbl_first_address;
    @BindView(R.id.edt_first_address)
    EditText edt_first_address;
    @BindView(R.id.lbl_second_address)
    TextView lbl_second_address;
    @BindView(R.id.edt_second_address)
    EditText edt_second_address;
    @BindView(R.id.lbl_postal_code)
    TextView lbl_postal_code;
    @BindView(R.id.edt_postal_code)
    EditText edt_postal_code;
    @BindView(R.id.lbl_deliver_name)
    TextView lbl_deliver_name;
    @BindView(R.id.edt_deliver_name)
    EditText edt_deliver_name;
    @BindView(R.id.crd_active)
    CardView crd_active;
    @BindView(R.id.check_active)
    CheckBox check_active;
    OrderInfo orderInfo = new OrderInfo();
    WebServicelistener servicelistener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        initialView();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Gson gson = new Gson();
            orderInfo = gson.fromJson(extras.getString("user_info"), OrderInfo.class);
            edt_phone.setText(orderInfo.getUtel());
            edt_mobile.setText(orderInfo.getUtel2());
            edt_first_address.setText(orderInfo.getSAdd());
            edt_second_address.setText(orderInfo.getSAdd2());
            edt_postal_code.setText(orderInfo.getPostalCode());
            edt_deliver_name.setText(orderInfo.getsReceiver());
        }
        BindViewControl();
    }

    private void initialView() {
        btn_send.setTypeface(Fonts.VazirBoldFD);
        lbl_edit_profile.setTypeface(Fonts.VazirBoldFD);
        btn_Back.setTypeface(Fonts.VazirBoldFD);
        lbl_phone.setTypeface(Fonts.VazirBoldFD);
        edt_phone.setTypeface(Fonts.VazirBoldFD);
        lbl_mobile.setTypeface(Fonts.VazirBoldFD);
        edt_mobile.setTypeface(Fonts.VazirBoldFD);
        lbl_first_address.setTypeface(Fonts.VazirBoldFD);
        edt_first_address.setTypeface(Fonts.VazirBoldFD);
        lbl_second_address.setTypeface(Fonts.VazirBoldFD);
        edt_second_address.setTypeface(Fonts.VazirBoldFD);
        lbl_postal_code.setTypeface(Fonts.VazirBoldFD);
        edt_postal_code.setTypeface(Fonts.VazirBoldFD);
        lbl_deliver_name.setTypeface(Fonts.VazirBoldFD);
        edt_deliver_name.setTypeface(Fonts.VazirBoldFD);
        check_active.setTypeface(Fonts.VazirBoldFD);
    }

    private void BindViewControl() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInfo userInfo=new UserInfo();
                userInfo.setOrderID(orderInfo.getID());
                userInfo.setPostalCode(edt_postal_code.getText().toString());
                userInfo.setSAdd(edt_first_address.getText().toString());
                userInfo.setSAdd2(edt_second_address.getText().toString());
                userInfo.setUtel(edt_phone.getText().toString());
                userInfo.setUtel2(edt_mobile.getText().toString());
                userInfo.setsReceiver(edt_deliver_name.getText().toString());
                change_user_info(userInfo);
            }
        });
        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        edt_first_address.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_first_address) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        edt_second_address.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_second_address) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void change_user_info(UserInfo userInfo) {
        final SweetAlertDialog pDialog=new SweetAlertDialog(EditProfileActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(true);
        Gson gson=new Gson();
        String json=gson.toJson(userInfo);
        OrderRepository orderRepository = new OrderRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                feedback feedback=(feedback)object;
                if(feedback.Status==1)
                {pDialog.hide();
                    Intent intent=new Intent(EditProfileActivity.this,SuccessfullyActivity.class);
                    intent.putExtra("successfully_message", "تغییرات مورد نظر با موفقیت اعمال شد.");
                    startActivity(intent);
                    finish();
                }else
                {
                    pDialog.hide();
                    new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText("تغییرات اعمال نشد لطفا بعدا تلاش کنید.")
                            .setConfirmText("بستن")
                            .show();
                }
            }
            @Override
            public void OnFail(Object object) {
                pDialog.hide();
                new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("خطا")
                        .setContentText("تغییرات اعمال نشد لطفا بعدا تلاش کنید.")
                        .setConfirmText("بستن")
                        .show();
            }
        };
        pDialog.show();
        orderRepository.ChangeUserInfo(servicelistener, userInfo);
    }
}
