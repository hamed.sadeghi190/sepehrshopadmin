package com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers;

import android.graphics.Typeface;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;

/**
 * Created by Mohammad Hamed Sadeghi on 2017/10/08.
 * Email : hamed.sadeghi190@gmail.com
 */

public class Fonts {
    public static Typeface IranSans;
    public static Typeface Yekan;
    public static Typeface Homa;
    public static Typeface Vazir;
    public static Typeface VazirFD;
    public static Typeface VazirBoldFD;
    public static Typeface VazirBold;
    public static Typeface VazirLight;
    public static Typeface VazirMedium;
    public static Typeface VazirThin;
    public static Typeface Cocon_next_arabic_light;
    public static void LoadFonts() {
        IranSans = Typeface.createFromAsset(G.context.getAssets(), "Fonts/iran.ttf");
        Yekan = Typeface.createFromAsset(G.context.getAssets(), "Fonts/yekan.ttf");
        Homa = Typeface.createFromAsset(G.context.getAssets(), "Fonts/homa.ttf");
        //vazir pack
        VazirFD = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazirfd.ttf");
        VazirBoldFD = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazirboldfd.ttf");
        Vazir = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazir.ttf");
        VazirBold = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazirboldfd.ttf");
        VazirLight = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazir_light.ttf");
        VazirMedium = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazir_medium.ttf");
        VazirThin = Typeface.createFromAsset(G.context.getAssets(), "Fonts/vazir_thin.ttf");

        //cocon
        Cocon_next_arabic_light=Typeface.createFromAsset(G.context.getAssets(),"Fonts/cocon-next.arabic-light.ttf");
    }
}
