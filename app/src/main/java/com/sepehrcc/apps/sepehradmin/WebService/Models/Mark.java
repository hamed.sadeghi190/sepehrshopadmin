package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;


public class Mark {
    @SerializedName("ID")
    private int _ID;

    public int GetMarkID() {
        return _ID;
    }

    public Mark setMarkID(int value) {
        _ID = value;
        return this;
    }

    @SerializedName("Name")
    private String _Name;

    public String GetMarkName() {
        return _Name;
    }

    public Mark setMarkName(String value) {
        _Name = value;
        return this;
    }

    @SerializedName("Image")
    private String _Image;

    public String GetMarkImage() {
        if(_Image!=null) {
            return _Image;
        }
        return "";
    }

    public Mark setMarkImage(String value) {
        _Image = value;
        return this;
    }

    @SerializedName("Count")
    private int _Count;

    public int GetMarkCount() {
        return _Count;
    }
    public Mark setMarkCount(int value) {
        _Count = value;
        return this;
    }
}
