package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Categoray_list_Adapter;
import com.sepehrcc.apps.sepehradmin.DbModels.ProductGroup;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.GroupRepository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CategoryListActivity extends AppCompatActivity {
    @BindView(R.id.txtGroups)
    TextView txtGroups;
    @BindView(R.id.product_search_view)
    SearchView editSearch;
    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_adding_category)
    com.github.clans.fab.FloatingActionButton fab_adding_category;
    @BindView(R.id.lin_progress)
    LinearLayout lin_progress;
    Categoray_list_Adapter adapter;
    ArrayList<Group> data = new ArrayList<>();
    WebServicelistener servicelistener;
    LinearLayoutManager mLayoutManager;
    boolean IsForResualt = false;
    int offset = 0;
    int limit = 10;
    int totalcount;
    private boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    Handler handler;
    List<ProductGroup> LocalData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_category_list);
        G.CurrentActivity = this;
        mLayoutManager = new LinearLayoutManager(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this);
        init();
        handler = new Handler();
        BindControlsEvents();
        data.clear();
        Bundle Extras = getIntent().getExtras();
        if (Extras != null) {
            if (Extras.containsKey("IsForResualt")) {
                IsForResualt = Extras.getBoolean("IsForResualt");
            }
        }
       GetDataFromServer();
    }

    private void GetDataFromServer() {
        if (G.isNetworkAvailable()) {
            lin_progress.setVisibility(View.VISIBLE);
            if (limit > 0) {
                LocalData = new Select().from(ProductGroup.class).where("GParent=0").offset(offset).limit(limit).execute();
                ArrayList<Group> tdata = new ArrayList<> ();
                for (ProductGroup Gitem:LocalData)
                {
                    tdata.add(new Group(Gitem));
                }
                if (totalcount == 0 && tdata.size() == 0) {
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                    finish();
                } else {
                    data.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list(data);
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                }
            }
            if (limit == 0) {
                lin_progress.setVisibility(View.GONE);
            }


        } else if (!G.isNetworkAvailable()) {
            final SweetAlertDialog pDialog1 = new SweetAlertDialog(CategoryListActivity.this, SweetAlertDialog.ERROR_TYPE);
            pDialog1
                    .setTitleText("خطا!")
                    .setContentText("شما به اینترنت متصل نیستید!")
                    .setConfirmText("امتحان مجدد")
                    .setCancelText("بستن")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GetDataFromServer();
                            pDialog1.dismiss();
                        }
                    })
                    .show();
        }
    }

    private void init() {
        editSearch.setIconifiedByDefault(false);
        editSearch.setFocusable(false);
        editSearch.setIconified(true);
        txtGroups.setTypeface(Fonts.VazirBold);
        lin_progress.setVisibility(View.GONE);
    }

    private void BindControlsEvents() {
        fab_adding_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddCategorayActivity.class);
                startActivity(intent);
            }
        });
        editSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    data.clear();
                    fill_list(data);
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    searchGroup(newText);
                } else {
                    data.clear();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    GetDataFromServer();
                }
                return false;
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            if (editSearch.getQuery().toString().equals("")) {
                                GetDataFromServer();
                            } else {
                                searchGroup(editSearch.getQuery().toString());
                            }
                        }
                    }
                }
            }
        });
    }

    private void fill_list(ArrayList<Group> data) {
        adapter = new Categoray_list_Adapter(data, this, IsForResualt);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.scrollToPosition(pastVisiblesItems);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", result);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        data.clear();
        loading = false;
        offset = 0;
        limit = 10;
        totalcount = 0;
        GetDataFromServer();
    }

    public void searchGroup(String key) {
        GroupRepository repository = new GroupRepository();
        lin_progress.setVisibility(View.GONE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Group> tdata = (ArrayList<Group>) value;
                    data.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list(data);
                    loading = false;
                    lin_progress.setVisibility(View.GONE);
                } catch (Throwable ignored) {
                }
            }

            @Override
            public void OnFail(Object object) {
                lin_progress.setVisibility(View.GONE);
            }
        };
        if (limit > 0) {
            repository.search(servicelistener, 0, offset, limit, key);
        }
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }


}
