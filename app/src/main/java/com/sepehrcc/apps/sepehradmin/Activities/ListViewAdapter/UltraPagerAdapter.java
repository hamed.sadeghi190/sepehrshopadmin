package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SuccessfullyActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.CryptLib;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Theme;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ThemeRepository;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.logging.Handler;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class UltraPagerAdapter extends PagerAdapter {
    private boolean isMultiScr;
    List<Theme> list = Collections.emptyList();
    Context context;
    WebServicelistener servicelistener;
    SweetAlertDialog pDialog;

    public UltraPagerAdapter(boolean isMultiScr, List<Theme> list, Context context) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        this.isMultiScr = isMultiScr;
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        View v = LayoutInflater.from(container.getContext())
                .inflate(R.layout.lst_theme_row, container, false);
        UltraPagerAdapter.ListInterface listinterface = (UltraPagerAdapter.ListInterface) context;
        listinterface.previewTheme(position);
        TextView textView = (TextView) v.findViewById(R.id.lbl_theme_name);
        final ImageView imageView = (ImageView) v.findViewById(R.id.img_theme);
        LinearLayout btn_preview = (LinearLayout) v.findViewById(R.id.lin_preview);
        LinearLayout btn_select = (LinearLayout) v.findViewById(R.id.lin_select);
        TextView lbl_select = (TextView) v.findViewById(R.id.lbl_select);
        TextView lbl_preview = (TextView) v.findViewById(R.id.lbl_preview);
        textView.setText(list.get(position).GetThemeTName());
        textView.setTypeface(Fonts.VazirBoldFD);
        lbl_select.setTypeface(Fonts.VazirBoldFD);
        lbl_preview.setTypeface(Fonts.VazirBoldFD);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                Picasso.with(context)
                        .load("https://sepehrcc.com/images/themes/" + list.get(position).GetThemeTPicAdd())
                        .placeholder(R.drawable.ic_no_image)
                        .error(R.drawable.ic_no_image)
                        .into(imageView);
            }
        });
        builder.build()
                .load("https://sepehrcc.com/images/themes/" + list.get(position).GetThemeTPicAdd())
                .placeholder(R.drawable.ic_no_image)
                .into(imageView);
        btn_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(context).show("http://" + list.get(position).GetThemeTName() + ".sepehrcc.com");

            }
        });
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTheme(position);
            }
        });
        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        RelativeLayout view = (RelativeLayout) object;
        container.removeView(view);
    }

    public interface ListInterface {
        public void previewTheme(int position);
    }

    private void selectTheme(int position) {
        int themeid = list.get(position).GetThemeTId();
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(true);
        ThemeRepository themeRepository = new ThemeRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {

                try {
                    CryptLib crypt = new CryptLib();
                    String password = "987654321";
                    String key = CryptLib.SHA256(G.appData.shopInfo.GetApiKey().toLowerCase(), 32); //32 bytes = 256 bit
                    password = G.appData.shopInfo.GetSecretKey().toLowerCase() + password;
                    password = crypt.encrypt(password, key, G.appData.shopInfo.GetSecretKey().toLowerCase());
                    password = URLEncoder.encode(password, "utf-8");
                    OkHttpClient client = new OkHttpClient();
                    String uri = "http://" + G.appData.shopInfo.GetsubDomain() + ".sepehrcc.com/api/shop.ashx?apikey=" +
                            G.appData.shopInfo.GetApiKey() + "&do=clearcache&pssword=" + password;
                    Request request = new Request.Builder()
                            .url(uri)
                            .build();
                    client.newCall(request).execute();
                    pDialog.hide();
                    Intent intent = new Intent(context, SuccessfullyActivity.class);
                    intent.putExtra("successfully_message", "قالب مورد نظر با موفقیت انتخاب شد");
                    context.startActivity(intent);

                } catch (Exception e) {
                    pDialog.hide();
                    Intent intent = new Intent(context, SuccessfullyActivity.class);
                    intent.putExtra("successfully_message", "قالب مورد نظر با موفقیت انتخاب شد");
                    context.startActivity(intent);
                }
            }

            @Override
            public void OnFail(Object object) {
                pDialog.hide();
                Intent intent = new Intent(context, SuccessfullyActivity.class);
                intent.putExtra("delete_message", "متاسفانه در خواست شما با شکست مواجه شد لطفا بعدا تلاش کنید");
                context.startActivity(intent);
            }
        };
        pDialog.show();
        themeRepository.SetTheme(servicelistener, themeid);
    }
}