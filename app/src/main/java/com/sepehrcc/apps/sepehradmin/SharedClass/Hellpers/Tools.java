package com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers;

import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;

import com.gmail.samehadar.iosdialog.IOSDialog;
import com.sepehrcc.apps.sepehradmin.R;

/**
 * Created by Hamed Sadeghi on 11/15/2017.
 */

public class Tools {
    public static IOSDialog CreateWaitingDialog(Context contex, String text) {
        return new IOSDialog.Builder(contex)
                .setSpinnerColorRes(R.color.md_blue_500)
                .setMessageColorRes(R.color.gray)
                .setTitleColorRes(R.color.gray)
                .setMessageContent(text)
                .setSpinnerClockwise(false)
                .setCancelable(true)
                .setSpinnerDuration(120)
                .setMessageContentGravity(Gravity.END)
                .build();
    }
}
