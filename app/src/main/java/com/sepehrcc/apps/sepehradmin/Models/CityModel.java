package com.sepehrcc.apps.sepehradmin.Models;

/**
 * Created by Administrator on 03/06/2018.
 */

public class CityModel {
    int ID;
    int PID;
    String Name;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPID() {
        return PID;
    }

    public void setPID(int PID) {
        this.PID = PID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
