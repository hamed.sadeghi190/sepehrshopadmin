package com.sepehrcc.apps.sepehradmin.WebService.Models;
/**
 * Created by Hamed on 11/14/2017.
 */

import com.google.gson.annotations.SerializedName;
import com.sepehrcc.apps.sepehradmin.DbModels.ProductGroup;

import java.util.ArrayList;

public class Group {
    @SerializedName("gId")
    private int _gId;
    public int GetID() { return _gId; }
    public Group SetID(int value) {_gId = value;return this;}

    @SerializedName("gDesc")
    private String _gDesc;
    public String GetDescription() { return _gDesc; }
    public Group SetDescription(String value) {_gDesc = value;return this;}

    @SerializedName("gkinds")
    private ArrayList<KindGroup> _gkinds  = new ArrayList<>();
    public ArrayList<KindGroup>  GetKinds() { return _gkinds; }
    public Group SetKinds(ArrayList<KindGroup>  value) {_gkinds.addAll(value);return this;}

    @SerializedName("gName")
    private String _gName;
    public String GetName() { return _gName; }
    public Group SetName(String value) {_gName = value;return this;}

    @SerializedName("gParent")
    private int _gParent;
    public int GetParentID() { return _gParent; }
    public Group SetParentID(int value) {_gParent = value;return this;}

    @SerializedName("gImage")
    private String _gImage;
    public String GetImage() { return _gImage; }
    public Group SetImage(String value) {_gImage = value;return this;}

    @SerializedName("gCKala")
    private int _gCKala;
    public int GetKalaCount() { return _gCKala; }
    public Group SetKalaCount(int value) {_gCKala = value;return this;}

    @SerializedName("gCGroup")
    private int _gCGroup;
    public int GetGroupSubCount() { return _gCGroup; }
    public Group SetGroupSubCount(int value) {_gCGroup = value;return this;}

    @SerializedName("gtImage")
    private String _gtImage;
    public String getThumbNail() { return _gtImage; }
    public Group setThumbnail(String value) {_gtImage = value;return this;}
    public Group(){

    }
    public Group(ProductGroup Value){
        this.SetID(Value.GetID());
        this.SetDescription(Value.GetDescription());
        this.SetName(Value.GetGName());
        this.SetParentID(Value.GetGParent());
        this.SetImage(Value.GetGImage());
        this.setThumbnail(Value.GetGTImage());
        this.SetKalaCount(Value.GetGCKala());
        this.SetGroupSubCount(Value.GetGCGroup());
    }
}
