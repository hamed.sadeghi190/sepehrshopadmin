package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import java.util.List;

public class EditorFontSizeSpinnerAdapter extends ArrayAdapter<String> {


   public EditorFontSizeSpinnerAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(Fonts.VazirBold);
        view.setTextColor(Color.WHITE);
        view.setTextSize(18);
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(Fonts.VazirBold);
        view.setTextColor(Color.WHITE);
        view.setTextSize(18);
        return view;
    }
}