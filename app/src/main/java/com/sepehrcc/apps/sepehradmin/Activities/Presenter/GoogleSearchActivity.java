package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.GoogleSearchAdapter;
import com.sepehrcc.apps.sepehradmin.GoogleModel.items;
import com.sepehrcc.apps.sepehradmin.GoogleModel.object;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class GoogleSearchActivity extends Activity implements GoogleSearchAdapter.selectPhoto {

    EditText eText;
    Button btn;
    RecyclerView list;
    ProgressBar progressBar;

    private static final String TAG = "searchApp";
    static String result = null;
    Integer responseCode = null;
    String responseMessage = "";
    ArrayList<items> items = new ArrayList<items>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_search);
        Log.d(TAG, "**** APP START ****");
        Bundle extras = getIntent().getExtras();

        // / GUI init
        eText = (EditText) findViewById(R.id.edittext);
        btn = (Button) findViewById(R.id.button);
        list = (RecyclerView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.pb_loading_indicator);
        btn.setTypeface(Fonts.VazirBold);
        eText.setTypeface(Fonts.VazirBoldFD);
        if (extras != null) {
            eText.setText(extras.getString("kala_name") + "");
            if (!eText.getText().toString().equals("")) {
                search();
            }
        }
        // button onClick
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                final String searchString = eText.getText().toString();
//                Log.d(TAG, "Searching for : " + searchString);
//                //  resultTextView.setText("Searching for : " + searchString);
//
//                // hide keyboard
//                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//
//                // looking for
//                String searchStringNoSpaces = searchString.replace(" ", "+");
//
//                // Your API key
//                // TODO replace with your value
//                String key = "AIzaSyADNkNDO6NYmfUDAczFscDZ00mQlHozK5Q";
//
//                // Your Search Engine ID
//                // TODO replace with your value
//                String cx = "009166592887137657034:c836agkhbys";
//                String urlString = "https://www.googleapis.com/customsearch/v1?q=" + searchStringNoSpaces + "&num=10&start=1&alt=json&searchType=image&key=AIzaSyADNkNDO6NYmfUDAczFscDZ00mQlHozK5Q&cx=009166592887137657034:c836agkhbys";
//
//                URL url = null;
//                try {
//                    url = new URL(urlString);
//                } catch (MalformedURLException e) {
//                    Log.e(TAG, "ERROR converting String to URL " + e.toString());
//                }
//                Log.d(TAG, "Url = " + urlString);
//
//
//                // start AsyncTask
//                GoogleSearchAsyncTask searchTask = new GoogleSearchAsyncTask();
//                searchTask.execute(url);
                search();
            }
        });

    }

    private void search() {
        final String searchString = eText.getText().toString();
        Log.d(TAG, "Searching for : " + searchString);
        //  resultTextView.setText("Searching for : " + searchString);

        // hide keyboard
        // InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        // inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        // looking for
        String searchStringNoSpaces = searchString.replace(" ", "+");

        // Your API key
        // TODO replace with your value
        String key = "AIzaSyADNkNDO6NYmfUDAczFscDZ00mQlHozK5Q";

        // Your Search Engine ID
        // TODO replace with your value
        String cx = "009166592887137657034:c836agkhbys";
        String urlString = "https://www.googleapis.com/customsearch/v1?q=" + searchStringNoSpaces + "&num=10&start=1&alt=json&searchType=image&key=AIzaSyADNkNDO6NYmfUDAczFscDZ00mQlHozK5Q&cx=009166592887137657034:c836agkhbys";

        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            Log.e(TAG, "ERROR converting String to URL " + e.toString());
        }
        Log.d(TAG, "Url = " + urlString);


        // start AsyncTask
        GoogleSearchAsyncTask searchTask = new GoogleSearchAsyncTask();
        searchTask.execute(url);
    }

    @Override
    public void SelectPhoto(String link) {
        Intent intent = new Intent();
        intent.putExtra("photo_link", link);
        setResult(12, intent);
        finish();
    }


    private class GoogleSearchAsyncTask extends AsyncTask<URL, Integer, String> {

        protected void onPreExecute() {
            Log.d(TAG, "AsyncTask - onPreExecute");
            // show progressbar
            progressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected String doInBackground(URL... urls) {

            URL url = urls[0];
            Log.d(TAG, "AsyncTask - doInBackground, url=" + url);

            // Http connection
            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                Log.e(TAG, "Http connection ERROR " + e.toString());
            }


            try {
                responseCode = conn.getResponseCode();
                responseMessage = conn.getResponseMessage();
            } catch (IOException e) {
                Log.e(TAG, "Http getting response code ERROR " + e.toString());
            }

            Log.d(TAG, "Http response code =" + responseCode + " message=" + responseMessage);

            try {

                if (responseCode == 200) {

                    // response OK

                    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;

                    while ((line = rd.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    rd.close();

                    conn.disconnect();

                    result = sb.toString();

                    Log.d(TAG, "result=" + result);

                    return result;

                } else {

                    // response problem

                    String errorMsg = "Http ERROR response " + responseMessage + "\n" + "Make sure to replace in code your own Google API key and Search Engine ID";
                    Log.e(TAG, errorMsg);
                    result = errorMsg;
                    return result;

                }
            } catch (IOException e) {
                Log.e(TAG, "Http Response ERROR " + e.toString());
            }


            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            Log.d(TAG, "AsyncTask - onProgressUpdate, progress=" + progress);

        }

        protected void onPostExecute(String result) {

            Log.d(TAG, "AsyncTask - onPostExecute, result=" + result);

            // hide progressbar
            progressBar.setVisibility(View.GONE);

            // make TextView scrollable
            /////resultTextView.setMovementMethod(new ScrollingMovementMethod());
            // show result
            //  resultTextView.setText(result);
            Gson gson = new Gson();
            object object = gson.fromJson(result, object.class);
            items = object.getItems();
            fill_list();
        }


    }

    private void fill_list() {
        list.setLayoutManager(new GridLayoutManager(this, 2));
        GoogleSearchAdapter googleSearchAdapter = new GoogleSearchAdapter(items, this);
        list.setAdapter(googleSearchAdapter);
    }

}
