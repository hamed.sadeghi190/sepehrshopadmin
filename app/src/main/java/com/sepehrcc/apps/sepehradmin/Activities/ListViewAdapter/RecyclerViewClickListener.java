package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.view.View;

/**
 * Created by Administrator on 18/11/2017.
 */

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
