package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddProductActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohammad Hamed Sadeghi on 2017/10/12.
 * Email : hamed.sadeghi190@gmail.com
 */

public class FirstProductFragment extends Fragment {
    View view;
    @BindView(R.id.Btn_first_step_plus)
    ImageView Btn_first_step_plus;
    @BindView(R.id.txt_reg_first_product)
    TextView txt_reg_first_product;
    @BindView(R.id.txt_first_steps)
    TextView txt_first_steps;
    @BindView(R.id.txt_how_to)
    TextView txt_how_to;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_firstproduct, container, false);
        ButterKnife.bind(this, view);
        initialView();
        BindControlsEvent();
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    private void initialView() {
        txt_reg_first_product.setTypeface(Fonts.VazirBold);
        txt_first_steps.setTypeface(Fonts.VazirLight);
        txt_how_to.setTypeface(Fonts.VazirLight);
    }

    private void BindControlsEvent() {
        Btn_first_step_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddProductActivity.class);
                startActivity(intent);
            }
        });
    }
}
