package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 12/11/2017.
 */

public class View_Holder_Sub_Group_Product_Gallery extends RecyclerView.ViewHolder {
    @BindView(R.id.sub_group_product_gallery_card_view)
    CardView sub_group_product_gallery_card_view;
    @BindView(R.id.sub_group_product_txt_gallery)
    TextView sub_group_product_txt_gallery;
    @BindView(R.id.sub_group_product_img_gallery)
    ImageView sub_group_product_img_gallery;
    @BindView(R.id.sub_group_product_gallery_ripple)
    RippleView sub_group_product_gallery_ripple;

    View_Holder_Sub_Group_Product_Gallery(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        sub_group_product_txt_gallery.setTypeface(Fonts.VazirFD);
    }
}
