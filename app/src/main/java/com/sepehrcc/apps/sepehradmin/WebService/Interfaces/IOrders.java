package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.ChangeOrder;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderDetials;
import com.sepehrcc.apps.sepehradmin.WebService.Models.UserInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;


import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;


public interface IOrders {
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Order/list/{Offset}/{limit}")
    Call<feedback> GetAllOrders(@Header("Authorization") String AuthKey, @Path("Offset") int Offset,@Path("limit") int limit);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Order/list/{type}/{Offset}/{limit}")
    Call<feedback> GetOrders(@Header("Authorization") String AuthKey,@Path("type") int type, @Path("Offset") int Offset,@Path("limit") int limit);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Order/search/{Offset}/{limit}/{key}")
    Call<feedback> SearchOrders(@Header("Authorization") String AuthKey, @Path("Offset") int Offset,@Path("limit") int limit,@Path("key") String key);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Order/detail/{id}")
    Call<feedback> GetDetailOrder(@Header("Authorization") String AuthKey, @Path("id") int id);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Order/ChangeStatus")
    Call<feedback> ChangeOrderStatus(@Header("Authorization") String AuthKey, @Body ChangeOrder changeOrder);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("order/ChangeUserInfo")
    Call<feedback> ChangeUserInfo(@Header("Authorization") String AuthKey, @Body UserInfo userInfo);

}
