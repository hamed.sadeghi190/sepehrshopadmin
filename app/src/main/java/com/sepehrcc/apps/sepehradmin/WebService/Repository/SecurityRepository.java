package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.LoginModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Register;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ShopInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class SecurityRepository {

    public void Login(final WebServicelistener lisitner, LoginModel value) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.Login(value);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                                lisitner.OnFail(ErrorModel);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
                    lisitner.OnFail(ErrorModel);
                }
            });

        } catch (Exception ex) {
            ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
            lisitner.OnFail(ErrorModel);
        }
    }

    public void shopInfo(final WebServicelistener lisitner) {
        try {
            final Call<ShopInfo> CallServer = WebServiceHandler.SecurityClient.shopInfo(G.ApiToken);
            CallServer.enqueue(new Callback<ShopInfo>() {
                @Override
                public void onResponse(Response<ShopInfo> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                lisitner.OnFail(null);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    } else {
                        ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                        lisitner.OnFail(null);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.ConnectFail);
                    lisitner.OnFail(ErrorModel);
                }
            });

        } catch (Exception ex) {
            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.ConnectFail);
            lisitner.OnFail(ErrorModel);
        }
    }

    public void SendRegisterCode(final WebServicelistener lisitner, Register value) {

        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.SendRegisterCode(value);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200"))
                            {
                                lisitner.OnComplete(response.body());
                            }
                            else
                            {
                                feedback fback = new feedback(400,ErrorHandler.Error);
                                lisitner.OnFail(fback);
                            }
                        }
                        else
                         {
                             feedback fback = new feedback(400,ErrorHandler.Error);
                             lisitner.OnFail(fback);
                        }
                    }
                    else {
                         feedback fback = new feedback(400,ErrorHandler.Error);
                        lisitner.OnFail(fback);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    feedback fback = new feedback(500,ErrorHandler.ConnectFail);
                    lisitner.OnFail(fback);
                }
            });

        } catch (Exception ex) {
            feedback fback = new feedback(500,ErrorHandler.ConnectFail);
            lisitner.OnFail(fback);
        }
    }

    public void SendRetryCode(final WebServicelistener lisitner, String phone) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.SendRetryCode(phone);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                                lisitner.OnFail(ErrorModel);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
                    lisitner.OnFail(ErrorModel);
                }
            });

        } catch (Exception ex) {
            ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
            lisitner.OnFail(ErrorModel);
        }
    }

    public void SendVerifyRegister(final WebServicelistener lisitner, Register value) {

        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.VerifyRegister(value);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                feedback fback = new feedback(400,ErrorHandler.Error);
                                lisitner.OnFail(fback);
                            }
                        } else {
                            feedback fback = new feedback(400,ErrorHandler.Error);
                            lisitner.OnFail(fback);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    feedback fback = new feedback(500,ErrorHandler.ConnectFail);
                    lisitner.OnFail(fback);
                }
            });

        } catch (Exception ex) {

            feedback fback = new feedback(500,ex.getMessage());
            lisitner.OnFail(fback);
        }
    }

    public void SendloginCode(final WebServicelistener lisitner, String phone) {

        final ResponseBase Resulat = new ResponseBase();
        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.SendLoginSms(phone);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                feedback fback = new feedback(400,ErrorHandler.Error);
                                lisitner.OnFail(fback);
                            }
                        } else {
                            feedback fback = new feedback(400,ErrorHandler.Error);
                            lisitner.OnFail(fback);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    feedback fback = new feedback(500,ErrorHandler.ConnectFail);
                    lisitner.OnFail(fback);
                }
            });

        } catch (Exception ex) {
            feedback fback = new feedback(500,ErrorHandler.ConnectFail);
            lisitner.OnFail(fback);
        }
    }

    public void SendVerifylogin(final WebServicelistener lisitner, String phone, String code) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.VerifyLogin(phone, code);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                                lisitner.OnFail(ErrorModel);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
                    lisitner.OnFail(ErrorModel);
                }
            });

        } catch (Exception ex) {
            ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
            lisitner.OnFail(ErrorModel);
        }
    }
    public void DoLogin(final WebServicelistener lisitner, int sub,String phone, String UCode) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.SecurityClient.DoLogin(sub,phone, UCode);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            } else {
                                ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                                lisitner.OnFail(ErrorModel);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
                    lisitner.OnFail(ErrorModel);
                }
            });

        } catch (Exception ex) {
            ResponseBase ErrorModel = new ResponseBase(500, ErrorHandler.ConnectFail);
            lisitner.OnFail(ErrorModel);
        }
    }
}
