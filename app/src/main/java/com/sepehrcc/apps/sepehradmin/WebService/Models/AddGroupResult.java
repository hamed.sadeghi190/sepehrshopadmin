package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;

import org.jsoup.Connection;

/**
 * Created by Administrator on 22/11/2017.
 */


public class AddGroupResult extends ResponseBase {

    @SerializedName("id")
    private int _gId;
    public int GetID() { return _gId; }

    public AddGroupResult SetID(int value) {_gId = value;return this;}

    AddGroupResult(int Status, String Message) {
        super(Status, Message);
    }

}
