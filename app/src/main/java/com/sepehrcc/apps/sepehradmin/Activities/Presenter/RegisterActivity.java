package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {
    //    @BindView(R.id.lbl_title_register)
//    TextView lbl_title_register;
//    @BindView(R.id.edit_user_name)
//    TextInputLayout edit_user_name;
//    @BindView(R.id.edt_user_name)
//    EditText edt_user_name;
//    @BindView(R.id.edit_user_email)
//    TextInputLayout edit_user_email;
//    @BindView(R.id.edt_user_email)
//    EditText edt_user_email;
//    @BindView(R.id.edit_store_address)
//    TextInputLayout edit_store_address;
//    @BindView(R.id.edt_store_address)
//    EditText edt_store_address;
//    @BindView(R.id.edit_mobile_number)
//    TextInputLayout edit_mobile_number;
//    @BindView(R.id.edt_mobile_number)
//    EditText edt_mobile_number;
//    //    @BindView(R.id.edit_store_id)
////    TextInputLayout edit_store_id;
////    @BindView(R.id.edt_store_id)
////    EditText edt_store_id;
////    @BindView(R.id.edit_store_name)
////    TextInputLayout edit_store_name;
////    @BindView(R.id.edt_store_name)
////    EditText edt_store_name;
//    @BindView(R.id.edit_store_password)
//    TextInputLayout edit_store_password;
//    @BindView(R.id.edt_store_password)
//    EditText edt_store_password;
//    @BindView(R.id.lbl_text_preview)
//    TextView lbl_text_preview;
//    @BindView(R.id.lbl_address_attach)
//    TextView lbl_address_attach;
//    @BindView(R.id.lbl_set_your_domain_later)
//    TextView lbl_set_your_domain_later;
//
//    @BindView(R.id.btn_register)
//    Button btn_register;
//    @BindView(R.id.edit_store_password_repeat)
//    TextInputLayout edit_store_password_repeat;
//    @BindView(R.id.edt_store_password_repeat)
//    EditText edt_store_password_repeat;
//
//    public static final Pattern regular_expression = Pattern.compile("[\u0600-\u06FF\u0750-\u077F\u0590-\u05FF\uFE70-\uFEFF]");
//    String edit_address_content = "";
    @BindView(R.id.lbl_coming_soon)
    TextView lbl_coming_soon;
    @BindView(R.id.btn_exit)
    Button btn_exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_register);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);
        //initialView();
        // bindViewControl();
        lbl_coming_soon.setTypeface(Fonts.VazirBoldFD);
        btn_exit.setTypeface(Fonts.VazirBoldFD);

        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().remove("Token").commit();
                Intent i=new Intent(RegisterActivity.this,SplashScreenActivity.class);
                startActivity(i);
                finish();

            }
        });
    }

//    private void initialView() {
//        lbl_title_register.setTypeface(Fonts.VazirBold);
//        edit_user_name.setTypeface(Fonts.VazirBold);
//        edt_user_name.setTypeface(Fonts.VazirBold);
//        edt_user_name.requestFocus();
//        edit_user_email.setTypeface(Fonts.VazirBold);
//        edt_user_email.setTypeface(Fonts.VazirBold);
//        edit_store_address.setTypeface(Fonts.VazirBold);
//        edt_store_address.setTypeface(Fonts.VazirBold);
//        edit_mobile_number.setTypeface(Fonts.VazirBold);
//        edt_mobile_number.setTypeface(Fonts.VazirBold);
//        lbl_text_preview.setTypeface(Fonts.VazirBold);
//        lbl_address_attach.setTypeface(Fonts.VazirBold);
//        lbl_set_your_domain_later.setTypeface(Fonts.VazirBold);
////        edit_store_id.setTypeface(Fonts.VazirBold);
////        edt_store_id.setTypeface(Fonts.VazirBold);
////        edit_store_name.setTypeface(Fonts.VazirBold);
////        edt_store_name.setTypeface(Fonts.VazirBold);
//        edit_store_password.setTypeface(Fonts.VazirBold);
//        edt_store_password.setTypeface(Fonts.VazirBold);
//        btn_register.setTypeface(Fonts.VazirBold);
//        edit_store_password_repeat.setTypeface(Fonts.VazirBold);
//        edt_store_password_repeat.setTypeface(Fonts.VazirBold);
//    }
//
//    private void bindViewControl() {
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//              //  if (attempRegister()) {
//                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
//             //   }
//            }
//        });
//        edt_store_address.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (!isPersianhWord(charSequence.toString())) {
//                    edit_address_content = charSequence.toString();
//                    lbl_text_preview.setText(edt_store_address.getText().toString());
//                } else {
//                    edt_store_address.setText(edit_address_content);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//    }
//
//    public static boolean isPersianhWord(String string) {
//        return regular_expression.matcher(string).find();
//    }
//
//    private boolean passwordValidator() {
//        if (!(edt_store_password.getText().toString().equals("")) && edt_store_password.getText().toString().length() >= 6 && edt_store_password.getText().toString().length() <= 18 && !(edt_store_password.getText().toString().contains(" "))) {
//            return true;
//        }
//        return false;
//    }
//
//    private boolean repeatPasswordValidator() {
//        if (edt_store_password_repeat.getText().toString().equals(edt_store_password.getText().toString())) {
//            return true;
//        }
//        return false;
//    }
//
//    private boolean addressValidator() {
//        if (!(edt_store_address.getText().toString().equals("")) && edt_store_address.getText().toString().matches("[a-z]+[a-zA-Z0-9._-]*") && !(edt_store_address.getText().toString().contains(" "))) {
//            return true;
//        }
//        return false;
//    }
//
//    private boolean nameValidator() {
//        if (!(edt_user_name.getText().toString().equals("")) && edt_user_name.getText().toString().length() >= 3 && edt_user_name.getText().toString().length() <= 18) {
//            return true;
//        }
//        return false;
//    }
//
//    private boolean emailValidator() {
//        if (!(edt_user_email.getText().toString().equals("")) && edt_user_email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.[a-z]+") && !(edt_user_email.getText().toString().contains(" "))) {
//            return true;
//        }
//        return false;
//
//    }
//
//    private boolean mobileValidator() {
//
//        return android.util.Patterns.PHONE.matcher(edt_mobile_number.getText().toString()).matches();
//    }
//
//    private boolean attempRegister() {
//        boolean cancle = true;
//        edt_store_password.setError(null);
//        edt_store_password_repeat.setError(null);
//        edt_store_address.setError(null);
//        edt_user_name.setError(null);
//        edt_user_email.setError(null);
//        edt_mobile_number.setError(null);
//        if(!nameValidator())
//        {
//            edt_user_name.setError("نام و نام خانوادگی باید بین 3 تا 18 کاراکتر باشد");
//            cancle = false;
//            edt_user_name.requestFocus ();
//        }
//
//        if(!repeatPasswordValidator())
//        {
//            cancle=false;
//            edt_store_password_repeat.setError("رمز عبورها با هم برابر نیستند");
//            edt_store_password_repeat.requestFocus();
//        }
//        if(!addressValidator())
//        {
//            edt_store_address.setError("آدرس باید حتما با حروف کوچک شروع شود و فاقد فاصله و حروف اضافه باشد");
//            cancle = false;
//            edt_store_address.requestFocus ();
//        }
//        if(!emailValidator())
//        {
//            edt_user_email.setError("لطفا یک ایمیل صحیح وارد کنید");
//            cancle = false;
//            edt_user_email.requestFocus();
//        }
//        if(!mobileValidator())
//        {
//            edt_mobile_number.setError("لطفا یک شماره موبایل صحیح وارد کنید");
//            cancle = false;
//            edt_mobile_number.requestFocus();
//        }
//        if(!passwordValidator())
//        {
//            cancle=false;
//            edt_store_password.setError("رمز عبور باید حداقل 6 و حداکثر 18 کاراکتر باشد و فاقد فاصله باشد");
//            edt_store_password.requestFocus();
//        }
//        return cancle;
//    }
//
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(RegisterActivity.this,SplashScreenActivity.class);
        startActivity(i);
        finish();
    }
}
