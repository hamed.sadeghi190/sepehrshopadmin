package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sepehrcc.apps.sepehradmin.GoogleModel.items;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;


public class GoogleSearchAdapter extends RecyclerView.Adapter<GoogleSearchAdapter.GroupViewHolder> {
    List<items> list = Collections.emptyList();
    Context context;

    public GoogleSearchAdapter(List<items> list, Context context) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.gallery_list_item, viewGroup, false);
        GroupViewHolder brand = new GroupViewHolder(v);
        return brand;
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {
        Picasso.with(context).load(list.get(position).getImage().getThumbnailLink()).into(holder.img_gallery);
        holder.gallery_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GoogleSearchAdapter.selectPhoto selectPhoto = (GoogleSearchAdapter.selectPhoto) context;
                selectPhoto.SelectPhoto(list.get(position).getLink());
            }
        });
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        ImageView img_gallery;
        CardView gallery_card_view;

        GroupViewHolder(View itemView) {
            super(itemView);
            img_gallery = (ImageView) itemView.findViewById(R.id.img_gallery);
            gallery_card_view = (CardView) itemView.findViewById(R.id.gallery_card_view);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    public interface selectPhoto {
        public void SelectPhoto(String link);
    }
}