package com.sepehrcc.apps.sepehradmin.WebService.Models;

import java.util.ArrayList;

/**
 * Created by Administrator on 28/05/2018.
 */

public class OrderDetials {
    public ArrayList<OrderItems> items = new ArrayList<OrderItems>();
    public OrderInfo info = new OrderInfo();

    public ArrayList<OrderItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<OrderItems> items) {
        this.items = items;
    }

    public OrderInfo getInfo() {
        return info;
    }

    public void setInfo(OrderInfo info) {
        this.info = info;
    }
}
