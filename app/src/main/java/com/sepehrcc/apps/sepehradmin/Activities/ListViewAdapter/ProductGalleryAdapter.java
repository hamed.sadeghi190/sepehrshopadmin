package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddProductActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SlidinngGalleryActivity;
import com.sepehrcc.apps.sepehradmin.Models.Gallery;

import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ProductGalleryAdapter extends RecyclerView.Adapter<View_Holder_Product_Gallery> {

    List<ProductGallery> list;
    Context context;
    WebServicelistener listener;
    WebServicelistener listener2;

    public ProductGalleryAdapter(List<ProductGallery> list, Context context, WebServicelistener servicelistener) {
        this.list = list;
        this.context = context;
        this.listener = servicelistener;
    }

    @Override
    public View_Holder_Product_Gallery onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_product_gallery_list_item, parent, false);
        return new View_Holder_Product_Gallery(v);

    }

    @Override
    public void onBindViewHolder(final View_Holder_Product_Gallery holder, final int position) {
        final ProductGallery citem = list.get(position);
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (citem.IsAddButton)
                { listener.OnComplete(this);}
                else if(G.is_prodict_change){
                    G.is_prodict_change=false;
                    Gson g = new Gson();
                    String images = g.toJson(list);
                    Type collectionType = new TypeToken<List<ProductGallery>>() {
                    }.getType();
                    List productGalleries=g.fromJson(images,collectionType);
                    productGalleries.remove(0);
                    images=g.toJson(productGalleries);
                    Intent i = new Intent(context, SlidinngGalleryActivity.class);
                    i.putExtra("images", images);
                    context.startActivity(i);
                }
            }
        });
        if (citem.IsAddButton) {
            holder.img.setImageResource(R.drawable.ic_add_img);
            holder.img_delete.setVisibility(View.GONE);
        } else {
            if (citem.FromWeb) {
                try {
                    Picasso.with(context)
                            .load(citem.image)
                            .placeholder(R.drawable.ic_no_photos)
                            .error(R.drawable.ic_no_photos)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .into(holder.img);
                } catch (Exception e) {
                    // holder.imageView.setImageResource(R.drawable.img_toys);
                }
            } else {
                File imgFile = new File(citem.image);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.img.setImageBitmap(myBitmap);
                }
            }
        }

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position != 0) {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog
                            .setTitleText("هشدار!")
                            .setContentText("آیا می خواهید این عکس را حذف کنید؟")
                            .setCancelText("خیر")
                            .setConfirmText("بله")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sweetAlertDialog) {

                                    final ProductGallery item = list.get(position);
                                    if (item.FromWeb) {
                                        listener2 = new WebServicelistener() {
                                            @Override
                                            public void OnComplete(Object object) {
                                                remove(item);
                                                sweetAlertDialog.hide();
                                            }

                                            @Override
                                            public void OnFail(Object object) {

                                            }
                                        };
                                        if (item._iId == 0)
                                            new KalaRepository().DeletePhoto(listener2, item.kalaid, item._iId, "aks");
                                        else
                                            new KalaRepository().DeletePhoto(listener2, item.kalaid, item._iId, "album");

                                    } else {
                                        remove(item);
                                        sweetAlertDialog.hide();
                                    }
                                }
                            }).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, ProductGallery gallery) {
        list.add(position, gallery);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(ProductGallery order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

}