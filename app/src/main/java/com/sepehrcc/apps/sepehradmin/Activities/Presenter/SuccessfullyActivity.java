package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SuccessfullyActivity extends AppCompatActivity {
    @BindView(R.id.lbl_congraduolation)
    TextView lbl_congraduolation;
    @BindView(R.id.lbl_you_successfully_sent)
    TextView lbl_you_successfully_sent;
    @BindView(R.id.btn_back)
    Button btn_back;
    @BindView(R.id.animation_success)
    LottieAnimationView animation_success;
    @BindView(R.id.animation_delete)
    LottieAnimationView animation_delete;
    String text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_successfully);
        ButterKnife.bind(this);
        initialView();
        BindViewControl();
    }

    private void initialView() {
        lbl_congraduolation.setTypeface(Fonts.VazirBoldFD);
        lbl_you_successfully_sent.setTypeface(Fonts.VazirBoldFD);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            try {
                text = bundle.getString("successfully_message", "");
                if (text != null && !text.equals("")) {
                    animation_success.setVisibility(View.VISIBLE);
                    lbl_you_successfully_sent.setText(text);
                } else {
                    text = bundle.getString("delete_message", "");
                    if (text != null && !text.equals("")) {
                        animation_success.setVisibility(View.GONE);
                        animation_delete.setVisibility(View.VISIBLE);lbl_congraduolation.setVisibility(View.INVISIBLE);
                        lbl_you_successfully_sent.setText(text);
                    }
                }
            } catch (Exception e) {

            }
        }
        btn_back.setTypeface(Fonts.VazirBoldFD);
    }

    public void BindViewControl() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
