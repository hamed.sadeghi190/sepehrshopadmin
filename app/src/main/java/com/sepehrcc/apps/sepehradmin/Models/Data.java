package com.sepehrcc.apps.sepehradmin.Models;

/**
 * Created by Mohammad Hamed Sadeghi on 2017/10/16.
 * Email : hamed.sadeghi190@gmail.com
 */
public class Data {
    public String title;
    public String description;
    public int imageId;

    public Data(String title, String description, int imageId) {
        this.title = title;
        this.description = description;
        this.imageId = imageId;
    }

}