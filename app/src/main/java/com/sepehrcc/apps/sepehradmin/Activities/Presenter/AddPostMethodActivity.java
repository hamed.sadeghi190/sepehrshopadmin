package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.SpinnerAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.PostsCopy;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.PostRepository;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddPostMethodActivity extends AppCompatActivity {
    @BindView(R.id.btn_send)
    TextView btn_send;
    @BindView(R.id.lbl_add_post_title)
    TextView lbl_add_post_title;
    @BindView(R.id.btn_Back)
    TextView btn_Back;
    @BindView(R.id.crd_post_title)
    CardView crd_post_title;
    @BindView(R.id.lbl_add_post)
    TextView lbl_add_post;
    @BindView(R.id.txt_ly_edt_post_title)
    TextInputLayout txt_ly_edt_post_title;
    @BindView(R.id.edt_post_title)
    EditText edt_post_title;
    @BindView(R.id.crd_calculate_post)
    CardView crd_calculate_post;
    @BindView(R.id.lbl_calculate_methode)
    TextView lbl_calculate_methode;
    @BindView(R.id.fml_calculate_methode)
    FrameLayout fml_calculate_methode;
    @BindView(R.id.spin_calculate_methode)
    Spinner spin_calculate_methode;
    @BindView(R.id.crd_calculate_zarib)
    CardView crd_calculate_zarib;
    @BindView(R.id.lbl_cal_zarib)
    TextView lbl_cal_zarib;
    @BindView(R.id.lin_percent_money)
    LinearLayout lin_percent_money;
    @BindView(R.id.txt_ly_meney_percent)
    TextInputLayout txt_ly_meney_percent;
    @BindView(R.id.edt_money_percent)
    EditText edt_money_percent;
    @BindView(R.id.lbl_toman)
    TextView lbl_toman;
    @BindView(R.id.fml_formol)
    FrameLayout fml_formol;
    @BindView(R.id.spin_formol)
    Spinner spin_formol;
    @BindView(R.id.lbl_zarib_desc)
    TextView lbl_zarib_desc;
    @BindView(R.id.crd_description)
    CardView crd_description;
    @BindView(R.id.lbl_description)
    TextView lbl_description;
    @BindView(R.id.edt_desc)
    EditText edt_desc;
    @BindView(R.id.crd_active)
    CardView crd_active;
    @BindView(R.id.check_free_post)
    CheckBox check_free_post;
    @BindView(R.id.txt_ly_free_post)
    TextInputLayout txt_ly_free_post;
    @BindView(R.id.edt_free_post)
    EditText edt_free_post;
    @BindView(R.id.lbl_free_post_more)
    TextView lbl_free_post_more;
    @BindView(R.id.check_active)
    CheckBox check_active;
    @BindView(R.id.lbl_title_example)
    TextView lbl_title_example;
    WebServicelistener servicelistener;
    PostsCopy posts = new PostsCopy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post_method);
        G.CurrentActivity = this;
        ButterKnife.bind(this);
        initialView();
        BindViewControl();

    }

    @Override
    protected void onResume() {
        super.onResume();
        G.CheckInfo();
    }

    private void initialView() {
        btn_send.setTypeface(Fonts.VazirBold);
        lbl_add_post_title.setTypeface(Fonts.VazirBold);
        btn_Back.setTypeface(Fonts.VazirBold);
        lbl_add_post.setTypeface(Fonts.VazirLight);
        txt_ly_edt_post_title.setTypeface(Fonts.VazirLight);
        edt_post_title.setTypeface(Fonts.VazirLight);
        lbl_calculate_methode.setTypeface(Fonts.VazirLight);
        lbl_cal_zarib.setTypeface(Fonts.VazirLight);
        txt_ly_meney_percent.setTypeface(Fonts.VazirLight);
        edt_money_percent.setTypeface(Fonts.VazirFD);
        lbl_toman.setTypeface(Fonts.VazirLight);
        lbl_zarib_desc.setTypeface(Fonts.VazirLight);
        lbl_description.setTypeface(Fonts.VazirLight);
        edt_desc.setTypeface(Fonts.VazirLight);
        check_free_post.setTypeface(Fonts.VazirLight);
        txt_ly_free_post.setTypeface(Fonts.VazirLight);
        edt_free_post.setTypeface(Fonts.VazirFD);
        lbl_free_post_more.setTypeface(Fonts.VazirLight);
        check_active.setTypeface(Fonts.VazirLight);
        lbl_title_example.setTypeface(Fonts.VazirLight);
        edt_post_title.requestFocus();
        SpinnerAdapter spinAdapter = new SpinnerAdapter(AddPostMethodActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.calculate_post_methods)));
        spin_calculate_methode.setAdapter(spinAdapter);
        spinAdapter = new SpinnerAdapter(AddPostMethodActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.formol_methods)));
        spin_formol.setAdapter(spinAdapter);
        posts.SetdEnable(false);
    }

    private void BindViewControl() {
        edt_desc.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_desc) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        check_free_post.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    posts.SetdFreeShipingLimit(0);
                }
            }
        });
        check_active.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    posts.SetdEnable(true);
                } else if (!(b)) {
                    posts.SetdEnable(false);
                }
            }
        });
        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!attemp_add_post()) {
                    posts.SetdName(edt_post_title.getText().toString());
                    posts.SetdDesc(edt_desc.getText().toString());
                    AddPostMethod(posts);
                } else {
                    Toast.makeText(AddPostMethodActivity.this, "لطفا تمامی فیلدها را با دقت تکمیل کنید", Toast.LENGTH_LONG).show();
                }
            }
        });
        spin_calculate_methode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (adapterView.getSelectedItem().toString()) {
                    case "مقدار ثابت (تومان)":
                        posts.SetdMethod(0);
                        lin_percent_money.setVisibility(View.VISIBLE);
                        lbl_toman.setText("تومان");
                        fml_formol.setVisibility(View.GONE);
                        lbl_zarib_desc.setText("به هر سفارش بابت هزینه ی ارسال اضافه می شود");
                        break;
                    case "درصد از مبلغ کالا":
                        posts.SetdMethod(1);
                        lin_percent_money.setVisibility(View.VISIBLE);
                        lbl_toman.setText("درصد");
                        fml_formol.setVisibility(View.GONE);
                        lbl_zarib_desc.setText("از هزینه کل سفارش به سفارش بابت هزینه ی ارسال اضافه می شود");
                        break;
                    case "فرمول":
                        lin_percent_money.setVisibility(View.GONE);
                        fml_formol.setVisibility(View.VISIBLE);
                        lbl_zarib_desc.setText("هزینه ی ارسال بر اساس وزن کالاها و تعرفه ی پست محاسبه می شود. لطفا مطمئن شوید که تنظیمات مربوطه را در تب تنظیمات همین صفحه انجام داده اید.");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spin_formol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (adapterView.getSelectedItem().toString()) {
                    case "فرمول پیشتاز داخلی":
                        posts.SetdMethod(2);
                        break;
                    case "فرمول سفارش داخلی":
                        posts.SetdMethod(3);
                        break;
                    case "فرمول ویژه داخلی":
                        posts.SetdMethod(4);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        edt_free_post.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edt_free_post.removeTextChangedListener(this);

                try {
                    String originalString = editable.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    posts.SetdFreeShipingLimit(Integer.parseInt(originalString));
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    edt_free_post.setText(formattedString);
                    edt_free_post.setSelection(edt_free_post.getText().length());

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                edt_free_post.addTextChangedListener(this);
            }
        });
        edt_money_percent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edt_money_percent.removeTextChangedListener(this);

                try {
                    String originalString = editable.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    posts.SetdAmount(Integer.parseInt(originalString));
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    edt_money_percent.setText(formattedString);
                    edt_money_percent.setSelection(edt_money_percent.getText().length());

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                edt_money_percent.addTextChangedListener(this);
            }
        });
    }

    private void AddPostMethod(PostsCopy post) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(false);
        pDialog.show();
        PostRepository postRepository = new PostRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                pDialog.hide();
                Intent intent = new Intent(AddPostMethodActivity.this, SuccessfullyActivity.class);
                intent.putExtra("successfully_message", "روش ارسال مورد نظر با موفقیت اضافه شد");
                startActivity(intent);
                finish();
            }

            @Override
            public void OnFail(Object object) {
                int a = 0;
            }
        };
        postRepository.AddPost(servicelistener, post);
    }

    private Boolean attemp_add_post() {
        edt_post_title.setError(null);
        String post_title = edt_post_title.getText().toString();
        View focuseView = null;
        Boolean cancle = false;
        if (post_title.length() == 0 || post_title.equals("") || post_title == null) {
            edt_post_title.setError("فیلد عنوان نمی تواند خالی باشد");
            cancle = true;
            focuseView = edt_post_title;
        }
        if (cancle) {
            focuseView.requestFocus();
        }
        return cancle;
    }
}
