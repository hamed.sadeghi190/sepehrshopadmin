package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sepehrcc.apps.sepehradmin.R;
import com.squareup.picasso.Picasso;


public class ImageGalleryFragment extends Fragment {
    String ImageUrl = "";

    public ImageGalleryFragment() {
        // Required empty public constructor
    }


    public static ImageGalleryFragment newInstance(String ImageUrl) {
        ImageGalleryFragment fragment = new ImageGalleryFragment();
        Bundle args = new Bundle();
        args.putString("LINK", ImageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageUrl = getArguments().getString("LINK");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_gallery, container, false);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Picasso.with(getContext()).load(ImageUrl).centerInside().resize(size.x, size.y)
                .into((ImageView) view.findViewById(R.id.image));

        return view;
    }


}
