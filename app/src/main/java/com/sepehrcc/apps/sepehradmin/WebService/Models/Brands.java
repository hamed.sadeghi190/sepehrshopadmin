package com.sepehrcc.apps.sepehradmin.WebService.Models;

/**
 * Created by Administrator on 14/05/2018.
 */

public class Brands {
    String title;
    int count ;
    int image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
