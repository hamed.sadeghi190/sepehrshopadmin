package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.PaymentMethodAdapters;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.PostMethodAdapters;
import com.sepehrcc.apps.sepehradmin.Models.PaymentModel;
import com.sepehrcc.apps.sepehradmin.Models.PostModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Posts;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.BankRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.PostRepository;
import com.thefinestartist.finestwebview.FinestWebView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.http.POST;

public class PostMethodsActivity extends AppCompatActivity implements PostMethodAdapters.ListInterface {
    @BindView(R.id.btn_send)
    TextView btn_send;
    @BindView(R.id.lbl_post_title)
    TextView lbl_post_title;
    @BindView(R.id.btn_Back)
    TextView btn_Back;
    @BindView(R.id.list_post_methods)
    RecyclerView list_post_methods;
    @BindView(R.id.lin_post_question)
    LinearLayout lin_post_question;
    @BindView(R.id.img_info)
    ImageView img_info;
    @BindView(R.id.lbl_post_q)
    TextView lbl_post_q;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.fab_adding_post)
    FloatingActionButton fab_adding_post;
    @BindView(R.id.loadingBar)
    ProgressBar loadingBar;
    @BindView(R.id.lbl_no_post_method)
    TextView lbl_no_post_method;

    PostMethodAdapters postMethodAdapters;
    ArrayList<Posts> post_methods = new ArrayList<Posts>();
    WebServicelistener servicelistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_methods);
        ButterKnife.bind(this);
        initialView();
        BindViewControl();
        getPostMehods();
    }

    private void initialView() {
        btn_send.setTypeface(Fonts.VazirBoldFD);
        lbl_post_title.setTypeface(Fonts.VazirBoldFD);
        btn_Back.setTypeface(Fonts.VazirBoldFD);
        lbl_post_q.setTypeface(Fonts.VazirLight);
        lbl_no_post_method.setTypeface(Fonts.VazirLight);
    }

    private void BindViewControl() {
        lbl_post_q.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(PostMethodsActivity.this).show("http://help.sepehrcc.com/article/postapp");
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin_post_question.setVisibility(View.GONE);
            }
        });

        fab_adding_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(PostMethodsActivity.this, AddPostMethodActivity.class);
                startActivity(intent);
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditList();
            }
        });
        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void fill_list() {

        postMethodAdapters = new PostMethodAdapters(post_methods, PostMethodsActivity.this);
        list_post_methods.setAdapter(postMethodAdapters);
        list_post_methods.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void getPostMehods() {
        post_methods.clear();
        fill_list();
        loadingBar.setVisibility(View.VISIBLE);
        PostRepository postRepository = new PostRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                loadingBar.setVisibility(View.GONE);
                lbl_no_post_method.setVisibility(View.GONE);
                post_methods = (ArrayList<Posts>) object;
                fill_list();
            }

            @Override
            public void OnFail(Object object) {
                loadingBar.setVisibility(View.GONE);
                lbl_no_post_method.setVisibility(View.VISIBLE);
            }
        };
        postRepository.GetPostss(servicelistener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPostMehods();
    }

    @Override
    public void onListUpdate(List<Posts> list) {
        post_methods = (ArrayList<Posts>) list;
    }

    private void EditList() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(false);
        pDialog.show();
        PostRepository postRepository = new PostRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                pDialog.hide();
                Intent intent = new Intent(PostMethodsActivity.this, SuccessfullyActivity.class);
                intent.putExtra("successfully_message", "تغییرات در روش های ارسال با موفقیت انجام شد");
                startActivity(intent);
                finish();
            }
            @Override
            public void OnFail(Object object) {

            }
        };
        postRepository.MultiEdit(servicelistener, post_methods);
    }
}
