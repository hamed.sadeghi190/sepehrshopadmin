package com.sepehrcc.apps.sepehradmin.GoogleModel;

import java.util.ArrayList;

/**
 * Created by Administrator on 30/05/2018.
 */

public class object {
    String kind;
    url url =new url();
    queries queries=new queries();
   context context=new context();
    searchInformation searchInformation=new searchInformation();
    ArrayList<items> items=new ArrayList<items>();

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public com.sepehrcc.apps.sepehradmin.GoogleModel.url getUrl() {
        return url;
    }

    public void setUrl(com.sepehrcc.apps.sepehradmin.GoogleModel.url url) {
        this.url = url;
    }

    public com.sepehrcc.apps.sepehradmin.GoogleModel.queries getQueries() {
        return queries;
    }

    public void setQueries(com.sepehrcc.apps.sepehradmin.GoogleModel.queries queries) {
        this.queries = queries;
    }

    public com.sepehrcc.apps.sepehradmin.GoogleModel.context getContext() {
        return context;
    }

    public void setContext(com.sepehrcc.apps.sepehradmin.GoogleModel.context context) {
        this.context = context;
    }

    public com.sepehrcc.apps.sepehradmin.GoogleModel.searchInformation getSearchInformation() {
        return searchInformation;
    }

    public void setSearchInformation(com.sepehrcc.apps.sepehradmin.GoogleModel.searchInformation searchInformation) {
        this.searchInformation = searchInformation;
    }

    public ArrayList<com.sepehrcc.apps.sepehradmin.GoogleModel.items> getItems() {
        return items;
    }

    public void setItems(ArrayList<com.sepehrcc.apps.sepehradmin.GoogleModel.items> items) {
        this.items = items;
    }
}
