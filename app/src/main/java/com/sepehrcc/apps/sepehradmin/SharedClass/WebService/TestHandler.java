package com.sepehrcc.apps.sepehradmin.SharedClass.WebService;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Administrator on 08/04/2018.
 */

public class TestHandler {

    private static retrofit.Retrofit retrofit2=new retrofit.Retrofit.Builder()
            .baseUrl("192.168.1.5:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public interface get_sms {
        @GET("/api/authentication.ashx?do=sendcode&phonenumber={phonenumber}") // specify the sub url for the base url
        Call<String> get_message(@Path("phonenumber") String phonenumber); // will give you the json data
    }
}
