package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SubGroupsActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Target;
import java.util.ArrayList;

public class Categoray_list_Adapter extends RecyclerView.Adapter<View_Holder_Category> {


    private ArrayList<Group> list;
    private Context context;
    private boolean IsForResualt;

    public Categoray_list_Adapter(ArrayList<Group> list, Context context, boolean IsForResualt) {
        this.list = list;
        this.context = context;
        this.IsForResualt = IsForResualt;
    }

    @Override
    public View_Holder_Category onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_categoray_row_themplate, parent, false);
        return new View_Holder_Category(view);
    }

    public static void imageDownload(Context ctx, final View_Holder_Category holder, final int GID, final String url) {
        com.squareup.picasso.Target target = new com.squareup.picasso.Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] paths = url.split("/");
                        String ImgName = paths[paths.length - 1];
                        File file = new File(G.GroupImages + "/gid" + GID + ImgName);
                        try {
                            file.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                            ostream.flush();
                            ostream.close();
                            holder.imageView.setImageBitmap(bitmap);

                        } catch (Exception ignored) {

                        }
                    }
                }).start();




            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                holder.imageView.setImageResource(R.drawable.ic_no_image);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                holder.imageView.setImageResource(R.drawable.ic_no_photos);
            }
        };

        Picasso.with(ctx).load(G.PhotoUrl + "/" + url).into(target);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final View_Holder_Category holder, @SuppressLint("RecyclerView") final int position) {
        Group citem = list.get(position);
        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(citem.GetName());
        holder.count.setText(citem.GetKalaCount() + G.context.getString(R.string.product));
        Picasso.with(context)
                .load(G.PhotoUrl + citem.GetImage())
                .placeholder(R.drawable.ic_loading)
                .error(R.drawable.img_holder)
                .into(holder.imageView);
//        if (citem.GetImage() != null) {
//            String[] paths = citem.GetImage().split("/");
//            String ImgName = paths[paths.length - 1];
//            File file = new File(G.GroupImages + "/gid" + citem.GetID() + ImgName);
//            if (file.exists()) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                holder.imageView.setImageBitmap(myBitmap);
//            } else {
//                imageDownload(context, holder, citem.GetID(), citem.GetImage());
//            }
//        }
        holder.product_list_ripple1.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                DoClick();
            }

            private void DoClick() {
                if (IsForResualt) {
                    Gson gson = new Gson();
                    Group result = list.get(position);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", gson.toJson(result));
                    G.CurrentActivity.setResult(Activity.RESULT_OK, returnIntent);
                    G.CurrentActivity.finish();
                } else {
                    Intent IntentClient = new Intent(context, SubGroupsActivity.class);
                    Gson gson = new Gson();
                    String jsonstring = gson.toJson(list.get(position));
                    IntentClient.putExtra("groupname", list.get(position).GetName());
                    IntentClient.putExtra("groupid", list.get(position).GetID());
                    IntentClient.putExtra("group", jsonstring);
                    context.startActivity(IntentClient);
                }
            }
        });
        holder.product_list_ripple2.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                DoClick();
            }

            private void DoClick() {
                if (IsForResualt) {
                    Gson gson = new Gson();
                    Group result = list.get(position);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", gson.toJson(result));
                    G.CurrentActivity.setResult(Activity.RESULT_OK, returnIntent);
                    G.CurrentActivity.finish();
                } else {
                    Intent IntentClient = new Intent(context, SubGroupsActivity.class);
                    Gson gson = new Gson();
                    String jsonstring = gson.toJson(list.get(position));
                    IntentClient.putExtra("groupname", list.get(position).GetName());
                    IntentClient.putExtra("groupid", list.get(position).GetID());
                    IntentClient.putExtra("group", jsonstring);
                    context.startActivity(IntentClient);
                }
            }
        });
        holder.product_list_ripple3.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent IntentClient = new Intent(context, SubGroupsActivity.class);
                Gson gson = new Gson();
                String jsonstring = gson.toJson(list.get(position));
                IntentClient.putExtra("groupname", list.get(position).GetName());
                IntentClient.putExtra("groupid", list.get(position).GetID());
                IntentClient.putExtra("group", jsonstring);
                if (IsForResualt) {
                    IntentClient.putExtra("IsForResualt", true);
                    G.CurrentActivity.startActivityForResult(IntentClient, 2);
                } else {
                    context.startActivity(IntentClient);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Group data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Group data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }
}

