package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;

/**
 * Created by Hamed on 22/11/2017.
 */

public class AddResponse extends ResponseBase {
    @SerializedName("id")
    public int id;

    public AddResponse(int Status, String Message, int id) {
        super(Status, Message);
        this.id = id;

    }

    public AddResponse() {
    }
}
