package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.OrderDetailActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.OrderRepository;

import java.util.Collections;
import java.util.List;

public class All_Order_list_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Order> list = Collections.emptyList();
    Context context;

    public All_Order_list_Adapter(List<Order> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v;
        View_Holder_Order holder;
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_date, parent, false);
                return new ViewHolderDate(v);

            case 1:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_order_row_themplate, parent, false);
                return new View_Holder_Order(v);
        }
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_order_row_themplate, parent, false);
        holder = new View_Holder_Order(v);
        return holder;

    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position).getType() == 0) {
            return 1;
        } else if(list.get(position).getType() == 1) {
            return 0;
        }
        return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        View_Holder_Order viewHolder;
        ViewHolderDate viewHolderDate;
        switch (holder.getItemViewType()) {
            case 0:
                viewHolderDate = (ViewHolderDate) holder;
                viewHolderDate.lbl_date.setText(list.get(position).GetOrdrCode());
                break;
            case 1:
                viewHolder = (View_Holder_Order) holder;
                viewHolder.name.setText(list.get(position).GetOrdrUname());
                viewHolder.ID.setText("# " + list.get(position).GetOrdrID() + "/" + list.get(position).GetOrdrCode());
                viewHolder.price.setText(list.get(position).GetOrdrCost() + "");
                if (list.get(position).GetOrdrPayed() > 0) {
                   // viewHolder.img_paid.setVisibility(View.VISIBLE);
                    viewHolder.lbl_payed.setVisibility(View.VISIBLE);
                } else {
                   // viewHolder.img_paid.setVisibility(View.INVISIBLE);
                    viewHolder.lbl_payed.setVisibility(View.INVISIBLE);
                }
                switch (list.get(position).GetOrdrState()) {
                    case 1:
                        viewHolder.lbl_order_status.setText("جدید");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_new_background));
                        break;
                    case 2:
                        viewHolder.lbl_order_status.setText("تایید شده");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_verify_background));
                        break;
                    case 3:
                        viewHolder.lbl_order_status.setText("آماده ارسال");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                        break;
                    case 4:
                        viewHolder.lbl_order_status.setText("ارسال شده");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                        break;
                    case 5:
                        viewHolder.lbl_order_status.setText("کامل شده");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_complete_background));
                        break;
                    case 6:
                        viewHolder.lbl_order_status.setText("معلق");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_archive_background));


                        break;
                }
                viewHolder.rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                    @Override
                    public void onComplete(RippleView rippleView) {
                        Gson gson = new Gson();
                        String order_json = gson.toJson(list.get(position));
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("order_object", order_json);
                        intent.putExtra("is_paid",list.get(position).GetOrdrPayed());
                        context.startActivity(intent);
                    }
                });
                break;
            case 2:
                viewHolder = (View_Holder_Order) holder;
                viewHolder.name.setText(list.get(position).GetOrdrUname());
                viewHolder.ID.setText("# " + list.get(position).GetOrdrID() + "/" + list.get(position).GetOrdrCode());
                viewHolder.price.setText(list.get(position).GetOrdrCost() + "");
                if (list.get(position).GetOrdrPayed() > 0) {
                    viewHolder.img_paid.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.img_paid.setVisibility(View.INVISIBLE);
                }
                switch (list.get(position).GetOrdrState()) {
                    case 1:
                        viewHolder.lbl_order_status.setText("جدید");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_new_background));
                        break;
                    case 2:
                        viewHolder.lbl_order_status.setText("تایید شده");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_verify_background));
                        break;
                    case 3:
                        viewHolder.lbl_order_status.setText("آماده ارسال");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                        break;
                    case 4:
                        viewHolder.lbl_order_status.setText("ارسال شده");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_ready_to_sent_background));
                        break;
                    case 5:
                        viewHolder.lbl_order_status.setText("کامل شده");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_complete_background));
                        break;
                    case 6:
                        viewHolder.lbl_order_status.setText("معلق");
                        viewHolder.lbl_order_status.setBackground(context.getResources().getDrawable(R.drawable.sty_order_status_archive_background));


                        break;
                }
                viewHolder.rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                    @Override
                    public void onComplete(RippleView rippleView) {
                        Gson gson = new Gson();
                        String order_json = gson.toJson(list.get(position));
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("order_object", order_json);
                        intent.putExtra("is_paid",list.get(position).GetOrdrPayed());
                        context.startActivity(intent);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, Order order) {
        list.add(position, order);
        notifyItemInserted(position);
    }

    public void remove(Order order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolderDate extends RecyclerView.ViewHolder {
        TextView lbl_date;

        public ViewHolderDate(View itemView) {
            super(itemView);
            lbl_date = (TextView) itemView.findViewById(R.id.lbl_date);
            lbl_date.setTypeface(Fonts.VazirFD);
        }
    }
}

