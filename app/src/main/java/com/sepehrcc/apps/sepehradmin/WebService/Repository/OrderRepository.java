package com.sepehrcc.apps.sepehradmin.WebService.Repository;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ChangeOrder;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderDetials;
import com.sepehrcc.apps.sepehradmin.WebService.Models.UserInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class OrderRepository {

    public void GetAllOrders(final WebServicelistener listener, int offset,int limit) {

        final ArrayList<Order> Resulat = new ArrayList<Order>();
        try {
            final Call<feedback> CallServer = WebServiceHandler.OrderClient.GetAllOrders(G.ApiToken, offset,limit);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                feedback feedback= response.body();
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<ArrayList<Order>>() {
                                }.getType();
                                ArrayList<Order> orders=new ArrayList<Order>();
                                orders=gson.fromJson(feedback.value,collectionType);
                                listener.OnComplete(orders);
                            } else {
                                ArrayList<Order> orders = new ArrayList<Order>();
                                listener.OnComplete(orders);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void GetOrders(final WebServicelistener listener,int type, int offset,int limit) {

        final ArrayList<Order> Resulat = new ArrayList<Order>();
        try {
            final Call<feedback> CallServer = WebServiceHandler.OrderClient.GetOrders(G.ApiToken,type, offset,limit);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                feedback feedback= response.body();
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<ArrayList<Order>>() {
                                }.getType();
                                ArrayList<Order> orders=new ArrayList<Order>();
                                orders=gson.fromJson(feedback.value,collectionType);
                                listener.OnComplete(orders);
                            } else {
                                ArrayList<Order> orders = new ArrayList<Order>();
                                listener.OnComplete(orders);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void GetOrderDetail(final WebServicelistener listener,int id) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.OrderClient.GetDetailOrder(G.ApiToken,id);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                feedback feedback = response.body();
                                Gson gson=new Gson();
                                OrderDetials order=gson.fromJson(feedback.value,OrderDetials.class);
                                listener.OnComplete(order);
                            } else {
                                OrderDetials order = new OrderDetials();
                                listener.OnComplete(order);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void ChangeOrderStatus(final WebServicelistener listener, ChangeOrder changeOrder) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.OrderClient.ChangeOrderStatus(G.ApiToken,changeOrder);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {

                                listener.OnComplete(response.body());
                            } else {

                                listener.OnComplete(response.body());
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void ChangeUserInfo(final WebServicelistener listener, UserInfo userInfo) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.OrderClient.ChangeUserInfo(G.ApiToken,userInfo);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                listener.OnComplete(response.body());
                            } else {
                                listener.OnComplete(response.body());
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void SearchOrders(final WebServicelistener listener, int offset,int limit,String key) {

        try {
            final Call<feedback> CallServer = WebServiceHandler.OrderClient.SearchOrders(G.ApiToken, offset,limit,key);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                feedback feedback=response.body();
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<ArrayList<Order>>() {
                                }.getType();
                                ArrayList<Order> orders = gson.fromJson(feedback.value,collectionType);
                                listener.OnComplete(orders);
                            } else {
                                ArrayList<Order> orders = new ArrayList<Order>();
                                listener.OnComplete(orders);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
}
