package com.sepehrcc.apps.sepehradmin.GoogleModel;

/**
 * Created by Administrator on 30/05/2018.
 */

public class url {
    String type;
    String template;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
