package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;



public class KindGroup {
    //no change 0
    //delete 1
    //add 2
    @SerializedName("kGroupId")
    private int _kGroupId;
    public int GetGroupID() { return _kGroupId; }
    public KindGroup SetGroupID(int value) {_kGroupId = value;return this;}

    @SerializedName("KID")
    private int _KID;
    public int GetID() { return _KID; }
    public KindGroup SetID(int value) {_KID = value;return this;}

    @SerializedName("kTitle")
    private String _kTitle;
    public String GetTitle() { return _kTitle; }
    public KindGroup SetTitle(String value) {_kTitle = value;return this;}

    @SerializedName("kStatus")
    private int _kStatus;
    public int GetStatus() { return _kStatus; }
    public KindGroup SetStatus(int value) {_kStatus = value;return this;}

    private boolean Checked;
    public boolean GetChecked() { return Checked; }
    public KindGroup SetChecked(boolean value) {Checked = value;return this;}
}
