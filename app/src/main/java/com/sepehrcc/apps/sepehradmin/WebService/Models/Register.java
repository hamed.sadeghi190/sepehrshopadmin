package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed Sadeghi on 28/04/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class Register {
    @SerializedName("siteName")
    public String siteName;
    @SerializedName("subDomain")
    public String subDomain;
    @SerializedName("phoneNumber")
    public String phoneNumber;
    @SerializedName("userName")
    public String userName;
    @SerializedName("code")
    public String code;
    @SerializedName("Fullname")
    public String Fullname;

    public void SetsiteName(String value)
    {
        siteName = value;
    }
    public void SetsubDomain(String value)
    {
        subDomain = value;
    }
    public void SetphoneNumber(String value)
    {
        phoneNumber = value;
    }
    public void SetuserName(String value)
    {
        userName = value;
    }
    public void SetFullname(String value)
    {
        Fullname = value;
    }
    public void SetPin(String value) {
        code = value;
    }
}
