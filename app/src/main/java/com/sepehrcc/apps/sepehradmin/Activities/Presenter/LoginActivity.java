package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.LoginModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.LoginResualt;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.lbl_title_login)
    TextView lbl_title_login;
    @BindView(R.id.edit_store_id)
    TextInputLayout edit_store_id;
    @BindView(R.id.edit_store_name)
    TextInputLayout edit_store_name;
    @BindView(R.id.edit_store_password)
    TextInputLayout edit_store_password;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.lbl_forget_password)
    TextView lbl_forget_password;
    @BindView(R.id.edt_store_id)
    EditText edt_store_id;
    @BindView(R.id.edt_store_name)
    EditText edt_store_name;
    @BindView(R.id.edt_store_password)
    EditText edt_store_password;
    WebServicelistener listener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_login);
        ButterKnife.bind(this);
        G.CurrentActivity = this;
        initialView();
        bindControlEvents();
       // settestaccount();
    }

    private void settestaccount() {
        edt_store_id.setText("rangirangishop");
        edt_store_name.setText("fati_afshari@yahoo.com");
        edt_store_password.setText("123456");
    }

    private void initialView() {
        lbl_title_login.setTypeface(Fonts.VazirBold);
        edit_store_id.setTypeface(Fonts.VazirBold);
        edit_store_name.setTypeface(Fonts.VazirBold);
        edit_store_password.setTypeface(Fonts.VazirBold);
        btn_login.setTypeface(Fonts.VazirBold);
        lbl_forget_password.setTypeface(Fonts.VazirBold);
        edt_store_id.setTypeface(Fonts.VazirBold);
        edt_store_name.setTypeface(Fonts.VazirBold);
        edt_store_password.setTypeface(Fonts.VazirBold);
    }

    private void bindControlEvents() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void onClick(View view) {
                if (attempLogin()) {
                    login();
                }
            }
        });
    }

    private void login() {
        final SweetAlertDialog alert = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        alert.setTitleText("بررسی اطلاعات");

        LoginModel loginModel = new LoginModel();
        if (edt_store_id.getText().toString().contains(".")) {
            loginModel.SetUrl(edt_store_id.getText().toString().toLowerCase());
        } else if (!edt_store_id.getText().toString().contains(".")) {
            loginModel.SetUrl(edt_store_id.getText().toString().toLowerCase() + ".sepehrcc.com");
        }
        loginModel.SetUserName(edt_store_name.getText().toString());
        loginModel.SetPassword(edt_store_password.getText().toString());
        listener = new WebServicelistener() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void OnComplete(Object object) {

                feedback fb = (feedback) object;
                if(fb.Status==1) {
                    LoginResualt resualt = new Gson().fromJson(fb.value,LoginResualt.class);
                    alert.dismiss();
                    G.ApiToken = "Bearer " + resualt.GetToken();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Token", G.ApiToken).commit();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("TokenDate", resualt.GetValidTo()).commit();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("SubDomain", edt_store_id.getText().toString()).commit();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("UserFullName", resualt.GetUserFullName()).commit();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putLong("Login_time",System.currentTimeMillis()).commit();
                    G.BaseUrl = "http://www." + edt_store_id.getText().toString() + ".sepehrcc.com/adminapi/";
                    G.PhotoUrl = "http://www." + edt_store_id.getText().toString() + ".sepehrcc.com";
                    G.Seturls();
                    Intent intent = new Intent(LoginActivity.this, LoadingSplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void OnFail(Object object) {
                Log.i("error", object.toString());
                ResponseBase response = (ResponseBase) object;
                alert.hide();
                if (response.Status == 400) {
                    new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText(" اطلاعات وارد شده صحیح نمی باشد .")
                            .show();
                } else if (response.Status == 500) {
                    new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText("متاسفانه ارتباط با سرور برقرار نشد.")
                            .show();
                }
            }
        };
        SecurityRepository securityRepository = new SecurityRepository();
        alert.show();
        securityRepository.Login(listener, loginModel);
    }
    private boolean passwordValidation() {
        return !(edt_store_password.getText().toString().equals("")) && edt_store_password.getText().toString().length() >= 6 && edt_store_password.getText().toString().length() <= 25 && !(edt_store_password.getText().toString().contains(" "));
    }
    private boolean idValidator() {
        return !(edt_store_id.getText().toString().equals("")) && edt_store_id.getText().toString().toLowerCase().matches("[a-z]+[a-zA-Z0-9._-]*") && !(edt_store_id.getText().toString().contains(" "));
    }
    private boolean usernameValidator() {
        return !(edt_store_name.getText().toString().equals("")) && edt_store_name.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.[a-z]+") && !(edt_store_name.getText().toString().contains(" "));
    }
    private boolean attempLogin() {
        boolean cancle = true;
        edt_store_id.setError(null);
        edt_store_name.setError(null);
        edt_store_password.setError(null);
        if (!idValidator()) {
            edt_store_id.setError("آدرس باید حتما با حروف کوچک شروع شود و فاقد فاصله و حروف اضافه باشد");
            cancle = false;
            edt_store_id.requestFocus();
        }
        if (!usernameValidator()) {
            edt_store_name.setError("لطفا یک ایمیل صحیح وارد کنید");
            cancle = false;
            edt_store_name.requestFocus();
        }

        if (!passwordValidation()) {
            edt_store_password.setError("رمز عبور باید حداقل 6 و حداکثر 18 کاراکتر باشد و فاقد فاصله باشد");
            cancle = false;
            edt_store_password.requestFocus();
        }
        return cancle;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(LoginActivity.this, SplashScreenActivity.class);
        startActivity(i);
        finish();
    }

}
