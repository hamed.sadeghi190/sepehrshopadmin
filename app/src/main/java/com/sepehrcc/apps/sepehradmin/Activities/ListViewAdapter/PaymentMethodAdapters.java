package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import java.util.Collections;
import java.util.List;


public class PaymentMethodAdapters extends RecyclerView.Adapter<PaymentMethodAdapters.GroupViewHolder> {
    List<Bank> list = Collections.emptyList();
    Context context;
    int pos;
    GroupViewHolder viewHolder;

    public PaymentMethodAdapters(List<Bank> list, Context context) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lst_payment_method_row, viewGroup, false);
        GroupViewHolder pay = new GroupViewHolder(v);
        return pay;
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {
        viewHolder = holder;
        pos = position;
        viewHolder.lbl_payment_name.setText(list.get(pos).GetpName());
        switch (list.get(pos).GetpName())
        {
            case "بانک ملی":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_melli);
                break;
            case "بانک ملت":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_mellat);
                break;
            case "MasterCard":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_mastercard);
                break;
            case "اعتبار":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_credit);
                break;
            case "شرکت کارت اعتباری ایران کیش":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_kish);
                break;
            case "payPal":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_paypal);
                break;
            case "زرین پال":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_zarinpal);
                break;
            case "پارسیان":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_parsian);
                break;
            case "پرداخت نقدی":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_payonlocation);
                break;
            case "بررسی":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_cheque);
                break;
            case "حواله":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_havale);
                break;
            case "سامان":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_saman);
                break;
            case "پرداخت به پستچی":
                viewHolder.img_payment_item.setImageResource(R.drawable.ic_post);
                break;
        }
        if (list.get(position).GetpEnable()) {
            viewHolder.switch_payment.setChecked(true);
            list.get(pos).SetpEnable(true);
            viewHolder.lin_civil_payment.setBackgroundColor(context.getResources().getColor(R.color.white));

        } else {
            viewHolder.switch_payment.setChecked(false);
            list.get(pos).SetpEnable(false);
            viewHolder.lin_civil_payment.setBackgroundColor(context.getResources().getColor(R.color.grey_hex_ce));
        }

        holder.ripple_payment.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                ListInterface listinterface = (ListInterface) context;
                if (list.get(position).GetpEnable()) {
                    holder.switch_payment.setChecked(false);
                    list.get(position).SetpEnable(false);
                    listinterface.onListUpdate(list);
                } else if (!(list.get(position).GetpEnable())) {
                    holder.switch_payment.setChecked(true);
                    list.get(position).SetpEnable(true);
                    listinterface.onListUpdate(list);
                }
            }
        });
//        viewHolder.switch_payment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                ListInterface listinterface = (ListInterface) context;
//                if (b) {
//                    list.get(pos).SetpEnable(true);
//                    listinterface.onListUpdate(list);
//                } else if ((!b)) {
//                    list.get(pos).SetpEnable(false);
//                    listinterface.onListUpdate(list);
//                }
//            }
//        });
        holder.switch_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListInterface listinterface = (ListInterface) context;
                if (list.get(position).GetpEnable()) {
                    holder.switch_payment.setChecked(false);
                    list.get(position).SetpEnable(false);
                    listinterface.onListUpdate(list);
                } else if (!(list.get(position).GetpEnable())) {
                    holder.switch_payment.setChecked(true);
                    list.get(position).SetpEnable(true);
                    listinterface.onListUpdate(list);
                }
            }
        });
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        LinearLayout lin_item_payment;
        CardView crd_payment;
        RippleView ripple_payment;
        LinearLayout lin_civil_payment;
        ImageView img_payment_item;
        TextView lbl_payment_name;
        Switch switch_payment;

        GroupViewHolder(View itemView) {
            super(itemView);
            lin_item_payment = (LinearLayout) itemView.findViewById(R.id.lin_item_payment);
            crd_payment = (CardView) itemView.findViewById(R.id.crd_payment);
            ripple_payment = (RippleView) itemView.findViewById(R.id.ripple_payment);
            lin_civil_payment = (LinearLayout) itemView.findViewById(R.id.lin_civil_payment);
            img_payment_item = (ImageView) itemView.findViewById(R.id.img_payment_item);
            lbl_payment_name = (TextView) itemView.findViewById(R.id.lbl_payment_name);
            switch_payment = (Switch) itemView.findViewById(R.id.switch_payment);
            lbl_payment_name.setTypeface(Fonts.VazirLight);


        }
    }


    public interface ListInterface {
        public void onListUpdate(List<Bank> list);
    }
}

