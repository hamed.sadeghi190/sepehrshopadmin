package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Update;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.ChechTypeAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.ProductGalleryAdapter;
import com.sepehrcc.apps.sepehradmin.DbModels.KalaDB;
import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DomainModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KalaTypes;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.DomainRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thefinestartist.finestwebview.FinestWebView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddProductActivity extends AppCompatActivity implements IPickResult {
    @BindView(R.id.txt_add_product_title)
    TextView txt_add_product_title;
    @BindView(R.id.btn_Back)
    TextView BtnBack;
    @BindView(R.id.btn_send)
    TextView BtnSend;
    @BindView(R.id.lbl_toman)
    TextView lblToman;
    @BindView(R.id.lbl_take_photo_advise)
    TextView lbl_take_photo_advise;
    @BindView(R.id.Txt_Prodcut_description)
    EditText Txt_Prodcut_description;
    @BindView(R.id.edt_Prodcut_Name)
    EditText edt_Prodcut_Name;
    @BindView(R.id.Txt_Prodcut_price)
    EditText Txt_Prodcut_price;
    @BindView(R.id.edt_Prodcut_group)
    EditText edt_Prodcut_group;
    @BindView(R.id.Txt_Prodcut_barand)
    EditText Txt_Prodcut_barand;
    @BindView(R.id.txt_ly_edt_Prodcut_Name)
    TextInputLayout txt_ly_edt_Prodcut_Name;
    @BindView(R.id.txt_ly_Txt_Prodcut_price)
    TextInputLayout txt_ly_Txt_Prodcut_price;
    @BindView(R.id.txt_ly_edt_Prodcut_group)
    TextInputLayout txt_ly_edt_Prodcut_group;
    @BindView(R.id.txt_ly_Txt_Prodcut_barand)
    TextInputLayout txt_ly_Txt_Prodcut_barand;
    @BindView(R.id.txt_ly_Txt_Prodcut_desc)
    TextInputLayout txt_ly_Txt_Prodcut_desc;
    @BindView(R.id.lbl_kala_pic)
    TextView lbl_kala_pic;
    @BindView(R.id.lbl_select)
    TextView lbl_select;
    @BindView(R.id.lbl_select2)
    TextView lbl_select2;
    @BindView(R.id.lbl_types)
    TextView lbl_types;
    @BindView(R.id.add_new_type)
    TextView add_new_type;
    @BindView(R.id.product_gallery_list)
    RecyclerView product_gallery_list;
    @BindView(R.id.check_type_list)
    RecyclerView check_type_list;
    @BindView(R.id.lbl_tip)
    TextView lbl_tip;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.lin_post_question)
    LinearLayout lin_post_question;

    ProductGalleryAdapter adpter;
    List<ProductGallery> Photodata;
    ChechTypeAdapter typeAdapter;
    List<KindGroup> chech_data = new ArrayList<KindGroup>();
    boolean isopened = false;
    SweetAlertDialog pDialog;
    Group GModel = null;
    Kala kalaObj = new Kala();
    Kala EditObject;
    String ImageUrl;
    WebServicelistener imagelisitner;
    WebServicelistener servicelistener;
    int return_price;
    String ImageName = "";
    PickSetup setup = new PickSetup();
    Mark kala_mark = new Mark();
    SweetAlertDialog googledialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_add_product);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        G.CurrentActivity = this;
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getBoolean("IsEdit", false)) {
                Gson gson = new Gson();
                EditObject = gson.fromJson(extras.getString("Product"), Kala.class);
                LoadInfo(EditObject);
                init2();
                InitialView();
                BindControlsEvents();
            }
        } else {
            init2();
            InitialView();
            BindControlsEvents();
        }
        setup
                .setTitle("انتخاب")
                .setTitleColor(Color.BLACK)
                .setBackgroundColor(Color.WHITE)
                .setProgressText("صبر کنید...")
                .setProgressTextColor(Color.BLACK)
                .setCancelText("بستن")
                .setCancelTextColor(Color.BLACK)
                .setButtonTextColor(Color.BLACK)
                .setMaxSize(500)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText("دوربین")
                .setGalleryButtonText("گالری")
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setSystemDialog(false)
                .setCameraToPictures(false)
                .setGalleryIcon(R.drawable.ic_gallery_picker)
                .setCameraIcon(R.drawable.ic_photo_camera);

    }

    private void LoadInfo(Kala value) {
        edt_Prodcut_Name.setText(value.GetName());
        edt_Prodcut_group.setText(value.GetGroupName());
        Txt_Prodcut_price.setText(String.valueOf(value.GetPrice()));
        Txt_Prodcut_description.setText(value.GetDescription());
    }

    private void init2() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        googledialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        imagelisitner = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                //   loadImagePicker();
                select_google_phone();
            }

            @Override
            public void OnFail(Object object) {
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        isopened = false;
    }

    private void InitialView() {
        txt_add_product_title.setTypeface(Fonts.VazirBold);
        BtnBack.setTypeface(Fonts.VazirBold);
        BtnSend.setTypeface(Fonts.VazirBold);
        lblToman.setTypeface(Fonts.VazirBold);
        lbl_take_photo_advise.setTypeface(Fonts.VazirBold);
        Txt_Prodcut_description.setTypeface(Fonts.VazirBold);
        edt_Prodcut_Name.setTypeface(Fonts.VazirBold);
        Txt_Prodcut_price.setTypeface(Fonts.VazirBold);
        edt_Prodcut_group.setTypeface(Fonts.VazirBold);
        Txt_Prodcut_barand.setTypeface(Fonts.VazirBold);
        txt_ly_edt_Prodcut_Name.setTypeface(Fonts.VazirBold);
        txt_ly_Txt_Prodcut_price.setTypeface(Fonts.VazirBold);
        txt_ly_edt_Prodcut_group.setTypeface(Fonts.VazirBold);
        txt_ly_Txt_Prodcut_barand.setTypeface(Fonts.VazirBold);
        txt_ly_Txt_Prodcut_desc.setTypeface(Fonts.VazirBold);
        lbl_kala_pic.setTypeface(Fonts.VazirBold);
        lbl_select.setTypeface(Fonts.VazirBold);
        lbl_select2.setTypeface(Fonts.VazirBold);
        lbl_types.setTypeface(Fonts.VazirBold);
        add_new_type.setTypeface(Fonts.VazirBold);
        lbl_tip.setTypeface(Fonts.VazirBold);
        Photodata = fill_with_data();
        fill_list(Photodata);
        //chech_data=fill_with_data_type();
        //fill_list_type(chech_data);
    }


    @SuppressLint("ClickableViewAccessibility")
    private void BindControlsEvents() {
        lbl_select2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Intent = new Intent(G.CurrentActivity, BrandsActivity.class);
                startActivityForResult(Intent, 11);
            }
        });
        Txt_Prodcut_description.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.Txt_Prodcut_description) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        BtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                G.desc_save = "";
                Intent intent = new Intent(G.CurrentActivity, ProductListActivity.class);
                startActivity(intent);
                finish();
            }
        });

        lbl_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (G.appData.shopInfo.GetGroupCount() != 0) {
                    Intent IntentClient = new Intent(G.CurrentActivity, CategoryListActivity.class);
                    IntentClient.putExtra("IsForResualt", true);
                    startActivityForResult(IntentClient, 2);
                } else if (G.appData.shopInfo.GetGroupCount() == 0) {
                    Intent intent = new Intent(G.CurrentActivity, FirstGroupActivity.class);
                    startActivity(intent);
                }
            }
        });
        edt_Prodcut_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (G.appData.shopInfo.GetGroupCount() != 0) {
                    Intent IntentClient = new Intent(G.CurrentActivity, CategoryListActivity.class);
                    IntentClient.putExtra("IsForResualt", true);
                    startActivityForResult(IntentClient, 2);
                } else if (G.appData.shopInfo.GetGroupCount() == 0) {
                    Intent intent = new Intent(G.CurrentActivity, FirstGroupActivity.class);
                    startActivity(intent);
                }
            }
        });
        BtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!attemp_add_product()) {
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("ارسال ...");
                    pDialog.setCancelable(true);
                    kalaObj.SetName(edt_Prodcut_Name.getText().toString());
                    kalaObj.SetPrice(return_price);
                    G.desc_save = Txt_Prodcut_description.getText().toString();
                    kalaObj.SetDescription(G.desc_save);
                    if (kala_mark != null) {
                        kalaObj.SetMark(kala_mark.GetMarkID() + "");
                    }
                    kalaObj.SetkMarkName(Txt_Prodcut_barand.getText().toString());
                    if (GModel != null) {
                        kalaObj.SetGroup(GModel.GetID());
                        kalaObj.SetGroupName(GModel.GetName());
                    }
                    ArrayList<KalaTypes> Types = new ArrayList<>();
                    for (int x = 0; x < chech_data.size(); x++) {
                        boolean vaziat = chech_data.get(x).GetChecked();
                        if (vaziat)
                            Types.add(new KalaTypes().SetKindID(chech_data.get(x).GetID()));
                    }
                    kalaObj.SetKinds(Types);
                    servicelistener = new WebServicelistener() {
                        @Override
                        public void OnComplete(Object object) {
                            final AddResponse response = (AddResponse) object;
                            if (response != null) {
                                G.desc_save = "";
                                final int responseid = response.id;
                                for (int index = 1; index < Photodata.size(); index++) {
                                    final int finalIndex = index;
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            WebServicelistener lis = new WebServicelistener() {
                                                @Override
                                                public void OnComplete(Object object) {
                                                    if (finalIndex == 1) {
                                                        File file = new File(Photodata.get(finalIndex).image);
                                                        String linkp = "/host/" + G.appData.shopInfo.GetsubDomain() + "/AppProduct/_tmb_img" +
                                                                kalaObj.GetID() + file.getName();
                                                        new Update(KalaDB.class)
                                                                .set("kImage = '" + linkp + "'")
                                                                .where("kId = " + responseid)
                                                                .execute();
                                                    }
                                                }

                                                @Override
                                                public void OnFail(Object object) {
                                                    int b = 0;
                                                }
                                            };
                                            new KalaRepository().UploadPhoto(lis, Photodata.get(finalIndex).image, response.id);
                                        }
                                    });
                                    thread.start();
                                }
                                pDialog.hide();
                                KalaDB newkala = new KalaDB(kalaObj.SetID(response.id));
                                newkala.save();
                                G.appData.shopInfo.SetKalaCount(G.appData.shopInfo.GetKalaCount() + 1);
                                Intent intent = new Intent(G.CurrentActivity, SuccessfullyActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void OnFail(Object object) {
                        }
                    };
                    pDialog.show();
                    Gson g = new Gson();
                    String json = g.toJson(kalaObj);
                    new KalaRepository().Add(servicelistener, kalaObj);
                } else {
                    Toast.makeText(G.CurrentActivity, "لطفا تمام فیلد ها را به درستی تکمیل کنید", Toast.LENGTH_LONG).show();
                }
            }
        });
        add_new_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(G.CurrentActivity, TypesActivity.class);
                ArrayList<String> hhh = new ArrayList<String>();
                for (int j = 0; j < chech_data.size(); j++) {
                    hhh.add(chech_data.get(j).GetTitle());
                }
                i.putStringArrayListExtra("hhh", hhh);
                startActivityForResult(i, 4);
            }
        });

        lbl_take_photo_advise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/post"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(AddProductActivity.this).show("http://help.sepehrcc.com/post");

            }
        });
        lbl_tip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/gallery"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(AddProductActivity.this).show("http://help.sepehrcc.com/article/better_pics");

            }
        });
        Txt_Prodcut_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Txt_Prodcut_price.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    Txt_Prodcut_price.setText(formattedString);
                    Txt_Prodcut_price.setSelection(Txt_Prodcut_price.getText().length());
                    if (!Txt_Prodcut_price.getText().toString().equals("") && Txt_Prodcut_price.getText().toString() != null) {
                        return_price = Integer.parseInt(originalString);
                    } else {
                        return_price = 0;
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                Txt_Prodcut_price.addTextChangedListener(this);
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin_post_question.setVisibility(View.GONE);
            }
        });

    }

    private boolean AllControlsFill() {
        return (!edt_Prodcut_Name.getText().toString().equals("")) && (!Txt_Prodcut_price.getText().toString().equals(""));
    }

    private void loadImagePicker() {
        PickImageDialog.build(setup).setOnPickResult(new IPickResult() {
            @Override
            public void onPickResult(PickResult pickResult) {
                if (pickResult.getError() == null) {
                    save_image(pickResult.getBitmap());
                }
            }
        }).show(AddProductActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            String rich_edit_text_result = data.getStringExtra("rich_edit_text_html");
            Txt_Prodcut_description.setText(rich_edit_text_result);
        } else if (resultCode == 4) {
            chech_data.clear();
            for (int i = 0; i < data.getStringArrayListExtra("typelist").size(); i++) {
                //chech_data.add(new CheckType(data.getStringArrayListExtra("typelist").get(i)));
            }
            fill_list_type();
        } else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                Gson gson = new Gson();
                GModel = gson.fromJson(result, Group.class);
                edt_Prodcut_group.setText(GModel.GetName());
                chech_data.addAll(GModel.GetKinds());
                fill_list_type();
            }
        } else if (resultCode == 10) {
            save_image_croped(G.b);
        }
        if (resultCode == 11) {
            Gson g = new Gson();
            Type collectionType = new TypeToken<Mark>() {
            }.getType();
            String brand_json = data.getStringExtra("result_brand");
            kala_mark = g.fromJson(brand_json, collectionType);
            Txt_Prodcut_barand.setText(kala_mark.GetMarkName());
        }
        if (resultCode == 12) {
            //progress bar
            googledialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            googledialog.setTitleText("در حال دانلود تصویر ...");
            googledialog.setCancelable(true);
            final String link = data.getStringExtra("photo_link");
            try {
                googledialog.show();
                String url = link;
                url = url.replaceAll(" ", "%20");
                Glide.with(getApplicationContext())
                        .load(url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                googledialog.hide();
                                save_image(bitmap);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                googledialog.hide();
                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(AddProductActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("خطا")
                                        .setContentText("تصویر دانلود نشد لطفا بعدا تلاش کنید")
                                        .setConfirmText("بستن")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.hide();
                                            }
                                        })
                                        .show();
                            }
                        });
            } catch (Exception e) {
                googledialog.show();
                String url = link;
                url = url.replaceAll(" ", "%20");
                Glide.with(getApplicationContext())
                        .load(url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                googledialog.hide();
                                save_image(bitmap);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                googledialog.hide();
                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(AddProductActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("خطا")
                                        .setContentText("تصویر دانلود نشد لطفا بعدا تلاش کنید")
                                        .setConfirmText("بستن")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.hide();
                                            }
                                        })
                                        .show();
                            }
                        });
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        G.desc_save = "";
    }

    private void fill_list(List<ProductGallery> data) {
        G.is_prodict_change = false;
        adpter = new ProductGalleryAdapter(data, this, imagelisitner);
        product_gallery_list.setAdapter(adpter);
        product_gallery_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
    }

    public List<ProductGallery> fill_with_data() {
        List<ProductGallery> gallery = new ArrayList<>();
        gallery.add(new ProductGallery(null, true));
        return gallery;
    }

    private void fill_list_type() {
        typeAdapter = new ChechTypeAdapter(chech_data, this);
        check_type_list.setAdapter(typeAdapter);
        check_type_list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            save_image(pickResult.getBitmap());
        }
    }

    public void save_image(Bitmap result) {
        G.b = result;
        Intent i = new Intent(AddProductActivity.this, CropActivity.class);
        i.putExtra("PhotoName", "sepehrproduct.jpg");
        startActivityForResult(i, 10);
    }

    public void save_image_croped(Bitmap result) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut;
        ImageName = "sepehrgroup" + Photodata.size() + ".jpg";
        ImageUrl = path + "/" + ImageName;
        File file = new File(path, ImageName); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        try {
            fOut = new FileOutputStream(file);
            result.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush();
            fOut.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            Photodata.add(new ProductGallery(ImageUrl, false));
            fill_list(Photodata);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Boolean attemp_add_product() {
        edt_Prodcut_Name.setError(null);
        Txt_Prodcut_price.setError(null);
        edt_Prodcut_group.setError(null);
        String product_name = edt_Prodcut_Name.getText().toString();
        int product_price = return_price;
        String product_group = edt_Prodcut_group.getText().toString();
        String product_mark = Txt_Prodcut_barand.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (product_name.equals("") || product_name == null || product_name.length() < 1) {
            edt_Prodcut_Name.setError("فیلد نام کالا نمی تواند خالی باشد");
            focusView = edt_Prodcut_Name;
            cancel = true;
        }
        if (product_group.equals("") || product_group == null || product_group.length() < 1) {
            edt_Prodcut_group.setError("فیلد نام گروه نمی تواند خالی باشد");
            focusView = edt_Prodcut_group;
            cancel = true;
        }
        if (product_mark.equals("") || product_mark == null || product_mark.length() < 1) {
            Txt_Prodcut_barand.setError("فیلد برند کالا نمی تواند خالی باشد");
            focusView = Txt_Prodcut_barand;
            cancel = true;
        }
//        if (Txt_Prodcut_price.getText().toString().length() == 0) {
//            Txt_Prodcut_price.setError("فیلد قیمت نمی تواند خالی باشد");
//            focusView = Txt_Prodcut_price;
//            cancel = true;
//        }
        if (cancel) {
            focusView.requestFocus();
        }
        return cancel;
    }

    private void select_google_phone() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddProductActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.select_google_phone, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        LinearLayout lin_google = (LinearLayout) dialogView.findViewById(R.id.lin_google);
        LinearLayout lin_phone = (LinearLayout) dialogView.findViewById(R.id.lin_phone);
        TextView lbl_google = (TextView) dialogView.findViewById(R.id.lbl_google);
        TextView lbl_phone = (TextView) dialogView.findViewById(R.id.lbl_phone);
        lbl_google.setTypeface(Fonts.VazirBold);
        lbl_phone.setTypeface(Fonts.VazirBold);
        lin_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddProductActivity.this, GoogleSearchActivity.class);
                intent.putExtra("kala_name", edt_Prodcut_Name.getText().toString() + "");
                startActivityForResult(intent, 12);
                b.dismiss();
            }
        });
        lin_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImagePicker();
                b.dismiss();
            }
        });
        b.show();
    }
}
