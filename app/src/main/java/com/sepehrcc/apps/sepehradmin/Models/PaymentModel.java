package com.sepehrcc.apps.sepehradmin.Models;

/**
 * Created by Administrator on 15/04/2018.
 */

public class PaymentModel {
    String name;
    boolean select;
    int image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
