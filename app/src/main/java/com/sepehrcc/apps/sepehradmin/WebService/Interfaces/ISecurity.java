package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.LoginModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.LoginResualt;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Register;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ShopInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;


import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;


public interface ISecurity {
    //this Header is for emphesize on json type is to sent
    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @POST("login")
    Call<feedback> Login(@Body LoginModel model);

    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("Shops/info")
    Call<ShopInfo> shopInfo(@Header("Authorization") String AuthKey);

    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @POST("Shops/sendcode")
    Call<feedback> SendRegisterCode(@Body Register model);
    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("Shops/sendretrycode/{PhoneNumber}")
    Call<feedback> SendRetryCode(@Path("PhoneNumber") String PhoneNumber);


    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @POST("Shops/verifyregister")
    Call<feedback> VerifyRegister(@Body Register model);

    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("login/Phone/{PhoneNumber}")
    Call<feedback> SendLoginSms(@Path("PhoneNumber") String PhoneNumber);

    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("login/PhoneVerify/{PhoneNumber}/{code}")
    Call<feedback> VerifyLogin(@Path("PhoneNumber") String PhoneNumber,@Path("code") String code);
    @Headers({
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("login/DoLogin/{sub}/{phone}/{ucode}")
    Call<feedback> DoLogin(@Path("sub") int sub,@Path("phone") String phone,@Path("ucode") String ucode);
}
