package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed Sadeghi on 08/05/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class DomainModel {
    @SerializedName("DomainID")
    public int DomainID ;
    public int getDomainID() {
        return DomainID;
    }
    public DomainModel setDomainID(int value) {
        DomainID = value;
        return this;
    }
    @SerializedName("DomainName")
    public String DomainName ;
    public String getDomainName() {
        return DomainName;
    }
    public DomainModel setDomainName(String value) {
        DomainName = value;
        return this;
    }
    @SerializedName("primary")
    public Boolean primary ;
    public Boolean getprimary() {
        return primary;
    }
    public DomainModel setprimary(Boolean value) {
        primary = value;
        return this;
    }
}

