package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 15/11/2017.
 */


public class AddTypeAdapter extends RecyclerView.Adapter<View_Holder_Add_Type> {

    List<KindGroup> list = Collections.emptyList();
   public  List<KindGroup> changeKind=new ArrayList<KindGroup>();
    Context context;

    public AddTypeAdapter(List<KindGroup> list, Context context) {
        this.list = list;
        for (int i = 0; i <list.size() ; i++) {
            KindGroup kg=new KindGroup();
            kg.SetTitle(list.get(i).GetTitle());
            kg.SetID(list.get(i).GetID());
            kg.SetStatus(0);
            changeKind.add(kg);
        }
        this.context = context;
    }

    @Override
    public View_Holder_Add_Type onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_add_type_row_themplate, parent, false);
        View_Holder_Add_Type holder = new View_Holder_Add_Type(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_Add_Type holder, final int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.lbl_type_item_title.setText(list.get(position).GetTitle());

        holder.type_list_ripple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                changeKind.get(position).SetStatus(1);
                for (int i = 0; i < G.changekindslist.size(); i++) {
                  if(G.changekindslist.get(i).GetTitle().toString().equals(list.get(position).GetTitle().toString()))
                  {
                    G.changekindslist.get(i).SetStatus(1);
                  }
                }
                //G.changekindslist.get(position).SetStatus(1);
                remove(list.get(position));

            }
        });


    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, KindGroup gallery) {
        list.add(position, gallery);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(KindGroup order) {
        int position = list.indexOf(order);
        list.remove(position);
       // notifyDataSetChanged();
      //  notifyItemChanged(position);
       notifyItemRemoved(position);
       //moskel darehhhhhh :)
        notifyDataSetChanged();
    }

}
