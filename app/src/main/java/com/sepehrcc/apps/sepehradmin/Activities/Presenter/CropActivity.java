package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CropActivity extends AppCompatActivity {
    @BindView(R.id.cropImageView)
    CropImageView cropImageView;
    @BindView(R.id.btn_crop)
    Button btn_crop;
    @BindView(R.id.croped_image)
    ImageView croped_image;
    @BindView(R.id.lin_verify_crop)
    LinearLayout lin_verify_crop;
    @BindView(R.id.btn_crop_decline)
    Button btn_crop_decline;
    @BindView(R.id.btn_crop_verify)
    Button btn_crop_verify;
    @BindView(R.id.lin_crop)
    LinearLayout lin_crop;
    SweetAlertDialog pDialog;
    String photo_name;

    public Bitmap resizebmp(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int max = w;
        int maxi = 0;
        if (h > max) {
            max = h;
            maxi = 1;
        }
        Bitmap s = Bitmap.createBitmap(max, max, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(s);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(s, 0, 0, null);
        canvas.save();
        int left = 0;
        int top = 0;
        if (maxi == 0) {
            top = (max - h) / 2;
        } else {
            left = (max - w) / 2;
        }
        return createSingleImageFromMultipleImages(s, bmp, left, top);
    }

    private Bitmap createSingleImageFromMultipleImages(Bitmap firstImage, Bitmap secondImage, int left, int top) {

        Bitmap result = Bitmap.createBitmap(firstImage.getWidth(), firstImage.getHeight(), firstImage.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(firstImage, 0f, 0f, null);
        canvas.drawBitmap(secondImage, left, top, null);
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);
        ButterKnife.bind(this);
        if (G.b != null) {
            G.b = resizebmp(G.b);
        }
        cropImageView.setImageBitmap(G.b);

        cropImageView.setAspectRatio(1, 1);
        cropImageView.setFixedAspectRatio(true);
        cropImageView.setAutoZoomEnabled(true);
        cropImageView.setGuidelines(CropImageView.Guidelines.ON);
        //cropImageView.setImageBitmap(bitmap);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photo_name = bundle.getString("PhotoName");
        }
        initialView();
        pDialog = new SweetAlertDialog(CropActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("در حال پردازش لطفا صبر کنید ...");
        pDialog.setCancelable(false);
        BindViewControl();
    }

    private void BindViewControl() {
        btn_crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap cropped = cropImageView.getCroppedImage(1000, 1000, CropImageView.RequestSizeOptions.RESIZE_FIT);
                lin_verify_crop.setVisibility(View.VISIBLE);
                lin_crop.setVisibility(View.GONE);
                croped_image.setImageBitmap(cropped);
            }
        });
        btn_crop_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin_verify_crop.setVisibility(View.GONE);
                lin_crop.setVisibility(View.VISIBLE);
            }
        });
        btn_crop_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pDialog.show();
                if (photo_name.equals("sepehrbrand.jpg")) {
                    Bitmap cropped = cropImageView.getCroppedImage(100, 100, CropImageView.RequestSizeOptions.RESIZE_FIT);
                    save_image(cropped, photo_name);
                    G.b = cropped;
                    Intent i = new Intent();
                    setResult(10, i);
                    finish();
                } else {
                    Bitmap cropped = cropImageView.getCroppedImage(1000, 1000, CropImageView.RequestSizeOptions.RESIZE_FIT);
                    save_image(cropped, photo_name);
                    G.b = cropped;
                    Intent i = new Intent();
                    setResult(10, i);
                    finish();
                }
            }
        });
    }

    private void initialView() {
        btn_crop.setTypeface(Fonts.Vazir);
        btn_crop_decline.setTypeface(Fonts.Vazir);
        btn_crop_verify.setTypeface(Fonts.Vazir);
    }

    public void save_image(Bitmap result, String photo_name) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        String ImageUrl = path + "/" + photo_name;
        Integer counter = 0;
        File file = new File(path, photo_name); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        result.compress(Bitmap.CompressFormat.JPEG, 100, fOut);// saving the Bitmap to a file compressed as a JPEG with 85% compression rate
        try {
            assert fOut != null;
            fOut.flush(); // Not really required
            fOut.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
