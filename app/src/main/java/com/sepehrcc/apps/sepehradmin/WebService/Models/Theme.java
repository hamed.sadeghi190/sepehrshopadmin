package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;


public class Theme {

    @SerializedName("TId")
    private int _TId;

    public int GetThemeTId() {
        return _TId;
    }

    public Theme SetThemeTId(int value) {
        _TId = value;
        return this;
    }

    @SerializedName("TName")
    private String _TName;

    public String GetThemeTName() {
        return _TName;
    }

    public Theme SetThemeTName(String value) {
        _TName = value;
        return this;
    }

    @SerializedName("TPicAdd")
    private String _TPicAdd;

    public String GetThemeTPicAdd() {
        return _TPicAdd;
    }

    public Theme SetThemeTPicAdd(String value) {
        _TPicAdd = value;
        return this;
    }

    @SerializedName("TTheme")
    private String _TTheme;

    public String GetThemeTTheme() {
        return _TTheme;
    }

    public Theme SetThemeTTheme(String value) {
        _TTheme = value;
        return this;
    }

    @SerializedName("TMaster")
    private String _TMaster;

    public String GetThemeTMaster() {
        return _TMaster;
    }

    public Theme SetThemeTMaster(String value) {
        _TMaster = value;
        return this;
    }

    @SerializedName("TStatus")
    private Short _TStatus;

    public Short GetThemeTStatus() {
        return _TStatus;
    }

    public Theme SetThemeTStatus(Short value) {
        _TStatus = value;
        return this;
    }

    @SerializedName("TPriority")
    private int _TPriority;

    public int GetThemeTPriority() {
        return _TPriority;
    }

    public Theme SetThemeTPriority(int value) {
        _TPriority = value;
        return this;
    }

    @SerializedName("TShopId")
    private int _TShopId;

    public int GetThemeTShopId() {
        return _TShopId;
    }

    public Theme SetThemeTShopId(int value) {
        _TShopId = value;
        return this;
    }


}
