package com.sepehrcc.apps.sepehradmin.Models;

/**
 * Created by Administrator on 12/11/2017.
 */

public class SubGroup {
    public String title;
    public int count;
    public int image;

    public SubGroup(String title, int count, int image) {
        this.title = title;
        this.count = count;
        this.image = image;

    }
}
