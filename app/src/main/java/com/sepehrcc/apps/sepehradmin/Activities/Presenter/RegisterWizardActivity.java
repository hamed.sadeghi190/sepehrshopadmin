package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Register;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressWarnings("ALL")
public class RegisterWizardActivity extends AppCompatActivity {
    Register RegModel = new Register();
    @BindView(R.id.viewPager)
    ViewFlipper Flipper;
    //*** Page 1 "******//
    @BindView(R.id.lbl_ShopAdmin)
    TextView lbl_ShopAdmin;
    @BindView(R.id.edt_FullName)
    EditText edt_FullName;
    @BindView(R.id.btn_next)
    BootstrapButton Btn_Next;
    //*** Page 2"******//
    @BindView(R.id.lbl_AdminPhone)
    TextView lbl_AdminPhone;
    @BindView(R.id.lbl_AdminPhoneHint)
    TextView lbl_AdminPhoneHint;
    @BindView(R.id.edt_Admin_phonenumber)
    EditText edt_Admin_phonenumber;
    @BindView(R.id.btn_Check_Phone)
    BootstrapButton btn_Check_Phone;
    //*** Page 3 "******//
    @BindView(R.id.lbl_Shop_name)
    TextView lbl_Shop_name;
    @BindView(R.id.lbl_ShopDesc)
    TextView lbl_ShopDesc;
    @BindView(R.id.edt_shopName)
    EditText edt_shopName;
    @BindView(R.id.btn_next_shopname)
    BootstrapButton btn_next_shopname;
    //*** Page 4 "******//
    @BindView(R.id.lbl_subdomain)
    TextView lbl_subdomain;
    @BindView(R.id.lbl_subdomain_hint)
    TextView lbl_subdomain_hint;
    @BindView(R.id.lbl_sepehrcc)
    TextView lbl_sepehrcc;
    @BindView(R.id.edt_subdomain)
    EditText edt_subdomain;
    @BindView(R.id.btn_send_register)
    BootstrapButton btn_send_register;
    WebServicelistener listener;
    Dialog dialog;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_register_wizard);
        G.CurrentActivity = this;
        ButterKnife.bind(this);
        InitialView();
        BindEventContorl();
    }


    private void InitialView() {

        lbl_ShopAdmin.setTypeface(Fonts.VazirBold);
        edt_FullName.setTypeface(Fonts.Vazir);
        Btn_Next.setTypeface(Fonts.Vazir);

        lbl_AdminPhone.setTypeface(Fonts.VazirBold);
        lbl_AdminPhoneHint.setTypeface(Fonts.Vazir);
        edt_Admin_phonenumber.setTypeface(Fonts.Vazir);
        btn_Check_Phone.setTypeface(Fonts.Vazir);

        lbl_Shop_name.setTypeface(Fonts.VazirBold);
        lbl_ShopDesc.setTypeface(Fonts.Vazir);
        edt_shopName.setTypeface(Fonts.Vazir);
        btn_next_shopname.setTypeface(Fonts.VazirBold);

        lbl_subdomain.setTypeface(Fonts.VazirBold);
        lbl_subdomain_hint.setTypeface(Fonts.Vazir);
        edt_subdomain.setTypeface(Fonts.Vazir);
        btn_send_register.setTypeface(Fonts.VazirBold);

    }

    private void BindEventContorl() {

        Btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_FullName.getText().length() > 0) {
                    RegModel.SetFullname(edt_FullName.getText().toString());
                    RegModel.SetuserName(edt_FullName.getText().toString());
                    position++;
                    Flipper.setDisplayedChild(position);
                } else {
                    Toast.makeText(G.CurrentActivity, "لطفا نام خود را وارد نمایید.  ", Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_Check_Phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String check = edt_Admin_phonenumber.getText().toString();
                if (check.length() == 0) {
                    Toast.makeText(G.CurrentActivity, "Enter Your phone please! ", Toast.LENGTH_LONG).show();
                } else if (!check.matches("(\\+98|0)?9\\d{9}")) {
                    Toast.makeText(G.CurrentActivity, "شماره موبایل نامعتبر است ", Toast.LENGTH_LONG).show();
                } else {
                    RegModel.SetphoneNumber(edt_Admin_phonenumber.getText().toString());
                    position++;
                    Flipper.setDisplayedChild(position);
                }
            }
        });

        btn_next_shopname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_shopName.getText().length() > 0) {
                    RegModel.SetsiteName(edt_shopName.getText().toString());
                    position++;
                    Flipper.setDisplayedChild(position);
                } else {
                    Toast.makeText(G.CurrentActivity, "لطفا نام خود را وارد نمایید.  ", Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_send_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_subdomain.getText().toString().length() > 0) {
                    RegModel.SetsubDomain(edt_subdomain.getText().toString());
                    dialog = new Dialog(G.CurrentActivity);
                    listener = new WebServicelistener() {
                        @Override
                        public void OnComplete(Object object) {
                            dialog.dismiss();
                            feedback fback = (feedback) object;
                            if (fback.Status == 1) {
                                Intent intent = new Intent(G.CurrentActivity, SignupVerifyActivity.class);
                                intent.putExtra("phone", edt_Admin_phonenumber.getText().toString());
                                intent.putExtra("action", "signup");
                                intent.putExtra("pin", fback.exception);
                                String json = new Gson().toJson(RegModel);
                                intent.putExtra("info", json);
                                startActivity(intent);
                            } else if (fback.Status == 4) {
                                new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("خطا")
                                        .setContentText("این شماره قبلا ثبت شده است")
                                        .show();
                            } else {
                                new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("خطا")
                                        .setContentText(fback.Message)
                                        .show();
                            }
                        }

                        @Override
                        public void OnFail(Object object) {
                            dialog.dismiss();
                            feedback fback = (feedback) object;
                            new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("خطا")
                                    .setContentText(fback.Message)
                                    .show();
                        }
                    };
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custom_loading);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    try {
                        dialog.show();
                    }
                    catch (Exception ignored)
                    {}
                    new SecurityRepository().SendRegisterCode(listener, RegModel);
                } else {
                    Toast.makeText(G.CurrentActivity, "لطفا ادرس موقت را وارد نمایید.  ", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (position == 0) {
            super.onBackPressed();
        } else {
            position--;
            Flipper.setDisplayedChild(position);
        }
    }
}
