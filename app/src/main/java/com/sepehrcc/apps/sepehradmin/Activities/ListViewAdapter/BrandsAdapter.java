package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.app.Activity;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.BrandsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.DomainsActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Brands;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.DomainRepository;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.thefinestartist.utils.service.ServiceUtil.getLayoutInflater;

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.GroupViewHolder> {
    List<Mark> list = Collections.emptyList();
    Context context;
Boolean is_search=false;
    public BrandsAdapter(List<Mark> list, Context context,Boolean is_search) {
        this.context = context;
        this.list = list;
        G.CurrentActivity= (Activity) context;
        this.is_search=is_search;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lst_brand_row, viewGroup, false);
        GroupViewHolder brand = new GroupViewHolder(v);
        return brand;
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {
        if(is_search)
        {
            holder.product_list_ripple3.setVisibility(View.GONE);
            holder.product_list_ripple4.setVisibility(View.GONE);
        }
        holder.lbl_brand_list_item_title.setText(list.get(position).GetMarkName());
        holder.lbl_brand_list_item_count.setText("تعداد کالاها : "+list.get(position).GetMarkCount());
        Picasso.Builder builder = new Picasso.Builder(context);
        if(list.get(position).GetMarkImage().contains("host/"))
        {
            String link=G.PhotoUrl+list.get(position).GetMarkImage();
            Picasso.with(context)
                    .load(link)
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(holder.img_brand_item);
        }
        else if(list.get(position).GetMarkImage()!=null) {
            Picasso.with(context)
                    .load(G.PhotoUrl + "/Images/MarkImage/" + list.get(position).GetMarkImage())
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(holder.img_brand_item);
        }
        holder.brand_list_ripple1.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Gson g=new Gson();
                Mark result=list.get(position);
                Intent i=new Intent();
                i.putExtra("result_brand",g.toJson(result));
                G.CurrentActivity.setResult(11,i);
                G.CurrentActivity.finish();
            }
        });
        holder.brand_list_ripple2.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Gson g=new Gson();
                Mark result=list.get(position);
                Intent i=new Intent();
                i.putExtra("result_brand",g.toJson(result));
                G.CurrentActivity.setResult(11,i);
                G.CurrentActivity.finish();
            }
        });
        holder.product_list_ripple3.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                EditBrand editBrand = (EditBrand) context;
                editBrand.EditBrand(list.get(position));

            }
        });
        holder.product_list_ripple4.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                EditBrand editBrand = (EditBrand) context;
                editBrand.EditPhoto(list.get(position));
            }
        });
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        LinearLayout lin_brand_item;
        CardView brand_card_view;
        RippleView brand_list_ripple1;
        RippleView brand_list_ripple2;
        CircleImageView img_brand_item;
        TextView lbl_brand_list_item_title;
        TextView lbl_brand_list_item_count;
        RippleView product_list_ripple3;
        TextView lbl_modify_brand;
        RippleView product_list_ripple4;
        TextView lbl_modify_brand_photo;


        GroupViewHolder(View itemView) {
            super(itemView);
            lin_brand_item = (LinearLayout) itemView.findViewById(R.id.lin_brand_item);
            brand_card_view = (CardView) itemView.findViewById(R.id.brand_card_view);
            brand_list_ripple1 = (RippleView) itemView.findViewById(R.id.brand_list_ripple1);
            brand_list_ripple2 = (RippleView) itemView.findViewById(R.id.brand_list_ripple2);
            product_list_ripple3 = (RippleView) itemView.findViewById(R.id.product_list_ripple3);
            lbl_brand_list_item_title = (TextView) itemView.findViewById(R.id.lbl_brand_list_item_title);
            lbl_brand_list_item_count = (TextView) itemView.findViewById(R.id.lbl_brand_list_item_count);
            lbl_modify_brand = (TextView) itemView.findViewById(R.id.lbl_modify_brand);
            img_brand_item = (CircleImageView) itemView.findViewById(R.id.img_brand_item);
            product_list_ripple4=(RippleView)itemView.findViewById(R.id.product_list_ripple4);
            lbl_modify_brand_photo=(TextView)itemView.findViewById(R.id.lbl_modify_brand_photo);
            lbl_brand_list_item_title.setTypeface(Fonts.VazirLight);
            lbl_brand_list_item_count.setTypeface(Fonts.VazirFD);
            lbl_modify_brand.setTypeface(Fonts.VazirFD);
            lbl_modify_brand_photo.setTypeface(Fonts.VazirFD);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    public interface EditBrand {
        public void EditBrand(Mark mark);
        public void EditPhoto(Mark mark);
    }
}