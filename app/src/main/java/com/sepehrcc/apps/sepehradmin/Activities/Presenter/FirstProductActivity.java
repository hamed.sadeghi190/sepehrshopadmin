package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.thefinestartist.finestwebview.FinestWebView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FirstProductActivity extends AppCompatActivity {
    @BindView(R.id.Btn_first_step_plus)
    ImageView Btn_first_step_plus;
    @BindView(R.id.txt_reg_first_product)
    TextView txt_reg_first_product;
    @BindView(R.id.txt_first_steps)
    TextView txt_first_steps;
    @BindView(R.id.txt_how_to)
    TextView txt_how_to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_first_product);
        ButterKnife.bind(this);
        initialView();
        BindControlsEvent();
    }
    private void initialView() {
        txt_reg_first_product.setTypeface(Fonts.VazirBold);
        txt_first_steps.setTypeface(Fonts.VazirLight);
        txt_how_to.setTypeface(Fonts.VazirLight);
    }

    private void BindControlsEvent() {
        Btn_first_step_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddProductActivity.class);
                startActivity(intent);
            }
        });
        txt_how_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/products"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(FirstProductActivity.this).show("http://help.sepehrcc.com/products");

            }
        });
    }
}
