package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.DomainListAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DomainModel;

import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.DomainRepository;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class DomainsActivity extends AppCompatActivity {
    @BindView(R.id.txt_add_product_title)
    TextView txt_add_product_title;
    @BindView(R.id.lbl_yourdomain_title)
    TextView lbl_yourdomain_title;
    @BindView(R.id.lbl_yourdomain_name)
    TextView lbl_yourdomain_name;
    @BindView(R.id.list_domains)
    RecyclerView list_domains;
    @BindView(R.id.add_menu)
    FloatingActionMenu add_menu;
    @BindView(R.id.fab_adding_domain)
    FloatingActionButton fab_adding_domain;
    @BindView(R.id.fab_set_existing_domain)
    FloatingActionButton fab_set_existing_domain;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;

    ArrayList<DomainModel> domains = new ArrayList<DomainModel>();
    DomainListAdapter domainListAdapter;
    WebServicelistener servicelistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domains);
        ButterKnife.bind(this);
        initialView();
        BindViewControl();
        getDomanis();
    }

    private void initialView() {
        txt_add_product_title.setTypeface(Fonts.VazirBoldFD);
        lbl_yourdomain_title.setTypeface(Fonts.VazirBoldFD);
        lbl_yourdomain_name.setTypeface(Fonts.VazirBoldFD);
        progressbar.setVisibility(View.VISIBLE);
    }

    private void BindViewControl() {
        fab_adding_domain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DomainsActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_check_domain, null);
                dialogBuilder.setView(dialogView);

                final EditText edt_domain = (EditText) dialogView.findViewById(R.id.edt_domain);
                TextView lbl_admin_tip = (TextView) dialogView.findViewById(R.id.lbl_admin_tip);
                TextView lbl_domain_title = (TextView) dialogView.findViewById(R.id.lbl_domain_title);
                final Button btn_verify = (Button) dialogView.findViewById(R.id.btn_verify);
                final TextView lbl_domain_status = (TextView) dialogView.findViewById(R.id.lbl_domain_status);
                edt_domain.setText("");
                edt_domain.setTypeface(Fonts.VazirBoldFD);
                lbl_admin_tip.setTypeface(Fonts.VazirBoldFD);
                lbl_domain_title.setTypeface(Fonts.VazirBoldFD);
                btn_verify.setTypeface(Fonts.VazirBoldFD);
                lbl_domain_status.setTypeface(Fonts.VazirBoldFD);
                btn_verify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DomainRepository domainRepository = new DomainRepository();
                        servicelistener = new WebServicelistener() {
                            @Override
                            public void OnComplete(Object object) {
                                feedback feedback = (feedback) object;
                                if (feedback.Status == 1) {
                                    btn_verify.setText("ثبت درخواست");
                                    btn_verify.setBackgroundColor(getResources().getColor(R.color.light_brown));
                                    lbl_domain_status.setText("دامین مورد نظر آزاد است");
                                } else {
                                    btn_verify.setText("چک کردن");
                                    btn_verify.setBackgroundColor(getResources().getColor(R.color.SepehrGreen));
                                    lbl_domain_status.setText("دامین مورد نظر قبلا ثبت شده است");
                                }
                            }

                            @Override
                            public void OnFail(Object object) {

                            }
                        };
                        domainRepository.WhoisUrl(servicelistener, edt_domain.getText().toString());
                    }
                });
                dialogBuilder.setNegativeButton("لغو", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
                b.getButton(DialogInterface.BUTTON_POSITIVE).setTypeface(Fonts.VazirBoldFD);
                b.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirBoldFD);
            }
        });
        fab_set_existing_domain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DomainsActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_set_existing_domain, null);
                dialogBuilder.setView(dialogView);

                final EditText edt_domain = (EditText) dialogView.findViewById(R.id.edt_domain);
                TextView lbl_admin_tip = (TextView) dialogView.findViewById(R.id.lbl_admin_tip);
                TextView lbl_domain_title = (TextView) dialogView.findViewById(R.id.lbl_domain_title);
                edt_domain.setText("");
                edt_domain.setTypeface(Fonts.VazirBoldFD);
                lbl_admin_tip.setTypeface(Fonts.VazirBoldFD);
                lbl_domain_title.setTypeface(Fonts.VazirBoldFD);
                dialogBuilder.setPositiveButton("ارسال", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        final SweetAlertDialog pDialog=new SweetAlertDialog(DomainsActivity.this,SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                        pDialog.setTitleText("ارسال ...");
                        pDialog.setCancelable(true);
                        DomainModel domainModel = new DomainModel();
                        domainModel.setDomainName(edt_domain.getText().toString());
                        DomainRepository domainRepository = new DomainRepository();
                        servicelistener = new WebServicelistener() {
                            @Override
                            public void OnComplete(Object object) {
                                feedback feedback = (feedback) object;
                                if (feedback.Status == 1) {
                                    pDialog.hide();
                                    getDomanis();
                                }else if(feedback.Status==2)
                                {
                                    pDialog.hide();
                                    SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(DomainsActivity.this, SweetAlertDialog.ERROR_TYPE);
                                    sweetAlertDialog.setTitleText("خطا")
                                            .setContentText("لطفا آدرس های سرور را روی دامنه خود ست و مجددا تلاش کنید.")
                                            .setConfirmText("بستن")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.hide();
                                                }
                                            })
                                            .show();
                                }
                            }

                            @Override
                            public void OnFail(Object object) {
                                pDialog.hide();
                                SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(DomainsActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("خطا")
                                        .setContentText("دامنه مورد نظر ست نشد لطفا بعدا تلاش کنید")
                                        .setConfirmText("بستن")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.hide();
                                            }
                                        })
                                        .show();
                            }
                        };
                        domainRepository.SetAddedDoamain(servicelistener, domainModel);
                        pDialog.show();
                    }
                });
                dialogBuilder.setNegativeButton("لغو", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
                b.getButton(DialogInterface.BUTTON_POSITIVE).setTypeface(Fonts.VazirBoldFD);
                b.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirBoldFD);
            }
        });
    }

    private void fill_list() {

        for (int i = 0; i < domains.size(); i++) {
            if (domains.get(i).getprimary()) {
                lbl_yourdomain_name.setText(domains.get(i).getDomainName());
                domains.remove(i);
            }
        }
        domainListAdapter = new DomainListAdapter(domains, DomainsActivity.this);
        list_domains.setAdapter(domainListAdapter);
        list_domains.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

    }

    private void getDomanis() {
        DomainRepository domainRepository = new DomainRepository();
        progressbar.setVisibility(View.VISIBLE);

        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                progressbar.setVisibility(View.GONE);
                feedback feedback = (feedback) object;
                if (feedback.Status == 1) {
                    Gson gson = new Gson();
                    Type collectionType = new TypeToken<ArrayList<DomainModel>>() {
                    }.getType();

                    try {
                        domains = gson.fromJson(feedback.value, collectionType);

                        if (domains.size() > 0 && domains.get(0).DomainName != null) {
                            if (!domains.get(0).DomainName.trim().equals(""))
                                fill_list();
                        } else {
                            lbl_yourdomain_name.setText("شما هنوز دامنه ای ثبت نکرده اید.");
                            lbl_yourdomain_name.setBackground(null);
                        }
                    } catch (JsonSyntaxException | IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void OnFail(Object object) {
                progressbar.setVisibility(View.GONE);
            }
        };
        domainRepository.GetDomains(servicelistener);
    }

    private void add_domain(DomainModel domainModel) {
        DomainRepository domainRepository = new DomainRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                feedback feedback = (feedback) object;
                if (feedback.Status == 1) {

                }
            }

            @Override
            public void OnFail(Object object) {
            }
        };
        domainRepository.SetAddedDoamain(servicelistener, domainModel);

    }
}
