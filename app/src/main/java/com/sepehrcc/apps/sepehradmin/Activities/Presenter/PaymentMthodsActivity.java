package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.PaymentMethodAdapters;
import com.sepehrcc.apps.sepehradmin.Models.PaymentModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.BankRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.PostRepository;
import com.thefinestartist.finestwebview.FinestWebView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class PaymentMthodsActivity extends AppCompatActivity implements PaymentMethodAdapters.ListInterface {
    ArrayList<Bank> payment_methods = new ArrayList<Bank>();
    PaymentMethodAdapters paymentMethodAdapters;
    @BindView(R.id.list_payment_methods)
    RecyclerView list_payment_methods;
    @BindView(R.id.btn_send)
    TextView btn_send;
    @BindView(R.id.txt_add_product_title)
    TextView txt_add_product_title;
    @BindView(R.id.btn_Back)
    TextView btn_Back;
    @BindView(R.id.lin_bank_question)
    LinearLayout lin_bank_question;
    @BindView(R.id.img_info)
    ImageView img_info;
    @BindView(R.id.lbl_bank_q)
    TextView lbl_bank_q;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.loadingBar)
    ProgressBar loadingBar;
    WebServicelistener servicelistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_mthods);
        ButterKnife.bind(this);
        initialView();
        BindViewControl();
        GetPaymentMethods();
    }

    private void initialView() {
        btn_send.setTypeface(Fonts.VazirBoldFD);
        txt_add_product_title.setTypeface(Fonts.VazirBoldFD);
        btn_Back.setTypeface(Fonts.VazirBoldFD);
        lbl_bank_q.setTypeface(Fonts.VazirLight);
    }

    private void BindViewControl() {
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin_bank_question.setVisibility(View.GONE);
            }
        });
        lbl_bank_q.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(PaymentMthodsActivity.this).show("http://help.sepehrcc.com/article/bank");

            }
        });
        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditList();
            }
        });
    }

    private void fill_list() {
        paymentMethodAdapters = new PaymentMethodAdapters(payment_methods, PaymentMthodsActivity.this);
        list_payment_methods.setAdapter(paymentMethodAdapters);
        list_payment_methods.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    public void GetPaymentMethods() {
        BankRepository bankRepository = new BankRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                loadingBar.setVisibility(View.GONE);
                payment_methods = (ArrayList<Bank>) object;
                for (int i = 0; i < payment_methods.size(); i++) {
                    Log.i(payment_methods.get(i).GetpName(), payment_methods.get(i).GetpEnable() + "");
                }
                fill_list();
            }

            @Override
            public void OnFail(Object object) {

            }
        };
        bankRepository.GetBank(servicelistener);
    }

    @Override
    public void onListUpdate(List<Bank> list) {
        payment_methods = (ArrayList<Bank>) list;
    }

    private void EditList() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(false);
        pDialog.show();
        BankRepository bankRepository = new BankRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                pDialog.hide();
                Intent intent = new Intent(PaymentMthodsActivity.this, SuccessfullyActivity.class);
                intent.putExtra("successfully_message", "تغییرات در روش های ارسال با موفقیت انجام شد");
                startActivity(intent);
                finish();
            }

            @Override
            public void OnFail(Object object) {

            }
        };
        bankRepository.MultiEditBank(servicelistener, payment_methods);
    }
}
