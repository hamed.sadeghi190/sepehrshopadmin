package com.sepehrcc.apps.sepehradmin.Models;


public class PostModel {
    String name;
    String description;
    int image;
    boolean select;
    String vahed;

    public String getVahed() {
        return vahed;
    }

    public void setVahed(String vahed) {
        this.vahed = vahed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
