package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.provider.SyncStateContract;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Models.OrderItemModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderItems;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {

    List<OrderItems> modelList = Collections.emptyList();
    Context context;

    public OrderItemsAdapter(ArrayList<OrderItems> modelList, Context context) {
        this.modelList = modelList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_order_items_row, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int item_number = position + 1;
        holder.lbl_item_number.setText(item_number + " .");
        holder.lbl_item_full_name.setText(modelList.get(position).getkName());
        holder.lbl_item_count_name.setText((int)modelList.get(position).getoCount() + "");
        holder.lbl_price.setText(G.formatPrice(modelList.get(position).getoPrice() + ""));
//        if (modelList.get(position).gettColor()==null&& modelList.get(position).gettSize()==null&& modelList.get(position).getTmaterial()==null) {
//            holder.lbl_model_amount.setText("ندارد");
//        }
        if (modelList.get(position).gettColor()!=null&& modelList.get(position).gettSize()==null&& modelList.get(position).getTmaterial()==null) {
            holder.lbl_model_amount.setText(modelList.get(position).gettColor() + "");
        }
        if (modelList.get(position).gettSize()!=null && modelList.get(position).gettColor()==null&& modelList.get(position).getTmaterial()==null) {
            holder.lbl_model_amount.setText(modelList.get(position).gettSize() + "");
        }
        if (modelList.get(position).gettSize()==null && modelList.get(position).gettColor()==null && modelList.get(position).getTmaterial()!=null) {
            holder.lbl_model_amount.setText(modelList.get(position).getTmaterial()+"");
        }

        if (modelList.get(position).gettSize()!=null && modelList.get(position).gettColor()!=null&& modelList.get(position).getTmaterial()==null) {
            holder.lbl_model_amount.setText(modelList.get(position).gettColor());
            holder.lbl_model_amount.append(" - " + modelList.get(position).gettSize());
        }
        if (modelList.get(position).gettSize()!=null && modelList.get(position).getTmaterial()!=null&& modelList.get(position).gettColor()==null) {
            holder.lbl_model_amount.setText(modelList.get(position).gettSize());
            holder.lbl_model_amount.append( " - " + modelList.get(position).getTmaterial());
        }
        if (modelList.get(position).gettColor()!=null && modelList.get(position).getTmaterial()!=null&& modelList.get(position).gettSize()==null) {
            holder.lbl_model_amount.setText(modelList.get(position).gettColor());
            holder.lbl_model_amount.append( " - " + modelList.get(position).getTmaterial());
        }

        if (modelList.get(position).gettColor()!=null && modelList.get(position).getTmaterial()!=null&& modelList.get(position).gettSize()!=null) {
            holder.lbl_model_amount.setText(modelList.get(position).gettColor());
            holder.lbl_model_amount.append( " - " + modelList.get(position).gettSize());
            holder.lbl_model_amount.append( " - " + modelList.get(position).getTmaterial());
        }
        Picasso.with(context)
                .load(G.PhotoUrl+modelList.get(position).getkAksAdd())
                .placeholder(R.drawable.ic_no_image)
                .error(R.drawable.ic_no_image)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(holder.img_item_imge);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv1;

        TextView lbl_item_number;
        ImageView img_item_imge;
        TextView lbl_item_full_name;
        TextView lbl_item_count_title;
        TextView lbl_item_count_name;
        TextView lbl_model_title;
        TextView lbl_model_amount;
        TextView lbl_price_title;
        TextView lbl_price;
        TextView lbl_vahed;

        ViewHolder(View itemView) {
            super(itemView);
            cv1 = (CardView) itemView.findViewById(R.id.cv1);
            lbl_item_number = (TextView) itemView.findViewById(R.id.lbl_item_number);
            img_item_imge = (ImageView) itemView.findViewById(R.id.img_item_imge);
            lbl_item_full_name = (TextView) itemView.findViewById(R.id.lbl_item_full_name);

            lbl_item_count_title = (TextView) itemView.findViewById(R.id.lbl_item_count_title);
            lbl_item_count_name = (TextView) itemView.findViewById(R.id.lbl_item_count_name);
            lbl_model_title = (TextView) itemView.findViewById(R.id.lbl_model_title);
            lbl_model_amount = (TextView) itemView.findViewById(R.id.lbl_model_amount);
            lbl_price_title = (TextView) itemView.findViewById(R.id.lbl_price_title);
            lbl_price = (TextView) itemView.findViewById(R.id.lbl_price);
            lbl_vahed = (TextView) itemView.findViewById(R.id.lbl_vahed);


            lbl_item_number.setTypeface(Fonts.VazirBoldFD);
            lbl_item_full_name.setTypeface(Fonts.VazirBoldFD);
            lbl_item_count_title.setTypeface(Fonts.VazirBoldFD);
            lbl_item_count_name.setTypeface(Fonts.VazirBoldFD);
            lbl_model_title.setTypeface(Fonts.VazirBoldFD);
            lbl_model_amount.setTypeface(Fonts.VazirBoldFD);
            lbl_price_title.setTypeface(Fonts.VazirBoldFD);
            lbl_price.setTypeface(Fonts.VazirBoldFD);
            lbl_vahed.setTypeface(Fonts.VazirBoldFD);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}