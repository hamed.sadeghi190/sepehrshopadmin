package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddProductActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.ChangeGroupActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.ChangeProductActivity;
import com.sepehrcc.apps.sepehradmin.Models.Data;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class Recycler_View_Adapter extends RecyclerView.Adapter<Recycler_View_Adapter.View_Holder> {

    List<Kala> list;
    Context context;
    View_Holder holder;

    public Recycler_View_Adapter(List<Kala> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_products_row_item, parent, false);
        holder = new View_Holder(v);
        return holder;

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(View_Holder holder, int position) {
        final Kala citem = list.get(position);
        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.title.setText(citem.GetName() + "");
        holder.description.setText(citem.GetGroupName() + "");
        holder.imageView.setImageResource(R.drawable.img_toys);
        Picasso.with(context)
                .load(G.PhotoUrl + citem.GetImage())
                .placeholder(R.drawable.ic_loading)
                .error(R.drawable.img_holder)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(holder.imageView);
        holder.price.setText(G.formatPrice(citem.GetPrice() + ""));
        holder.product_list_ripple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Gson gson = new Gson();
                String JsonString = gson.toJson(citem);
                Intent intent = new Intent(context, ChangeProductActivity.class);
                intent.putExtra("IsEdit",true);
                intent.putExtra("Product",JsonString);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Kala data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Kala data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }


    class View_Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardView)
        CardView cv;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.lbl_price)
        TextView price;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.imageView)
        CircleImageView imageView;
        @BindView(R.id.lbl_price_toman)
        TextView price_toman;
        @BindView(R.id.product_list_ripple)
        RippleView product_list_ripple;
        View_Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            price.setTypeface(Fonts.VazirFD);
            title.setTypeface(Fonts.VazirFD);
            description.setTypeface(Fonts.VazirFD);
            price_toman.setTypeface(Fonts.VazirFD);
        }

    }
}

