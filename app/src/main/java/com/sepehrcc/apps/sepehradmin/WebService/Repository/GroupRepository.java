package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.sepehrcc.apps.sepehradmin.SharedClass.G.Upload_url;


public class GroupRepository {

    public void AddGroup(final WebServicelistener lisitner, Group value) {
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.GroupsClient.AddNewGroup(G.ApiToken, value);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });

        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void GetGroups(final WebServicelistener lisitner, int GroupID, int Offset, int Limit) {

        final ArrayList<Group> Resulat = new ArrayList<Group>();
        try {
            final Call<ArrayList<Group>> CallServer = WebServiceHandler.GroupsClient.GetSubGroups(G.ApiToken, GroupID, Offset, Limit);
            CallServer.enqueue(new Callback<ArrayList<Group>>() {
                @Override
                public void onResponse(Response<ArrayList<Group>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Group> groups = response.body();
                                lisitner.OnComplete(groups);
                            } else {
                                ArrayList<Group> groups = new ArrayList<Group>();
                                lisitner.OnComplete(groups);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    lisitner.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
            int a =1;
        }
    }

    public void EditGroup(final WebServicelistener lisitner, Group value) {

        final AddResponse Resulat = new AddResponse();
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.GroupsClient.EditGroup(G.ApiToken, value);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });

        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void DeleteGroup(final WebServicelistener lisitner, int GroupID) {

        final ResponseBase Resulat = new ResponseBase();
        try {
            final Call<ResponseBase> CallServer = WebServiceHandler.GroupsClient.DeleteGroup(G.ApiToken, GroupID);
            CallServer.enqueue(new Callback<ResponseBase>() {
                @Override
                public void onResponse(Response<ResponseBase> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                lisitner.OnComplete(response.body());
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });

        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void UploadPhoto(final WebServicelistener lisitner, String imageUrl, int PhotoID) {
        File file = new File(imageUrl);

        AndroidNetworking.upload(Upload_url + "FileUpload")
                .addMultipartFile("File", file)
                .setTag("uploadTest")
                .addHeaders("Type", "Group")
                .addHeaders("Id", String.valueOf(PhotoID))
                .addHeaders("Authorization", G.ApiToken)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        lisitner.OnComplete(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        int a = 0;
                        lisitner.OnFail(error);
                    }
                });
    }

    public void search(final WebServicelistener lisitner, int ParentID, int Offset, int Limit, String key) {

        final ArrayList<Group> Resulat = new ArrayList<Group>();
        try {
            final Call<ArrayList<Group>> CallServer = WebServiceHandler.GroupsClient.Search(G.ApiToken, ParentID, Offset, Limit, key);
            CallServer.enqueue(new Callback<ArrayList<Group>>() {
                @Override
                public void onResponse(Response<ArrayList<Group>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            ArrayList<Group> groups = response.body();
                            lisitner.OnComplete(groups);
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    lisitner.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {
//
        }
    }

    public void GetAllGroups(final WebServicelistener lisitner)
    {
        try {
            final Call<ArrayList<Group>> CallServer = WebServiceHandler.GroupsClient.GetAllGroups(G.ApiToken);
            CallServer.enqueue(new Callback<ArrayList<Group>>() {
                @Override
                public void onResponse(Response<ArrayList<Group>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Group> groups = response.body();
                                lisitner.OnComplete(groups);
                            } else {
                                ArrayList<Group> groups = new ArrayList<Group>();
                                lisitner.OnComplete(groups);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }else {
                        ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                        lisitner.OnFail(ErrorModel);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    lisitner.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error  + " : " + ex.getMessage());
            lisitner.OnFail(ErrorModel);
        }
    }
}
