package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.ChechTypeAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.ProductGalleryAdapter;
import com.sepehrcc.apps.sepehradmin.DbModels.KalaDB;
import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KalaTypes;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Photos;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thefinestartist.finestwebview.FinestWebView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChangeProductActivity extends AppCompatActivity implements IPickResult {
    @BindView(R.id.txt_add_product_title)
    TextView txt_add_product_title;
    @BindView(R.id.btn_Back)
    TextView BtnBack;
    @BindView(R.id.btn_send)
    TextView BtnSend;
    @BindView(R.id.lbl_toman)
    TextView lblToman;
    @BindView(R.id.lbl_take_photo_advise)
    TextView lbl_take_photo_advise;
    @BindView(R.id.Txt_Prodcut_description)
    EditText Txt_Prodcut_description;
    @BindView(R.id.edt_Prodcut_Name)
    EditText edt_Prodcut_Name;
    @BindView(R.id.Txt_Prodcut_price)
    EditText Txt_Prodcut_price;
    @BindView(R.id.edt_Prodcut_group)
    EditText edt_Prodcut_group;
    @BindView(R.id.Txt_Prodcut_barand)
    EditText Txt_Prodcut_barand;
    @BindView(R.id.txt_ly_edt_Prodcut_Name)
    TextInputLayout txt_ly_edt_Prodcut_Name;
    @BindView(R.id.txt_ly_Txt_Prodcut_price)
    TextInputLayout txt_ly_Txt_Prodcut_price;
    @BindView(R.id.txt_ly_edt_Prodcut_group)
    TextInputLayout txt_ly_edt_Prodcut_group;
    @BindView(R.id.txt_ly_Txt_Prodcut_barand)
    TextInputLayout txt_ly_Txt_Prodcut_barand;
    @BindView(R.id.txt_ly_Txt_Prodcut_desc)
    TextInputLayout txt_ly_Txt_Prodcut_desc;
    @BindView(R.id.lbl_kala_pic)
    TextView lbl_kala_pic;
    @BindView(R.id.lbl_select)
    TextView lbl_select;
    @BindView(R.id.lbl_select2)
    TextView lbl_select2;
    @BindView(R.id.lbl_types)
    TextView lbl_types;
    @BindView(R.id.add_new_type)
    TextView add_new_type;
    @BindView(R.id.btn_product_del)
    Button btn_product_del;
    @BindView(R.id.product_gallery_list)
    RecyclerView product_gallery_list;
    ProductGalleryAdapter adpter;
    List<ProductGallery> data;
    @BindView(R.id.check_type_list)
    RecyclerView check_type_list;
    @BindView(R.id.lbl_tip)
    TextView lbl_tip;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.lin_post_question)
    LinearLayout lin_post_question;

    @BindView(R.id.lin_fb)
    LinearLayout lin_fb;
    @BindView(R.id.lbl_fb)
    TextView lbl_fb;

    ChechTypeAdapter typeAdapter;
    List<KindGroup> chech_data = new ArrayList<KindGroup>();
    boolean isopened = false;
    WebServicelistener servicelistener;
    WebServicelistener imagelisitner;
    SweetAlertDialog pDialog;
    Kala EditObject = new Kala();
    ArrayList<Photos> photos = new ArrayList<Photos>();
    String ImageUrl;
    List<ProductGallery> gallery = new ArrayList<>();
    PickSetup setup = new PickSetup();
    String ImageName = "";
    int price_value;
    Mark kala_mark = new Mark();
    SweetAlertDialog googledialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_change_product);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        G.CurrentActivity = this;
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        ButterKnife.bind(this);
        InitialView();
        init2();
        BindControlsEvents();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getBoolean("IsEdit", false)) {
                Gson gson = new Gson();
                EditObject = gson.fromJson(extras.getString("Product"), Kala.class);
                LoadInfo(EditObject);
                getKalaInfo(EditObject.GetID());
            }
        }
        setup.setTitle("انتخاب")
                .setTitleColor(Color.BLACK)
                .setBackgroundColor(Color.WHITE)
                .setProgressText("صبر کنید...")
                .setProgressTextColor(Color.BLACK)
                .setCancelText("بستن")
                .setCancelTextColor(Color.BLACK)
                .setButtonTextColor(Color.BLACK)
                .setMaxSize(500)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText("دوربین")
                .setGalleryButtonText("گالری")
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setSystemDialog(false)
                .setCameraToPictures(false)
                .setGalleryIcon(R.drawable.ic_gallery_picker)
                .setCameraIcon(R.drawable.ic_photo_camera);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isopened = false;
        G.is_prodict_change = true;
    }

    private void init2() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        googledialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        imagelisitner = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
               // loadImagePicker();
                select_google_phone();
            }

            @Override
            public void OnFail(Object object) {
            }
        };
    }

    private void loadImagePicker() {
        PickImageDialog.build(setup).setOnPickResult(new IPickResult() {
            @Override
            public void onPickResult(PickResult pickResult) {
                if (pickResult.getError() == null) {
                    save_image(pickResult.getBitmap());
                }
            }
        }).show(ChangeProductActivity.this);
    }


    private void InitialView() {
        txt_add_product_title.setTypeface(Fonts.VazirBold);
        BtnBack.setTypeface(Fonts.VazirBold);
        BtnSend.setTypeface(Fonts.VazirBold);
        lblToman.setTypeface(Fonts.VazirBold);
        lbl_take_photo_advise.setTypeface(Fonts.VazirBold);
        Txt_Prodcut_description.setTypeface(Fonts.VazirBold);
        edt_Prodcut_Name.setTypeface(Fonts.VazirBold);
        Txt_Prodcut_price.setTypeface(Fonts.VazirBold);
        edt_Prodcut_group.setTypeface(Fonts.VazirBold);
        Txt_Prodcut_barand.setTypeface(Fonts.VazirBold);
        txt_ly_edt_Prodcut_Name.setTypeface(Fonts.VazirBold);
        txt_ly_Txt_Prodcut_price.setTypeface(Fonts.VazirBold);
        txt_ly_edt_Prodcut_group.setTypeface(Fonts.VazirBold);
        txt_ly_Txt_Prodcut_barand.setTypeface(Fonts.VazirBold);
        txt_ly_Txt_Prodcut_desc.setTypeface(Fonts.VazirBold);
        lbl_kala_pic.setTypeface(Fonts.VazirBold);
        lbl_select.setTypeface(Fonts.VazirBold);
        lbl_select2.setTypeface(Fonts.VazirBold);
        lbl_types.setTypeface(Fonts.VazirBold);
        add_new_type.setTypeface(Fonts.VazirBold);
        btn_product_del.setTypeface(Fonts.VazirBold);
        lbl_tip.setTypeface(Fonts.VazirBold);
        lbl_fb.setTypeface(Fonts.VazirLight);

    }

    @SuppressLint("ClickableViewAccessibility")
    private void BindControlsEvents() {
        lin_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//                /fastbuy.aspx?PId=
                ClipData clip = ClipData.newPlainText("label", G.appData.shopInfo.GetDomain()+"/fb/"+EditObject.GetID());
                if(G.appData.shopInfo.GetDomain()==null || G.appData.shopInfo.GetDomain().equals(""))
                {
                    clip = ClipData.newPlainText("label", G.appData.shopInfo.GetsubDomain()+".sepehrcc.com/fb/"+EditObject.GetID());

                }
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getApplicationContext(),"در کلیپ بورد ذخیره شد.",Toast.LENGTH_LONG).show();
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangeProductActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_fast_buy, null);
                dialogBuilder.setView(dialogView);
                TextView lbl_admin_tip = (TextView) dialogView.findViewById(R.id.lbl_admin_tip);
                TextView lbl_fast_buy_link=(TextView)dialogView.findViewById(R.id.lbl_fast_buy_link);
                TextView lbl_tip2=(TextView)dialogView.findViewById(R.id.lbl_tip2);
                lbl_admin_tip.setTypeface(Fonts.VazirBoldFD);
                lbl_fast_buy_link.setTypeface(Fonts.VazirBoldFD);
                lbl_tip2.setTypeface(Fonts.VazirBoldFD);
                if(G.appData.shopInfo.GetDomain()!=null && !G.appData.shopInfo.GetDomain().equals("")) {
                    lbl_fast_buy_link.setText(G.appData.shopInfo.GetDomain() + "/fb/" + EditObject.GetID());
                }else {
                    lbl_fast_buy_link.setText(G.appData.shopInfo.GetsubDomain() + ".sepehrcc.com/fb/" + EditObject.GetID());
                }
                lbl_fast_buy_link.setTextIsSelectable(true);
                dialogBuilder.setNegativeButton("بستن", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
                b.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirBoldFD);

            }
        });
        lbl_select2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Intent = new Intent(ChangeProductActivity.this, BrandsActivity.class);
                startActivityForResult(Intent, 11);
            }
        });
        Txt_Prodcut_description.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.Txt_Prodcut_description) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        Txt_Prodcut_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Txt_Prodcut_price.removeTextChangedListener(this);

                try {
                    String originalString = editable.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);
                    price_value = Integer.parseInt(originalString);
                    //setting text after format to EditText
                    Txt_Prodcut_price.setText(formattedString);
                    Txt_Prodcut_price.setSelection(Txt_Prodcut_price.getText().length());

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                Txt_Prodcut_price.addTextChangedListener(this);
            }
        });
        BtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                G.desc_save = "";
                //Intent intent = new Intent(G.CurrentActivity, ProductListActivity.class);
                // startActivity(intent);
                finish();
            }
        });
        btn_product_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SweetAlertDialog sdialog = new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.WARNING_TYPE);
                sdialog.setTitleText("حذف کالا");
                sdialog.setContentText("آیا مطمئن هستید ؟");
                sdialog.setConfirmText("تایید");
                sdialog.setCancelText("انصراف");
                sdialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        final SweetAlertDialog pDialog = new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.PROGRESS_TYPE);
                        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                        pDialog.setTitleText("ارسال اطلاعات ...");
                        pDialog.setCancelable(false);
                        pDialog.show();
                        WebServicelistener lis = new WebServicelistener() {
                            @Override
                            public void OnComplete(Object object) {
                                sdialog.dismiss();
                                pDialog.dismiss();
                                new Delete().from(KalaDB.class).where(" kId = ? ", EditObject.GetID()).execute();
                                G.appData.shopInfo.SetKalaCount(G.appData.shopInfo.GetKalaCount() - 1);
                                finish();
                            }

                            @Override
                            public void OnFail(Object object) {
                                int b = 0;
                            }
                        };
                        new KalaRepository().Delete(lis, EditObject.GetID());
                    }
                });
                sdialog.show();

            }
        });

        lbl_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent IntentClient = new Intent(G.CurrentActivity, CategoryListActivity.class);
                IntentClient.putExtra("IsForResualt", true);
                startActivityForResult(IntentClient, 2);
            }
        });
        edt_Prodcut_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent IntentClient = new Intent(G.CurrentActivity, CategoryListActivity.class);
                IntentClient.putExtra("IsForResualt", true);
                startActivityForResult(IntentClient, 2);
            }
        });
        BtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText("ارسال ...");
                pDialog.setCancelable(true);
                Kala kalaObj = new Kala();
                kalaObj.SetID(EditObject.GetID());
                kalaObj.SetName(edt_Prodcut_Name.getText().toString());
                kalaObj.SetPrice(price_value);
                kalaObj.SetDescription(Txt_Prodcut_description.getText().toString());
                if(kala_mark!=null)
                {
                    kalaObj.SetMark(kala_mark.GetMarkID()+"");
                }
                kalaObj.SetkMarkName(Txt_Prodcut_barand.getText().toString());
                kalaObj.SetGroup(EditObject.GetGroup());
                kalaObj.SetGroupName(EditObject.GetGroupName());

                ArrayList<KalaTypes> Types = new ArrayList<>();
                for (int x = 0; x < chech_data.size(); x++) {
                    boolean vaziat = chech_data.get(x).GetChecked();
                    if (vaziat)
                        Types.add(new KalaTypes().SetKindID(chech_data.get(x).GetID()));
                }
                kalaObj.SetKinds(Types);
                servicelistener = new WebServicelistener() {
                    @Override
                    public void OnComplete(Object object) {
                        G.desc_save = "";
                        pDialog.hide();
                        for (final ProductGallery pgitem : gallery) {
                            if (!pgitem.FromWeb && !pgitem.IsAddButton) {
                                Thread thread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        WebServicelistener lis = new WebServicelistener() {
                                            @Override
                                            public void OnComplete(Object object) {
                                                int a = 0;
                                            }

                                            @Override
                                            public void OnFail(Object object) {
                                                int b = 0;
                                            }
                                        };
                                        new KalaRepository().UploadPhoto(lis, pgitem.image, EditObject.GetID());
                                    }
                                });
                                thread.run();
                            }
                        }
                        Intent intent = new Intent(ChangeProductActivity.this, SuccessfullyActivity.class);
                        intent.putExtra("successfully_message", "تغییرات مورد نظر با موفقیت اضافه شد");
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void OnFail(Object object) {
                    }
                };
                pDialog.show();
                new KalaRepository().Edit(servicelistener, kalaObj);
            }
        });
        lbl_take_photo_advise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/post"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(ChangeProductActivity.this).show("http://help.sepehrcc.com/post");

            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin_post_question.setVisibility(View.GONE);
            }
        });
        lbl_tip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/gallery"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(ChangeProductActivity.this).show("help.sepehrcc.com/article/better_pics");

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == 1) {
            String rich_edit_text_result = data.getStringExtra("rich_edit_text_html");
            Txt_Prodcut_description.setText(rich_edit_text_result);
        } else if (resultCode == 4) {
            chech_data.clear();
            for (int i = 0; i < data.getStringArrayListExtra("typelist").size(); i++) {

                //  chech_data.add(new CheckType(data.getStringArrayListExtra("typelist").get(i)));
            }
            //fill_list_type(chech_data);
        } else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                Gson gson = new Gson();
                Group GModel = gson.fromJson(result, Group.class);
                EditObject.SetGroup(GModel.GetID());
                edt_Prodcut_group.setText(GModel.GetName());
                chech_data.addAll(GModel.GetKinds());
                fill_list_type();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (resultCode == 10) {
            save_image_croped(G.b);
        }
        if (resultCode == 11) {
            Gson g = new Gson();
            Type collectionType = new TypeToken<Mark>() {
            }.getType();
            String brand_json = data.getStringExtra("result_brand");
            kala_mark = g.fromJson(brand_json, collectionType);
            Txt_Prodcut_barand.setText(kala_mark.GetMarkName());
        }
        if (resultCode == 12) {
            googledialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            googledialog.setTitleText("در حال دانلود تصویر ...");
            googledialog.setCancelable(true);
            final String link = data.getStringExtra("photo_link");
            try {
                googledialog.show();
                String url = link;
                url = url.replaceAll(" ", "%20");
                Glide.with(getApplicationContext())
                        .load(url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                googledialog.hide();
                                save_image(bitmap);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                googledialog.hide();
                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ChangeProductActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("خطا")
                                        .setContentText("تصویر دانلود نشد لطفا بعدا تلاش کنید")
                                        .setConfirmText("بستن")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.hide();
                                            }
                                        })
                                        .show();
                            }
                        });
            } catch (Exception e) {
                googledialog.show();
                String url = link;
                url = url.replaceAll(" ", "%20");
                Glide.with(getApplicationContext())
                        .load(url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                googledialog.hide();
                                save_image(bitmap);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                googledialog.hide();
                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ChangeProductActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("خطا")
                                        .setContentText("تصویر دانلود نشد لطفا بعدا تلاش کنید")
                                        .setConfirmText("بستن")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.hide();
                                            }
                                        })
                                        .show();
                            }
                        });
            }
        }
    }

    private void fill_list_type() {
        typeAdapter = new ChechTypeAdapter(chech_data, this);
        check_type_list.setAdapter(typeAdapter);
        check_type_list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        G.desc_save = "";
    }

    private void fill_list(List<ProductGallery> data) {
        G.is_prodict_change = true;
        adpter = new ProductGalleryAdapter(data, this, imagelisitner);
        product_gallery_list.setAdapter(adpter);
        product_gallery_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    private void LoadInfo(Kala value) {
        edt_Prodcut_Name.setText(value.GetName());
        edt_Prodcut_group.setText(value.GetGroupName());
        Txt_Prodcut_barand.setText(value.GetMark());
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        String price = formatter.format(value.GetPrice());
        Txt_Prodcut_price.setText(price);
        String s = value.GetDescription() + "";
        s = s.replaceAll("<br />", "\n");
        Txt_Prodcut_description.setText(s);
    }

    private void getKalaInfo(int id) {
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                Kala kala = (Kala) object;
                G.desc_save = kala.GetDescription();
                Document message = Jsoup.parse(kala.GetDescription());
                String s = kala.GetDescription() + "";
                s = s.replaceAll("<br />", "\n");
                Txt_Prodcut_description.setText(s);
                Txt_Prodcut_barand.setText(kala.GetkMarkName());
                Photos kImage = new Photos();
                kImage.SetPhotoAddress(kala.GetImage());
                kImage.SetAlbumID(0);
                kImage.SetPhotoID(0);
                photos.clear();
                if(kala.GetImage()!=null) {
                    photos.add(kImage);
                }
                photos.addAll(kala.GetPhotos());
                gallery.add(new ProductGallery(null, true));
                for (Photos item : photos) {
                    ProductGallery pgobject = new ProductGallery();
                    pgobject.IsAddButton = false;
                    pgobject.FromWeb = true;
                    pgobject.image = G.PhotoUrl + "/" + item.GetPhotoAddress();
                    pgobject._iId = item.GetPhotoID();
                    pgobject._iAlbum = item.GetAlbumID();
                    pgobject.kalaid = EditObject.GetID();
                    gallery.add(pgobject);
                }
                fill_list(gallery);
            }

            @Override
            public void OnFail(Object object) {
            }
        };
        new KalaRepository().GetKalaInfo(servicelistener, id);
    }

    public void save_image(Bitmap result) {
        G.b = result;
        Intent i = new Intent(ChangeProductActivity.this, CropActivity.class);
        i.putExtra("PhotoName", "sepehrproduct.jpg");
        startActivityForResult(i, 10);
    }

    public void save_image_croped(Bitmap result) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut;
        ImageName = "sepehrgroup" + gallery.size() + 1 + ".jpg";
        ImageUrl = path + "/" + ImageName;
        File file = new File(path, ImageName);
        try {

            fOut = new FileOutputStream(file);
            result.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush();
            fOut.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            ProductGallery pgobject = new ProductGallery();
            pgobject.FromWeb = false;
            pgobject.IsAddButton = false;
            pgobject.image = ImageUrl;
            gallery.add(pgobject);
            fill_list(gallery);
        } catch (IOException e) {
            G.log("error", e.getMessage());
        }
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            save_image(pickResult.getBitmap());
        }
    }
    private void select_google_phone() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangeProductActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.select_google_phone, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        LinearLayout lin_google = (LinearLayout) dialogView.findViewById(R.id.lin_google);
        LinearLayout lin_phone = (LinearLayout) dialogView.findViewById(R.id.lin_phone);
        TextView lbl_google = (TextView) dialogView.findViewById(R.id.lbl_google);
        TextView lbl_phone = (TextView) dialogView.findViewById(R.id.lbl_phone);
        lbl_google.setTypeface(Fonts.VazirBold);
        lbl_phone.setTypeface(Fonts.VazirBold);
        lin_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangeProductActivity.this, GoogleSearchActivity.class);
                intent.putExtra("kala_name", edt_Prodcut_Name.getText().toString() + "");
                startActivityForResult(intent, 12);
                b.dismiss();
            }
        });
        lin_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImagePicker();
                b.dismiss();
            }
        });
        b.show();
    }
}
