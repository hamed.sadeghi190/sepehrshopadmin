package com.sepehrcc.apps.sepehradmin.DbModels;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;

/**
 * Created by Hamed Sadeghi on 16/05/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
@Table(name = "ProductGroup")
public class ProductGroup extends Model {

    @Column(name = "GID")
    private int GID;
    @Column(name = "Description")
    private String Description;
    @Column(name = "GName")
    private String GName;
    @Column(name = "GImage")
    private String GImage;
    @Column(name = "GTImage")
    private String GTImage;
    @Column(name = "GParent")
    private int GParent;
    @Column(name = "GCKala")
    private int GCKala;
    @Column(name = "GCGroup")
    private int GCGroup;

    public int GetID() {
        return GID;
    }
    public void SetID(final int ID) {
        this.GID = ID;
    }
    public String GetDescription() {
        return Description;
    }
    public void SetDescription(final String description) {
        this.Description = description;
    }
    public String GetGName() {
        return GName;
    }
    public void SetGName(final String GName) {
        this.GName = GName;
    }
    public String GetGImage() {
        return GImage;
    }
    public void SetGImage(final String GImage) {
        this.GImage = GImage;
    }
    public String GetGTImage() {
        return GTImage;
    }
    public void SetGTImage(final String GTImage) {
        this.GTImage = GTImage;
    }
    public int GetGParent() {
        return GParent;
    }
    public void SetGParent(final int GParent) {
        this.GParent = GParent;
    }
    public int GetGCKala() {
        return GCKala;
    }
    public void SetGCKala(final int GCKala) {
        this.GCKala = GCKala;
    }
    public int GetGCGroup() {
        return GCGroup;
    }
    public void SetGCGroup(final int GCGroup) {
        this.GCGroup = GCGroup;
    }
    public ProductGroup() {
        super();
    }
    public ProductGroup(Group values) {
        super();
        SetID(values.GetID());
        SetDescription(values.GetDescription());
        SetGName(values.GetName());
        SetGImage(values.GetImage());
        SetGTImage(values.getThumbNail());
        SetGParent(values.GetParentID());
        SetGCKala(values.GetKalaCount());
        SetGCGroup(values.GetGroupSubCount());
    }
    public void  UpdateInfo(Group values) {
        SetID(values.GetID());
        SetDescription(values.GetDescription());
        SetGName(values.GetName());
        SetGImage(values.GetImage());
        SetGTImage(values.getThumbNail());
        SetGParent(values.GetParentID());
        SetGCKala(values.GetKalaCount());
        SetGCGroup(values.GetGroupSubCount());
    }
}
