package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.EditorFontSizeSpinnerAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import org.jsoup.Jsoup;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.richeditor.RichEditor;

public class ProductDescriptionActivity extends AppCompatActivity {

    @BindView(R.id.btn_Back)
    TextView btn_Back;
    @BindView(R.id.btn_send)
    TextView btn_send;
    @BindView(R.id.Txt_Prodcut_description_title)
    TextView Txt_title;
    @BindView(R.id.txt_rich_editor)
    RichEditor txt_rich_editor;
    @BindView(R.id.btn_action_bold)
    ImageView btn_action_bold;
    @BindView(R.id.btn_action_italic)
    ImageView btn_action_italic;
    @BindView(R.id.btn_action_underline)
    ImageView btn_action_underline;
    //    @BindView(R.id.spinner_action_font_size)
//    Spinner spinner_action_font_size;
//    @BindView(R.id.btn_action_txt_color)
//    ImageView btn_action_txt_color;
//    @BindView(R.id.btn_action_bg_color)
//    ImageView btn_action_bg_color;
    @BindView(R.id.btn_action_align_right)
    ImageView btn_action_align_right;
    @BindView(R.id.btn_action_align_center)
    ImageView btn_action_align_center;
    @BindView(R.id.btn_action_align_left)
    ImageView btn_action_align_left;
    @BindView(R.id.btn_action_insert_bullets)
    ImageView btn_action_insert_bullets;
    @BindView(R.id.btn_action_insert_numbers)
    ImageView btn_action_insert_numbers;
    @BindView(R.id.btn_action_insert_link)
    ImageView btn_action_insert_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_product_description);
        ButterKnife.bind(this);
        initialView();

    }

    private void initialView() {
        btn_Back.setTypeface(Fonts.VazirMedium);
        btn_send.setTypeface(Fonts.VazirMedium);
        Txt_title.setTypeface(Fonts.VazirBold);
        EditorFontSizeSpinnerAdapter editorFontSizeSpinnerAdapter = new EditorFontSizeSpinnerAdapter(ProductDescriptionActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.font_size_list)));
        // spinner_action_font_size.setAdapter(editorFontSizeSpinnerAdapter);
        // preview = (WebView) findViewById(R.id.lbl_preview);
//        txt_rich_editor.setEditorFontSize(8);

        txt_rich_editor.setPadding(10, 10, 10, 10);
        txt_rich_editor.setFocusable(true);
        txt_rich_editor.focusEditor();
        txt_rich_editor.setAlignRight();
        txt_rich_editor.setEditorFontColor(getResources().getColor(R.color.black));
        txt_rich_editor.loadCSS("file:///android_asset/CSS/demo.css");
        if (G.desc_save.length() > 0) {
            txt_rich_editor.setHtml(G.desc_save);
        }
//        if(getIntent().getStringExtra("rich_edit_text_content").length()>0){
//        txt_rich_editor.setHtml(getIntent().getStringExtra("rich_edit_text_content"));
//        }
    }

    @OnClick(R.id.btn_Back)
    public void btn_Back_click() {
      G.desc_save=  txt_rich_editor.getHtml();
        finish();
    }

    @OnClick(R.id.btn_action_bold)
    public void btn_action_bold_click() {
        txt_rich_editor.setBold();
    }

    @OnClick(R.id.btn_action_italic)
    public void btn_action_italic_click() {
        txt_rich_editor.setItalic();
    }

    @OnClick(R.id.btn_action_underline)
    public void btn_action_underline_click() {
        txt_rich_editor.setUnderline();
    }

    //    @OnClick(R.id.btn_action_heading)
//    public void btn_action_heading_click() {
//        txt_rich_editor.setHeading(1);
//    }
//    @OnClick(R.id.btn_action_txt_color)
//    public void btn_action_txt_color_click() {
//        select_font_color();
//    }
//    @OnClick(R.id.btn_action_bg_color)
//    public void btn_action_bg_color_click() {
//       select_back_color();
//    }
    @OnClick(R.id.btn_action_align_right)
    public void btn_action_align_right_click() {
        txt_rich_editor.setAlignRight();
    }

    @OnClick(R.id.btn_action_align_left)
    public void btn_action_align_left_click() {
        txt_rich_editor.setAlignLeft();

    }

    @OnClick(R.id.btn_action_align_center)
    public void btn_action_align_center_click() {
        txt_rich_editor.setAlignCenter();
    }

    @OnClick(R.id.btn_action_insert_bullets)
    public void btn_action_insert_bullets_click() {
        txt_rich_editor.setBullets();
    }

    @OnClick(R.id.btn_action_insert_numbers)
    public void btn_action_insert_numbers_click() {
        txt_rich_editor.setNumbers();
    }

    @OnClick(R.id.btn_action_insert_link)
    public void btn_action_insert_link_click() {
        insert_link();
    }

    @OnClick(R.id.btn_send)
    public void btn_send_click() {
        Intent intent = new Intent();
        String message = Jsoup.parse(txt_rich_editor.getHtml()).text();

        if (message.length() > 0) {
            intent.putExtra("rich_edit_text_html", message);
            G.desc_save = txt_rich_editor.getHtml();
            setResult(1, intent);
            finish();
        } else if (txt_rich_editor.getHtml().isEmpty()) {
            intent.putExtra("rich_edit_text_html", "");
            G.desc_save = "";
            setResult(1, intent);
            finish();
        }
        setResult(2, intent);
        finish();
    }

    @OnClick(R.id.btn_Back)
    public void btn_back_click() {
        Intent intent = new Intent();
        setResult(2, intent);
        finish();
    }

    public void select_back_color() {
        ColorPickerDialogBuilder colorPickerDialogBuilder = ColorPickerDialogBuilder.with(ProductDescriptionActivity.this);
        colorPickerDialogBuilder
                .setTitle("انتخاب رنگ پس زمینه متن")
                .initialColor(Color.WHITE)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {

                    }
                })
                .setPositiveButton("تایید", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {

                        txt_rich_editor.setTextBackgroundColor(selectedColor);
                    }
                })
                .setNegativeButton("رد", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        android.support.v7.app.AlertDialog builder = colorPickerDialogBuilder.build();
        builder.show();
        builder.getButton(DialogInterface.BUTTON_POSITIVE).setTypeface(Fonts.VazirThin);
        builder.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirThin);
        builder.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
        builder.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
        TextView title = (TextView) builder.findViewById(R.id.alertTitle);
//        LinearLayout top_panel=(LinearLayout)builder.findViewById(R.id.topPanel);
//        top_panel.setPadding(10,10,10,10);
        title.setTypeface(Fonts.VazirThin);

    }

    public void select_font_color() {
        ColorPickerDialogBuilder colorPickerDialogBuilder = ColorPickerDialogBuilder.with(ProductDescriptionActivity.this);
        colorPickerDialogBuilder
                .setTitle("انتخاب رنگ متن")
                .initialColor(Color.WHITE)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                    }
                })
                .setPositiveButton("تایید", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        txt_rich_editor.setTextColor(selectedColor);

                    }
                })
                .setNegativeButton("رد", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        android.support.v7.app.AlertDialog builder = colorPickerDialogBuilder.build();
        builder.show();
        builder.getButton(DialogInterface.BUTTON_POSITIVE).setTypeface(Fonts.VazirThin);
        builder.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirThin);
        builder.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
        builder.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
        TextView title = (TextView) builder.findViewById(R.id.alertTitle);
        title.setTypeface(Fonts.VazirThin);
//        LinearLayout top_panel=(LinearLayout)builder.findViewById(R.id.topPanel);
//        top_panel.setPadding(20,20,20,20);
    }

    public void insert_link() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(ProductDescriptionActivity.this);
        View dialog_view = li.inflate(R.layout.lst_editor_insert_link_dialog_view_item, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProductDescriptionActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(dialog_view);
        final TextView lbl_name = (TextView) dialog_view.findViewById(R.id.lbl_name);
        final TextView lbl_link = (TextView) dialog_view.findViewById(R.id.lbl_link);
        final EditText txt_link_name = (EditText) dialog_view.findViewById(R.id.txt_name);
        final EditText txt_link_link = (EditText) dialog_view.findViewById(R.id.txt_link);
        lbl_link.setTypeface(Fonts.VazirThin);
        lbl_name.setTypeface(Fonts.VazirThin);
        txt_link_link.setTypeface(Fonts.VazirThin);
        txt_link_name.setTypeface(Fonts.VazirThin);
        lbl_link.setTextSize(14);
        lbl_name.setTextSize(14);
        txt_link_link.setTextSize(14);
        txt_link_name.setTextSize(14);
        txt_link_link.setTextColor(Color.BLACK);
        txt_link_name.setTextColor(Color.BLACK);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("تایید",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (txt_link_link.getText().length() != 0 && txt_link_name.getText().length() != 0) {
                                    txt_rich_editor.insertLink(txt_link_link.getText().toString(), txt_link_name.getText().toString());

                                } else {
                                    Toast.makeText(ProductDescriptionActivity.this, "لینک اضافه نشد.لطفا تمام فیلد ها را با دقت پر کنید", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                .setNegativeButton("رد",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTypeface(Fonts.VazirThin);
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTypeface(Fonts.VazirThin);
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.BLACK);

    }

}
