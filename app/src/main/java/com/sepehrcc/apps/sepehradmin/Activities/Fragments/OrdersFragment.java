package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Order_list_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.ViewAllOrdersActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.OrderRepository;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrdersFragment extends Fragment {
    View view;
    @BindView(R.id.lin_order_header)
    LinearLayout lin_order_header;
    @BindView(R.id.txtOrders)
    TextView txtOrders;
    @BindView(R.id.lbl_all_orders)
    TextView lbl_all_orders;
    @BindView(R.id.lbl_more_order)
    TextView lbl_more_order;
    @BindView(R.id.rlt_new_orders)
    RelativeLayout rlt_new_orders;
    @BindView(R.id.rlt_new_orders_title)
    RelativeLayout rlt_new_orders_title;
    @BindView(R.id.lbl_new_orders)
    TextView lbl_new_orders;
    @BindView(R.id.lbl_all_new_orders)
    TextView lbl_all_new_orders;
    @BindView(R.id.list_new_orders)
    RecyclerView list_new_orders;
    @BindView(R.id.progress_new_orders)
    ProgressBar progress_new_orders;
    @BindView(R.id.rlt_orders_ready_to_sent)
    RelativeLayout rlt_orders_ready_to_sent;
    @BindView(R.id.rlt_order_ready_to_send_title)
    RelativeLayout rlt_order_ready_to_send_title;
    @BindView(R.id.lbl_orders_ready_to_sent)
    TextView lbl_orders_ready_to_sent;
    @BindView(R.id.lbl_order_ready_more)
    TextView lbl_order_ready_more;
    @BindView(R.id.list_ordrs_ready_to_sent)
    RecyclerView list_ordrs_ready_to_sent;
    @BindView(R.id.progress_ready_to_sent)
    ProgressBar progress_ready_to_sent;
    @BindView(R.id.rlt_all_orders)
    RelativeLayout rlt_all_orders;

    ArrayList<Order> orders = new ArrayList<Order>();
    ArrayList<Order> ready_to_sent_orders = new ArrayList<Order>();
    WebServicelistener servicelistener;
    public OrdersFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_orders_list, container, false);
        // getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ButterKnife.bind(this, view);
        init();
        BindViewControl();
        downloadOrder();
        DownloadReadyToSent();
        return view;
    }
    private void downloadOrder() {
        orders.clear();
        final OrderRepository repository = new OrderRepository();
        progress_new_orders.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    orders= (ArrayList<Order>) value;
                        progress_new_orders.setVisibility(View.GONE);
                        fillList();

                } catch (Throwable ignored) {
                }
            }
            @Override
            public void OnFail(Object object) {
                progress_new_orders.setVisibility(View.GONE);
            }
        };

            repository.GetOrders(servicelistener, 1,0, 3);




    }
    private void DownloadReadyToSent(){
        ready_to_sent_orders.clear();
        final OrderRepository repository = new OrderRepository();
        progress_ready_to_sent.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ready_to_sent_orders= (ArrayList<Order>) value;
                    progress_ready_to_sent.setVisibility(View.GONE);
                    fill_ready_to_sent_orders();

                } catch (Throwable ignored) {
                }
            }
            @Override
            public void OnFail(Object object) {
                progress_ready_to_sent.setVisibility(View.GONE);
            }
        };

        repository.GetOrders(servicelistener, 3,0, 10);



    }
    private void init() {
        txtOrders.setTypeface(Fonts.VazirBoldFD);
        lbl_all_orders.setTypeface(Fonts.VazirBoldFD);
        lbl_more_order.setTypeface(Fonts.VazirBoldFD);
        lbl_new_orders.setTypeface(Fonts.VazirBoldFD);
        lbl_all_new_orders.setTypeface(Fonts.VazirBoldFD);
        lbl_orders_ready_to_sent.setTypeface(Fonts.VazirBoldFD);
        lbl_order_ready_more.setTypeface(Fonts.VazirBoldFD);
        progress_new_orders.setVisibility(View.GONE);
        progress_ready_to_sent.setVisibility(View.GONE);
    }

    private void BindViewControl() {
        rlt_all_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), ViewAllOrdersActivity.class);
                intent.putExtra("list_type",0);
                startActivity(intent);
            }
        });
        lbl_all_new_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), ViewAllOrdersActivity.class);
                intent.putExtra("list_type",1);
                startActivity(intent);
            }
        });
        lbl_order_ready_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), ViewAllOrdersActivity.class);
                intent.putExtra("list_type",3);
                startActivity(intent);
            }
        });
    }

    private void fillList() {
//        orders.clear();
//        Order order = new Order();
//        order.SetOrderID(123);
//        order.SetOrderCode("456");
//        order.SetOrderCost("23000000000");
//        order.SetOrderState(1);
//        order.SetOrderUname("محمد رضایی");
//        orders.add(order);
//        order = new Order();
//        order.SetOrderID(123);
//        order.SetOrderCode("456");
//        order.SetOrderCost("23000000000");
//        order.SetOrderState(2);
//        order.SetOrderUname("محمد رضایی");
//        orders.add(order);
//        order = new Order();
//        order.SetOrderID(123);
//        order.SetOrderCode("456");
//        order.SetOrderCost("23000000000");
//        order.SetOrderState(4);
//        order.SetOrderUname("محمد رضایی");
//        orders.add(order);
        Order_list_Adapter order_list_adapter = new Order_list_Adapter(orders, getContext());
        list_new_orders.setAdapter(order_list_adapter);
        list_new_orders.setLayoutManager(new LinearLayoutManager(getContext()));
    }
    private void fill_ready_to_sent_orders(){
        Order_list_Adapter order_list_adapter = new Order_list_Adapter(ready_to_sent_orders, getContext());
        list_ordrs_ready_to_sent.setAdapter(order_list_adapter);
        list_ordrs_ready_to_sent.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}


//    private void init() {
//        Order_Edit_Search.setIconifiedByDefault(false);
//        Order_Edit_Search.setFocusable(false);
//        Order_Edit_Search.setIconified(true);
//        txtOrders.setTypeface(Fonts.VazirBold);
//        lbl_No_Orders.setTypeface(Fonts.VazirBold);
//        offset = 0;
//        limit = 10;
//        orders.clear();
//
//    }
//    private void downloadOrder() {
//        final OrderRepository repository = new OrderRepository();
//        lin_progress.setVisibility(View.VISIBLE);
//        servicelistener = new WebServicelistener() {
//            @Override
//            public void OnComplete(Object value) {
//                try {
//                    ArrayList<Order> tdata = (ArrayList<Order>) value;
//                    if (tdata==null)
//                    {
//                        loading = false;
//                        lin_progress.setVisibility(View.GONE);
//                        if(orders.size()==0) {
//                            rly_list.setVisibility(View.GONE);
//                        }
//                        else {
//                            rly_list.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    else if (totalcount == 0 && tdata.size() == 0) {
//                        loading = false;
//                        lin_progress.setVisibility(View.GONE);
//                    }
//                    else
//                        {
//                        if(offset==0)
//                        {
//                            orders.clear();
//                        }
//                        orders.addAll(tdata);
//                        totalcount += tdata.size();
//                        offset = totalcount - 1;
//                        if (tdata.size() < limit) {
//                            limit = 0;
//                            offset = 0;
//                        }
//                        fillList();
//                        loading = false;
//                        lin_progress.setVisibility(View.GONE);
//                    }
//                } catch (Throwable ignored) {
//                }
//            }
//            @Override
//            public void OnFail(Object object) {
//                lin_progress.setVisibility(View.GONE);
//            }
//        };
//        if (limit > 0) {
//            repository.GetOrders(servicelistener, offset, limit);
//        }
//
//        if (limit == 0) {
//            lin_progress.setVisibility(View.GONE);
//        }
//
//    }

//    private void fillList() {
//        adapter = new Order_list_Adapter(orders, getContext());
//        Order_list.setAdapter(adapter);
//        Order_list.scrollToPosition(pastVisiblesItems);
//        Order_list.setLayoutManager(mLayoutManager);
//        Order_list.scrollToPosition(pastVisiblesItems);
//    }

//    private void BindViewControl() {
//        Order_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0) //check for scroll down
//                {
//                    visibleItemCount = mLayoutManager.getChildCount();
//                    totalItemCount = mLayoutManager.getItemCount();
//                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                    if (!loading) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = true;
//                            if(Order_Edit_Search.getQuery().toString().equals(""))
//                            {
//                            downloadOrder();
//                            }else {
//                                search_order(Order_Edit_Search.getQuery().toString());
//                            }
//
//                        }
//                    }
//                }
//            }
//        });
//        Order_Edit_Search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                if (!newText.equals("")) {
//                    orders.clear();
//                    fillList();
//                    loading = true;
//                    offset = 0;
//                    limit = 10;
//                    totalcount = 0;
//                    pastVisiblesItems = 0;
//                    visibleItemCount = 0;
//                    totalItemCount = 0;
//                    search_order(newText);
//                } else {
//                    orders.clear();
//                    loading = true;
//                    offset = 0;
//                    limit = 10;
//                    totalcount = 0;
//                    pastVisiblesItems = 0;
//                    visibleItemCount = 0;
//                    totalItemCount = 0;
//                    downloadOrder();
//                }
//                return false;
//            }
//        });
//    }

//    private void search_order(String key) {
//        key=key.replace("/","-");
//        final OrderRepository repository = new OrderRepository();
//        lin_progress.setVisibility(View.VISIBLE);
//        servicelistener = new WebServicelistener() {
//            @Override
//            public void OnComplete(Object value) {
//                try {
//                    ArrayList<Order> tdata = (ArrayList<Order>) value;
//                    if (totalcount == 0 && tdata.size() == 0) {
//                        loading = false;
//                        lin_progress.setVisibility(View.GONE);
//                    } else {
//                        orders.addAll(tdata);
//                        totalcount += tdata.size();
//                        offset = totalcount - 1;
//                        if (tdata.size() < limit) {
//                            limit = 0;
//                            offset = 0;
//                        }
//                        fillList();
//                        loading = false;
//                        lin_progress.setVisibility(View.GONE);
//                    }
//                } catch (Throwable t) {
//                }
//            }
//            @Override
//            public void OnFail(Object object) {
//                lin_progress.setVisibility(View.GONE);
//            }
//        };
//        if (limit > 0) {
//            repository.SearchOrders(servicelistener, offset, limit,key);
//        }
//        if (limit == 0) {
//            lin_progress.setVisibility(View.GONE);
//        }
//    }