package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;


public interface IMark {
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("marks/list/{offset}/{limit}")
    Call<ArrayList<Mark>> GetMarks(@Header("Authorization") String AuthKey,@Path("offset") int offset,@Path("limit") int limit);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("marks/Search/{offset}/{limit}/{Key}")
    Call<ArrayList<Mark>> SearchMarks(@Header("Authorization") String AuthKey,@Path("offset") int offset,@Path("limit") int limit, @Path("Key") String Key);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("marks/add/{Brand}")
    Call<feedback> AddMarks(@Header("Authorization") String AuthKey, @Path("Brand") Mark Brand);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("marks/edit")
    Call<feedback> EditMark(@Header("Authorization") String AuthKey, @Body Mark mark);
}
