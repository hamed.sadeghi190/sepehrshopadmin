package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.EnhancedActivity;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.thefinestartist.finestwebview.FinestWebView;


import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends EnhancedActivity {
    @BindView(R.id.TxtMoto)
    TextView TxtMoto;
    @BindView(R.id.TxtMoto2)
    TextView TxtMoto2;
    @BindView(R.id.TxtAlert)
    TextView TxtAlert;
    @BindView(R.id.BtnLoginEmail)
    TextView BtnLoginEmail;
    @BindView(R.id.BtnCreateShop)
    Button BtnCreateShop;
    @BindView(R.id.BtnLogin)
    Button BtnLogin;
    @BindView(R.id.splash_video)
    VideoView splash_video;
    DisplayMetrics metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        String token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("Token", "");
        if (!token.equals("")) {
            if (G.Release) {
                String SubDomain = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("SubDomain", "");
            }
            long now=PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getLong("Login_time",0);
            long diff=System.currentTimeMillis()-PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getLong("Login_time",0);
            if(diff>600000|| G.appData.shopInfo.GetsubDomain()==null) {
                Intent intent = new Intent(this, LoadingSplashActivity.class);
                startActivity(intent);
                G.ApiToken = token;
                finish();
            }
            else{
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                G.ApiToken = token;
                finish();
            }
        }
        setContentView(R.layout.ly_activity_splash_screen);
        ButterKnife.bind(this);
        initialView();
        loadVideo();
        BindControlsEvents();
    }
    @Override
    protected void onResume() {
        super.onResume();
        loadVideo();
    }
    private void loadVideo() {
        metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Uri  uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash);
        splash_video.setMinimumWidth(width);
        splash_video.setMinimumHeight(height);
        splash_video.setVideoURI(uri);
        splash_video.start();
        splash_video.setSoundEffectsEnabled(false);
        splash_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setVolume(0f, 0f);
                mediaPlayer.setLooping(true);
            }
        });
    }
    private void BindControlsEvents() {
        BtnLoginEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashScreenActivity.this, LoginWithPhoneActivity.class);
                startActivity(intent);
                            }
        });
        BtnCreateShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SplashScreenActivity.this, RegisterWizardActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initialView() {
        TxtMoto.setTypeface(Fonts.Cocon_next_arabic_light);
        TxtMoto2.setTypeface(Fonts.Cocon_next_arabic_light);
        TxtAlert.setTypeface(Fonts.IranSans);
        BtnCreateShop.setTypeface(Fonts.Cocon_next_arabic_light);
        BtnLogin.setTypeface(Fonts.Cocon_next_arabic_light);
        BtnLoginEmail.setTypeface(Fonts.Cocon_next_arabic_light);
    }
}
