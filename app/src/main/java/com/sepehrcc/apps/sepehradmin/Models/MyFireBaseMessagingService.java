package com.sepehrcc.apps.sepehradmin.Models;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.ArrayMap;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.LoadingSplashActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.MainActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SplashScreenActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.thefinestartist.finestwebview.FinestWebView;

import java.lang.reflect.Type;
import java.util.Map;

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        NotificationModel notificationModel = new NotificationModel();
        Map data = remoteMessage.getData();
        try {
            Gson g=new Gson();
            Type collectionType = new TypeToken<NotificationModel>() {
            }.getType();
            notificationModel = g.fromJson(String.valueOf(data.get("data")),collectionType);
        } catch (Exception ignored) {
        }
        Notification notification;
        Intent notifyIntent = createIntent(Integer.parseInt(data.get("linkType").toString()),String.valueOf(data.get("url")));
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, notifyIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification.Builder(this)
                    .setContentTitle(String.valueOf(data.get("title")))
                    .setContentText(String.valueOf(data.get("text")))
                    .setSmallIcon(R.drawable.ic_sepehr_logo_splash)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true).build();
        } else {
            notification = new Notification.Builder(this)
                    .setContentTitle(String.valueOf(data.get("title")))
                    .setContentText(String.valueOf(data.get("text")))
                    .setSmallIcon(R.drawable.ic_sepehr_logo_splash)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true).getNotification();
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
    private Intent createIntent(int linktype,String link){
        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(10000);
        }
        Intent intent=new Intent(this, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        switch (linktype)
        {
            case 0:
                intent= new Intent(this, SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case 1:
                new FinestWebView.Builder(getApplicationContext()).show(link);
                break;
        }
        return intent;
    }
}
