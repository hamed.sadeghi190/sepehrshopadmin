package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DomainModel;


import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 08/05/2018.
 */

public class DomainListAdapter extends RecyclerView.Adapter<DomainListAdapter.DomainViewHolder> {
    List<DomainModel> list = Collections.emptyList();
    Context context;

    public DomainListAdapter(List<DomainModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public DomainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_domain_row, parent, false);
        DomainViewHolder domain = new DomainViewHolder(v);
        return domain;
    }

    @Override
    public void onBindViewHolder(DomainViewHolder holder, int position) {
        holder.lbl_domain_name.setText(list.get(position).getDomainName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class DomainViewHolder extends RecyclerView.ViewHolder {
        TextView lbl_domain_name;

        DomainViewHolder(View itemView) {
            super(itemView);
            lbl_domain_name = itemView.findViewById(R.id.lbl_domain_name);
            lbl_domain_name.setTypeface(Fonts.VazirBoldFD);
        }
    }
}

