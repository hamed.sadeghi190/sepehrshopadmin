package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;


public class Order {
    @SerializedName("ID")
    private int _ID;

    public int GetOrdrID() {
        return _ID;
    }

    public Order SetOrderID(int value) {
        _ID = value;
        return this;
    }

    @SerializedName("Code")
    private String _Code;

    public String GetOrdrCode() {
        return _Code;
    }

    public Order SetOrderCode(String value) {
        _Code = value;
        return this;
    }

    @SerializedName("Cost")
    private String _Cost;

    public String GetOrdrCost() {
        return _Cost;
    }

    public Order SetOrderCost(String value) {
        _Cost = value;
        return this;
    }
/*
* 1:jadid
* 2:taiid shode
* 3:amadeh ersal
* 4:ersal shode
* 5:kamel shaode
* 6:moalagh
* */
    @SerializedName("Status")
    private int _State;

    public int GetOrdrState() {
        return _State;
    }

    public Order SetOrderState(int value) {
        _State = value;
        return this;
    }

    @SerializedName("Uname")
    private String _Uname;

    public String GetOrdrUname() {
        return _Uname;
    }

    public Order SetOrderUname(String value) {
        _Uname = value;
        return this;
    }

    @SerializedName("Utel")
    private String _Utel;

    public String GetOrdrUtel() {
        return _Utel;
    }

    public Order SetOrderUtel(String value) {
        _Utel = value;
        return this;
    }
    @SerializedName("Utel2")
    private String _Utel2;

    public String GetOrdrUtel2() {
        return _Utel2;
    }

    public Order SetOrderUtel2(String value) {
        _Utel2 = value;
        return this;
    }
    @SerializedName("Payed")
    private int _Payed;

    public int GetOrdrPayed() {
        return _Payed;
    }

    public Order SetOrdrPayed(int value) {
        _Payed = value;
        return this;
    }
    @SerializedName("Date")
    private String _Date;

    public String GetOrdrDate() {
        if(_Date==null)
        {
            return "";
        }
        return _Date;
    }

    public Order SetOrderDate(String value) {
        _Date = value;
        return this;
    }
    @SerializedName("Media")
    private int _Media;

    public int GetOrdrMedia() {
        return _Media;
    }

    public Order SetOrdrMedia(int value) {
        _Media = value;
        return this;
    }
    @SerializedName("Source")
    private String _Source;

    public String GetOrdrSource() {
        return _Source;
    }

    public Order SetOrderSource(String value) {
        _Source = value;
        return this;
    }
    //0 normal
    //1 date
    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
