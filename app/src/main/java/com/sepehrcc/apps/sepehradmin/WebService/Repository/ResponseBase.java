package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.google.gson.annotations.SerializedName;


public class ResponseBase {
    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;

    public ResponseBase(int Status, String Message) {
        this.Status = Status;
        this.Message = Message;
    }

    public ResponseBase() {
    }
}
