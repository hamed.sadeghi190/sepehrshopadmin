package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;

import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 14/11/2017.
 */


public class KindListAdapter extends RecyclerView.Adapter<View_Holder_kind_list> {

    List<KindGroup> list = Collections.emptyList();
    Context context;

    public KindListAdapter(List<KindGroup> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder_kind_list onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_kindlist_row_themplate, parent, false);
        View_Holder_kind_list holder = new View_Holder_kind_list(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_kind_list holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.lbl_kind.setText(list.get(position).GetTitle());

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, KindGroup gallery) {
        list.add(position, gallery);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(KindGroup order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
    }

}