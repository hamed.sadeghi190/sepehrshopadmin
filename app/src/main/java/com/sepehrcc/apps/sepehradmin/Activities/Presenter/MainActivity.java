package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.Activity;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.GestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.CategorayListFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.ChooseProductCategoryFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.EmptyOrdersFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.FirstProductFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.HomePageFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.OrdersFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.ProductListFragment;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.SettingFragments;
import com.sepehrcc.apps.sepehradmin.Activities.Fragments.SettingInterface;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.EnhancedActivity;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends EnhancedActivity implements SettingInterface {
    RelativeLayout ActiveButton;
    @BindView(R.id.BtnSetting)
    RelativeLayout BtnSetting;
    @BindView(R.id.BtnOrders)
    RelativeLayout BtnOrders;
    @BindView(R.id.BtnProducts)
    RelativeLayout BtnProducts;
    @BindView(R.id.BtnHome)
    RelativeLayout BtnHome;

    @BindView(R.id.BtnSettingText)
    TextView BtnSettingText;
    @BindView(R.id.BtnOrdersText)
    TextView BtnOrdersText;
    @BindView(R.id.BtnProductsText)
    TextView BtnProductsText;
    @BindView(R.id.BtnHomeText)
    TextView BtnHomeText;

    @BindView(R.id.ImgSettingButton)
    ImageView ImgSettingButton;
    @BindView(R.id.ImgOrdersButton)
    ImageView ImgOrdersButton;
    @BindView(R.id.ImgProductsButton)
    ImageView ImgProductsButton;
    @BindView(R.id.ImgHomeButton)
    ImageView ImgHomeButton;

    @BindView(R.id.ripple_home)
    RippleView ripple_home;
    @BindView(R.id.ripple_order)
    RippleView ripple_order;
    //    @BindView(R.id.ripple_product)
//    RippleView ripple_product;
    @BindView(R.id.ripple_setting)
    RippleView ripple_setting;
    @BindView(R.id.lbl_badge)
    TextView lbl_badge;

    boolean close = false;

    //changes
    private HomePageFragment HomeFrg;
    private SettingFragments SettingFrg;
    private FirstProductFragment FirstProductFrg;
    private EmptyOrdersFragment emptyOrdersFrg;
    private ChooseProductCategoryFragment productListFrg;
    //    private ProductListFragment productListFrg;
    private OrdersFragment ordersFragment;
    private CategorayListFragment categoryfragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_main);
        G.CurrentActivity = this;
        init();
        ButterKnife.bind(this);
        ActiveButton = BtnHome;
        initialView();
        BindControlsEvents();

    }

    private void init() {
        HomeFrg = new HomePageFragment();
        SettingFrg = new SettingFragments();
        FirstProductFrg = new FirstProductFragment();
        emptyOrdersFrg = new EmptyOrdersFragment();
        productListFrg = new ChooseProductCategoryFragment();
        ordersFragment = new OrdersFragment();
        categoryfragment = new CategorayListFragment();
        loadFragment(HomeFrg);
    }

    private void initialView() {
        BtnSettingText.setTypeface(Fonts.VazirFD);
        BtnOrdersText.setTypeface(Fonts.VazirFD);
        BtnProductsText.setTypeface(Fonts.VazirFD);
        BtnHomeText.setTypeface(Fonts.VazirFD);
        lbl_badge.setTypeface(Fonts.VazirFD);
        if (G.appData.shopInfo.GetNewOrderCount() <= 99) {
            lbl_badge.setVisibility(View.VISIBLE);
            lbl_badge.setText(G.appData.shopInfo.GetNewOrderCount()+"");
        } else {
            lbl_badge.setVisibility(View.VISIBLE);
            lbl_badge.setText("+99");
        }
        if(G.appData.shopInfo.GetNewOrderCount()==0)
        {
            lbl_badge.setVisibility(View.GONE);
        }
    }

    private void BindControlsEvents() {
        ripple_home.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                ChangeActiveButton(BtnHome);
            }
        });
        BtnProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeActiveButton(BtnProducts);
            }
        });
//        ripple_product.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
//            @Override
//            public void onComplete(RippleView rippleView) {
//                ChangeActiveButton(BtnProducts);
//            }
//        });
        ripple_order.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                ChangeActiveButton(BtnOrders);
            }
        });
        ripple_setting.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                ChangeActiveButton(BtnSetting);
            }
        });
    }

    private void ChangeActiveButton(RelativeLayout Value) {
        if (ActiveButton.getId() == Value.getId())
            return;
        switch (Value.getId()) {
            case R.id.BtnHome:
                BtnHomeText.setTextColor(getResources().getColor(R.color.MenuPressed));
                BtnHomeText.setTextAppearance(this, R.style.MentTextBold);
                ImgHomeButton.setImageResource(R.drawable.ic_home_pressed);
                DisableActiveButton(BtnHome);
                loadFragment(HomeFrg);
                break;
            case R.id.BtnProducts:
                BtnProductsText.setTextColor(getResources().getColor(R.color.MenuPressed));
                BtnProductsText.setTextAppearance(this, R.style.MentTextBold);
                ImgProductsButton.setImageResource(R.drawable.ic_products_pressed);
                DisableActiveButton(BtnProducts);
                loadFragment(productListFrg);
                break;
            case R.id.BtnOrders:
                BtnOrdersText.setTextColor(getResources().getColor(R.color.MenuPressed));
                BtnOrdersText.setTextAppearance(this, R.style.MentTextBold);
                ImgOrdersButton.setImageResource(R.drawable.ic_shopping_cart_pressed);
                DisableActiveButton(BtnOrders);
                loadFragment(ordersFragment);
                break;
            case R.id.BtnSetting:
                BtnSettingText.setTextColor(getResources().getColor(R.color.MenuPressed));
                BtnSettingText.setTextAppearance(this, R.style.MentTextBold);
                ImgSettingButton.setImageResource(R.drawable.ic_settings_pressed);
                DisableActiveButton(BtnSetting);
                loadFragment(SettingFrg);
                break;
        }
        initialView();
    }

    private void DisableActiveButton(RelativeLayout value) {

        switch (ActiveButton.getId()) {
            case R.id.BtnHome:
                BtnHomeText.setTextColor(getResources().getColor(R.color.MenuNormal));
                BtnHomeText.setTextAppearance(this, R.style.MentTextNormal);
                ImgHomeButton.setImageResource(R.drawable.ic_home);
                break;
            case R.id.BtnProducts:
                BtnProductsText.setTextColor(getResources().getColor(R.color.MenuNormal));
                BtnProductsText.setTextAppearance(this, R.style.MentTextNormal);
                ImgProductsButton.setImageResource(R.drawable.ic_products);
                break;
            case R.id.BtnOrders:
                BtnOrdersText.setTextColor(getResources().getColor(R.color.MenuNormal));
                BtnOrdersText.setTextAppearance(this, R.style.MentTextNormal);
                ImgOrdersButton.setImageResource(R.drawable.ic_shopping_cart);
                break;
            case R.id.BtnSetting:
                BtnSettingText.setTextColor(getResources().getColor(R.color.MenuNormal));
                BtnSettingText.setTextAppearance(this, R.style.MentTextNormal);
                ImgSettingButton.setImageResource(R.drawable.ic_settings);
                break;
        }
        ActiveButton = value;
        initialView();
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment);
        fragmentTransaction.commit(); // save the changes

    }

    @Override
    public void onBackPressed() {
        if (close) {
            close = false;
            super.onBackPressed();
        } else {
            close = true;
            Toast.makeText(getApplicationContext(), "برای خروج دکمه برگشت را دو بار بزنید", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void OnClick1() {
        ChangeActiveButton(BtnSetting);
    }

    @Override
    protected void onResume() {
        super.onResume();
        G.CurrentActivity = this;
        G.CheckInfo();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

}
