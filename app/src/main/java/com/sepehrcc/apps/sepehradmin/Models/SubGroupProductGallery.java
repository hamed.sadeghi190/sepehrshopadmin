package com.sepehrcc.apps.sepehradmin.Models;

/**
 * Created by Administrator on 12/11/2017.
 */

public class SubGroupProductGallery {
    public int sub_group_product_image;
    public String sub_group_product_name;
    public SubGroupProductGallery(int sub_group_product_image,String sub_group_product_name)
    {
        this.sub_group_product_image=sub_group_product_image;
        this.sub_group_product_name=sub_group_product_name;
    }

}
