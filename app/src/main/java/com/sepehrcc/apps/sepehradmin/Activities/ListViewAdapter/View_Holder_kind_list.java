package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 18/11/2017.
 */

public class View_Holder_kind_list extends RecyclerView.ViewHolder {

    @BindView(R.id.lbl_kind)
    TextView lbl_kind;

    public View_Holder_kind_list(View itemview) {
        super(itemview);
        ButterKnife.bind(this, itemView);
        lbl_kind.setTypeface(Fonts.VazirBoldFD);
    }
}