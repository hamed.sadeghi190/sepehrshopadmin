package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.KindListAdapter;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Path;

import static com.sepehrcc.apps.sepehradmin.SharedClass.G.Upload_url;

public class KalaRepository {

    public void Add(final WebServicelistener lisitner, Kala value) {
        final AddResponse Resulat = new AddResponse();
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.KalaClient.AddNewProduct(G.ApiToken, value);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            URL url = response.raw().request().url();
                            Log.i("url_debug", url.toString());
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });

        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void Edit(final WebServicelistener lisitner, Kala value) {
        final AddResponse Resulat = new AddResponse();
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.KalaClient.EditProduct(G.ApiToken, value, value.GetID());
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            URL url = response.raw().request().url();
                            Log.i("url_debug", url.toString());
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });

        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void GetAllKala(final WebServicelistener lisitner, int Offset, int Limit) {
        final ArrayList<Kala> Resulat = new ArrayList<Kala>();
        try {
            final Call<ArrayList<Kala>> CallServer = WebServiceHandler.KalaClient.GetKala(G.ApiToken, Offset, Limit);
            CallServer.enqueue(new Callback<ArrayList<Kala>>() {
                @Override
                public void onResponse(Response<ArrayList<Kala>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Kala> products = response.body();
                                lisitner.OnComplete(products);
                            } else {
                                ArrayList<Kala> products = new ArrayList<Kala>();
                                lisitner.OnComplete(products);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });
        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void GetKalaInfo(final WebServicelistener lisitner, int id) {
        final ArrayList<Kala> Resulat = new ArrayList<Kala>();
        try {
            final Call<Kala> CallServer = WebServiceHandler.KalaClient.GetKalaInfo(G.ApiToken, id);
            CallServer.enqueue(new Callback<Kala>() {
                @Override
                public void onResponse(Response<Kala> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                Kala product = response.body();
                                lisitner.OnComplete(product);
                            } else {
                                Kala product = new Kala();
                                lisitner.OnComplete(product);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//
                }
            });
        } catch (Exception ex) {
//
        }
    }

    public void UploadPhoto(final WebServicelistener lisitner, String imageUrl, int PhotoID) {
        File file = new File(imageUrl);

        AndroidNetworking.upload(Upload_url + "FileUpload")
                .addMultipartFile("File", file)
                .setTag("uploadTest")
                .addHeaders("Type", "Kala")
                .addHeaders("Id", String.valueOf(PhotoID))
                .addHeaders("Authorization", G.ApiToken)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        lisitner.OnComplete(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        lisitner.OnFail(error);
                        // handle error
                    }
                });
    }

    public void GetKalaByGroup(final WebServicelistener lisitner, int GroupID, int Offset, int Limit) {
        final ArrayList<Kala> Resulat = new ArrayList<Kala>();
        try {
            final Call<ArrayList<Kala>> CallServer = WebServiceHandler.KalaClient.GetKalaByGroup(G.ApiToken, GroupID, Offset, Limit);
            CallServer.enqueue(new Callback<ArrayList<Kala>>() {
                @Override
                public void onResponse(Response<ArrayList<Kala>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            ArrayList<Kala> products = response.body();
                            lisitner.OnComplete(products);
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
//                    Groups.Status = ErrorHandler.Error;
//                    Groups.Message = ErrorHandler.ConnectFail;
//                    lisitner.OnFail(Groups);
                }
            });
        } catch (Exception ex) {
//            Groups.Status = ErrorHandler.Error;
//            Groups.Message = ErrorHandler.ConnectFail;
//            lisitner.OnFail(Groups);
        }
    }

    public void search(final WebServicelistener lisitner, int Offset, int Limit, String key) {

        final ArrayList<Kala> Resulat = new ArrayList<Kala>();
        try {
            final Call<ArrayList<Kala>> CallServer = WebServiceHandler.KalaClient.Search(G.ApiToken, Offset, Limit, key);
            CallServer.enqueue(new Callback<ArrayList<Kala>>() {
                @Override
                public void onResponse(Response<ArrayList<Kala>> response, Retrofit retrofit) {
                    int a = 0;
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            URL url = response.raw().request().url();
                            Log.i("url_debug", url.toString());
                            ArrayList<Kala> kalas = response.body();
                            lisitner.OnComplete(kalas);
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    lisitner.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {
            int a = 0;
        }
    }

    public void DeletePhoto(final WebServicelistener lisitner, int id, int pid, String type) {
        final ResponseBase Resulat = new ResponseBase();
        try {
            final Call<ResponseBase> CallServer = WebServiceHandler.KalaClient.DeletePhoto(G.ApiToken, id, pid, type);
            CallServer.enqueue(new Callback<ResponseBase>() {
                @Override
                public void onResponse(Response<ResponseBase> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    int a = 0;
                }
            });

        } catch (Exception ex) {
        }
    }

    public void Delete(final WebServicelistener lisitner, int id) {
        final ResponseBase Resulat = new ResponseBase();
        try {
            final Call<ResponseBase> CallServer = WebServiceHandler.KalaClient.DeleteProduct(G.ApiToken, id);
            CallServer.enqueue(new Callback<ResponseBase>() {
                @Override
                public void onResponse(Response<ResponseBase> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    int a = 0;
                }
            });

        } catch (Exception ex) {
            int a = 0;
        }
    }

    public void GetAllForCache(final WebServicelistener lisitner) {
        try {
            final Call<ArrayList<Kala>> CallServer = WebServiceHandler.KalaClient.GetKalaForCache(G.ApiToken);
            CallServer.enqueue(new Callback<ArrayList<Kala>>() {
                @Override
                public void onResponse(Response<ArrayList<Kala>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Kala> products = response.body();
                                lisitner.OnComplete(products);
                            } else {
                                ArrayList<Kala> products = new ArrayList<Kala>();
                                lisitner.OnComplete(products);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, t.getMessage());
                    lisitner.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {
            ResponseBase ErrorModel = new ResponseBase(500, ex.getMessage());
            lisitner.OnFail(ErrorModel);
        }
    }
}
