package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.LoginResualt;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Register;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ShopTable;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignupVerifyActivity extends AppCompatActivity {

    CountDownTimer cdt;
    String Action;
    @BindView(R.id.lbl_time_counter)
    TextView lbl_time_counter;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.txt_phonenumber)
    TextView txt_phonenumber;
    @BindView(R.id.txt_sendcode_alert)
    TextView txt_sendcode_alert;
    Register RegisterObj;
    String pin = "";
    String phonenumber;
    PinEntryEditText pinEntry;
    boolean SendRetry = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_signup_verify);
        ButterKnife.bind(this);
        G.CurrentActivity = this;

        initclick();
        initview();
        registerReceiver(broadcastReceiver, new IntentFilter("broadCastMessage"));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            phonenumber = bundle.getString("phone");
            Action = bundle.getString("action");
            pin = bundle.getString("pin");
            String info = bundle.getString("info");
            Gson gson = new Gson();
            RegisterObj = gson.fromJson(info, Register.class);
            txt_phonenumber.setText(phonenumber);
        }

        pinEntry = findViewById(R.id.txt_pin_entry);
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(pinEntry.getWindowToken(), 0);

                    if (Action.equals("login")) {
                        DoLogin(pinEntry.getText().toString());
                    } else {
                        DoReigster(pinEntry.getText().toString());
                    }
                }
            });
        }
        StartTimer();
    }

    private void StartTimer() {
        cdt = new CountDownTimer(120000, 1000) {
            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                long Time = millisUntilFinished / 1000;
                long Minutes = Time / 60;
                long Secounds = Time - (Minutes * 60);
                if (Minutes == 0)
                    lbl_time_counter.setText("00:" + Secounds);
                else if (Secounds < 10)
                    lbl_time_counter.setText(Minutes + ":0" + Secounds);
                else
                    lbl_time_counter.setText(Minutes + ":" + Secounds);
            }

            public void onFinish() {
                lbl_time_counter.setText("  ارسال مجدد ");
                SendRetry = true;
            }
        }.start();
    }

    private void DoReigster(String epin) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SignupVerifyActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.custom_loading, null);
        builder.setView(dialoglayout);
        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.show();
        WebServicelistener listener = new WebServicelistener() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void OnComplete(Object object) {
                dialog.dismiss();
                feedback fback = (feedback) object;
                if (fback.Status == 1) {
                    LoginResualt fback2 = new Gson().fromJson(fback.value, LoginResualt.class);
                    if (fback2.GetToken() != null) {
                        G.ApiToken = "Bearer " + fback2.GetToken();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Token", G.ApiToken).commit();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("TokenDate", fback2.GetValidTo()).commit();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("SubDomain", fback2.GetSubDomain()).commit();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("UserFullName", fback2.GetUserFullName()).commit();
                        G.BaseUrl = "http://www." + fback2.GetSubDomain() + ".sepehrcc.com/adminapi/";
                        G.PhotoUrl = "http://www." + fback2.GetSubDomain() + ".sepehrcc.com";
                        G.Seturls();
                        Intent intent = new Intent(G.CurrentActivity, LoadingSplashActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(G.CurrentActivity, "token null", Toast.LENGTH_LONG).show();
                    }
                } else if (fback.Status == 72) {
                    dialog.dismiss();
                    new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText("این شماره قبلا ثبت شده است")
                            .show();
                } else {
                    dialog.dismiss();
                    new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText(fback.Message)
                            .show();
                }
            }

            @Override
            public void OnFail(Object object) {
                feedback fback = (feedback) object;
                dialog.dismiss();
                new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("خطا")
                        .setContentText(fback.Message)
                        .show();

            }
        };
        RegisterObj.SetPin(epin);
        new SecurityRepository().SendVerifyRegister(listener, RegisterObj);
    }


    private void DoLogin(String epin) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SignupVerifyActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.custom_loading, null);
        builder.setView(dialoglayout);
        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.show();

        WebServicelistener listener = new WebServicelistener() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void OnComplete(Object object) {
                dialog.dismiss();
                feedback fback = (feedback) object;
                if (fback.Status == 1) {
                    if (!fback.exception.isEmpty()) {
                        List<ShopTable> shops;
                        shops = Arrays.asList(new Gson().fromJson(fback.exception, ShopTable[].class));
                        if (shops.size() > 0) {
                            dialog.dismiss();
                            Type collectionType = new TypeToken<ArrayList<ShopTable>>() {
                            }.getType();
                            final ArrayList<ShopTable> shoplist = new Gson().fromJson(fback.exception, collectionType);

                            String[] items = new String[shoplist.size()];
                            for (int i=0;i<shoplist.size();i++)
                            {
                                items[i]=shoplist.get(i).STitle;
                            }
                            new MaterialDialog.Builder(G.CurrentActivity)
                                    .title(R.string.Select)
                                    .items(items)
                                    .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                        @Override
                                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                            ShopTable item = shoplist.get(which);
                                            logiWithSelectedShop(item.SId, phonenumber, item.SUname);
                                            return true;
                                        }
                                    })
                                    .titleGravity(GravityEnum.CENTER)
                                    .itemsGravity(GravityEnum.END)
                                    .contentGravity(GravityEnum.END)
                                    .titleColorRes(R.color.md_purple_400)
                                    .typeface(Fonts.VazirMedium,Fonts.Vazir)
                                    .positiveText("ورود")
                                    .show();

                        } else {
                            dialog.dismiss();
                            Toast.makeText(G.CurrentActivity, "token null", Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    dialog.dismiss();
                    Toast.makeText(G.CurrentActivity, fback.Message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void OnFail(Object object) {
                int x = 0;
                ResponseBase response = (ResponseBase) object;
                dialog.dismiss();
                Toast.makeText(G.CurrentActivity, response.Message, Toast.LENGTH_LONG).show();
            }
        };
        new SecurityRepository().SendVerifylogin(listener, phonenumber, epin);

    }

    private void initview() {
        txt_phonenumber.setTypeface(Fonts.IranSans);
        txt_sendcode_alert.setTypeface(Fonts.IranSans);
        lbl_time_counter.setTypeface(Fonts.IranSans);
        btn_login.setTypeface(Fonts.IranSans);
    }

    private void initclick() {

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (pinEntry.getText().length() == 4) {
                    if (Action.equals("login")) {
                        DoLogin(pinEntry.getText().toString());
                    } else {
                        DoReigster(pinEntry.getText().toString());
                    }
                }
            }
        });
        lbl_time_counter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SendRetry) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignupVerifyActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    View dialoglayout = inflater.inflate(R.layout.custom_loading, null);
                    builder.setView(dialoglayout);
                    final AlertDialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    Window window = dialog.getWindow();
                    if (window != null) {
                        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    }
                    WebServicelistener listener = new WebServicelistener() {
                        @SuppressLint("ApplySharedPref")
                        @Override
                        public void OnComplete(Object object) {
                            dialog.dismiss();
                            feedback fback = (feedback) object;
                            if (fback.Status == 1) {
                                pin = fback.value;
                                StartTimer();
                                Toast.makeText(G.CurrentActivity, fback.Message, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(G.CurrentActivity, fback.Message, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void OnFail(Object object) {
                            dialog.dismiss();
                        }
                    };
                    new SecurityRepository().SendRetryCode(listener, phonenumber);

                }
            }
        });
    }



    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            assert b != null;
            String message = b.getString("message");
            pinEntry.setText(message);
        }
    };
    private void logiWithSelectedShop(int sub, String phone, String ucode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(G.CurrentActivity);
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.custom_loading, null);
        builder.setView(dialoglayout);
        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.show();

        WebServicelistener listener = new WebServicelistener() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void OnComplete(Object object) {
                feedback fback = (feedback) object;
                if (fback.Status == 1) {
                    LoginResualt LoginFeedBack = new Gson().fromJson(fback.value, LoginResualt.class);
                    if (LoginFeedBack.GetToken() != null) {
                        G.ApiToken = "Bearer " + LoginFeedBack.GetToken();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("Token", G.ApiToken).commit();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("TokenDate", LoginFeedBack.GetValidTo()).commit();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("SubDomain", LoginFeedBack.GetSubDomain()).commit();
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("UserFullName", LoginFeedBack.GetUserFullName()).commit();
                        G.BaseUrl = "http://www." + LoginFeedBack.GetSubDomain() + ".sepehrcc.com/adminapi/";
                        G.PhotoUrl = "http://www." + LoginFeedBack.GetSubDomain() + ".sepehrcc.com";
                        G.Seturls();
                        dialog.dismiss();
                        Intent intent = new Intent(G.CurrentActivity, LoadingSplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        dialog.dismiss();
                        Toast.makeText(G.CurrentActivity, fback.Message, Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void OnFail(Object object) {
                dialog.dismiss();
            }
        };
        new SecurityRepository().DoLogin(listener, sub, phone, ucode);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
