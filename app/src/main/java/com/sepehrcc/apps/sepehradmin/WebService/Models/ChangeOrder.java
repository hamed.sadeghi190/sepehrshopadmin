package com.sepehrcc.apps.sepehradmin.WebService.Models;



public class ChangeOrder {
    public int ID;
    public int Status;
    public String Yadasht;
    public String Rahgiry;
    public  String StatusText;

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getYadasht() {
        return Yadasht;
    }

    public void setYadasht(String yadasht) {
        Yadasht = yadasht;
    }

    public String getRahgiry() {
        return Rahgiry;
    }

    public void setRahgiry(String rahgiry) {
        Rahgiry = rahgiry;
    }
}
