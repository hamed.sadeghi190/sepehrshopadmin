package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 24/04/2018.
 */

public class Posts {
    @SerializedName("dId")
    private int dId;

    public int  GetdId() {
        return dId;
    }
    public Posts SetdId(int value) {
        dId = value;
        return this;
    }
    @SerializedName("dName")
    private String dName;

    public String  GetdName() {
        return dName;
    }
    public Posts SetdName(String value) {
        dName = value;
        return this;
    }
    @SerializedName("dDesc")
    private String dDesc;

    public String  GetdDesc() {
        return dDesc;
    }
    public Posts SetdDesc(String value) {
        dDesc = value;
        return this;
    }

    @SerializedName("dAmount")
    private int dAmount;

    public int  GetdAmount() {
        return dAmount;
    }
    public Posts SetdAmount(int value) {
        dAmount = value;
        return this;
    }

    @SerializedName("dNum")
    private int dNum;

    public int  GetdNum() {
        return dNum;
    }
    public Posts SetdNum(int value) {
        dNum = value;
        return this;
    }
    @SerializedName("dEnable")
    private Boolean dEnable;

    public Boolean  GetdEnable() {
        return dEnable;
    }
    public Posts SetdEnable(Boolean value) {
        dEnable = value;
        return this;
    }
    @SerializedName("sub")
    private int sub;

    public int  Getsub() {
        return sub;
    }
    public Posts Setsub(int value) {
        sub = value;
        return this;
    }
    @SerializedName("dMethod")
    private int dMethod;

    public int  GetdMethod() {
        return dMethod;
    }
    public Posts SetdMethod(int value) {
        dMethod = value;
        return this;
    }

    @SerializedName("dFreeShipingLimit")
    private int dFreeShipingLimit;

    public int  GetdFreeShipingLimit() {
        return dFreeShipingLimit;
    }
    public Posts SetdFreeShipingLimit(int value) {
        dFreeShipingLimit = value;
        return this;
    }

}
