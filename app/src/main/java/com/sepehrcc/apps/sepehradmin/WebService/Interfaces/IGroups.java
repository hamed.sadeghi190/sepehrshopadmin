package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.AddGroupResult;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;


public interface IGroups {


    @Headers({
            "content-type: application/json",
            "Accept:application/json",

    })
    @GET("group/listall")
    Call<ArrayList<Group>> GetAllGroups(@Header("Authorization") String AuthKey);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",

    })
    @GET("group/list/{GroupID}/{Offset}/{Limit}")
    Call<ArrayList<Group>> GetSubGroups(@Header("Authorization") String AuthKey,@Path("GroupID") int GroupID, @Path("Offset") int Offset, @Path("Limit") int Limit);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("group/add")
    Call<AddResponse> AddNewGroup(@Header("Authorization") String AuthKey, @Body Group value);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("group/edit")
    Call<AddResponse> EditGroup(@Header("Authorization") String AuthKey,@Body Group value);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("group/delete/{GroupID}")
    Call<ResponseBase> DeleteGroup(@Header("Authorization") String AuthKey, @Path("GroupID") int GroupID);

    @Headers({
            "content-type: multipart/form-data",
            "Accept:application/json",
            "Type:Group",
    })
    @Multipart
    @POST("FileUpload")
    Call<AddGroupResult> UploadGroupImage(@Header("Authorization") String AuthKey,@Part("Id") String id , @Part("File") RequestBody name);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("group/search/{parentid}/{Offset}/{limit}/{key}")
    Call<ArrayList<Group>> Search(@Header("Authorization") String AuthKey,@Path("parentid") int ParentID, @Path("Offset") int Offset, @Path("limit") int Limit,@Path("key") String Key);
}
