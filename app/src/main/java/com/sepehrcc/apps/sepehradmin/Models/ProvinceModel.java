package com.sepehrcc.apps.sepehradmin.Models;

import java.util.ArrayList;

/**
 * Created by Administrator on 03/06/2018.
 */

public class ProvinceModel {
    int ID;
    String Name;
    ArrayList<CityModel> Citites = new ArrayList<CityModel>();

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ArrayList<CityModel> getCitites() {
        return Citites;
    }

    public void setCitites(ArrayList<CityModel> citites) {
        Citites = citites;
    }
}
