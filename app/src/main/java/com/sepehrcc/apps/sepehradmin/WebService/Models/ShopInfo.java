package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Administrator on 29/11/2017.
 */

public class ShopInfo {

    @SerializedName("GroupCount")
    private int _GroupCount ;
    public int GetGroupCount () { return _GroupCount; }
    public ShopInfo SetGroupCount (int value) {_GroupCount = value;return this;}

    @SerializedName("KalaCount")
    private int _KalaCount  ;
    public int GetKalaCount  () { return _KalaCount ; }
    public ShopInfo SetKalaCount  (int value) {_KalaCount  = value;return this;}

    @SerializedName("OrderCount")
    private int _OrderCount;
    public int GetOrderCount() { return _OrderCount ; }
    public ShopInfo SetOrderCount(int value) {_OrderCount  = value;return this;}

    @SerializedName("NewOrderCount")
    private int _NewOrderCount;
    public int GetNewOrderCount() { return _NewOrderCount ; }
    public ShopInfo SetNewOrderCount(int value) {_NewOrderCount  = value;return this;}


    public boolean  GetDomainRegistr () {
        return _Domain != null && _Domain.contains(".");
    }

    @SerializedName("paymentMethodCount")
    private int _paymentMethodCount;
    public int GetpaymentMethodCount() { return _paymentMethodCount ; }
    public ShopInfo SetPaymentMethodCount(int value) {_paymentMethodCount  = value;return this;}

    @SerializedName("SendMethodCount")
    private int _SendMethodCount;
    public int GetSendMethodCount() { return _SendMethodCount ; }
    public ShopInfo SetSendMethodCount(int value) {_SendMethodCount  = value;return this;}

    @SerializedName("Domain")
    private String _Domain;
    public String GetDomain() { return _Domain ; }
    public ShopInfo SetDomain(String value) {_Domain  = value;return this;}

    @SerializedName("subDomain")
    private String _subDomain;
    public String GetsubDomain() { return _subDomain ; }
    public ShopInfo SetsubDomain(String value) {_subDomain  = value;return this;}

    @SerializedName("RegisterDate")
    private String RegisterDate;
    public String GetRegisterDate() { return RegisterDate ; }
    public ShopInfo SetRegisterDate(String value) {RegisterDate  = value;return this;}
    @SerializedName("ExpireDate")
    private String ExpireDate;
    public String GetExpireDate() { return ExpireDate ; }
    public ShopInfo SetExpireDate(String value) {ExpireDate  = value;return this;}

    @SerializedName("PlanName")
    private String PlanName;
    public String GetPlanName() { return PlanName ; }
    public ShopInfo SetPlanName(String value) {PlanName  = value;return this;}

    @SerializedName("Percent")
    private int Percent  ;
    public int GetPercent  () { return Percent ; }
    public ShopInfo SetPercent  (int value) {Percent  = value;return this;}

    @SerializedName("Plan")
    private int Plan  ;
    public int GetPlan  () { return Plan ; }
    public ShopInfo SetPlan  (int value) {Plan  = value;return this;}
    @SerializedName("ApiKey")
    private String ApiKey;
    public String GetApiKey() { return ApiKey ; }
    public ShopInfo SetApiKey(String value) {ApiKey  = value;return this;}

    @SerializedName("SecretKey")
    private String SecretKey;
    public String GetSecretKey() { return SecretKey ; }
    public ShopInfo SetSecretKey(String value) {SecretKey  = value;return this;}

    @SerializedName("Today")
    private String Today;
    public String GetToday() { return Today ; }
    public ShopInfo SetToday(String value) {Today  = value;return this;}

    @SerializedName("Docs")
    private ArrayList<DocTable> Docs = new ArrayList<>();
    public ArrayList<DocTable> getDocs() {
        return Docs;
    }



}
