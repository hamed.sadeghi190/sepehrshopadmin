package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import java.util.ArrayList;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

public interface IBanks {
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Shops/banks")
    Call<ArrayList<Bank>> GetBanks(@Header("Authorization") String AuthKey);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Shops/banks/edit")
    Call<AddResponse> MultiEditBanks(@Header("Authorization") String AuthKey, @Body ArrayList<Bank> value);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Shops/banks/edit/{id}")
    Call<AddResponse> MonoEditBanks(@Header("Authorization") String AuthKey, @Body Bank value, @Path("id") int id);


}
