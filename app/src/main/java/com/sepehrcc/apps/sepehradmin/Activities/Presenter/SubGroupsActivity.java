package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.icu.util.BuddhistCalendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.gmail.samehadar.iosdialog.IOSDialog;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Categoray_list_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Recycler_View_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.SubGroup_List_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Sub_Group_Product_Gallery_Adapter;
import com.sepehrcc.apps.sepehradmin.DbModels.ProductGroup;
import com.sepehrcc.apps.sepehradmin.Models.Category;
import com.sepehrcc.apps.sepehradmin.Models.SubGroup;
import com.sepehrcc.apps.sepehradmin.Models.SubGroupProductGallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Tools;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.GroupRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.KalaRepository;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SubGroupsActivity extends AppCompatActivity {
    @BindView(R.id.txt_sub_group)
    TextView txt_sub_group;
    @BindView(R.id.sub_group_search_view)
    SearchView sub_group_search_view;
    @BindView(R.id.sub_group_list)
    RecyclerView sub_group_list;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    @BindView(R.id.subgroup_tilte)
    TextView subgroup_tilte;
    @BindView(R.id.subgroup_modify)
    TextView subgroup_modify;
    @BindView(R.id.img_group_photo)
    ImageView img_group_photo;
    @BindView(R.id.subgroup_product_count)
    TextView subgroup_product_count;
    @BindView(R.id.subgroup_product_title)
    TextView subgroup_product_title;
    @BindView(R.id.subgroup_product_more)
    TextView subgroup_product_more;
    @BindView(R.id.subgroup_subgroup_count)
    TextView subgroup_subgroup_count;
    @BindView(R.id.subgroup_subgroup_title)
    TextView subgroup_subgroup_title;
    @BindView(R.id.subgroup_subgroup_more)
    TextView subgroup_subgroup_more;
    @BindView(R.id.lbl_subgroups_subgroup_title)
    TextView lbl_subgroups_subgroup_title;
    @BindView(R.id.add_menu)
    FloatingActionMenu add_menu;
    @BindView(R.id.fab_adding_group)
    FloatingActionButton fab_adding_group;
    @BindView(R.id.fab_adding_product)
    FloatingActionButton fab_adding_product;
    @BindView(R.id.determinateBar)
    ProgressBar determinateBar;
    @BindView(R.id.lin_kala)
    LinearLayout lin_kala;
    // SubGroup_List_Adapter adapter;
    Sub_Group_Product_Gallery_Adapter gallery_adapter;
    //List<SubGroup> data;
    List<SubGroupProductGallery> gallery_data;
    Categoray_list_Adapter adapter;
    ArrayList<Group> data = new ArrayList<>();
    ArrayList<Kala> Kaladata = new ArrayList<>();
    WebServicelistener servicelistener;
    LinearLayoutManager mLayoutManager;
    Recycler_View_Adapter kalaadapter;
    boolean IsForResualt = false;
    int offset = 0;
    int limit = 10;
    int totalcount;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int groupid = 0;
    Group currentGroup = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_sub_gruops);
        G.CurrentActivity = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);
        mLayoutManager = new LinearLayoutManager(this);
        Bundle Extras = getIntent().getExtras();
        if (Extras != null) {
            if (Extras.containsKey("IsForResualt")) {
                IsForResualt = Extras.getBoolean("IsForResualt");
            }
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            groupid = bundle.getInt("groupid", 0);
            String Citem = bundle.getString("group", "");
            if (!(Citem.equals(""))) {
                Gson gson = new Gson();
                try {
                    currentGroup = gson.fromJson(Citem, Group.class);
                } catch (Exception ignored) {
                }
            }

        }
        GetDataFromLocalDB();
        init();
        BindControlsEvents();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void fill_list(ArrayList<Group> data) {
        adapter = new Categoray_list_Adapter(data, this, IsForResualt);
        sub_group_list.setAdapter(adapter);
        sub_group_list.setLayoutManager(mLayoutManager);
    }

    private void init() {
        sub_group_search_view.setIconifiedByDefault(false);
        sub_group_search_view.setFocusable(false);
        sub_group_search_view.setIconified(true);
        txt_sub_group.setTypeface(Fonts.VazirBold);
        subgroup_tilte.setTypeface(Fonts.VazirBold);
        subgroup_modify.setTypeface(Fonts.VazirBold);
        subgroup_product_count.setTypeface(Fonts.VazirBoldFD);
        subgroup_product_title.setTypeface(Fonts.VazirBoldFD);
        subgroup_product_more.setTypeface(Fonts.VazirBoldFD);
        subgroup_subgroup_count.setTypeface(Fonts.VazirBoldFD);
        subgroup_subgroup_title.setTypeface(Fonts.VazirBoldFD);
        subgroup_subgroup_more.setTypeface(Fonts.VazirBoldFD);
        lbl_subgroups_subgroup_title.setTypeface(Fonts.VazirBoldFD);
        sub_group_search_view.requestFocus();
        subgroup_subgroup_count.setText(currentGroup.GetGroupSubCount() + "");
        subgroup_product_count.setText(currentGroup.GetKalaCount() + "");
        fill_list(data);
        subgroup_tilte.setText(getIntent().getStringExtra("groupname"));
        try {
            String url = G.PhotoUrl + currentGroup.GetImage();
            url = url.replaceAll(" ", "%20");
            Picasso.with(getApplicationContext())
                    .load(url)
                    .placeholder(R.drawable.ic_no_photos)
                    .error(R.drawable.ic_no_photos)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(img_group_photo);
        } catch (Exception e) {
        }

    }

    private void BindControlsEvents() {
        sub_group_search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    data.clear();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    searchGroup(newText);
                } else {
                    data.clear();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    GetDataFromLocalDB();
                }


                return false;
            }
        });

        mainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                return false; //*SEE NOTE BELOW

            }
        });
        fab_adding_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_menu.close(false);
                Intent intent = new Intent(SubGroupsActivity.this, AddSubGroupActivity.class);
                intent.putExtra("groupid", getIntent().getIntExtra("groupid", 0));
                startActivity(intent);


            }
        });
        fab_adding_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_menu.close(false);
                Intent intent = new Intent(SubGroupsActivity.this, AddProductActivity.class);
                startActivity(intent);

            }
        });
        sub_group_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;

                            if (sub_group_search_view.getQuery().toString().equals("")) {
                                GetDataFromLocalDB();
                            } else {
                                searchGroup(sub_group_search_view.getQuery().toString());
                            }
                        }
                    }
                }
            }
        });
        subgroup_modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SubGroupsActivity.this, ChangeGroupActivity.class);
                Gson gson = new Gson();
                String jsonstring = gson.toJson(currentGroup);
                intent.putExtra("group", jsonstring);
                startActivity(intent);
                finish();
            }
        });
        lin_kala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SubGroupsActivity.this, ProductListActivity.class);
                i.putExtra("groupID", currentGroup.GetID());
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", result);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        } else {

        }
    }

    private void GetDataFromLocalDB() {
        if (G.isNetworkAvailable()) {
            determinateBar.setVisibility(View.VISIBLE);
            if (limit > 0) {
                List<ProductGroup> LocalData = new Select().from(ProductGroup.class).where("GParent=" + groupid).offset(offset).limit(limit).execute();
                ArrayList<Group> tdata = new ArrayList<>();
                for (ProductGroup Gitem : LocalData) {
                    tdata.add(new Group(Gitem));
                }
                if (totalcount == 0 && tdata.size() == 0) {
                    loading = false;
                    determinateBar.setVisibility(View.GONE);
                } else {
                    data.addAll(tdata);
                    totalcount += tdata.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                        offset = 0;
                    }
                    fill_list(data);
                    loading = false;
                    determinateBar.setVisibility(View.GONE);
                }
            }
            if (limit == 0) {
                determinateBar.setVisibility(View.GONE);
            }

        } else if (!G.isNetworkAvailable()) {
            final SweetAlertDialog pDialog1 = new SweetAlertDialog(SubGroupsActivity.this, SweetAlertDialog.ERROR_TYPE);
            pDialog1
                    .setTitleText("خطا!")
                    .setContentText("شما به اینترنت متصل نیستید!")
                    .setConfirmText("امتحان مجدد")
                    .setCancelText("بستن")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            GetDataFromLocalDB();
                            pDialog1.dismiss();
                        }
                    }).show();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        data.clear();
        loading = true;
        offset = 0;
        limit = 10;
        totalcount = 0;
        pastVisiblesItems = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        GetDataFromLocalDB();
    }

    public void searchGroup(String key) {
        GroupRepository repository = new GroupRepository();
        determinateBar.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Group> tdata = (ArrayList<Group>) value;
                    data.addAll(tdata);
                    totalcount += data.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                    }
                    fill_list(data);
                } catch (Throwable ignored) {
                }
            }

            @Override
            public void OnFail(Object object) {
            }
        };
        // dialog2.show();
        determinateBar.setVisibility(View.INVISIBLE);
        if (limit > 0)
            repository.search(servicelistener, currentGroup.GetID(), offset, limit, key);
    }

    public void getKala() {
        KalaRepository repository = new KalaRepository();
        determinateBar.setVisibility(View.VISIBLE);
        //final IOSDialog dialog2 = Tools.CreateWaitingDialog(this, "دریافت اطلاعات");
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Kala> tdata = (ArrayList<Kala>) value;
                    Kaladata.addAll(tdata);
                    totalcount += data.size();
                    offset = totalcount - 1;
                    if (tdata.size() < limit) {
                        limit = 0;
                    }
                    fill_list_kala(Kaladata);

                } catch (Throwable t) {
                }
                // dialog2.dismiss();
            }

            @Override
            public void OnFail(Object object) {
            }
        };
        //dialog2.show();
        determinateBar.setVisibility(View.INVISIBLE);
        if (limit > 0)
            repository.GetKalaByGroup(servicelistener, currentGroup.GetID(), offset, limit);
    }

    private void fill_list_kala(ArrayList<Kala> data) {
        kalaadapter = new Recycler_View_Adapter(data, this);
        sub_group_list.setAdapter(kalaadapter);
        sub_group_list.setLayoutManager(mLayoutManager);
    }
}
