package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.SpinnerAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ChangeOrder;
import com.sepehrcc.apps.sepehradmin.WebService.Models.OrderInfo;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.OrderRepository;

import java.lang.reflect.Type;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ChangeOrderDetailActivity extends AppCompatActivity {
    @BindView(R.id.btn_Save)
    Button btn_Save;
    @BindView(R.id.lbl_order_details)
    TextView lbl_order_details;
    @BindView(R.id.btn_cancle)
    Button btn_cancle;
    @BindView(R.id.lbl_status)
    TextView lbl_status;
    @BindView(R.id.fml_status)
    FrameLayout fml_status;
    @BindView(R.id.spin_status)
    Spinner spin_status;
    @BindView(R.id.lbl_post_delivery)
    TextView lbl_post_delivery;
    @BindView(R.id.edt_post_delivery)
    EditText edt_post_delivery;
    @BindView(R.id.lbl_note)
    TextView lbl_note;
    @BindView(R.id.fml_note)
    FrameLayout fml_note;
    @BindView(R.id.edt_note)
    EditText edt_note;
    @BindView(R.id.edt_status_sentence)
    EditText edt_status_sentence;
    OrderInfo orderInfo = new OrderInfo();
    WebServicelistener servicelistener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_order_detail);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Gson gson = new Gson();
            String orderInfo1 = extras.getString("order_info");
            Type collectionType = new TypeToken<OrderInfo>() {
            }.getType();
            orderInfo = gson.fromJson(orderInfo1, collectionType);
        }
        init();
        fill_information();
        BindViewControl();
    }

    private void init() {
        btn_Save.setTypeface(Fonts.VazirBold);
        lbl_order_details.setTypeface(Fonts.VazirBold);
        btn_cancle.setTypeface(Fonts.VazirBold);
        lbl_status.setTypeface(Fonts.VazirBold);
        lbl_post_delivery.setTypeface(Fonts.VazirBold);
        edt_post_delivery.setTypeface(Fonts.VazirBold);
        lbl_note.setTypeface(Fonts.VazirBold);
        edt_note.setTypeface(Fonts.VazirBold);
        edt_status_sentence.setTypeface(Fonts.VazirBold);
        SpinnerAdapter spinAdapter = new SpinnerAdapter(ChangeOrderDetailActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.order_status)));
        spin_status.setAdapter(spinAdapter);
    }

    private void BindViewControl() {
        spin_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (adapterView.getSelectedItem().toString()) {
                    case "جدید":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما به فروشگاه ارسال شده است");
                        }
                        break;
                    case "تایید شده":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما تایید شد");
                        }
                        break;
                    case "آماده ارسال":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {

                            edt_status_sentence.setText("سفارش شما آماده ارسال است");
                        }
                        break;
                    case "ارسال شده":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما ارسال شده است");
                        }
                        break;
                    case "کامل شده":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("تمامي مراحل خريد شما به پايان رسيد");
                        }
                        break;
                    case "معلق":
                        if (i != orderInfo.getStatus() - 1 || orderInfo.getsStxt().equals("")) {
                            edt_status_sentence.setText("سفارش شما در دست بررسي است");
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        edt_note.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.edt_note) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeOrder changeOrder = new ChangeOrder();
                changeOrder.setID(orderInfo.getID());
                changeOrder.setRahgiry(edt_post_delivery.getText().toString());
                changeOrder.setYadasht(edt_note.getText().toString());
                changeOrder.setStatus(spin_status.getSelectedItemPosition() + 1);
                changeOrder.setStatusText(edt_status_sentence.getText().toString());
                change_order(changeOrder);
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void fill_information(){
        spin_status.setSelection(orderInfo.getStatus() - 1);
        edt_post_delivery.setText(orderInfo.getRahgiry() + "");
        edt_note.setText(orderInfo.getYadasht());
        edt_status_sentence.setText(orderInfo.getsStxt());
    }
    private void change_order(ChangeOrder changeOrder) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(ChangeOrderDetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("ارسال ...");
        pDialog.setCancelable(true);
        Gson gson = new Gson();
        String json = gson.toJson(changeOrder);
        OrderRepository orderRepository = new OrderRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                feedback feedback = (feedback) object;
                if (feedback.Status == 1) {
                    pDialog.hide();
                    Intent intent = new Intent(ChangeOrderDetailActivity.this, SuccessfullyActivity.class);
                    intent.putExtra("successfully_message", "تغییرات مورد نظر با موفقیت اعمال شد.");
                    startActivity(intent);
                } else {
                    pDialog.hide();
                    new SweetAlertDialog(ChangeOrderDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطا")
                            .setContentText("تغییرات اعمال نشد لطفا بعدا تلاش کنید.")
                            .setConfirmText("بستن")
                            .show();
                }
            }

            @Override
            public void OnFail(Object object) {
                pDialog.hide();
                new SweetAlertDialog(ChangeOrderDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("خطا")
                        .setContentText("تغییرات اعمال نشد لطفا بعدا تلاش کنید.")
                        .setConfirmText("بستن")
                        .show();
            }
        };
        pDialog.show();
        orderRepository.ChangeOrderStatus(servicelistener, changeOrder);
    }
}
