package com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers;

import android.graphics.Color;

import java.util.Random;

public class RandomColor {
    public int color;

    public RandomColor() {
        Random rand = new Random();
        int num = rand.nextInt(5);
        switch (num) {
            case 0:
                color = Color.argb(255,255,0,64);
                break;
            case 1:
                color = Color.argb(255,0,64,255);
                break;
            case 2:
                color = Color.argb(255,77,153,0);
                break;
            case 3:
                color = Color.argb(255,255,128,0);
                break;
            case 4:
                color = Color.BLACK;
                break;
            case 5:
                color = Color.GRAY;
                break;
        }
    }

    public int getColor() {
        return color;
    }
}
