package com.sepehrcc.apps.sepehradmin.WebService.Repository;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Mark;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.sepehrcc.apps.sepehradmin.SharedClass.G.Upload_url;

public class MarkRepository {

    public void GetMarks(final WebServicelistener listener, int offset,int limit) {
        try {
            final Call<ArrayList<Mark>> CallServer = WebServiceHandler.MarkClient.GetMarks(G.ApiToken,offset,limit);
            CallServer.enqueue(new Callback<ArrayList<Mark>>() {
                @Override
                public void onResponse(Response<ArrayList<Mark>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Mark> marks = response.body();
                                listener.OnComplete(marks);
                            } else {
                                ArrayList<Mark> marks = new ArrayList<Mark>();
                                listener.OnComplete(marks);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {
        }
    }

    public void SearchMarks(final WebServicelistener listener, String Key, int offset, int limit) {
        try {
            final Call<ArrayList<Mark>> CallServer = WebServiceHandler.MarkClient.SearchMarks(G.ApiToken, offset, limit, Key);
            CallServer.enqueue(new Callback<ArrayList<Mark>>() {
                @Override
                public void onResponse(Response<ArrayList<Mark>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Mark> marks = response.body();
                                listener.OnComplete(marks);
                            } else {
                                ArrayList<Mark> marks = new ArrayList<Mark>();
                                listener.OnComplete(marks);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void AddMark(final WebServicelistener listener, Mark mark) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.MarkClient.EditMark(G.ApiToken,mark);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                listener.OnComplete(response.body());
                            } else {
                                listener.OnComplete(response.body());
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void EditMark(final WebServicelistener listener, Mark mark) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.MarkClient.EditMark(G.ApiToken,mark);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                listener.OnComplete(response.body());
                            } else {
                                listener.OnComplete(response.body());
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void UploadPhoto(final WebServicelistener lisitner, String imageUrl, int BrandID) {
        File file = new File(imageUrl);
        AndroidNetworking.upload(Upload_url + "FileUpload")
                .addMultipartFile("File", file)
                .setTag("uploadTest")
                .addHeaders("Type", "Brand")
                .addHeaders("Id", String.valueOf(BrandID))
                .addHeaders("Authorization", G.ApiToken)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        lisitner.OnComplete(response);
                    }
                    @Override
                    public void onError(ANError error) {
                        lisitner.OnFail(error);
                    }
                });
    }
}
