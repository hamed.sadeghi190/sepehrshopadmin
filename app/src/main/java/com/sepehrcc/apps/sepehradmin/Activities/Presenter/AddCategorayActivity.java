package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.KindListAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Photo_Adapter;
import com.sepehrcc.apps.sepehradmin.DbModels.ProductGroup;
import com.sepehrcc.apps.sepehradmin.Models.Gallery;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.GroupRepository;
import com.thefinestartist.finestwebview.FinestWebView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddCategorayActivity extends AppCompatActivity implements IPickResult {
    @BindView(R.id.edit_group_name)
    TextInputLayout edit_group_name;
    @BindView(R.id.edit_new_group_desc)
    TextInputLayout edit_new_group_desc;
    @BindView(R.id.txt_new_group)
    TextView txt_new_group;
    @BindView(R.id.txt_how_to_grouping)
    TextView txt_how_to_grouping;
    @BindView(R.id.txt_example)
    TextView txt_example;
    @BindView(R.id.btn_Save)
    Button btn_send;
    @BindView(R.id.btn_cancle)
    Button btn_cancle;
    @BindView(R.id.txt_category_desc)
    EditText txt_category_desc;
    @BindView(R.id.edt_Prodcut_Name)
    EditText edt_Prodcut_Name;
    @BindView(R.id.img_take_pic)
    ImageView img_take_pic;
    @BindView(R.id.lbl_group_pic)
    TextView lbl_group_pic;
    @BindView(R.id.kind_list)
    RecyclerView kind_list;
    @BindView(R.id.lbl_types)
    TextView lbl_types;
    @BindView(R.id.add_new_type)
    TextView add_new_type;
    KindListAdapter typeAdapter;
    List<Gallery> galleries;
    Photo_Adapter adpter;
    boolean isOpened = false;
    WebServicelistener servicelistener;
    SweetAlertDialog pDialog;
    List<KindGroup> chech_data = new ArrayList<KindGroup>();
    String ImageUrl = "";
    boolean has_image = false;
    PickSetup setup = new PickSetup();
    boolean image_croped = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_add_category);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        initialView();
        init();
        BindViewControls();
        setup
                .setTitle("انتخاب")
                .setTitleColor(Color.BLACK)
                .setBackgroundColor(Color.WHITE)
                .setProgressText("صبر کنید...")
                .setProgressTextColor(Color.BLACK)
                .setCancelText("بستن")
                .setCancelTextColor(Color.BLACK)
                .setButtonTextColor(Color.BLACK)
                .setMaxSize(500)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText("دوربین")
                .setGalleryButtonText("گالری")
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setSystemDialog(false)
                .setCameraToPictures(false)
                .setGalleryIcon(R.drawable.ic_gallery_picker)
                .setCameraIcon(R.drawable.ic_photo_camera);
    }

    private void initialView() {
        edit_group_name.setTypeface(Fonts.VazirBold);
        edit_new_group_desc.setTypeface(Fonts.VazirBold);
        txt_new_group.setTypeface(Fonts.VazirBold);

        txt_how_to_grouping.setTypeface(Fonts.VazirBold);
        txt_example.setTypeface(Fonts.VazirBold);
        btn_send.setTypeface(Fonts.VazirBold);
        btn_cancle.setTypeface(Fonts.VazirBold);
        edt_Prodcut_Name.setTypeface(Fonts.VazirBold);
        txt_category_desc.setTypeface(Fonts.VazirBold);
        lbl_group_pic.setTypeface(Fonts.VazirBold);
        add_new_type.setTypeface(Fonts.VazirBold);
        lbl_types.setTypeface(Fonts.VazirBold);
    }

    private void init() {
    }

    @SuppressLint("ClickableViewAccessibility")
    private void BindViewControls() {
        txt_category_desc.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {

                if (view.getId() == R.id.txt_category_desc) {
                    view.getParent().getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!attemp_add_group()) {
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("ارسال ...");
                    pDialog.setCancelable(true);

                    final Group groupObj = new Group();
                    groupObj.SetName(edt_Prodcut_Name.getText().toString());
                    if (txt_category_desc.getText() != null)
                        G.desc_save = txt_category_desc.getText().toString();
                    groupObj.SetDescription(G.desc_save);
                    ArrayList<KindGroup> _gkinds = new ArrayList<KindGroup>();

                    for (int i = 0; i < chech_data.size(); i++) {
                        KindGroup kind = new KindGroup();
                        // kind.SetGroupID(25);
                        kind.SetStatus(2);
                        kind.SetTitle(chech_data.get(i).GetTitle());
                        _gkinds.add(kind);
                    }
                    groupObj.SetKinds(_gkinds);

                    servicelistener = new WebServicelistener() {
                        @Override
                        public void OnComplete(Object object) {
                            AddResponse response = (AddResponse) object;
                            groupObj.SetID(response.id);
                            ProductGroup Ngroup = new ProductGroup(groupObj);
                            Ngroup.save();
                            if (has_image) {

                                WebServicelistener lis = new WebServicelistener() {
                                    @Override
                                    public void OnComplete(Object object) {
                                        G.desc_save = "";
                                        pDialog.hide();
                                        G.appData.shopInfo.SetGroupCount(G.appData.shopInfo.GetGroupCount() + 1);
                                        Intent intent = new Intent(AddCategorayActivity.this, SuccessfullyActivity.class);
                                        intent.putExtra("successfully_message", "گروه مورد نظر با موفقیت اضافه شد");
                                        startActivity(intent);
                                        finish();
                                    }

                                    @Override
                                    public void OnFail(Object object) {
                                        new SweetAlertDialog(AddCategorayActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("خطا")
                                                .setContentText("عکس آپلود نشد.")
                                                .setConfirmText("بستن")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                        finish();
                                                    }
                                                })
                                                .show();
                                    }
                                };
                                new GroupRepository().UploadPhoto(lis, ImageUrl, response.id);

                            } else {
                                G.desc_save = "";
                                pDialog.hide();
                                G.appData.shopInfo.SetGroupCount(G.appData.shopInfo.GetGroupCount() + 1);
                                Intent intent = new Intent(AddCategorayActivity.this, SuccessfullyActivity.class);
                                intent.putExtra("successfully_message", "گروه مورد نظر با موفقیت اضافه شد");
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void OnFail(Object object) {
                            pDialog.hide();
                            new SweetAlertDialog(AddCategorayActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("خطا!")
                                    .setContentText("گروه مورد نظراضافه نشد")
                                    .setConfirmText("بستن")
                                    .show();
                        }
                    };

                    pDialog.show();
                    new GroupRepository().AddGroup(servicelistener, groupObj);
                } else {
                    Toast.makeText(AddCategorayActivity.this, "لطفا تمامی فیلد ها را به درستی تکمیل کنید", Toast.LENGTH_LONG).show();
                }
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                G.desc_save = "";
                finish();

            }
        });
        img_take_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(setup).setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult pickResult) {
                        if (pickResult.getError() == null) {
                            save_image(pickResult.getBitmap());
                        } else {
                        }
                    }
                }).show(AddCategorayActivity.this);
            }
        });
        add_new_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AddCategorayActivity.this, TypesActivity.class);
                ArrayList<String> kindList = new ArrayList<String>();
                for (int j = 0; j < chech_data.size(); j++) {
                    kindList.add(chech_data.get(j).GetTitle());
                }
                i.putStringArrayListExtra("hhh", kindList);
                startActivityForResult(i, 4);
            }
        });
        txt_how_to_grouping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(AddCategorayActivity.this).show("http://help.sepehrcc.com/groups");

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            String rich_edit_text_result = data.getStringExtra("rich_edit_text_html");
            txt_category_desc.setText(rich_edit_text_result);
        } else if (resultCode == 4) {
            chech_data.clear();
            for (int i = 0; i < data.getStringArrayListExtra("typelist").size(); i++) {
                KindGroup kg = new KindGroup();
                kg.SetTitle(data.getStringArrayListExtra("typelist").get(i));
                chech_data.add(kg);
            }
            fill_list_type(chech_data);
        } else if (resultCode == 10) {
            save_image_croped(G.b);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpened = false;
        G.CheckInfo();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        G.desc_save = "";
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            save_image(pickResult.getBitmap());

        }
    }

    private void fill_list_type(List<KindGroup> data) {
        typeAdapter = new KindListAdapter(data, this);
        kind_list.setAdapter(typeAdapter);
        kind_list.setLayoutManager(new LinearLayoutManager(this));
    }

    public void save_image(Bitmap result) {
        G.b = result;
        Intent i = new Intent(AddCategorayActivity.this, CropActivity.class);
        i.putExtra("PhotoName", "sepehrgroup.jpg");
        startActivityForResult(i, 10);
        image_croped = false;
    }

    public void save_image_croped(Bitmap result) {
        img_take_pic.setImageBitmap(result);
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        ImageUrl = path + "/sepehrgroup.jpg";
        Integer counter = 0;
        File file = new File(path, "sepehrgroup.jpg");
        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        result.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
        try {
            fOut.flush(); // Not really required
            fOut.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            has_image = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        image_croped = false;
    }

    private Boolean attemp_add_group() {
        edt_Prodcut_Name.setError(null);
        String group_name = edt_Prodcut_Name.getText().toString();
        Boolean cancle = false;
        View focusView = null;
        if (group_name.length() == 0 || group_name.equals("") || group_name == null) {
            edt_Prodcut_Name.setError("نام گروه نمی تواند خالی باشد");
            cancle = true;
            focusView = edt_Prodcut_Name;
        }
        if (cancle) {
            focusView.requestFocus();
        }
        return cancle;
    }
}
