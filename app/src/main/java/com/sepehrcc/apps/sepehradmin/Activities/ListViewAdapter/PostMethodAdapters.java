package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.ChangePostMethodActivity;
import com.sepehrcc.apps.sepehradmin.Models.PaymentModel;
import com.sepehrcc.apps.sepehradmin.Models.PostModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Posts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class PostMethodAdapters extends RecyclerView.Adapter<PostMethodAdapters.GroupViewHolder> {
    List<Posts> list = Collections.emptyList();
    Context context;

    public PostMethodAdapters(List<Posts> list, Context context) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lst_post_method_row, viewGroup, false);
        GroupViewHolder pay = new GroupViewHolder(v);
        return pay;
    }

    @Override
    public void onBindViewHolder(final GroupViewHolder holder, final int position) {
        holder.lbl_post_name.setText(list.get(position).GetdName() + "");
        holder.lbl_post_status.setText(G.formatPrice(list.get(position).GetdAmount() + ""));
        if (list.get(position).GetdEnable()) {
            holder.switch_post.setChecked(true);
        }
        else{
            holder.lin_civil_post.setBackgroundColor(context.getResources().getColor(R.color.grey_hex_ce));
        }
        switch (list.get(position).GetdMethod()) {
            case 0:
                holder.lbl_post_status_type.setText("تومان");
                break;
            case 1:
                holder.lbl_post_status_type.setText("درصد");
                break;
//            case 3:
//                switch (list.get(position).GetdAmount())
//                {
            case 2:
                holder.lbl_post_status.setText("فرمول پیشتاز داخلی");
                break;
            case 3:
                holder.lbl_post_status.setText("فرمول سفارشی داخلی");
                break;
            case 4:
                holder.lbl_post_status.setText("فرمول ویژه داخلی");
                break;
//                }
//                break;
        }
        holder.ripple_post.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (list.get(position).GetdEnable()) {
                    holder.switch_post.setChecked(false);
                } else {
                    holder.switch_post.setChecked(true);
                }
            }
        });
        holder.switch_post.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ListInterface listinterface = (ListInterface) context;
                if (b) {
                    list.get(position).SetdEnable(true);
                    listinterface.onListUpdate(list);
                } else {
                    list.get(position).SetdEnable(false);
                    listinterface.onListUpdate(list);
                }
            }
        });
        holder.switch_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChangePostMethodActivity.class);
                Gson g = new Gson();
                String json = g.toJson(list.get(position));
                intent.putExtra("post_method", json);
                context.startActivity(intent);
            }
        });
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        LinearLayout lin_item_post;
        CardView crd_post;
        RippleView ripple_post;
        LinearLayout lin_civil_post;
        ImageView img_post_item;
        TextView lbl_post_name;
        TextView lbl_post_status;
        Switch switch_post;
        TextView lbl_post_status_type;
        ImageView img_edit;

        GroupViewHolder(View itemView) {
            super(itemView);
            lin_item_post = (LinearLayout) itemView.findViewById(R.id.lin_item_post);
            crd_post = (CardView) itemView.findViewById(R.id.crd_post);
            ripple_post = (RippleView) itemView.findViewById(R.id.ripple_post);
            lin_civil_post = (LinearLayout) itemView.findViewById(R.id.lin_civil_post);
            img_post_item = (ImageView) itemView.findViewById(R.id.img_post_item);
            lbl_post_name = (TextView) itemView.findViewById(R.id.lbl_post_name);
            lbl_post_status = (TextView) itemView.findViewById(R.id.lbl_post_status);
            switch_post = (Switch) itemView.findViewById(R.id.switch_post);
            lbl_post_status_type = (TextView) itemView.findViewById(R.id.lbl_post_status_type);
            img_edit = (ImageView) itemView.findViewById(R.id.img_edit);
            lbl_post_name.setTypeface(Fonts.VazirLight);
            lbl_post_status.setTypeface(Fonts.VazirFD);
            lbl_post_status_type.setTypeface(Fonts.VazirFD);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface ListInterface {
        public void onListUpdate(List<Posts> list);
    }
}