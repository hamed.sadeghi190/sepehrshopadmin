package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DomainModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Hamed Sadeghi on 08/05/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public interface IDomain {

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Domain/list")
    Call<feedback> GetDomains(@Header("Authorization") String AuthKey);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Domain/whois/{site}")
    Call<feedback> WhoisDomain(@Path("site") String url);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Domain/add")
    Call<feedback> SetAddedDomain(@Header("Authorization") String AuthKey, @Body DomainModel domainModel);
    /*
    * 1:sabt shode
    * 2: nsa set nashode
    * */
}
