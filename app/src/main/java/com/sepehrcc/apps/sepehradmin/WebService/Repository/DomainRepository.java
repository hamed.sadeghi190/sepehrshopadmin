package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DomainModel;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DomainRepository {
    public void WhoisUrl(final WebServicelistener listener,String DomainUrl) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.DomainClinet.WhoisDomain(DomainUrl);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.headers().get("AckStatus") != null) {

                        listener.OnComplete(response.body());

                    } else {
                        ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                        listener.OnFail(ErrorModel);
                    }
                }
                @Override
                public void onFailure(Throwable t) {

                }
            });

        } catch (Exception ex) {
        }
    }
    public void GetDomains(final WebServicelistener listener) {

        try {
            final Call<feedback> CallServer = WebServiceHandler.DomainClinet.GetDomains(G.ApiToken);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                listener.OnComplete(response.body());
                            } else {
                                ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                                listener.OnFail(ErrorModel);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
    public void SetAddedDoamain(final WebServicelistener lisitner, DomainModel domainModel) {
        try {
            final Call<feedback> CallServer = WebServiceHandler.DomainClinet.SetAddedDomain(G.ApiToken, domainModel);
            CallServer.enqueue(new Callback<feedback>() {
                @Override
                public void onResponse(Response<feedback> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            lisitner.OnComplete(response.body());
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            lisitner.OnFail(ErrorModel);
                        }
                    }
                }
                @Override
                public void onFailure(Throwable t) {
                }
            });

        } catch (Exception ex) {

        }
    }
}
