package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Posts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Theme;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * Created by Administrator on 22/05/2018.
 */

public interface ITheme {
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Theme/list")
    Call<ArrayList<Theme>> GetThemes(@Header("Authorization") String AuthKey);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Theme/set/{themeid}")
    Call<AddResponse> SetTheme(@Header("Authorization") String AuthKey, @Path("themeid") int themeid);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("http://192.168.1.5:3000/api/shop.ashx?apikey=B65DC9E9-991F-4586-BE0F-98FA1B27BFE9&do=clearcache&password=O4V1S1zx1m4nv%2BgGHs4Oie7wKD7sDaLSUMVPtwUBXG8%3D%0A")
    Call<AddResponse> ClearCache();
}
