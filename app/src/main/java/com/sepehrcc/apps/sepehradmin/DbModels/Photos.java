package com.sepehrcc.apps.sepehradmin.DbModels;

import com.activeandroid.annotation.Column;

/**
 * Created by Hamed Sadeghi on 13/06/2018.
 * Email : hamed.sadeghi190@gmail.com
 */

public class Photos {
   @Column(name ="iId")
    private int _iId;
    public int GetPhotoID() { return _iId; }
    public Photos SetPhotoID(int value) {_iId = value;return this;}

   @Column(name ="iName")
    private String _iName;
    public String GetPhotoAddress() { return _iName; }
    public Photos SetPhotoAddress(String value) {_iName = value;return this;}

   @Column(name ="iAlbum")
    private int _iAlbum;
    public int GetAlbumID() { return _iAlbum; }
    public Photos SetAlbumID(int value) {_iAlbum = value;return this;}
}
