package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed on 28/11/2017.
 */

public class KalaTypes {

    @SerializedName("KindID")
    private int _kKindId;
    public int GetKindID() { return _kKindId; }
    public KalaTypes SetKindID(int value) {_kKindId = value;return this;}

    @SerializedName("KalaID")
    private int _KalaId;
    public int GetKalaID() { return _KalaId; }
    public KalaTypes SetKalaID(int value) {_KalaId = value;return this;}

    @SerializedName("Sub")
    private int _sub;
    public int GetSubID() { return _sub; }
    public KalaTypes SetSubID(int value) {_sub = value;return this;}

}
