package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.All_Order_list_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Order_list_Adapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.ShamsiDate;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Order;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.OrderRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewAllOrdersActivity extends AppCompatActivity {
    @BindView(R.id.add_product_appbar)
    ButtonBarLayout add_product_appbar;
    @BindView(R.id.lbl_title)
    TextView lbl_title;
    @BindView(R.id.editSearch_Orders)
    SearchView editSearch_Orders;
    @BindView(R.id.lst_orders)
    RecyclerView lst_orders;
    @BindView(R.id.lin_progress)
    LinearLayout lin_progress;
    ArrayList<Order> orders = new ArrayList<Order>();
    WebServicelistener servicelistener;
    LinearLayoutManager mLayoutManager;
    int offset = 0;
    int limit = 10;
    int totalcount;
    private boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    int current_year=2000;
    int current_month=2000;
    int current_day=2000;
int list_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_orders);
        ButterKnife.bind(this);
        mLayoutManager = new LinearLayoutManager(this);
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            list_type=extras.getInt("list_type");
        }
        init();
        BindViewControl();
        downloadOrder();



    }

    private void init() {
        editSearch_Orders.setIconifiedByDefault(false);
        editSearch_Orders.setFocusable(false);
        editSearch_Orders.setIconified(true);
        lbl_title.setTypeface(Fonts.VazirBoldFD);
        offset = 0;
        limit = 10;
        orders.clear();
        current_year=2000;
        current_month=2000;
        current_day=2000;
    }

    private void fillList() {
        for (int i = 0; i <orders.size() ; i++) {

            String[] whole_date = orders.get(i).GetOrdrDate().split(" ");
            String[] date = whole_date[0].split("/");
            String shown_date = "";
            ShamsiDate shamsiDate = new ShamsiDate();
            int y = Integer.parseInt(date[0]);
            int m = Integer.parseInt(date[1]);
            int d = Integer.parseInt(date[2]);
            shown_date = shamsiDate.shamsiDate(y, m, d);
            date = shown_date.split("/");
            if(Integer.parseInt(date[0])<current_year || (Integer.parseInt(date[0])==current_year && Integer.parseInt(date[1])<current_month ) || (Integer.parseInt(date[0])==current_year && Integer.parseInt(date[1])==current_month && Integer.parseInt(date[2])<current_day)) {
                current_year = Integer.parseInt(date[0]);
                current_month = Integer.parseInt(date[1]);
                current_day = Integer.parseInt(date[2]);
                Order order = new Order();
                order.setType(1);
                order.SetOrderDate(orders.get(i).GetOrdrDate());
                order.SetOrderCode(current_year + "/" + current_month + "/" + current_day);
                orders.add(i, order);
            }
        }
        All_Order_list_Adapter order_list_adapter = new All_Order_list_Adapter(orders, this);
        lst_orders.setAdapter(order_list_adapter);
        lst_orders.setLayoutManager(mLayoutManager);
        lst_orders.scrollToPosition(pastVisiblesItems);
    }

    private void downloadOrder() {
        final OrderRepository repository = new OrderRepository();
        lin_progress.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Order> tdata = (ArrayList<Order>) value;
                    if (tdata == null) {
                        loading = false;
                        lin_progress.setVisibility(View.GONE);

                    } else if (totalcount == 0 && tdata.size() == 0) {
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    } else {
                        if (offset == 0) {
                             current_year=2000;
                             current_month=2000;
                             current_day=2000;
                        }
                        orders.addAll(tdata);
                        totalcount += tdata.size();
                        offset = totalcount - 1;
                        if (tdata.size() < limit) {
                            limit = 0;
                            offset = 0;
                        }
                        fillList();
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    }
                } catch (Throwable ignored) {
                }
            }

            @Override
            public void OnFail(Object object) {
                lin_progress.setVisibility(View.GONE);
            }
        };
        if (limit > 0) {
            repository.GetOrders(servicelistener, list_type, offset, limit);
        }

        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }

    }

    private void BindViewControl() {
        lst_orders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            if (editSearch_Orders.getQuery().toString().equals("")) {
                                downloadOrder();
                            } else {
                                search_order(editSearch_Orders.getQuery().toString());
                            }

                        }
                    }
                }
            }
        });
        editSearch_Orders.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!newText.equals("")) {
                    orders.clear();
                    current_year=2000;
                     current_month=2000;
                    current_day=2000;
                    fillList();
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    search_order(newText);
                } else {
                    orders.clear();
                     current_year=2000;
                     current_month=2000;
                     current_day=2000;
                    loading = true;
                    offset = 0;
                    limit = 10;
                    totalcount = 0;
                    pastVisiblesItems = 0;
                    visibleItemCount = 0;
                    totalItemCount = 0;
                    downloadOrder();
                }
                return false;
            }
        });
    }

    private void search_order(String key) {
        key = key.replace("/", "-");
        final OrderRepository repository = new OrderRepository();
        lin_progress.setVisibility(View.VISIBLE);
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object value) {
                try {
                    ArrayList<Order> tdata = (ArrayList<Order>) value;
                    if (totalcount == 0 && tdata.size() == 0) {
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    } else {
                        orders.addAll(tdata);
                        totalcount += tdata.size();
                        offset = totalcount - 1;
                        if (tdata.size() < limit) {
                            limit = 0;
                            offset = 0;
                        }
                        fillList();
                        loading = false;
                        lin_progress.setVisibility(View.GONE);
                    }
                } catch (Throwable t) {
                }
            }

            @Override
            public void OnFail(Object object) {
                lin_progress.setVisibility(View.GONE);
            }
        };
        if (limit > 0) {
            repository.SearchOrders(servicelistener, offset, limit, key);
        }
        if (limit == 0) {
            lin_progress.setVisibility(View.GONE);
        }
    }

    private String getDate() {
        DateFormat dfDate = new SimpleDateFormat("yyyy/MM/dd");
        String date = dfDate.format(Calendar.getInstance().getTime());
        DateFormat dfTime = new SimpleDateFormat("HH:mm:ss");
        String time = dfTime.format(Calendar.getInstance().getTime());
        return date + " " + time;
    }
}
