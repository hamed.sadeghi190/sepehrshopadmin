
package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Models.TasksModel;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;

import java.util.ArrayList;

import static com.sepehrcc.apps.sepehradmin.SharedClass.G.Inflator;


public class TasksAdapter extends ArrayAdapter<TasksModel>
{
	public TasksAdapter(ArrayList<TasksModel> myarray )
	{
		super(G.CurrentActivity, R.layout.lst_tasks_row_themplate , myarray);
	}

	public static class ViewHolder 
	{	
		public TextView ItemText;
		public LinearLayout MenuItem;
		public ViewHolder (View view)
		{
			try
			{
//				ItemText    = (TextView) view.findViewById(R.id.TxtCityName);
//				MenuItem    = (LinearLayout) view.findViewById(R.id.CityRowLayout);
			}
			catch (Exception e)
			{
				//G.LogMyTexts(e.toString());
			}
		}

		public void Fill (ArrayAdapter<TasksModel> adaptor, TasksModel Item , int Position)
		{
//			ItemText.setText(Item.getName());
//			ItemText.setTypeface(G.Fontiran);
		}

	}
	@Override
	public View getView (int position, View convertView, ViewGroup parent)
	{

		ViewHolder holder  ;
		TasksModel Item = getItem(position);
		if(convertView==null)
		{
			convertView = G.Inflator.inflate(R.layout.lst_tasks_row_themplate,parent,false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		}
		else
		{
			holder = new ViewHolder(convertView);
		}
		holder.Fill(this, Item, position);
		return convertView;
	}
}
