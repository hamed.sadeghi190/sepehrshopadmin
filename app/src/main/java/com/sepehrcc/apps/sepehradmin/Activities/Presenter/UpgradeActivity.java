package com.sepehrcc.apps.sepehradmin.Activities.Presenter;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.SpinnerAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.thefinestartist.finestwebview.FinestWebView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpgradeActivity extends AppCompatActivity {
    @BindView(R.id.lbl_version_name)
    TextView lbl_version_name;

    @BindView(R.id.lbl_expire_date)
    TextView lbl_expire_date;

    @BindView(R.id.lbl_upgrade)
    TextView lbl_upgrade;
    @BindView(R.id.fml_plans)
    FrameLayout fml_plans;
    @BindView(R.id.spin_plans)
    Spinner spin_plans;
    @BindView(R.id.lbl_price)
    TextView lbl_price;
    @BindView(R.id.lbl_vahed)
    TextView lbl_vahed;
    @BindView(R.id.lbl_grow_percent)
    TextView lbl_grow_percent;
    @BindView(R.id.btn_pay)
    Button btn_pay;
    @BindView(R.id.lbl_show_possiblities)
    TextView lbl_show_possiblities;
    @BindView(R.id.progress3)
    CircularProgressBar progress3;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade);
        ButterKnife.bind(this);
        init();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        lbl_version_name.setTypeface(Fonts.VazirBoldFD);
        lbl_expire_date.setTypeface(Fonts.VazirFD);

        lbl_upgrade.setTypeface(Fonts.VazirBoldFD);
        lbl_price.setTypeface(Fonts.VazirBoldFD);
        lbl_vahed.setTypeface(Fonts.VazirFD);
        btn_pay.setTypeface(Fonts.VazirFD);
        lbl_grow_percent.setTypeface(Fonts.VazirFD);
        lbl_show_possiblities.setTypeface(Fonts.VazirFD);
        lbl_price.setText(G.formatPrice("35000"));
        lbl_version_name.setText("نسخه "+G.appData.shopInfo.GetPlanName());
        int remain_days=365-((365*G.appData.shopInfo.GetPercent())/100);
        lbl_grow_percent.setText(remain_days + "  روز باقی مانده");
        lbl_expire_date.setText(" از تاریخ  " + G.appData.shopInfo.GetRegisterDate() + "  تا " + G.appData.shopInfo.GetExpireDate());
        progress3.setProgress(100 - G.appData.shopInfo.GetPercent());
        SpinnerAdapter spinAdapter = new SpinnerAdapter(UpgradeActivity.this, android.R.layout.simple_spinner_item, Arrays.asList(getResources().getStringArray(R.array.sepehr_pelans)));
        spin_plans.setAdapter(spinAdapter);
        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (G.appData.shopInfo.GetDomain() != null&& !G.appData.shopInfo.GetDomain().equals("")) {
                    String text="http://"+G.appData.shopInfo.GetDomain()+"/admin/tamdid.aspx";
                    new FinestWebView.Builder(UpgradeActivity.this).show(text);
                } else {
                    new FinestWebView.Builder(UpgradeActivity.this).show("http://"+G.appData.shopInfo.GetsubDomain() + ".sepehrcc.com/admin/tamdid.aspx");

                }
            }
        });
        lbl_show_possiblities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(UpgradeActivity.this).show("https://sepehrcc.com/pricelist.aspx#sep_shop_priceList");

            }
        });

    }
}
