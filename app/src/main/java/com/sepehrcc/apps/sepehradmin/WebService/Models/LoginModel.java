package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed sadeghi on 11/14/2017.
 */

public class LoginModel {
    @SerializedName("Url")
    private String _Url;

    public String GetUrl() {
        return _Url;
    }

    public LoginModel SetUrl(String value) {
        _Url = value;
        return this;
    }

    @SerializedName("UserName")
    private String _UserName;

    public String GetUserName() {
        return _UserName;
    }

    public LoginModel SetUserName(String value) {
        _UserName = value;
        return this;
    }

    @SerializedName("Password")
    private String _Password;

    public String GetPassword() {
        return _Password;
    }

    public LoginModel SetPassword(String value) {
        _Password = value;
        return this;
    }
}
