package com.sepehrcc.apps.sepehradmin.WebService.Models;



public class OrderItems {
    public int oKalaID;
    public double oCount ;
    public int oDiscount ;
    public String oDescription;
    public int oPrice ;
    public int oSizeColorId ;
    public String kName;
    public int kPrice ;
    public String kCode ;
    public int kGroup ;
    public int kVirtualType;
    public int kVirtual ;
    public int kWeight ;
    public String kAksAdd ;
    public int sub ;
    public int kalaSub ;
    public String tColor ;
    public String tSize ;
    public String tmaterial ;
    public String tCode ;
    public String pCode ;
    public int pKalaId ;
    public String pTitle ;
    public int pPrice ;
    public int pPermisionUserGroup ;
    public int pPermision ;
    public int pKalaID ;
    public int pId ;

    public int getoKalaID() {
        return oKalaID;
    }

    public void setoKalaID(int oKalaID) {
        this.oKalaID = oKalaID;
    }

    public double getoCount() {
        return oCount;
    }

    public void setoCount(double oCount) {
        this.oCount = oCount;
    }

    public int getoDiscount() {
        return oDiscount;
    }

    public void setoDiscount(int oDiscount) {
        this.oDiscount = oDiscount;
    }

    public String getoDescription() {
        return oDescription;
    }

    public void setoDescription(String oDescription) {
        this.oDescription = oDescription;
    }

    public int getoPrice() {
        return oPrice;
    }

    public void setoPrice(int oPrice) {
        this.oPrice = oPrice;
    }

    public int getoSizeColorId() {
        return oSizeColorId;
    }

    public void setoSizeColorId(int oSizeColorId) {
        this.oSizeColorId = oSizeColorId;
    }

    public String getkName() {
        return kName;
    }

    public void setkName(String kName) {
        this.kName = kName;
    }

    public int getkPrice() {
        return kPrice;
    }

    public void setkPrice(int kPrice) {
        this.kPrice = kPrice;
    }

    public String getkCode() {
        return kCode;
    }

    public void setkCode(String kCode) {
        this.kCode = kCode;
    }

    public int getkGroup() {
        return kGroup;
    }

    public void setkGroup(int kGroup) {
        this.kGroup = kGroup;
    }

    public int getkVirtualType() {
        return kVirtualType;
    }

    public void setkVirtualType(int kVirtualType) {
        this.kVirtualType = kVirtualType;
    }

    public int getkVirtual() {
        return kVirtual;
    }

    public void setkVirtual(int kVirtual) {
        this.kVirtual = kVirtual;
    }

    public int getkWeight() {
        return kWeight;
    }

    public void setkWeight(int kWeight) {
        this.kWeight = kWeight;
    }

    public String getkAksAdd() {
        return kAksAdd;
    }

    public void setkAksAdd(String kAksAdd) {
        this.kAksAdd = kAksAdd;
    }

    public int getSub() {
        return sub;
    }

    public void setSub(int sub) {
        this.sub = sub;
    }

    public int getKalaSub() {
        return kalaSub;
    }

    public void setKalaSub(int kalaSub) {
        this.kalaSub = kalaSub;
    }

    public String gettColor() {
        return tColor;
    }

    public void settColor(String tColor) {
        this.tColor = tColor;
    }

    public String gettSize() {
        return tSize;
    }

    public void settSize(String tSize) {
        this.tSize = tSize;
    }

    public String getTmaterial() {
        return tmaterial;
    }

    public void setTmaterial(String tmaterial) {
        this.tmaterial = tmaterial;
    }
    public String gettCode() {
        return tCode;
    }

    public void settCode(String tCode) {
        this.tCode = tCode;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public int getpKalaId() {
        return pKalaId;
    }

    public void setpKalaId(int pKalaId) {
        this.pKalaId = pKalaId;
    }

    public String getpTitle() {
        return pTitle;
    }

    public void setpTitle(String pTitle) {
        this.pTitle = pTitle;
    }

    public int getpPrice() {
        return pPrice;
    }

    public void setpPrice(int pPrice) {
        this.pPrice = pPrice;
    }

    public int getpPermisionUserGroup() {
        return pPermisionUserGroup;
    }

    public void setpPermisionUserGroup(int pPermisionUserGroup) {
        this.pPermisionUserGroup = pPermisionUserGroup;
    }
    public int getpPermision() {
        return pPermision;
    }

    public void setpPermision(int pPermision) {
        this.pPermision = pPermision;
    }

    public int getpKalaID() {
        return pKalaID;
    }

    public void setpKalaID(int pKalaID) {
        this.pKalaID = pKalaID;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }
}
