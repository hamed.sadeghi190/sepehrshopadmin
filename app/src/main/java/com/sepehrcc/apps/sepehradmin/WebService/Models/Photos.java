package com.sepehrcc.apps.sepehradmin.WebService.Models;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Hamed Sadeghi on 28/11/2017.
 */

public class Photos {

    @SerializedName("iId")
    private int _iId;
    public int GetPhotoID() { return _iId; }
    public Photos SetPhotoID(int value) {_iId = value;return this;}

    @SerializedName("iName")
    private String _iName;
    public String GetPhotoAddress() { return _iName; }
    public Photos SetPhotoAddress(String value) {_iName = value;return this;}

    @SerializedName("iAlbum")
    private int _iAlbum;
    public int GetAlbumID() { return _iAlbum; }
    public Photos SetAlbumID(int value) {_iAlbum = value;return this;}
}
