package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

public class View_Holder_Order extends RecyclerView.ViewHolder {
    @BindView(R.id.order_card_view)
    CardView cv;
    @BindView(R.id.order_list_item_name)
    TextView name;
    @BindView(R.id.order_list_item_price)
    TextView price;
    @BindView(R.id.order_list_item_id)
    TextView ID;
    @BindView(R.id.vahed)
    TextView vahed;
    @BindView(R.id.order_list_ripple)
    RippleView rippleView;
    @BindView(R.id.lbl_order_status)
    TextView lbl_order_status;
    @BindView(R.id.img_paid)
    ImageView img_paid;
    @BindView(R.id.lbl_payed)
    TextView lbl_payed;

    public View_Holder_Order(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        name.setTypeface(Fonts.VazirBoldFD);
        ID.setTypeface(Fonts.VazirFD);
        price.setTypeface(Fonts.VazirFD);
        vahed.setTypeface(Fonts.VazirFD);
        lbl_order_status.setTypeface(Fonts.VazirFD);
        lbl_payed.setTypeface(Fonts.VazirFD);

    }
}
