package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.Models.Gallery;

import com.sepehrcc.apps.sepehradmin.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Hamed Sadeghi on 18/10/2017.
 */

public class Photo_Adapter extends RecyclerView.Adapter<View_Holder_Gallery> {

    List<Gallery> list = Collections.emptyList();
    Context context;

    public Photo_Adapter(List<Gallery> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder_Gallery onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_list_item, parent, false);
        View_Holder_Gallery holder = new View_Holder_Gallery(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_Gallery holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.img.setImageResource(list.get(position).image);
        holder.rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                //do somthing after ripple compelete
            }
        });

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, Gallery gallery) {
        list.add(position, gallery);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(Gallery order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
    }

}

