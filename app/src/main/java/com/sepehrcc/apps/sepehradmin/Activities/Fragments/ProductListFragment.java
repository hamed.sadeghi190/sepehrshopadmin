package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Recycler_View_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddCategorayActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddProductActivity;
import com.sepehrcc.apps.sepehradmin.Models.Data;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohammad Hamed Sadeghi on 2017/10/12.
 * Email : hamed.sadeghi190@gmail.com
 */

public class ProductListFragment extends Fragment {
    View view;
    @BindView(R.id.lbl_title)
    TextView lbl_title;
    @BindView(R.id.editSearch_Orders)
    SearchView product_Edit_Search;
    //    @BindView(R.id.FAb_Add_product)
//    FloatingActionButton FAb_Add_product;
    @BindView(R.id.fab_adding_group)
    FloatingActionButton fab_adding_group;
    @BindView(R.id.fab_adding_product)
    FloatingActionButton fab_adding_product;
    @BindView(R.id.add_menu)
    FloatingActionMenu add_menu;
    List<Data> data = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_product_list, container, false);
        ButterKnife.bind(this, view);
        G.context = getContext();
        product_Edit_Search.setIconifiedByDefault(false);
        product_Edit_Search.setFocusable(false);
        product_Edit_Search.setIconified(true);
//        fill_with_data();
        initialView(view);
        BindEvents();
        return view;
    }

    private void BindEvents() {
        fab_adding_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddProductActivity.class);
                startActivity(intent);
            }
        });
        fab_adding_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddCategorayActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initialView(View v) {
        lbl_title.setTypeface(Fonts.VazirBold);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(100);
        itemAnimator.setRemoveDuration(1000);
//        data.addAll(fill_with_data());


        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.lst_products);
//        Recycler_View_Adapter adapter = new Recycler_View_Adapter(data, getContext());
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

//    public List<Data> fill_with_data() {
//
//        List<Data> data2 = new ArrayList<>();
//
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_main_slider));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_box));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_cart));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_plus));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//        data2.add(new Data("عنوان کالا", "عنوان گروه ", R.drawable.img_empty));
//
//        return data2;
//    }
}
