package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by Hamed Sadeghi on 12/06/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class SimpleAdapter extends BaseAdapter{
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
