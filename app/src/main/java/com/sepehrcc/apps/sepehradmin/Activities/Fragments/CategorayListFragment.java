package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.Categoray_list_Adapter;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddCategorayActivity;
import com.sepehrcc.apps.sepehradmin.Models.Category;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CategorayListFragment extends Fragment {
    View view;
    @BindView(R.id.Btn_add_category)
    FloatingActionButton Btn_add_category;
    @BindView(R.id.txtGroups)
    TextView txtGroups;
    @BindView(R.id.product_search_view)
    SearchView editSearch;
    @BindView(R.id.list)
    RecyclerView recyclerView;


    Categoray_list_Adapter adapter;
    List<Category> data;

    public CategorayListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_category, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        G.context = getContext();
        ButterKnife.bind(this, view);
        init();
        BindControlsEvents();
        return view;
    }


    public List<Category> fill_with_data() {

        List<Category> data = new ArrayList<>();

        data.add(new Category("اسباب بازی", 10, R.drawable.img_toys));
        data.add(new Category("اسباب بازی", 10, R.drawable.img_toys));
        data.add(new Category("اسباب بازی", 10, R.drawable.img_toys));
        data.add(new Category("اسباب بازی", 10, R.drawable.img_toys));
        data.add(new Category("اسباب بازی", 10, R.drawable.img_toys));
        data.add(new Category("اسباب بازی", 10, R.drawable.img_toys));
        return data;
    }

    private void init() {
        editSearch.setIconifiedByDefault(false);
        editSearch.setFocusable(false);
        editSearch.setIconified(true);
        txtGroups.setTypeface(Fonts.VazirBold);
//        data = fill_with_data();
//        adapter = new Categoray_list_Adapter(data, getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
    private void BindControlsEvents() {
        Btn_add_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.CurrentActivity, AddCategorayActivity.class);
                startActivity(intent);
            }
        });
    }
}
