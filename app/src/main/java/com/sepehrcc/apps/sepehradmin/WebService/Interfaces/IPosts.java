package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Posts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.PostsCopy;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Administrator on 24/04/2018.
 */

public interface IPosts {
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("Shops/posts")
    Call<ArrayList<Posts>> GetPosts(@Header("Authorization") String AuthKey);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Shops/posts/add")
    Call<AddResponse> AddPost(@Header("Authorization") String AuthKey, @Body PostsCopy value);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Shops/posts/edit/{id}")
    Call<AddResponse> MonoEdit(@Header("Authorization") String AuthKey, @Body Posts value, @Path("id") int id);
    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @POST("Shops/posts/edit")
    Call<AddResponse> MultiEdit(@Header("Authorization") String AuthKey, @Body ArrayList<Posts> value);

}
