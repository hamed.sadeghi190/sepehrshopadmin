package com.sepehrcc.apps.sepehradmin.WebService.Interfaces;

import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Kala;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ResponseBase;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;

public interface IProducts {


    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @POST("Product/add")
    Call<AddResponse> AddNewProduct(@Header("Authorization") String AuthKey, @Body Kala value);

    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @POST("Product/delete/{id}")
    Call<ResponseBase> DeleteProduct(@Header("Authorization") String AuthKey, @Path("id") int kid);
    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @POST("Product/edit/{kalaId}")
    Call<AddResponse> EditProduct(@Header("Authorization") String AuthKey, @Body Kala value,@Path("kalaId") int id);

    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("product/all/{Offset}/{Limit}")
    Call<ArrayList<Kala>> GetKala(@Header("Authorization") String AuthKey, @Path("Offset") int Offset, @Path("Limit") int Limit);

    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("product/allforcache")
    Call<ArrayList<Kala>> GetKalaForCache(@Header("Authorization") String AuthKey);

    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("product/info/{id}")
    Call<Kala> GetKalaInfo(@Header("Authorization") String AuthKey, @Path("id") int id);

    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("product/bygroup/{GroupId}/{Offset}/{Limit}")
    Call<ArrayList<Kala>> GetKalaByGroup(@Header("Authorization") String AuthKey, @Path("GroupId") int GroupId, @Path("Offset") int Offset, @Path("Limit") int Limit);

    @Headers(value = {
            "Accept:application/json",
            "Type:Kala"
    })
    @Multipart
    @POST("FileUpload")
    Call<ResponseBase> UploadPhoto(@Header("Authorization") String AuthKey, @Header("Id") int KalaID, @Part("File") RequestBody File);

    @Headers({
            "content-type: application/json",
            "Accept:application/json",
    })
    @GET("product/search/{offset}/{limit}/{searchkey}")
    Call<ArrayList<Kala>> Search(@Header("Authorization") String AuthKey,@Path("offset") int offset, @Path("limit") int limit, @Path("searchkey") String searchkey);

    @Headers(value = {
            "content-type: application/json",
            "Accept:application/json"
    })
    @GET("Product/deletephoto/{kid}/{pid}/{type}")
    Call<ResponseBase> DeletePhoto(@Header("Authorization") String AuthKey,@Path("kid") int id,@Path("pid") int pid,@Path("type") String type);
}
