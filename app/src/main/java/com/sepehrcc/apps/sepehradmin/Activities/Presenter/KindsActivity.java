
package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.AddKindsAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.AddTypeAdapter;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.ChechTypeAdapter;
import com.sepehrcc.apps.sepehradmin.Models.CheckType;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class KindsActivity extends AppCompatActivity {
    @BindView(R.id.type_btn_Save)
    Button type_btn_Save;
    @BindView(R.id.txt_types_title)
    TextView txt_types_title;
    @BindView(R.id.type_btn_cancle)
    Button type_btn_cancle;
    @BindView(R.id.txt_ly_edt_type_kala)
    TextInputLayout txt_ly_edt_type_kala;
    @BindView(R.id.edt_type_kala)
    EditText edt_type_kala;
    @BindView(R.id.lbl_add_new_type)
    TextView lbl_add_new_type;
    @BindView(R.id.txt_example)
    TextView txt_example;
    @BindView(R.id.txt_example2)
    TextView txt_example2;
    @BindView(R.id.list_add_type)
    RecyclerView list_add_type;
    AddKindsAdapter typeAdapter;
    List<KindGroup> chech_data = new ArrayList<KindGroup>();
    List<KindGroup> without_change = new ArrayList<KindGroup>();
    String jsonstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_kinds);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);


        for (int i = 0; i < getIntent().getStringArrayListExtra("hhh").size(); i++) {
            KindGroup kindGroup = new KindGroup();
            chech_data.add(kindGroup.SetTitle(getIntent().getStringArrayListExtra("hhh").get(i)));
        }
        String Citem = getIntent().getStringExtra("withoutchangelist");
        if (!(Citem.equals(""))) {

            Gson gson = new Gson();
            try {
                Type type = new TypeToken<ArrayList<KindGroup>>() {
                }.getType();
                without_change = gson.fromJson(Citem, type);
            } catch (Exception ex) {

            }
        }
        fill_list_type(chech_data);
        initialView();
        BindViewControl();
    }

    private void initialView() {
        type_btn_Save.setTypeface(Fonts.VazirBoldFD);
        txt_types_title.setTypeface(Fonts.VazirBoldFD);
        type_btn_cancle.setTypeface(Fonts.VazirBoldFD);
        txt_ly_edt_type_kala.setTypeface(Fonts.VazirBoldFD);
        edt_type_kala.setTypeface(Fonts.VazirBoldFD);
        lbl_add_new_type.setTypeface(Fonts.VazirBoldFD);
        txt_example.setTypeface(Fonts.VazirBoldFD);
        txt_example2.setTypeface(Fonts.VazirBoldFD);

    }

    private void fill_list_type(List<KindGroup> data) {
        typeAdapter = new AddKindsAdapter(data, without_change, this);
        list_add_type.setAdapter(typeAdapter);
        list_add_type.setLayoutManager(new LinearLayoutManager(this));
    }

    private void BindViewControl() {
        lbl_add_new_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(edt_type_kala.getText().toString().equals(""))) {
                    KindGroup c = new KindGroup();
                    c.SetTitle(edt_type_kala.getText().toString());
                    chech_data.add(c);
                    fill_list_type(chech_data);
                    KindGroup kg = new KindGroup();
                    kg.SetTitle(edt_type_kala.getText().toString());
                    kg.SetStatus(2);
                    typeAdapter.wothout_change.add(kg);
                    G.changekindslist.add(kg);
                    edt_type_kala.setText("");

                }
            }
        });
        type_btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                ArrayList<String> test = new ArrayList<String>();
                for (int k = 0; k < chech_data.size(); k++) {
                    test.add(chech_data.get(k).GetTitle());
                }
                Gson gson = new Gson();
                jsonstring = gson.toJson(typeAdapter.wothout_change);
                i.putStringArrayListExtra("typelist", test);
                i.putExtra("withoutchangelist", jsonstring);
                setResult(4, i);
                finish();
            }
        });
        type_btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
