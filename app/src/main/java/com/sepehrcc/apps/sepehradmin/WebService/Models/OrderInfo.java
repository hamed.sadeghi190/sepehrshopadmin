package com.sepehrcc.apps.sepehradmin.WebService.Models;


public class OrderInfo {
    public int ID;
    public String Code;
    public String Cost;
    public int Spayed;
    public int Status;
    public int sDcost;
    public int sPcost;
    public int sBasket;
    public String sReceiver;
    public String Uname;
    public String Utel;
    public String Utel2;
    public String SAdd;
    public String SAdd2;
    public String Email;
    public String Tozihat;
    public String PostalCode;
    public String SDelivery;
    public String sStxt;
    public String SDate;
    public int city;
    public int Ostan;
    public String yadasht;
    public String Rahgiry;
    public int sPid;
    public String BankName;
    public int karmozd;
    public String PackageName;

    public int getsPid() {
        return sPid;
    }

    public void setsPid(int sPid) {
        this.sPid = sPid;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public int getKarmozd() {
        return karmozd;
    }

    public void setKarmozd(int karmozd) {
        this.karmozd = karmozd;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }


    public String getYadasht() {
        return yadasht;
    }

    public void setYadasht(String yadasht) {
        this.yadasht = yadasht;
    }

    public String getRahgiry() {
        if (Rahgiry != null && !Rahgiry.equals("null")) {
            return Rahgiry;
        }
        return "";
    }

    public void setRahgiry(String rahgiry) {
        Rahgiry = rahgiry;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String cost) {
        Cost = cost;
    }

    public int getSpayed() {
        return Spayed;
    }

    public void setSpayed(int spayed) {
        Spayed = spayed;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getsDcost() {
        return sDcost;
    }

    public void setsDcost(int sDcost) {
        this.sDcost = sDcost;
    }

    public int getsPcost() {
        return sPcost;
    }

    public void setsPcost(int sPcost) {
        this.sPcost = sPcost;
    }

    public int getsBasket() {
        return sBasket;
    }

    public void setsBasket(int sBasket) {
        this.sBasket = sBasket;
    }

    public String getsReceiver() {
        return sReceiver;
    }

    public void setsReceiver(String sReceiver) {
        this.sReceiver = sReceiver;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getUtel() {
        return Utel;
    }

    public void setUtel(String utel) {
        Utel = utel;
    }

    public String getUtel2() {
        return Utel2;
    }

    public void setUtel2(String utel2) {
        Utel2 = utel2;
    }

    public String getSAdd() {
        return SAdd;
    }

    public void setSAdd(String SAdd) {
        this.SAdd = SAdd;
    }

    public String getSAdd2() {
        return SAdd2;
    }

    public void setSAdd2(String SAdd2) {
        this.SAdd2 = SAdd2;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTozihat() {
        return Tozihat;
    }

    public void setTozihat(String tozihat) {
        Tozihat = tozihat;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getSDelivery() {
        return SDelivery;
    }

    public void setSDelivery(String SDelivery) {
        this.SDelivery = SDelivery;
    }

    public String getsStxt() {
        if (sStxt != null) {
            return sStxt;
        }
        return "";
    }

    public void setsStxt(String sStxt) {
        this.sStxt = sStxt;
    }

    public String getSDate() {
        return SDate;
    }

    public void setSDate(String SDate) {
        this.SDate = SDate;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getOstan() {
        return Ostan;
    }

    public void setOstan(int ostan) {
        Ostan = ostan;
    }
}
