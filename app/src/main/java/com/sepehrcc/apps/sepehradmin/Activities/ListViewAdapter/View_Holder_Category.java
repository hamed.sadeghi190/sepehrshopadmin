package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Hamed Sadeghi on 15/10/2017.
 */


public class View_Holder_Category extends RecyclerView.ViewHolder {
    @BindView(R.id.card_view)
    CardView cv;
    @BindView(R.id.list_item_title)
    TextView title;
    @BindView(R.id.list_item_count)
    TextView count;
    @BindView(R.id.sub_group)
    TextView sub_gruop;
    @BindView(R.id.flesh)
    ImageView felesh;
    @BindView(R.id.list_item_image)
    CircleImageView imageView;
    @BindView(R.id.product_list_ripple1)
    RippleView product_list_ripple1;
    @BindView(R.id.product_list_ripple2)
    RippleView product_list_ripple2;
    @BindView(R.id.product_list_ripple3)
    RippleView product_list_ripple3;
    View_Holder_Category(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        title.setTypeface(Fonts.VazirFD);
        sub_gruop.setTypeface(Fonts.VazirFD);
        count.setTypeface(Fonts.VazirFD);
    }
}
