package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.ImageViewPagerAdapter;
import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;
import com.sepehrcc.apps.sepehradmin.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SlidinngGalleryActivity extends AppCompatActivity {
    List<ProductGallery> productGalleries = new ArrayList<ProductGallery>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slidinng_gallery);
        ViewPager viewPager=(ViewPager)findViewById(R.id.viewPager) ;
        ImageView img_sliding_gallery=(ImageView)findViewById(R.id.img_sliding_gallery);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String images = bundle.getString("images");
            Gson gson=new Gson();
            Type collectionType = new TypeToken<List<ProductGallery>>() {
            }.getType();
            productGalleries=gson.fromJson(images,collectionType);
        }
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        viewPager.setAdapter(new ImageViewPagerAdapter(getSupportFragmentManager(), this, productGalleries));

    }
}
