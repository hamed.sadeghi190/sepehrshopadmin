package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed Sadeghi on 11/06/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class ShopTable {
    @SerializedName("sId")
    public int SId;
    public String SName;
    public String SSub;
    @SerializedName("sTitle")
    public String STitle;
    public String SSharh;
    public String SMail;
    public String SField;
    @SerializedName("sUname")
    public String SUname;
    public boolean SEnable;
    public String SDate;
    public String Stheme;
    public String SMaster;
    public String Si1;
    public String Si2;
    public String Si3;
    public String Si4;
    public String Si5;
    public String Si6;
    public String Si7;
    public String Si8;
    public String Si9;
    public String Si10;
    public int SStatus;
    public int SPlan;
    public String SExp;
    public String SAut;
    public String SDomain;
    public String SDomainReg;
    public String SDomainExp;
    public int SSpace;
    public short SLanguage;
    public int SNotificationCount;
    public double SSms;
    public String SSmsNum;
    public String SSmsUser;
    public String SSmsPass;
    public short Stype;
    public short SOriginalDomain;
    public int SPrice;
    public int SHostSpace;
    public int SUsedSpace;
    public String SResseler;
    public int SPayMonths;
    public short SSellStatus;
    public String SSmsProvider;
    public boolean SEmailState;
    public String SFaxUserName;
    public String SFaxPassword;
    public String SFaxNumber;
    public short SShopPlan;
    public short SSitePlan;
    public boolean SServiceModule;
    public boolean SAccountingModule;
    public boolean SSmsModule;
    public boolean SAdvModule;
    public boolean SIsCloud;
    public boolean SIsFree;
    public boolean SForAdmin;
    public String SApiKey;
    public String SReference;
    public short SHowFind;
    public int SAdminType;
    public String SSiteLogo;
    public boolean SApiFake;
    public String SBotName;
    public String SBotToken;
    public String SAppPackage;
    public boolean SIsSsl;
}
