package com.sepehrcc.apps.sepehradmin.WebService.Models;

/**
 * Created by Hamed Sadeghi on 27/06/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class DocTable {
    public int DId ;
    public int DKalaId ;
    public String DTitle ;
    public String DComment ;
    public String DMatn ;
    public String DAks ;
    public String DAutor ;
    public String DDate ;
    public boolean DShow ;
    public int Sub ;
    public boolean DMore ;
    public int DGroupId ;
    public float DRatting ;
    public int DRateCount ;
    public String DAccessUrl ;
}
