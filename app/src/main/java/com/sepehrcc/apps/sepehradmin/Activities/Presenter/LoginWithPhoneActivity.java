package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fxn.cue.Cue;
import com.fxn.cue.enums.Duration;
import com.fxn.cue.enums.Type;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginWithPhoneActivity extends AppCompatActivity {
    @BindView(R.id.txt_phonenumber)
    TextView txt_phonenumber;
    @BindView(R.id.btn_LoginWP)
    Button btn_LoginWP;
    WebServicelistener listener;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_login_with_phone);
        ButterKnife.bind(this);
        G.CurrentActivity = this;
        initialClicks();
    }

    @Override
    protected void onResume() {
        super.onResume();
        G.CurrentActivity=this;
    }

    private void initialClicks() {
        btn_LoginWP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Number = txt_phonenumber.getText().toString();
                if (Number.length() == 11 || Number.length()==12) {
                    String Sub = Number.substring(0, 2);
                    if (Sub.equals("09") || Sub.equals("98")) {
                        if (Sub.equals("98"))
                        {
                            Number = "0" + Number.substring(2,Number.length());
                        }
                        Check(Number);
                    } else {
                        Toast.makeText(G.CurrentActivity, "شماره تلفن را صحیح وارد نمایید", Toast.LENGTH_LONG).show();
                    }
                } else {

                    Toast.makeText(G.CurrentActivity, "شماره تلفن را صحیح وارد نمایید", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void Check(final String number) {
        dialog = new Dialog(G.CurrentActivity);
        listener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                dialog.dismiss();
                feedback fback = (feedback) object;
                if (fback.Status == 1) {
                    Intent intent = new Intent(G.CurrentActivity, SignupVerifyActivity.class);
                    intent.putExtra("phone", number);
                    intent.putExtra("pin", fback.value);
                    intent.putExtra("action", "login");
                    startActivity(intent);
                }
                else if (fback.Status == 3)
                {
                    new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("خطای ورود")
                            .setContentText("شماره وارد شده صحیح نمی باشد.")
                            .setCancelText("اصلاح شماره")
                            .setConfirmText("عضویت")
                            .showCancelButton(true)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    Intent intent = new Intent(G.CurrentActivity, SignUpActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.cancel();
                                }
                            })
                            .show();
                }
                else
                {
                    Toast.makeText(G.CurrentActivity, fback.Message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void OnFail(Object object) {
                dialog.dismiss();
                feedback fback = (feedback) object;
                Toast.makeText(G.CurrentActivity, fback.Message, Toast.LENGTH_LONG).show();
            }
        };
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
        new SecurityRepository().SendloginCode(listener, number);
    }
}
