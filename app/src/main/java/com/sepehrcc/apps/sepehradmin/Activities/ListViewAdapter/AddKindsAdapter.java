package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.WebService.Models.KindGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 25/11/2017.
 */

public class






AddKindsAdapter extends RecyclerView.Adapter<View_Holder_Add_Type> {

    List<KindGroup> list = Collections.emptyList();
    public List<KindGroup> wothout_change = new ArrayList<KindGroup>();
    Context context;

    public AddKindsAdapter(List<KindGroup> list, List<KindGroup> witout_change, Context context) {
        this.list = list;
        this.context = context;
        this.wothout_change = witout_change;
    }

    @Override
    public View_Holder_Add_Type onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_add_type_row_themplate, parent, false);
        View_Holder_Add_Type holder = new View_Holder_Add_Type(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_Add_Type holder, final int position) {

        holder.lbl_type_item_title.setText(list.get(position).GetTitle());

        holder.type_list_ripple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                for (int i = 0; i < wothout_change.size(); i++) {
                    if (wothout_change.get(i).GetTitle().equals(list.get(position).GetTitle())) {
                        wothout_change.get(i).SetStatus(1);
                    }
                }
                remove(list.get(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, KindGroup gallery) {
        list.add(position, gallery);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(KindGroup order) {
        int position = list.indexOf(order);
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

}
