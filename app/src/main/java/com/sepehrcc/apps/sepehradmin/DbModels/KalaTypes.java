package com.sepehrcc.apps.sepehradmin.DbModels;

import com.activeandroid.annotation.Column;

/**
 * Created by Hamed Sadeghi on 13/06/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
public class KalaTypes {

    @Column(name ="KindID")
    private int _kKindId;
    public int GetKindID() { return _kKindId; }
    public KalaTypes SetKindID(int value) {_kKindId = value;return this;}

    @Column(name ="KalaID")
    private int _KalaId;
    public int GetKalaID() { return _KalaId; }
    public KalaTypes SetKalaID(int value) {_KalaId = value;return this;}

    @Column(name ="Sub")
    private int _sub;
    public int GetSubID() { return _sub; }
    public KalaTypes SetSubID(int value) {_sub = value;return this;}

}