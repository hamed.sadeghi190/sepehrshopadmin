package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Register;
import com.sepehrcc.apps.sepehradmin.WebService.Models.feedback;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.SecurityRepository;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Hamed Sadeghi on 09/04/2018.
 * Email : hamed.sadeghi190@gmail.com
 */
@SuppressWarnings("ALL")
@SuppressLint("Registered")
public class SignUpActivity extends AppCompatActivity {
    @BindView(R.id.btn_SignUp)
    Button BtnSignup;
    @BindView(R.id.edt_phonenumber)
    EditText edt_phonenumber;
    @BindView(R.id.lbl_title_login)
    TextView lbl_title_login;
    @BindView(R.id.edt_store_id)
    EditText edt_store_id;
    @BindView(R.id.edt_store_title)
    EditText edt_store_title;
    @BindView(R.id.edt_user_fullname)
    EditText edt_user_fullname;
    @BindView(R.id.lbl_subdomain)
    TextView lbl_subdomain;
    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;
    WebServicelistener listener;
    Dialog dialog;

    public static void setFontForActivity(View view) {
        Typeface tf =  Typeface.createFromAsset(view.getContext().getAssets(), "Fonts/vazir.ttf");
        Typeface tfb = Typeface.createFromAsset(view.getContext().getAssets(), "Fonts/vazirbold.ttf");
        Typeface tfl = Typeface.createFromAsset(view.getContext().getAssets(), "Fonts/vazir_light.ttf");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_signup);
        ButterKnife.bind(this);
        G.CurrentActivity = this;
        BindControlsEvents();
        setFontForActivity(mainLayout);
    }

    private void BindControlsEvents() {

        BtnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(G.CurrentActivity);
                listener = new WebServicelistener() {
                    @Override
                    public void OnComplete(Object object) {
                        dialog.dismiss();
                        feedback fback = (feedback) object;
                        if (fback.Status == 1) {
                            Intent intent = new Intent(G.CurrentActivity, SignupVerifyActivity.class);
                            intent.putExtra("phone", edt_phonenumber.getText().toString());
                            intent.putExtra("action", "signup");
                            intent.putExtra("pin", fback.exception);
                            Register RegisterObj = FillRegisterModel();
                            String json = new Gson().toJson(RegisterObj);
                            intent.putExtra("info", json);
                            startActivity(intent);
                        } else if (fback.Status == 4) {
                            new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("خطا")
                                    .setContentText("این شماره قبلا ثبت شده است")
                                    .show();
                        } else {
                            new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("خطا")
                                    .setContentText(fback.Message)
                                    .show();
                        }
                    }

                    @Override
                    public void OnFail(Object object) {
                        dialog.dismiss();
                        feedback fback = (feedback) object;
                        new SweetAlertDialog(G.CurrentActivity, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("خطا")
                                .setContentText(fback.Message)
                                .show();
                    }
                };
                Register RegisterObj = FillRegisterModel();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                if (Validate()) {
                    dialog.show();
                    new SecurityRepository().SendRegisterCode(listener, RegisterObj);
                }

            }
        });


        edt_store_id.addTextChangedListener(new TextWatcher() {
            @SuppressLint("SetTextI18n")
            public void afterTextChanged(Editable s) {
                lbl_subdomain.setText("www." + s.toString() + ".sepehrcc.com");
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    private Boolean Validate() {
        String check = edt_phonenumber.getText().toString();
        if (!check.matches("(\\+98|0)?9\\d{9}")) {
            edt_phonenumber.setError("شماره موبایل نامعتبر هست");
            return false;
        }
        String fullname = edt_user_fullname.getText().toString();
        if (fullname.trim().length() == 0) {
            edt_user_fullname.setError("لطفا نام خود را وارد نمایید.");
            return false;
        }

        String shopTile = edt_store_title.getText().toString();
        if (fullname.trim().length() == 0) {
            edt_store_title.setError("لطفا عنوان فروشگاه را وارد نمایید.");
            return false;
        }
        String store_id = edt_store_id.getText().toString();
        if (store_id.trim().length() == 0) {
            edt_store_id.setError("لطفا نام فروشگاه را وارد نمایید.");
            return false;
        }
        return true;
    }

    private Register FillRegisterModel() {
        Register register_tmp = new Register();
        register_tmp.SetphoneNumber(edt_phonenumber.getText().toString());
        register_tmp.SetuserName(edt_user_fullname.getText().toString());
        register_tmp.SetsiteName(edt_store_title.getText().toString());
        register_tmp.SetsubDomain(edt_store_id.getText().toString());
        register_tmp.SetFullname(edt_user_fullname.getText().toString());
        register_tmp.SetPin("0");
        return register_tmp;
    }
}

