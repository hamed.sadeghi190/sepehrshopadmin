package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hamed Sadeghi on 18/10/2017.
 */

public class View_Holder_Gallery extends RecyclerView.ViewHolder {
    @BindView(R.id.gallery_card_view)
    CardView cv;
    @BindView(R.id.img_gallery)
    ImageView img;
    @BindView(R.id.gallery_ripple)
    RippleView rippleView;
    public View_Holder_Gallery(View itemview)
    {   super(itemview);
        ButterKnife.bind(this,itemView);

    }
}
