package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.thefinestartist.finestwebview.FinestWebView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FirstGroupActivity extends AppCompatActivity {
    @BindView(R.id.lbl_enter_first_group)
    TextView lbl_enter_first_group;

    @BindView(R.id.lbl_how_to_provide_products)
    TextView lbl_how_to_provide_products;
    @BindView(R.id.img_add_first_group)
    ImageView img_add_first_group;
    @BindView(R.id.img_first_group)
    ImageView img_first_group;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ly_activity_first_group);
        ButterKnife.bind(this);
        initalView();
        bindViewControl();
    }
    public void initalView(){
        lbl_enter_first_group.setTypeface(Fonts.VazirBoldFD);
        lbl_how_to_provide_products.setTypeface(Fonts.VazirBoldFD);
   }
    public void bindViewControl(){
        img_add_first_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(FirstGroupActivity.this,AddCategorayActivity.class);
                startActivity(intent);
                finish();
            }
        });
        lbl_enter_first_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://help.sepehrcc.com/products"));
//                startActivity(browserIntent);
                new FinestWebView.Builder(FirstGroupActivity.this).show("http://help.sepehrcc.com/products");

            }
        });


    }
}
