package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.Models.SubGroup;
import com.sepehrcc.apps.sepehradmin.Models.SubGroupProductGallery;
import com.sepehrcc.apps.sepehradmin.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 12/11/2017.
 */

public class Sub_Group_Product_Gallery_Adapter extends RecyclerView.Adapter<View_Holder_Sub_Group_Product_Gallery> {

    List<SubGroupProductGallery> list = Collections.emptyList();
    Context context;

    public Sub_Group_Product_Gallery_Adapter(List<SubGroupProductGallery> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder_Sub_Group_Product_Gallery onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lst_sub_group_product_gallery_template_item, parent, false);
        View_Holder_Sub_Group_Product_Gallery holder = new View_Holder_Sub_Group_Product_Gallery(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder_Sub_Group_Product_Gallery holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.sub_group_product_txt_gallery.setText(list.get(position).sub_group_product_name);
        holder.sub_group_product_img_gallery.setImageResource(list.get(position).sub_group_product_image);
        holder.sub_group_product_gallery_ripple.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                //do somthing after ripple compelete
            }
        });

    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, SubGroupProductGallery data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(SubGroupProductGallery data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

}