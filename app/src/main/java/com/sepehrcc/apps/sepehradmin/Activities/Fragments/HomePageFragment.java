package com.sepehrcc.apps.sepehradmin.Activities.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.AddProductActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.MainActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.PaymentMthodsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.PostMethodsActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.SplashScreenActivity;
import com.sepehrcc.apps.sepehradmin.Activities.Presenter.UpgradeActivity;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.DocTable;
import com.thefinestartist.finestwebview.FinestWebView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;


public class HomePageFragment extends Fragment {
    View view;
    @BindView(R.id.taskcard_one_title)
    TextView taskcard_one_title;
    @BindView(R.id.taskcard_one_desc)
    TextView taskcard_one_desc;
    @BindView(R.id.taskcard_one_action)
    TextView taskcard_one_action;
    @BindView(R.id.taskcard_one_more)
    TextView taskcard_one_more;
    @BindView(R.id.taskcard_two_title)
    TextView taskcard_two_title;
    @BindView(R.id.taskcard_two_action)
    TextView taskcard_two_action;
    @BindView(R.id.taskcard_three_title)
    TextView taskcard_three_title;
    @BindView(R.id.txt_uname)
    TextView txt_uname;
    @BindView(R.id.taskcard_three_desc)
    TextView taskcard_three_desc;
    @BindView(R.id.taskcard_three_action)
    TextView taskcard_three_action;
    @BindView(R.id.taskcard_three_guide)
    TextView taskcard_three_guide;
    @BindView(R.id.taskcard_four_title)
    TextView taskcard_four_title;
    @BindView(R.id.taskcard_four_desc)
    TextView taskcard_four_desc;
    @BindView(R.id.taskcard_four_action)
    TextView taskcard_four_action;
    @BindView(R.id.taskcard_four_more)
    TextView taskcard_four_more;
    @BindView(R.id.TxtDorod)
    TextView TxtDorod;
    @BindView(R.id.TxtletStart)
    TextView TxtletStart;
    @BindView(R.id.taskcard_two)
    CardView taskcard_two;
    @BindView(R.id.taskcard_two_img)
    ImageView taskcard_two_img;
    @BindView(R.id.taskcard_three)
    CardView taskcard_three;
    @BindView(R.id.taskcard_three_img)
    ImageView taskcard_three_img;
    @BindView(R.id.taskcard_four)
    CardView taskcard_four;
    @BindView(R.id.taskcard_four_img)
    ImageView taskcard_four_img;
    @BindView(R.id.taskcard_one)
    CardView taskcard_one;
    @BindView(R.id.taskcard_one_img)
    ImageView taskcard_one_img;
    @BindView(R.id.rlt_two)
    RelativeLayout rlt_two;
    @BindView(R.id.btn_openSite)
    LinearLayout btn_openSite;
    @BindView(R.id.lbl_yourwebsite_title)
    TextView lbl_yourwebsite_title;

    @BindView(R.id.crd_article_1)
    CardView crd_article_1;
    @BindView(R.id.img_article_1)
    ImageView img_article_1;
    @BindView(R.id.lbl_article_title_1)
    TextView lbl_article_title_1;
    @BindView(R.id.lbl_article_desc_1)
    TextView lbl_article_desc_1;
    @BindView(R.id.crd_article_2)
    CardView crd_article_2;
    @BindView(R.id.img_article_2)
    ImageView img_article_2;
    @BindView(R.id.lbl_article_title_2)
    TextView lbl_article_title_2;
    @BindView(R.id.lbl_article_desc_2)
    TextView lbl_article_desc_2;
    @BindView(R.id.crd_article_3)
    CardView crd_article_3;
    @BindView(R.id.img_article_3)
    ImageView img_article_3;
    @BindView(R.id.lbl_article_title_3)
    TextView lbl_article_title_3;
    @BindView(R.id.lbl_article_desc_3)
    TextView lbl_article_desc_3;
    @BindView(R.id.crd_article_4)
    CardView crd_article_4;
    @BindView(R.id.img_article_4)
    ImageView img_article_4;
    @BindView(R.id.lbl_article_title_4)
    TextView lbl_article_title_4;
    @BindView(R.id.lbl_article_desc_4)
    TextView lbl_article_desc_4;
    @BindView(R.id.crd_article_5)
    CardView crd_article_5;
    @BindView(R.id.img_article_5)
    ImageView img_article_5;
    @BindView(R.id.lbl_article_title_5)
    TextView lbl_article_title_5;
    @BindView(R.id.lbl_article_desc_5)
    TextView lbl_article_desc_5;
    @BindView(R.id.taskcard_upgrade)
    CardView taskcard_upgrade;
    @BindView(R.id.taskcard_upgrade_title)
    TextView taskcard_upgrade_title;
    @BindView(R.id.taskcard_upgrade_action)
    TextView taskcard_upgrade_action;
    @BindView(R.id.lbl_read_done_1)
    TextView lbl_read_done_1;
    @BindView(R.id.lbl_read_done_2)
    TextView lbl_read_done_2;
    @BindView(R.id.lbl_read_done_3)
    TextView lbl_read_done_3;
    @BindView(R.id.lbl_read_done_4)
    TextView lbl_read_done_4;
    @BindView(R.id.lbl_read_done_5)
    TextView lbl_read_done_5;
    @BindView(R.id.lbl_done_2)
    TextView lbl_done_2;
    @BindView(R.id.lbl_done_1)
    TextView lbl_done_1;
    @BindView(R.id.lbl_done_3)
    TextView lbl_done_3;
    @BindView(R.id.lbl_done_4)
    TextView lbl_done_4;

    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ly_frg_home, container, false);
        ButterKnife.bind(this, view);
        InitialView();
        bindViewControl();
        return view;
    }

    private void InitialView() {

        taskcard_one_title.setTypeface(Fonts.VazirBold);
        taskcard_one_desc.setTypeface(Fonts.Vazir);
        taskcard_one_action.setTypeface(Fonts.VazirBold);
        taskcard_one_more.setTypeface(Fonts.VazirBold);
        taskcard_two_title.setTypeface(Fonts.VazirBold);
        taskcard_two_action.setTypeface(Fonts.VazirBold);
        taskcard_three_title.setTypeface(Fonts.VazirBold);
        taskcard_three_desc.setTypeface(Fonts.Vazir);
        taskcard_three_action.setTypeface(Fonts.VazirBold);
        taskcard_three_guide.setTypeface(Fonts.VazirThin);
        taskcard_three_title.setTypeface(Fonts.VazirBold);
        taskcard_three_desc.setTypeface(Fonts.Vazir);
        taskcard_three_action.setTypeface(Fonts.VazirBold);
        taskcard_four_title.setTypeface(Fonts.VazirBold);
        taskcard_four_desc.setTypeface(Fonts.Vazir);
        taskcard_four_action.setTypeface(Fonts.VazirBold);
        taskcard_four_more.setTypeface(Fonts.VazirBold);
        txt_uname.setTypeface(Fonts.VazirBold);
        TxtDorod.setTypeface(Fonts.VazirBold);
        TxtletStart.setTypeface(Fonts.VazirLight);
        lbl_yourwebsite_title.setTypeface(Fonts.VazirLight);
        lbl_article_title_1.setTypeface(Fonts.VazirBoldFD);
        lbl_article_desc_1.setTypeface(Fonts.VazirFD);
        lbl_article_title_2.setTypeface(Fonts.VazirBoldFD);
        lbl_article_desc_2.setTypeface(Fonts.VazirFD);
        lbl_article_title_3.setTypeface(Fonts.VazirBoldFD);
        lbl_article_desc_3.setTypeface(Fonts.VazirFD);
        lbl_article_title_4.setTypeface(Fonts.VazirBoldFD);
        lbl_article_desc_4.setTypeface(Fonts.VazirFD);
        lbl_article_title_5.setTypeface(Fonts.VazirBoldFD);
        lbl_article_desc_5.setTypeface(Fonts.VazirFD);
        taskcard_upgrade_title.setTypeface(Fonts.VazirBold);
        taskcard_upgrade_action.setTypeface(Fonts.VazirBold);
        lbl_read_done_1.setTypeface(Fonts.VazirLight);
        lbl_read_done_2.setTypeface(Fonts.VazirLight);
        lbl_read_done_3.setTypeface(Fonts.VazirLight);
        lbl_read_done_4.setTypeface(Fonts.VazirLight);
        lbl_read_done_5.setTypeface(Fonts.VazirLight);
        lbl_done_2.setTypeface(Fonts.VazirLight);
        lbl_done_1.setTypeface(Fonts.VazirLight);
        lbl_done_3.setTypeface(Fonts.VazirLight);
        lbl_done_4.setTypeface(Fonts.VazirLight);
        String UserFullName = PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity.getApplicationContext()).getString("UserFullName", "");
        String textt = "شما از نسخه  " + G.appData.shopInfo.GetPlanName() + " استفاده می کنید.";
        taskcard_upgrade_title.setText(textt);
//        if (G.appData.shopInfo.GetPlan() == 3) {
//            taskcard_upgrade.setVisibility(View.GONE);
//        }

        if (!UserFullName.trim().equals("")) {
            SpannableString content = new SpannableString(UserFullName);
            content.setSpan(new UnderlineSpan(), 0, UserFullName.length() - 1, 0);
            txt_uname.setText(UserFullName);
        }
        LoadArticles();
        if (G.appData.shopInfo.GetpaymentMethodCount() != 0) {
            taskcard_one_img.setVisibility(View.VISIBLE);
            taskcard_one_action.setVisibility(View.GONE);
            taskcard_one.setBackgroundColor(getResources().getColor(R.color.selection));
            taskcard_one.setVisibility(View.GONE);
        }
        if (G.appData.shopInfo.GetSendMethodCount() != 0) {
            taskcard_three_guide.setVisibility(View.GONE);
            taskcard_three_img.setVisibility(View.VISIBLE);
            taskcard_three_action.setVisibility(View.GONE);
            taskcard_three.setBackgroundColor(getResources().getColor(R.color.selection));
            taskcard_three.setVisibility(View.GONE);
        }
        if (G.appData.shopInfo.GetKalaCount() != 0) {
            taskcard_two.setBackgroundColor(getResources().getColor(R.color.selection));
            taskcard_two_img.setVisibility(View.VISIBLE);
            taskcard_two_action.setVisibility(View.GONE);
            taskcard_two.setVisibility(View.GONE);
        }
        if (G.appData.shopInfo.GetKalaCount() == 0) {
            taskcard_two_img.setVisibility(View.GONE);
            taskcard_two_action.setVisibility(View.VISIBLE);
            taskcard_two.setVisibility(View.VISIBLE);
        }
        if (G.appData.shopInfo.GetDomainRegistr()) {
            taskcard_four.setBackgroundColor(getResources().getColor(R.color.selection));
            taskcard_four_img.setVisibility(View.VISIBLE);
            taskcard_four_action.setVisibility(View.GONE);
            taskcard_four_desc.setText("الان فروشگاه شما روی دامین " + G.appData.shopInfo.GetDomain() + " است");
            taskcard_four.setVisibility(View.GONE);
        } else {
            taskcard_four_img.setVisibility(View.GONE);
            taskcard_four_action.setVisibility(View.VISIBLE);
            taskcard_four_desc.setText("الان فروشگاه شما روی ساب دامین " + G.appData.shopInfo.GetsubDomain() + ".sepehrcc.com" + " است .شما می توانید یک دامنه ی رسمی روی فروشگاه خود ست کنید.");

        }
//        if (G.appData.shopInfo.GetPlanName().equals("الماس"))
//        {
//            taskcard_upgrade.setVisibility(View.GONE);
//        }
//        else {
//            taskcard_upgrade.setVisibility(View.VISIBLE);
//            taskcard_upgrade_title.setText("شما از نسخه " + G.appData.shopInfo.GetPlanName() + " استفاده می کنید .");
//        }
    }

    private void LoadArticles() {
        final ArrayList<DocTable> docs = new ArrayList<>(G.appData.shopInfo.getDocs());
        TextView lables[] = new TextView[5];
        lables[0] = lbl_article_title_1;
        lables[1] = lbl_article_title_2;
        lables[2] = lbl_article_title_3;
        lables[3] = lbl_article_title_4;
        lables[4] = lbl_article_title_5;

        CardView cards[] = new CardView[5];
        cards[0] = crd_article_1;
        cards[1] = crd_article_2;
        cards[2] = crd_article_3;
        cards[3] = crd_article_4;
        cards[4] = crd_article_5;
        for (int x = 0; x < 5; x++) {
            cards[x].setVisibility(View.GONE);
        }
        for (int x = 0; x < 5; x++) {
            final int finalX = x;
            cards[x].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new FinestWebView.Builder(G.CurrentActivity).show("http://sepehrcc.com/" + docs.get(finalX).DAccessUrl);
                }
            });
        }
        ImageView images[] = new ImageView[5];
        images[0] = img_article_1;
        images[1] = img_article_2;
        images[2] = img_article_3;
        images[3] = img_article_4;
        images[4] = img_article_5;
        TextView lablesdesc[] = new TextView[5];
        lablesdesc[0] = lbl_article_desc_1;
        lablesdesc[1] = lbl_article_desc_2;
        lablesdesc[2] = lbl_article_desc_3;
        lablesdesc[3] = lbl_article_desc_4;
        lablesdesc[4] = lbl_article_desc_5;


        int i = 0;
        if (docs.size() > 0) {
            for (DocTable citem : G.appData.shopInfo.getDocs()) {
                lables[i].setText(citem.DTitle);
                cards[i].setVisibility(View.VISIBLE);
                lablesdesc[i].setText(citem.DComment);
                if (!citem.DAks.isEmpty()) {
                    String url = "https://sepehrcc.com/" + citem.DAks;
                    Glide.with(this)
                            .load(url)
                            .centerCrop()
                            .placeholder(R.drawable.img_holder)
                            .into(images[i]);
                }
                i++;
            }
        }
    }


    private void bindViewControl() {
        taskcard_upgrade_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UpgradeActivity.class);
                startActivity(intent);
            }
        });
        btn_openSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (G.appData.shopInfo.GetDomainRegistr()) {
                    new FinestWebView.Builder(getActivity()).show("http://" + G.appData.shopInfo.GetDomain());
                } else {
                    new FinestWebView.Builder(getActivity()).show("http://" + G.appData.shopInfo.GetsubDomain() + ".sepehrcc.com");
                }
            }
        });

        taskcard_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddProductActivity.class);
                startActivity(i);
            }
        });
        taskcard_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(G.CurrentActivity, PostMethodsActivity.class);
                startActivity(intent);
            }
        });
        taskcard_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity = (MainActivity) G.CurrentActivity;
                mainActivity.OnClick1();

            }
        });
        taskcard_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(G.CurrentActivity, PaymentMthodsActivity.class);
                startActivity(intent);
            }
        });
        taskcard_two_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddProductActivity.class);
                startActivity(i);
            }
        });
        taskcard_one_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(G.CurrentActivity, PaymentMthodsActivity.class);
                startActivity(intent);
            }
        });
        taskcard_four_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity = (MainActivity) G.CurrentActivity;
                mainActivity.OnClick1();
            }
        });
        taskcard_three_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(G.CurrentActivity, PostMethodsActivity.class);
                startActivity(intent);
            }
        });
        taskcard_three_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(getActivity()).show("http://help.sepehrcc.com/post");
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();
        if (G.appData.shopInfo.GetKalaCount() != 0) {
            taskcard_two_img.setVisibility(View.VISIBLE);
            taskcard_two_action.setVisibility(View.GONE);
            taskcard_two.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

        String UserFullName = PreferenceManager.getDefaultSharedPreferences(G.CurrentActivity.getApplicationContext()).getString("UserFullName", "");
        if (!UserFullName.trim().equals("")) {
            SpannableString content = new SpannableString(UserFullName);
            content.setSpan(new UnderlineSpan(), 0, UserFullName.length() - 1, 0);
            txt_uname.setText(UserFullName);
        }

        if (G.appData.shopInfo.GetpaymentMethodCount() == 0) {
            taskcard_one_img.setVisibility(View.GONE);
            taskcard_one_action.setVisibility(View.VISIBLE);
        }
        if (G.appData.shopInfo.GetSendMethodCount() == 0) {
            taskcard_three_img.setVisibility(View.GONE);
            taskcard_three_action.setVisibility(View.VISIBLE);
        }
        if (G.appData.shopInfo.GetKalaCount() == 0) {
            taskcard_two_img.setVisibility(View.GONE);
            taskcard_two_action.setVisibility(View.VISIBLE);
        }

        if (G.appData.shopInfo.GetDomainRegistr()) {
            taskcard_four_img.setVisibility(View.VISIBLE);
            taskcard_four_action.setVisibility(View.GONE);
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        if (G.appData.shopInfo.GetKalaCount() != 0) {
            taskcard_two_img.setVisibility(View.VISIBLE);
            taskcard_two_action.setVisibility(View.GONE);
        }
        if (G.appData.shopInfo.GetKalaCount() == 0) {
            taskcard_two_img.setVisibility(View.GONE);
            taskcard_two_action.setVisibility(View.VISIBLE);
        }
        if (G.appData.shopInfo.GetDomainRegistr()) {
            taskcard_four_img.setVisibility(View.VISIBLE);
            taskcard_four_action.setVisibility(View.GONE);
        }
        if (!G.appData.shopInfo.GetDomainRegistr()) {
            taskcard_four_img.setVisibility(View.GONE);
            taskcard_four_action.setVisibility(View.VISIBLE);
        }

    }
}
