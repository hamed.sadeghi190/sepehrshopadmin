package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 13/11/2017.
 */

public class View_Holder_Product_Gallery extends RecyclerView.ViewHolder  {

    @BindView(R.id.product_gallery_card_view)
    CardView cv;
    @BindView(R.id.product_img_gallery)
    ImageView img;
    @BindView(R.id.product_gallery_ripple)
    RippleView rippleView;
    @BindView(R.id.img_delete)
    ImageView img_delete;
    public View_Holder_Product_Gallery(View itemview)
    {   super(itemview);
        ButterKnife.bind(this,itemView);

    }




}
