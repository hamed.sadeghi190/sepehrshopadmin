package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sepehrcc.apps.sepehradmin.Activities.Fragments.ImageGalleryFragment;
import com.sepehrcc.apps.sepehradmin.Models.ProductGallery;

import java.util.List;

/**
 * Created by Administrator on 11/03/2018.
 */

public class ImageViewPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private List<ProductGallery> images;

    public ImageViewPagerAdapter(FragmentManager fm,Context context,
                                 List<ProductGallery> images) {
        super(fm);
        this.context = context;
        this.images = images;
    }

    @Override
    public Fragment getItem(int i) {
        return ImageGalleryFragment.newInstance(images.get(i).image);
    }

    @Override
    public int getCount() {
        return images.size();
    }
}
