package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hamed on 11/18/2017.
 */

public class LoginResualt {
    @SerializedName("ValidTo")
    private String ValidTo;
    public String GetValidTo() { return ValidTo; }
    public LoginResualt SetValidTo(String value) {ValidTo = value;return this;}

    @SerializedName("Value")
    private String Token;
    public String GetToken() { return Token; }
    public LoginResualt SetToken(String value) {Token = value;return this;}

    @SerializedName("SubDomain")
    private String SubDomain;
    public String GetSubDomain() { return SubDomain; }
    public LoginResualt SetSubDomain(String value) {SubDomain = value;return this;}

    @SerializedName("UserFullName")
    private String UserFullName;
    public String GetUserFullName() { return UserFullName; }
    public LoginResualt SetSUserFullName(String value) {UserFullName = value;return this;}


}
