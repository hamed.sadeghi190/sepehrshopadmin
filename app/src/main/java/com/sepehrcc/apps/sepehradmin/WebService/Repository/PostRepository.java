package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Posts;
import com.sepehrcc.apps.sepehradmin.WebService.Models.PostsCopy;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Administrator on 24/04/2018.
 */

public class PostRepository {
    public void AddPost(final WebServicelistener listener, PostsCopy post) {


        try {
            final Call<AddResponse> CallServer = WebServiceHandler.PostClient.AddPost(G.ApiToken, post);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {

                            listener.OnComplete(response.body());

                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    int a = 0;
                }
            });
        } catch (Exception ex) {
            int a = 0;
        }
    }

    public void GetPostss(final WebServicelistener listener) {

        final ArrayList<Posts> Resulat = new ArrayList<Posts>();
        try {
            final Call<ArrayList<Posts>> CallServer = WebServiceHandler.PostClient.GetPosts(G.ApiToken);
            CallServer.enqueue(new Callback<ArrayList<Posts>>() {
                @Override
                public void onResponse(Response<ArrayList<Posts>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Posts> posts = response.body();
                                listener.OnComplete(posts);
                            } else {
                                ArrayList<Posts> posts = new ArrayList<Posts>();
                                listener.OnComplete(posts);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {
            int a = 0;
        }
    }

    public void MultiEdit(final WebServicelistener listener, ArrayList<Posts> postList) {
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.PostClient.MultiEdit(G.ApiToken, postList);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.headers().get("AckStatus") != null) {

                        listener.OnComplete(response.body());

                    } else {
                        ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                        listener.OnFail(ErrorModel);
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

        } catch (Exception ex) {
        }
    }
    public void MonoEdit(final WebServicelistener listener,Posts post,int id) {
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.PostClient.MonoEdit(G.ApiToken, post,id);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.headers().get("AckStatus") != null) {

                        listener.OnComplete(response.body());

                    } else {
                        ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                        listener.OnFail(ErrorModel);
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

        } catch (Exception ex) {
        }
    }
}
