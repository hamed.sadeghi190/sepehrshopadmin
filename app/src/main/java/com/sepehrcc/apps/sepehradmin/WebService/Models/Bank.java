package com.sepehrcc.apps.sepehradmin.WebService.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 24/04/2018.
 */

public class Bank {
    @SerializedName("pDesc")
    private String pDesc;

    public String GetpDesc() {
        return pDesc;
    }

    public Bank SetpDesc(String value) {
        pDesc = value;
        return this;
    }

    @SerializedName("pPin")
    private String pPin;

    public String GetpPin() {
        return pPin;
    }

    public Bank SetpPin(String value) {
        pPin = value;
        return this;
    }

    @SerializedName("pPin2")
    private String pPin2;

    public String GetpPin2() {
        return pPin2;
    }

    public Bank SetpPin2(String value) {
        pPin2 = value;
        return this;
    }

    @SerializedName("pPin3")
    private String pPin3;

    public String GetpPin3() {
        return pPin3;
    }

    public Bank SetpPin3(String value) {
        pPin3 = value;
        return this;
    }

    @SerializedName("pPin4")
    private String pPin4;

    public String GetpPin4() {
        return pPin4;
    }

    public Bank SetpPin4(String value) {
        pPin4 = value;
        return this;
    }

    @SerializedName("pPin5")
    private String pPin5;

    public String GetpPin5() {
        return pPin5;
    }

    public Bank SetpPin5(String value) {
        pPin5 = value;
        return this;
    }

    @SerializedName("pNum")
    private int pNum;

    public int GetpNum() {
        return pNum;
    }

    public Bank SetpNum(int value) {
        pNum = value;
        return this;
    }

    @SerializedName("pName")
    private String pName;

    public String GetpName() {
        return pName;
    }

    public Bank SetpName(String value) {
        pName = value;
        return this;
    }

    @SerializedName("pAmount")
    private int pAmount;

    public int GetpAmount() {
        return pNum;
    }

    public Bank SetpAmount(int value) {
        pAmount = value;
        return this;
    }

    @SerializedName("pEnable")
    private Boolean pEnable;

    public Boolean GetpEnable() {
        return pEnable;
    }

    public Bank SetpEnable(Boolean value) {
        pEnable = value;
        return this;
    }

    @SerializedName("sub")
    private int sub;

    public int Getsub() {
        return sub;
    }

    public Bank Setsub(int value) {
        sub = value;
        return this;
    }

    @SerializedName("pActive")
    private Boolean pActive;

    public Boolean GetpActive() {
        return pActive;
    }

    public Bank SetpActive(Boolean value) {
        pActive = value;
        return this;
    }

    @SerializedName("pMethod")
    private Boolean pMethod;

    public Boolean GetpMethod() {
        return pMethod;
    }

    public Bank SetpMethod(Boolean value) {
        pMethod = value;
        return this;
    }

    @SerializedName("pSalt")
    private String pSalt;

    public String GetpSalt() {
        return pSalt;
    }

    public Bank SetpSalt(String value) {
        pSalt = value;
        return this;
    }

    @SerializedName("FuncID")
    private String FuncID;

    public String GetFuncID() {
        return FuncID;
    }

    public Bank SetFuncID(String value) {
        FuncID = value;
        return this;
    }

}
