package com.sepehrcc.apps.sepehradmin.Models;

import com.sepehrcc.apps.sepehradmin.WebService.Models.Photos;

/**
 * Created by Administrator on 13/11/2017.
 */

public class ProductGallery {
    public String image;
    public boolean IsAddButton = false;
    public boolean FromWeb = false;
    public int _iId;
    public int _iAlbum;
    public String _iName;
    public  int kalaid ;
    public ProductGallery() {
    }
    public ProductGallery(String image) {
        this.image = image;
    }
    public ProductGallery(String image,boolean IsAddButton) {
        this.image = image;
        this.IsAddButton = IsAddButton;
    }
    public ProductGallery(String image,boolean IsAddButton,boolean FromWeb) {
        this.image = image;
        this.IsAddButton = IsAddButton;
        this.FromWeb = FromWeb;
    }
}
