package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Group;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class BankRepository {
    public void MultiEditBank(final WebServicelistener listener,ArrayList<Bank> banklist) {
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.BankClient.MultiEditBanks(G.ApiToken, banklist);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.headers().get("AckStatus") != null) {

                        listener.OnComplete(response.body());

                    } else {
                        ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                        listener.OnFail(ErrorModel);
                    }
                }
                @Override
                public void onFailure(Throwable t) {

                }
            });

        } catch (Exception ex) {
        }
    }
    public void GetBank(final WebServicelistener listener) {

        final ArrayList<Bank> Resulat = new ArrayList<Bank>();
        try {
            final Call<ArrayList<Bank>> CallServer = WebServiceHandler.BankClient.GetBanks(G.ApiToken);
            CallServer.enqueue(new Callback<ArrayList<Bank>>() {
                @Override
                public void onResponse(Response<ArrayList<Bank>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Bank> banks = response.body();
                                listener.OnComplete(banks);
                            } else {
                                ArrayList<Bank> banks = new ArrayList<Bank>();
                                listener.OnComplete(banks);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
}
