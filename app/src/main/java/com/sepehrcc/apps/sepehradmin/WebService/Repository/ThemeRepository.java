package com.sepehrcc.apps.sepehradmin.WebService.Repository;

import com.sepehrcc.apps.sepehradmin.SharedClass.G;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.ErrorHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServiceHandler;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.AddResponse;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Theme;
import java.util.ArrayList;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ThemeRepository {

    public void GetThemes(final WebServicelistener listener) {

        final ArrayList<Theme> Resulat = new ArrayList<Theme>();
        try {
            final Call<ArrayList<Theme>> CallServer = WebServiceHandler.ThemeClient.GetThemes(G.ApiToken);
            CallServer.enqueue(new Callback<ArrayList<Theme>>() {
                @Override
                public void onResponse(Response<ArrayList<Theme>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {
                                ArrayList<Theme> themes = response.body();
                                listener.OnComplete(themes);
                            } else {
                                ArrayList<Theme> themes = new ArrayList<Theme>();
                                listener.OnComplete(themes);
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }

    public void SetTheme(final WebServicelistener listener, int themeid) {

        final ArrayList<Theme> Resulat = new ArrayList<Theme>();
        try {
            final Call<AddResponse> CallServer = WebServiceHandler.ThemeClient.SetTheme(G.ApiToken, themeid);
            CallServer.enqueue(new Callback<AddResponse>() {
                @Override
                public void onResponse(Response<AddResponse> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        if (response.headers().get("AckStatus") != null) {
                            if (response.headers().get("AckStatus").equals("200")) {

                                listener.OnComplete(response.body());
                            } else {
                                listener.OnComplete(response.body());
                            }
                        } else {
                            ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                            listener.OnFail(ErrorModel);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    ResponseBase ErrorModel = new ResponseBase(400, ErrorHandler.Error);
                    listener.OnFail(ErrorModel);
                }
            });
        } catch (Exception ex) {

        }
    }
}
