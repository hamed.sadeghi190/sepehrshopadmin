package com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 12/11/2017.
 */

public class View_Holder_Sub_Group extends RecyclerView.ViewHolder {
    @BindView(R.id.sub_group_card_view)
    CardView sub_group_card_view;
    @BindView(R.id.sub_group_list_item_title)
    TextView sub_group_list_item_title;
    @BindView(R.id.sub_group_list_item_count)
    TextView sub_group_list_item_count;
    @BindView(R.id.sub_group_product)
    TextView sub_group_product;
    @BindView(R.id.sub_group_flesh)
    TextView sub_group_flesh;
    @BindView(R.id.sub_group_list_item_image)
    ImageView sub_group_list_item_image;
    @BindView(R.id.sub_group_list_ripple)
    RippleView sub_group_list_ripple;
    View_Holder_Sub_Group(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        sub_group_list_item_title.setTypeface(Fonts.VazirFD);
        sub_group_product.setTypeface(Fonts.VazirFD);
        sub_group_flesh.setTypeface(Fonts.VazirFD);
        sub_group_list_item_count.setTypeface(Fonts.VazirFD);
    }
}
