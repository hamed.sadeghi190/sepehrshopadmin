package com.sepehrcc.apps.sepehradmin.Activities.Presenter;

import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sepehrcc.apps.sepehradmin.Activities.ListViewAdapter.UltraPagerAdapter;
import com.sepehrcc.apps.sepehradmin.R;
import com.sepehrcc.apps.sepehradmin.SharedClass.Hellpers.Fonts;
import com.sepehrcc.apps.sepehradmin.SharedClass.WebService.WebServicelistener;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Bank;
import com.sepehrcc.apps.sepehradmin.WebService.Models.Theme;
import com.sepehrcc.apps.sepehradmin.WebService.Models.ThemeModel;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.BankRepository;
import com.sepehrcc.apps.sepehradmin.WebService.Repository.ThemeRepository;
import com.thefinestartist.finestwebview.FinestWebView;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.transformer.UltraDepthScaleTransformer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThemesActivity extends AppCompatActivity implements UltraPagerAdapter.ListInterface {
    @BindView(R.id.lbl_theme_title)
    TextView lbl_theme_title;

    @BindView(R.id.ultra_viewpager)
    UltraViewPager ultra_viewpager;
    @BindView(R.id.progress)
    ProgressBar progress;

    ArrayList<Theme> themeModel = new ArrayList<Theme>();
    WebServicelistener servicelistener;
    int preview_theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_themes);
        ButterKnife.bind(this);
        init();
        BindViewControl();
    }

    private void init() {

        lbl_theme_title.setTypeface(Fonts.VazirBoldFD);
        progress.setVisibility(View.VISIBLE);
        downloadTheme();
    }

    private void downloadTheme() {
        ThemeRepository themeRepository = new ThemeRepository();
        servicelistener = new WebServicelistener() {
            @Override
            public void OnComplete(Object object) {
                themeModel = (ArrayList<Theme>) object;
                progress.setVisibility(View.GONE);
                fill_list();
            }

            @Override
            public void OnFail(Object object) {
                progress.setVisibility(View.GONE);
            }
        };
        themeRepository.GetThemes(servicelistener);
    }

    private void fill_list() {
        ultra_viewpager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
        PagerAdapter adapter = new UltraPagerAdapter(true, themeModel, ThemesActivity.this);
        ultra_viewpager.setAdapter(adapter);
        ultra_viewpager.setMultiScreen(1f);
        // ultra_viewpager.setItemRatio(1.0f);
        // ultra_viewpager.setRatio(2.0f);
        // ultra_viewpager.setMaxHeight(800);
        //  ultra_viewpager.setAutoMeasureHeight(true);
        ultra_viewpager.initIndicator();
        ultra_viewpager.getIndicator()
                .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                .setFocusColor(Color.BLUE)
                .setNormalColor(Color.GRAY)
                .setRadius((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics()));
        ultra_viewpager.getIndicator().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        ultra_viewpager.setPageTransformer(false, new UltraDepthScaleTransformer());
        // ultra_viewpager.getIndicator().build();
        ultra_viewpager.setInfiniteLoop(true);
    }

    @Override
    public void previewTheme(int position) {
        preview_theme = position;
    }

    private void BindViewControl() {

    }
}
